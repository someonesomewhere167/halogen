#include "transform.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace halogen {
namespace components {
    Transform::Transform()
        : position()
        , rotation(1.0f, 0.0f, 0.0f, 0.0f)
        , scale(1.0f, 1.0f, 1.0f) {
    }

    glm::mat4 Transform::calculateMatrix() const {
        glm::mat4 scaleMat(1.0f);

        scaleMat = glm::scale(scaleMat, scale);

        glm::mat4 rotationMatrix = glm::toMat4(rotation);

        glm::mat4 translationMat(1.0f);
        translationMat = glm::translate(translationMat, position);

        return translationMat * rotationMatrix * scaleMat;
    }

    glm::vec3 Transform::transformDirection(glm::vec3 dir) const {
        return dir * rotation;
    }

    glm::vec3 Transform::inverseTransformPoint(glm::vec3 point) const {
        return glm::inverse(calculateMatrix()) * glm::vec4(point, 1.0f);
    }

    glm::vec3 Transform::transformPoint(glm::vec3 point) const {
        return calculateMatrix() * glm::vec4(point, 1.0f);
    }

    glm::vec3 Transform::inverseTransformDirection(glm::vec3 dir) const {
        return dir * glm::inverse(rotation);
    }
}
}