#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace halogen
{
    namespace components
    {
        struct Transform
        {
			Transform();
            glm::vec3 position;
            glm::quat rotation;
            glm::vec3 scale;

            glm::mat4 calculateMatrix() const;
			glm::vec3 transformDirection(glm::vec3 dir) const;
			glm::vec3 transformPoint(glm::vec3 point) const;
			glm::vec3 inverseTransformPoint(glm::vec3 point) const;
			glm::vec3 inverseTransformDirection(glm::vec3 dir) const;
        };
    }
}