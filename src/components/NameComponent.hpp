#pragma once
#include <string>

namespace halogen {
namespace components {
    struct NameComponent {
        NameComponent()
            : name() {}
        NameComponent(std::string name)
            : name(name) {}
        std::string name;
    };
}
}