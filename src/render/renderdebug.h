#pragma once
#include "nanovg.h"
#include <glm/glm.hpp>

namespace halogen
{
	namespace render
	{
		struct RenderDebugInfo
		{
			int triangleCount;
			int vertexCount;
			float deltaTime;
			int currentWidth;
			int currentHeight;
			int drawCalls;
			glm::vec3 lastCamPos;
		};
		void debugFrame(RenderDebugInfo info, NVGcontext* vg);
	}
}