// draw that nuklear stuff :ok_hand:
#include <glad.h>
#include "Shader.hpp"
#include "texture.h"
#include <glm/gtc/matrix_transform.hpp>
#include <hgio.h>
#include <input.h>
#include <nuklear_ws.h>
#include <stb_image.h>
#include <unordered_map>
#include <render.h>

struct hg_vertex {
    float pos[2];
    float uv[2];
    unsigned char col[4];
};

static const struct nk_draw_vertex_layout_element vertex_layout[] = {
    { NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(hg_vertex, pos) },
    { NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(hg_vertex, uv) },
    { NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(hg_vertex, col) },
    { NK_VERTEX_LAYOUT_END }
};

namespace halogen {
namespace render {
    extern int currentWidth;
    extern int currentHeight;

    nk_draw_null_texture nt;
    nk_buffer cmds, verts, idx;
    nk_convert_config cfg;
    nk_font_atlas atlas;
    nk_font* font;
    nk_context ctx;

    std::shared_ptr<Shader> uiShader;

    unsigned int vbo;
    unsigned int ebo;
    unsigned int vao;
    unsigned int fontTex;

    UniformBufferHandle_t ubh;

    void initialiseConfig() {
        cfg.shape_AA = NK_ANTI_ALIASING_ON;
        cfg.line_AA = NK_ANTI_ALIASING_ON;
        cfg.vertex_layout = vertex_layout;
        cfg.vertex_size = sizeof(hg_vertex);
        cfg.vertex_alignment = NK_ALIGNOF(hg_vertex);
        cfg.circle_segment_count = 22;
        cfg.arc_segment_count = 22;
        cfg.curve_segment_count = 22;
        cfg.global_alpha = 1.0f;
        cfg.null = nt;
    }

    void initialiseAtlas() {
        nk_font_atlas_init_default(&atlas);
        nk_font_atlas_begin(&atlas);

        unsigned long len;
        void* verdata = halogen::util::loadFileToBuffer("Verdana.ttf", len);
        font = nk_font_atlas_add_from_memory(&atlas, verdata, nk_size(len), 17.0f, 0);
        std::free(verdata);
        int w, h;
        const void* image = nk_font_atlas_bake(&atlas, &w, &h, NK_FONT_ATLAS_RGBA32);

        glGenTextures(1, &fontTex);
        glBindTexture(GL_TEXTURE_2D, fontTex);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
        glObjectLabel(GL_TEXTURE, fontTex, sizeof("Font Texture"), "Font Texture");
        nk_font_atlas_end(&atlas, nk_handle_id((int)fontTex), &nt);
    }

    void initNk() {
        uiShader = loadShader("nk_ui");

        initialiseConfig();
        initialiseAtlas();

        nk_init_default(&ctx, &font->handle);

        nk_buffer_init_default(&cmds);
        nk_buffer_init_default(&verts);
        nk_buffer_init_default(&idx);

        glGenBuffers(1, &vbo);
        glGenBuffers(1, &ebo);
        glGenVertexArrays(1, &vao);

        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(hg_vertex), (void*)offsetof(hg_vertex, pos));
        glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(hg_vertex), (void*)offsetof(hg_vertex, uv));
        glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, false, sizeof(hg_vertex), (void*)offsetof(hg_vertex, col));

        ubh = backend->CreateUniformBuffer(sizeof(glm::mat4));
    }

    void drawNk() {
        nk_input_begin(&ctx);
        double x, y;
        input::getCursorPos(&x, &y);
        nk_input_motion(&ctx, x, y);
        nk_input_button(&ctx, NK_BUTTON_LEFT, x, y, input::getAction("uiLClick"));
        nk_input_button(&ctx, NK_BUTTON_RIGHT, x, y, input::getAction("uiRClick"));
        nk_input_scroll(&ctx, nk_vec2(0.0f, input::getScrollWheel()));
        nk_input_end(&ctx);
        glEnable(GL_BLEND);
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_STENCIL_TEST);
        glEnable(GL_SCISSOR_TEST);

        backend->ActivateShaderCombo(uiShader->comboHandle);
        
        glm::mat4* projMat = (glm::mat4*)backend->MapUniformBuffer(ubh);
        *projMat = glm::ortho(0.0f, (float)currentWidth, (float)currentHeight, 0.0f);
        backend->UnmapUniformBuffer(ubh);
        const nk_draw_command* cmd = 0;

        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

        glBufferData(GL_ARRAY_BUFFER, 512 * 1024, 0, GL_STREAM_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 128 * 1024, 0, GL_STREAM_DRAW);

        void* vertices = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
        void* indices = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);

        initialiseConfig();
        nk_buffer_init_fixed(&verts, vertices, 512 * 1024);
        nk_buffer_init_fixed(&idx, indices, 128 * 1024);
        nk_convert(&ctx, &cmds, &verts, &idx, &cfg);

        glUnmapBuffer(GL_ARRAY_BUFFER);
        glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

        const nk_draw_index* offset = 0;

        nk_draw_foreach(cmd, &ctx, &cmds) {
            if (!cmd->elem_count)
                continue;

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, cmd->texture.id);

            glScissor(
                cmd->clip_rect.x,
                (currentHeight - (cmd->clip_rect.y + cmd->clip_rect.h)),
                cmd->clip_rect.w,
                cmd->clip_rect.h);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
            glDrawElements(GL_TRIANGLES, (GLsizei)cmd->elem_count, GL_UNSIGNED_SHORT, offset);
            offset += cmd->elem_count;
        };

        nk_clear(&ctx);

        glDisable(GL_BLEND);
        glDisable(GL_SCISSOR_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
    }

    nk_context* getNkContext() {
        return &ctx;
    }

    struct nk_image loadNkImage(std::string path) {
        uint64_t bytesRead;
        void* fdata = util::loadFileToBuffer(path, bytesRead);
        int x, y, channels;
        unsigned char* data = stbi_load_from_memory((stbi_uc*)fdata, bytesRead, &x, &y, &channels, 0);

        if (!data)
            throw std::runtime_error("Failed to load " + path);
        std::free(fdata);

        uint32_t tex;
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        stbi_image_free(data);
        return nk_image_id(tex);
    }
}
}