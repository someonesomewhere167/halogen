#include <glad.h>
#include "meshloader.h"
#include "render.h"

namespace halogen {
namespace render {
    UniformBufferHandle_t skyMUBH;
    UniformBufferHandle_t skyLMUBH;
    std::shared_ptr<Shader> skyShader;
    std::shared_ptr<Mesh> skyMesh;

    void InitializeSky() {
        skyMUBH = backend->CreateUniformBuffer(sizeof(MVPMatrices));
        skyLMUBH = backend->CreateUniformBuffer(sizeof(MVPMatrices));
        skyShader = loadShader("skybox");
        skyMesh = loadMesh("skydome");
    }

    void DrawSky(glm::mat4 viewMat, glm::mat4 projMat) {
        //     skyboxShader->setVec3("v3LightPos", directionalDir);
        //     skyboxShader->setVec3("v3InvWavelength", 1.0f / skyWavelength4);
        //     skyboxShader->setFloat("fCameraHeight", lastCam.transform.position.y);
        //     skyboxShader->setFloat("fCameraHeight2", powf(lastCam.transform.position.y, 2.0f));
        //     skyboxShader->setFloat("fInnerRadius", skyInnerRadius);
        //     skyboxShader->setFloat("fInnerRadius2", skyInnerRadius * skyInnerRadius);
        //     skyboxShader->setFloat("fOuterRadius", skyOuterRadius);
        //     skyboxShader->setFloat("fOuterRadius2", skyOuterRadius * skyOuterRadius);
        //     skyboxShader->setFloat("fKrESun", skyKr * skySunBrightness);
        //     skyboxShader->setFloat("fKmESun", skyKm * skySunBrightness);
        //     skyboxShader->setFloat("fKr4PI", skyKr4Pi);
        //     skyboxShader->setFloat("fKm4PI", skyKm4Pi);
        //     skyboxShader->setFloat("fScale", 1.0f / (skyOuterRadius - skyInnerRadius));
        //     skyboxShader->setFloat("fScaleDepth", skyRayleighScaleDepth);
        //     skyboxShader->setFloat("fScaleOverScaleDepth", 1.0f / (skyOuterRadius - skyInnerRadius) / skyRayleighScaleDepth);
        //     skyboxShader->setFloat("g", skyG);
        //     skyboxShader->setFloat("g2", skyG * skyG);

        //performDrawCall(skyboxCall, 0, 0, true);

        if (!skyMesh->uploaded)
            uploadMesh(skyMesh);

        MVPMatrices* matrices = (MVPMatrices*)backend->MapUniformBuffer(skyMUBH);
        matrices->model = glm::mat4(1.0f);
        matrices->view = viewMat;
        matrices->projection = projMat;
        backend->UnmapUniformBuffer(skyMUBH);

        backend->SetShaderComboUniformBuffer(0, skyMUBH, skyShader->comboHandle);
        backend->SetShaderComboUniformBuffer(1, skyMUBH, skyShader->comboHandle);

        backend->ActivateShaderCombo(skyShader->comboHandle);
        glBindVertexArray(skyMesh->vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skyMesh->ibo);
        glDrawElements(GL_TRIANGLES, skyMesh->indices.size(), GL_UNSIGNED_INT, 0);
    }
}
}