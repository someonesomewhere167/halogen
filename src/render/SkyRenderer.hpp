#pragma once
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    void InitializeSky();
    void DrawSky(glm::mat4 viewMat, glm::mat4 projMat);
}
}