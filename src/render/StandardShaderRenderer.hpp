#pragma once
#include <Material.hpp>
#include "render.h"

namespace halogen {
namespace render {
    class StandardShaderRenderer {
    public:
        StandardShaderRenderer();
        ~StandardShaderRenderer();
        void Draw();
    };
}
}