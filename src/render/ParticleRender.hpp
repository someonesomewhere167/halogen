#pragma once
#include "render.h"
#include <array>
#include <entt/entt.hpp>
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    class ParticleEmitter {
    public:
        ParticleEmitter(int particleCount);
        std::vector<glm::vec3> particlePositions;
        std::vector<glm::vec3> particleSpeeds;
        std::vector<float> particleLifetimes;
        unsigned int positionBuffer;
        bool initialised;
        unsigned int vao;
        int lastUsedParticle;
    };
    void InitialiseParticleSystem();
    void DrawEmitters(entt::DefaultRegistry& registry, Camera& camera, glm::mat4 projection, glm::mat4 view, float deltaTime);
}
}