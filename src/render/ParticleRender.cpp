#include <glad.h>
#include "ParticleRender.hpp"
#include "Shader.hpp"
#include "render.h"
#include <entt/entt.hpp>
#include <iostream>
#include <random>

namespace halogen {
namespace render {
    static const GLfloat particleQuad[] = {
        -0.5f,
        -0.5f,
        0.0f,
        0.5f,
        -0.5f,
        0.0f,
        -0.5f,
        0.5f,
        0.0f,
        0.5f,
        0.5f,
        0.0f,
    };

    unsigned int particleBuffer;
    std::shared_ptr<Shader> particleShader;

    ParticleEmitter::ParticleEmitter(int particleCount)
        : particlePositions(particleCount)
        , particleSpeeds(particleCount)
        , particleLifetimes(particleCount)
        , initialised(false) {
    }

    int FindUnusedParticle(ParticleEmitter& emitter) {
        for (int i = emitter.lastUsedParticle; i < emitter.particlePositions.size(); i++) {
            if (emitter.particleLifetimes[i] <= 0.0f) {
                emitter.lastUsedParticle = i;
                return i;
            }
        }

        for (int i = 0; i < emitter.lastUsedParticle; i++) {
            if (emitter.particleLifetimes[i] <= 0.0f) {
                emitter.lastUsedParticle = i;
                return i;
            }
        }

        return 0;
    }

    void InitialiseParticleSystem() {
        glGenBuffers(1, &particleBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(particleQuad), particleQuad, GL_STATIC_DRAW);
        particleShader = loadShader("particle");
    }

    void InitialiseEmitter(ParticleEmitter& emitter) {
        glGenBuffers(1, &emitter.positionBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, emitter.positionBuffer);
        glBufferData(GL_ARRAY_BUFFER, emitter.particlePositions.size() * 3 * sizeof(float), nullptr, GL_STREAM_DRAW);
        glGenVertexArrays(1, &emitter.vao);
        glBindVertexArray(emitter.vao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, emitter.positionBuffer);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
        glVertexAttribDivisor(0, 0);
        glVertexAttribDivisor(1, 1);
        emitter.initialised = true;
    }

    void CheckGL() {
        GLenum err = glGetError();
        if (err != 0) {
            std::cout << "OpenGL error: " << err << "\n";
#ifdef _MSC_VER
            __debugbreak();
#endif
        }
    }

    void RenderEmitter(ParticleEmitter& emitter, Camera& camera, glm::mat4 projection, glm::mat4 view) {
        glBindBuffer(GL_ARRAY_BUFFER, emitter.positionBuffer);
        CheckGL();
        // orphan the buffer for extra perf
        glBufferData(GL_ARRAY_BUFFER, emitter.particlePositions.size() * 3 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
        CheckGL();
        glBufferSubData(GL_ARRAY_BUFFER, 0, emitter.particlePositions.size() * sizeof(GLfloat) * 3, emitter.particlePositions.data());
        CheckGL();

        //glUseProgram(particleShader->programHandle);
        backend->ActivateShaderCombo(particleShader->comboHandle);
        CheckGL();
        //particleShader->setVec3("camRight", camera.transform.transformDirection(glm::vec3(1.0f, 0.0f, 0.0f)));
        //particleShader->setVec3("camUp", camera.transform.transformDirection(glm::vec3(0.0f, 1.0f, 0.0f)));
        //particleShader->setMat4("projection", projection);
        //particleShader->setMat4("view", view);

        glBindVertexArray(emitter.vao);
        CheckGL();
        glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, emitter.particlePositions.size());
        CheckGL();
    }

    void SimulateEmitter(ParticleEmitter& emitter, float deltaTime, glm::vec3 emitterPos) {
        for (int i = 0; i < emitter.particleSpeeds.size(); i++) {
            emitter.particleSpeeds[i] += glm::vec3(0.0f, -9.81f, 0.0f) * deltaTime;
        }

        for (int i = 0; i < emitter.particlePositions.size(); i++) {
            emitter.particlePositions[i] += emitter.particleSpeeds[i] * deltaTime;
        }

        for (int i = 0; i < emitter.particleLifetimes.size(); i++) {
            emitter.particleLifetimes[i] -= deltaTime;
            if (emitter.particleLifetimes[i] <= 0.0f) {
                emitter.particlePositions[i] = glm::vec3(-100.0f, -100.0f, -100.0f);
            }
        }

        int newParticles = 7500 * deltaTime;
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(-10.0f, 10.0f);

        for (int i = 0; i < newParticles; i++) {
            int j = FindUnusedParticle(emitter);
            emitter.particlePositions[j] = emitterPos;
            emitter.particleSpeeds[j] = glm::vec3(dist(e2), 5.0f, dist(e2));
            emitter.particleLifetimes[j] = 2.0f;
        }
    }

    void DrawEmitters(entt::DefaultRegistry& registry, Camera& camera, glm::mat4 projection, glm::mat4 view, float deltaTime) {
        glDisable(GL_CULL_FACE);
        registry.view<ParticleEmitter, components::Transform>().each([&](auto ent, ParticleEmitter& emitter, components::Transform& transform) {
            if (!emitter.initialised)
                InitialiseEmitter(emitter);

            // simulate particles
            SimulateEmitter(emitter, deltaTime, transform.position);

            RenderEmitter(emitter, camera, projection, view);
        });
        glEnable(GL_CULL_FACE);
    }
}
}