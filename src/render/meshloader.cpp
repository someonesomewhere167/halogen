#include "meshloader.h"
#include "hgio.h"
#include "physfs.h"
#include <imgui.h>
#include <memory>
#include <random>
#include <unordered_map>

namespace halogen {
namespace render {
    std::vector<Mesh*> loadedMeshes;
    std::unordered_map<std::string, std::shared_ptr<Mesh>> meshCache;

    std::random_device rd_m;
    std::mt19937 rng_m(rd_m());

    bool checkModelMagic(char* readMagic) {
        return strcmp(readMagic, "HGM2") == 0;
    }

    std::shared_ptr<Mesh> loadMesh(std::string name) {
        if (meshCache.count(name) > 0)
            return meshCache[name];

        //TODO: This could probably be redone using only one read call and performing operations on a buffer
        std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>();

        PHYSFS_File* meshFile = PHYSFS_openRead(("mesh/" + name + ".hgm").c_str());
        PHYSFS_ErrorCode errorCode = PHYSFS_getLastErrorCode();

        if (errorCode != PHYSFS_ERR_OK)
            printf("oof");

        char magicCheck[5];
        PHYSFS_readBytes(meshFile, magicCheck, 4);
        magicCheck[4] = 0;

        if (!checkModelMagic(magicCheck))
            throw std::runtime_error("Model magic string was invalid!");

        std::int32_t vertexCount;
        PHYSFS_readBytes(meshFile, &vertexCount, 4);

        mesh->vertices.resize(vertexCount);

        PHYSFS_readBytes(meshFile, mesh->vertices.data(), sizeof(Vertex) * vertexCount);

        int indexCount;
        PHYSFS_readBytes(meshFile, &indexCount, 4);

        mesh->indices.resize(indexCount);

        PHYSFS_readBytes(meshFile, mesh->indices.data(), 4 * indexCount);

        /*std::uniform_int_distribution<int> uni(0, vertexCount);
			for (int i = 0; i < indexCount; i++)
			{
				mesh->indices.push_back(uni(rng_m));
			}*/

        PHYSFS_close(meshFile);

        std::cout << "Loaded mesh with " << mesh->vertices.size() << " vertices and " << mesh->indices.size() << " indices"
                  << "\n";

        meshCache[name] = mesh;

        return mesh;
    }

    void unloadUnusedMeshes() {
#if IMGUI_ENABLED
        ImGui::Begin("Mesh Loader");
        ImGui::Text("%i meshes loaded", meshCache.size());
#endif
        std::vector<std::string> toUnload;
        for (auto pair : meshCache) {
            // if the use_count is 1, the only use is the cache
            // get rid of it!
            if (pair.second.use_count() == 1) {
                toUnload.push_back(pair.first);
            }
        }

        if (toUnload.size() > 0)
            std::cout << "Unloading " << toUnload.size() << " unused meshes."
                      << "\n";

        for (auto m : toUnload) {
            meshCache.erase(m);
        }
#if IMGUI_ENABLED
        ImGui::End();
#endif
    }
}
}