#include "render.h"

namespace halogen {
namespace render {
    Camera::Camera()
        : nearPlane(0.01f)
        , farPlane(10000.0f)
        , fov(90.0f)
        , frustrum()
        , transform() {
    }
}
}