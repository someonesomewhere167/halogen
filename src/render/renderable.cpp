#include "render.h"

namespace halogen {
namespace components {
    Renderable::Renderable(std::shared_ptr<render::Mesh> mesh, std::shared_ptr<render::Shader> shader, std::shared_ptr<render::Material> material)
        : mesh(mesh)
        , shader(shader)
        , material(material)
        , draw(true)
        , lastTransform() {
    }
}
}