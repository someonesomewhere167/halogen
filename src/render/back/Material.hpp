#pragma once
#include "render.h"
#include <glm/glm.hpp>
#include <memory>
#include <string>
#include <unordered_map>

namespace halogen {
namespace render {
#define MAX_MATERIAL_PARAMETERS 16
#define MAX_MATERIAL_SAMPLERS 27

    enum ShaderParameterType {
        Integer,
        Float,
        Vec2,
        Vec3,
        Vec4,
        Mat4
    };

    struct ShaderParameter {
        ShaderParameter();
        ShaderParameter(int value);
        ShaderParameter(float value);
        ShaderParameter(glm::vec2 value);
        ShaderParameter(glm::vec3 value);
        ShaderParameter(glm::vec4 value);
        ShaderParameter(glm::mat4 value);
        size_t nameHash = 0;
        ShaderParameterType type;
        union {
            int integerValue;
            float floatValue;
            glm::vec2 vec2Value;
            glm::vec3 vec3Value;
            glm::vec4 vec4Value;
            glm::mat4 mat4Value;
        };
    };

    struct ShaderSampler {
        ShaderSampler();
        ShaderSampler(std::shared_ptr<Texture> texture);
        size_t nameHash = 0;
        std::shared_ptr<Texture> texture;
    };

    class Material {
    public:
        std::string name;
        Material();
        int AddParameter(std::string name, ShaderParameter parameter);
        ShaderParameter& GetParameter(std::string name);
        ShaderParameter& GetParameter(int index);
        std::string GetParameterName(int index);

        int AddSampler(std::string name, ShaderSampler sampler);
        ShaderSampler& GetSampler(std::string name);
        ShaderSampler& GetSampler(int index);
        std::string GetSamplerName(int index);

    private:
        std::hash<std::string> stringHasher;

        ShaderParameter params[MAX_MATERIAL_PARAMETERS];
        std::string parameterNames[MAX_MATERIAL_PARAMETERS];
        uint8_t currentParemeterIndex;

        ShaderSampler samplers[MAX_MATERIAL_SAMPLERS];
        std::string samplerNames[MAX_MATERIAL_SAMPLERS];
        uint8_t currentSamplerIndex;
        friend void reloadMaterials();
    };

    std::shared_ptr<Material> loadMaterial(std::string name);
    void reloadMaterials();
}
}