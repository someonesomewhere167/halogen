#include "BackendInitialisationException.hpp"

namespace halogen {
namespace render {
    BackendIntilisationException::BackendIntilisationException(BackendFailureReason reason, RenderBackendType backendType, std::string otherData)
        : reason(reason)
        , otherData(otherData) {}

    std::string BackendTypeToString(RenderBackendType backendType) {
        switch (backendType) {
        case RenderBackendType::OpenGL:
            return "OpenGL";
            break;
        case RenderBackendType::D3D11:
            return "D3D11";
            break;
        case RenderBackendType::Vulkan:
            return "Vulkan";
            break;
        }
    }

    std::string FailureReasonToString(BackendFailureReason reason) {
        switch (reason) {
        case BackendFailureReason::ExternalLibrary:
            return "failure in an external library";
            break;
        case BackendFailureReason::MissingData:
            return "missing data";
            break;
        }
    }

    const char* BackendIntilisationException::what() const noexcept {
        return (std::string("Failed to initialise backend ") + BackendTypeToString(backendType), " due to " + FailureReasonToString(reason) + ". Other data was: " + otherData).c_str();
    }
}
}