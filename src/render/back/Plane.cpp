#include "Plane.hpp"

namespace halogen {
namespace render {
    void Plane::SetNormalAndPoint(glm::vec3 normal, glm::vec3 point) {
        this->normal = normal;
        this->distance = -glm::dot(normal, point);
    }

    void Plane::Set3Points(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) {
        glm::vec3 aux1, aux2;

        aux1 = v1 - v2;
        aux2 = v3 - v2;

        normal = glm::cross(aux2, aux1);

        normal = glm::normalize(normal);
        distance = -glm::dot(normal, v2);
    }

    void Plane::SetCoefficients(float a, float b, float c, float d) {
        normal = glm::vec3(a, b, c);

        float l = glm::length(normal);

        normal = glm::vec3(a / l, b / l, c / l);

        distance = d;
    }
}
}