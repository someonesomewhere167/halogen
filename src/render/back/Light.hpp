#pragma once
#include <glm/glm.hpp>
#include <framebuffer.h>

namespace halogen {
namespace render {
    enum class LightType {
        Directional,
        Point,
        Spot,
        Count
    };

    struct Light {
        bool enabled;
        LightType type;
        glm::vec3 color;
        glm::vec3 posDir; // position for point+spot, direction for directional
        float pointRange; // range for a point lamp
        bool shadowsEnabled;
        std::shared_ptr<Framebuffer> shadowFB;
    };
}
}