#include "glad.h"
#include "render.h"
#include <glm/gtx/norm.hpp>
#include <iostream>

namespace halogen {
namespace render {
    Mesh::Mesh()
        : vbo(0)
        , vao(0)
        , ibo(0)
        , uploaded(false)
        , dynamic(false)
        , boundingSphereRadius(-1.0f) {
    }

    void Mesh::CalculateBoundRadius() {
        if (vertices.size() == 0)
            return;
        boundingSphereRadius = 0.0f;
        for (int i = 0; i < vertices.size(); i++) {
            boundingSphereRadius = glm::max(glm::length2(vertices[i].pos), boundingSphereRadius);
        }
        boundingSphereRadius = sqrtf(boundingSphereRadius);
    }

    Mesh::~Mesh() {
        if (uploaded && isInitialised()) {
            glDeleteBuffers(1, &vbo);
            glDeleteBuffers(1, &ibo);
            glDeleteVertexArrays(1, &vao);
        }
    }

    void uploadMesh(std::shared_ptr<Mesh> mesh) {
        // Only generate buffers if this is the first upload
        if (!mesh->uploaded) {
            glGenVertexArrays(1, &mesh->vao);
            glGenBuffers(1, &mesh->vbo);
            glGenBuffers(1, &mesh->ibo);
        }

        GLenum usage = mesh->dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW;

        glBindVertexArray(mesh->vao);
        glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
        glBufferData(GL_ARRAY_BUFFER, mesh->vertices.size() * sizeof(Vertex), mesh->vertices.data(), usage);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size() * sizeof(unsigned int), mesh->indices.data(), usage);

        // Positions
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, pos));

        // Normals
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));

        // UVs
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));

        // Tangents
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));

        if (mesh->boundingSphereRadius < 0.0f)
            mesh->CalculateBoundRadius();
        mesh->uploaded = true;
        glBindVertexArray(0);
    }
}
}
