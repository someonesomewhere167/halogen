#include "Frustum.hpp"
#include "DebugDraw.h"
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

namespace halogen {
namespace render {
    Frustum::Frustum()
        : planes()
        , far()
        , farHeight()
        , farWidth()
        , fovRad()
        , near()
        , nearHeight()
        , nearWidth()
        , ratio() {}

    bool Frustum::ContainsSphere(glm::vec3 center, float radius) {
        for (int i = 0; i < 6; i++) {
            Plane plane = planes[i];
            float distance = plane.distance + glm::dot(plane.normal, center);
            if (distance < -radius)
                return false;
        }
        return true;
    }

    void Frustum::SetCameraInfo(float fovRad, float ratio, float near, float far, glm::vec3 position, glm::vec3 forward, glm::vec3 up) {
        float tang, nh, nw, fh, fw;
        tang = (float)tan(fovRad * 0.5);
        nh = near * tang;
        nw = nh * ratio;
        fh = far * tang;
        fw = fh * ratio;

        glm::vec3 dir, nc, fc, X, Y, Z;
        glm::vec3 ntl, ntr, nbl, nbr, ftl, ftr, fbl, fbr;

        Z = position - forward;
        Z = glm::normalize(Z);

        X = glm::cross(up, Z);
        X = glm::normalize(X);

        Y = glm::cross(Z, X);

        nc = position - Z * near;
        fc = position - Z * far;

        ntl = nc + Y * nh - X * nw;
        ntr = nc + Y * nh + X * nw;
        nbl = nc - Y * nh - X * nw;
        nbr = nc - Y * nh + X * nw;

        ftl = fc + Y * fh - X * fw;
        ftr = fc + Y * fh + X * fw;
        fbl = fc - Y * fh - X * fw;
        fbr = fc - Y * fh + X * fw;

        planes[FrustumPlane::Top].Set3Points(ntr, ntl, ftl);
        planes[FrustumPlane::Bottom].Set3Points(nbl, nbr, fbr);
        planes[FrustumPlane::Left].Set3Points(ntl, nbl, fbl);
        planes[FrustumPlane::Right].Set3Points(nbr, ntr, fbr);
        planes[FrustumPlane::Near].Set3Points(ntl, ntr, nbr);
        planes[FrustumPlane::Far].Set3Points(ftr, ftl, fbl);

        //util::DebugDraw::DrawLine(position, position + forward);

        //// bottom plane
        //util::DebugDraw::DrawLine(nbl, nbr);
        //util::DebugDraw::DrawLine(nbr, fbr);
        //util::DebugDraw::DrawLine(fbr, fbl);
        //util::DebugDraw::DrawLine(fbl, nbl);

        //// top plane
        //util::DebugDraw::DrawLine(ntr, ntl);
        //util::DebugDraw::DrawLine(ntl, ftl);
        //util::DebugDraw::DrawLine(ftl, ftr);
        //util::DebugDraw::DrawLine(ftr, ntr);

        //// near plane
        //util::DebugDraw::DrawLine(ntl, ntr);
        //util::DebugDraw::DrawLine(ntr, nbr);
        //util::DebugDraw::DrawLine(nbr, nbl);
        //util::DebugDraw::DrawLine(nbl, ntl);

        //// far plane
        //util::DebugDraw::DrawLine(ftl, ftr);
        //util::DebugDraw::DrawLine(ftr, fbr);
        //util::DebugDraw::DrawLine(fbr, fbl);
        //util::DebugDraw::DrawLine(fbl, ftl);

        //// left plane
        //util::DebugDraw::DrawLine(ntr, ftr);
        //util::DebugDraw::DrawLine(ftr, fbr);
        //util::DebugDraw::DrawLine(fbr, nbr);
        //util::DebugDraw::DrawLine(nbr, ntr);
    }

    void Frustum::SetFromViewProj(glm::mat4 viewProj) {
        planes[FrustumPlane::Left].SetCoefficients(
            viewProj[3][0] + viewProj[0][0],
            viewProj[3][2] + viewProj[0][1],
            viewProj[3][2] + viewProj[0][2],
            viewProj[3][3] + viewProj[0][3]);

        planes[FrustumPlane::Right].SetCoefficients(
            viewProj[3][0] - viewProj[0][0],
            viewProj[3][2] - viewProj[0][1],
            viewProj[3][2] - viewProj[0][2],
            viewProj[3][3] - viewProj[0][3]);

        planes[FrustumPlane::Top].SetCoefficients(
            viewProj[3][0] - viewProj[1][0],
            viewProj[3][2] - viewProj[1][1],
            viewProj[3][2] - viewProj[1][2],
            viewProj[3][3] - viewProj[1][3]);

        planes[FrustumPlane::Bottom].SetCoefficients(
            viewProj[3][0] + viewProj[1][0],
            viewProj[3][2] + viewProj[1][1],
            viewProj[3][2] + viewProj[1][2],
            viewProj[3][3] + viewProj[1][3]);

        planes[FrustumPlane::Near].SetCoefficients(
            viewProj[3][0] + viewProj[2][0],
            viewProj[3][2] + viewProj[2][1],
            viewProj[3][2] + viewProj[2][2],
            viewProj[3][3] + viewProj[2][3]);

        planes[FrustumPlane::Far].SetCoefficients(
            viewProj[3][0] - viewProj[2][0],
            viewProj[3][2] - viewProj[2][1],
            viewProj[3][2] - viewProj[2][2],
            viewProj[3][3] - viewProj[2][3]);
    }
}
}