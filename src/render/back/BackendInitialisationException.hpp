#pragma once
#include <RenderBackend.hpp>
#include <exception>
#include <string>

namespace halogen {
namespace render {
    enum class BackendFailureReason {
        ExternalLibrary,
        MissingData
    };

    class BackendIntilisationException : public std::exception {
    public:
        BackendIntilisationException(BackendFailureReason reason, RenderBackendType backendType, std::string otherData = "");
        const char* what() const throw();

    private:
        RenderBackendType backendType;
        BackendFailureReason reason;
        std::string otherData;
    };
}
}