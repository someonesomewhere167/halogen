#include "glad.h"
#include "texture.h"
#include "hgio.h"
#include "render.h"
#include "stb_image.h"
#include <SettingsManager.hpp>
#include <crn_decomp.h>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>

namespace halogen {
namespace render {
    std::mutex textureUploadMutex;
    std::vector<std::shared_ptr<Texture>> waitingTextures;
    std::unordered_map<std::string, std::shared_ptr<Texture>> textureCache;

    bool isFormatCompressed(TextureFormat format) {
        return format >= TextureFormat::RGBDXT1 && format <= TextureFormat::RGBADXT5;
    }

    // Adapted from example2
    uint32_t getCrunchTextureSize(crnd::crn_texture_info texInfo, int mip) {
        const crn_uint32 width = std::max(1U, texInfo.m_width >> mip);
        const crn_uint32 height = std::max(1U, texInfo.m_height >> mip);
        const crn_uint32 blocks_x = std::max(1U, (width + 3) >> 2);
        const crn_uint32 blocks_y = std::max(1U, (height + 3) >> 2);
        const crn_uint32 row_pitch = blocks_x * crnd::crnd_get_bytes_per_dxt_block(texInfo.m_format);
        const crn_uint32 total_face_size = row_pitch * blocks_y;

        return total_face_size;
    }

    uint32_t getRowPitch(crnd::crn_texture_info texInfo, int mip) {
        const crn_uint32 width = std::max(1U, texInfo.m_width >> mip);
        const crn_uint32 height = std::max(1U, texInfo.m_height >> mip);
        const crn_uint32 blocks_x = std::max(1U, (width + 3) >> 2);
        const crn_uint32 row_pitch = blocks_x * crnd::crnd_get_bytes_per_dxt_block(texInfo.m_format);

        return row_pitch;
    }

    std::shared_ptr<Texture> loadCompressedTexture(std::string name, bool useMips) {
        unsigned long len;
        void* buf = util::loadFileToBuffer("textures/" + name, len);
        if (buf == nullptr) {
            std::cerr << "Warning: Failed to load texture " << name << "\n";
            return std::make_shared<Texture>(0, 0, TextureFormat::R8, TextureUsage::Texture, std::vector<TextureData>());
        }
        crnd::crn_texture_info texInfo;
        if (!crnd::crnd_get_texture_info(buf, len, &texInfo)) {
            throw std::runtime_error("Failed to read info for " + name);
        }

        crnd::crnd_unpack_context context = crnd::crnd_unpack_begin(buf, len);

        if (!context) {
            throw std::runtime_error("Failed to start unpacking " + name);
        }

        TextureFormat format;
        crn_format fundamentalFormat = crnd::crnd_get_fundamental_dxt_format(texInfo.m_format);
        switch (fundamentalFormat) {
        case crn_format::cCRNFmtDXT1:
            format = TextureFormat::RGBADXT1;
            break;
        case crn_format::cCRNFmtDXT5:
            format = TextureFormat::RGBADXT5;
            break;
        case crn_format::cCRNFmtDXN_XY:
            format = (TextureFormat)GL_COMPRESSED_RG_RGTC2;
            break;
        default:
            throw std::runtime_error("Unrecognised texture format");
            break;
        }

        std::vector<TextureData> texData;

        if (texInfo.m_levels == 1) {

            uint32_t size = getCrunchTextureSize(texInfo, 0);
            void* imageData = std::malloc(size);
            if (!crnd::crnd_unpack_level(context, &imageData, size, getRowPitch(texInfo, 0), 0)) {
                throw std::runtime_error("Failed to unpack texture " + name);
            }
            texData.push_back({ imageData, size, texInfo.m_width, texInfo.m_height });

        } else {
            for (int i = 0; i < texInfo.m_levels; i++) {
                uint32_t size = getCrunchTextureSize(texInfo, i);
                void* imageData = std::malloc(size);
                if (!crnd::crnd_unpack_level(context, &imageData, size, getRowPitch(texInfo, i), i)) {
                    throw std::runtime_error("Failed to unpack texture " + name);
                }
                texData.push_back({ imageData, (int)size, (int)std::max(1U, texInfo.m_width >> i), (int)std::max(1U, texInfo.m_height >> i) });
            }
        }

        std::shared_ptr<Texture> tex = std::make_shared<Texture>(texInfo.m_width, texInfo.m_height, format, TextureUsage::Texture, texData);
        return tex;
    }

    std::shared_ptr<Texture> loadUncompressedTexture(std::string name, bool useMips) {
        unsigned long len;

        const stbi_uc* buf = (const stbi_uc*)util::loadFileToBuffer("textures/" + name, len);
        if (buf == nullptr) {
            std::cerr << "Warning: Failed to load texture " << name << "\n";
            return std::make_shared<Texture>(0, 0, TextureFormat::R8, TextureUsage::Texture, std::vector<TextureData>());
        }
        stbi_set_flip_vertically_on_load(false);
        int width, height, numChannels;
        void* data = stbi_load_from_memory(buf, len, &width, &height, &numChannels, 0);

        if (data == 0) {
            throw std::runtime_error("Failed to load texture: " + std::string(stbi_failure_reason()));
        }
        std::free((void*)buf);
        TextureFormat format;

        switch (numChannels) {
        case 4:
            format = TextureFormat::RGBA8;
            break;
        case 3:
            format = TextureFormat::RGB8;
            break;
        case 2:
            format = TextureFormat::RG8;
            break;
        case 1:
            format = TextureFormat::R8;
            break;
        default:
            throw std::runtime_error("Too many channels for texture " + name + "!");
        }

        TextureData td;
        td.data = data;
        td.dataLength = width * height * numChannels;
        td.width = width;
        td.height = height;

        std::vector<TextureData> texData = { td };
        std::shared_ptr<Texture> tex = std::make_shared<Texture>(width, height, format, TextureUsage::Texture, texData);
        return tex;
    }

    std::shared_ptr<Texture> loadTexture(std::string name, bool useMips) {
        if (textureCache.count(name) != 0)
            return textureCache[name];

        std::shared_ptr<Texture> tex;

        if (name.find(".crn") != std::string::npos) {
            tex = loadCompressedTexture(name, useMips);
        } else {
            tex = loadUncompressedTexture(name, useMips);
        }

        textureCache.insert({ name, tex });
        return tex;
    }

    Texture::Texture(int width, int height, TextureFormat format, TextureUsage usage, std::vector<TextureData> data)
        : handle(0)
        , data(data)
        , width(width)
        , height(height)
        , format(format)
        , isRenderTarget(usage == TextureUsage::Texture)
        , freeDataOnTextureDelete(true) {
        handle = backend->CreateTexture(width, height, format, usage);

        // if (!isRenderTarget) {
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //     glTexParameterf(
        //         GL_TEXTURE_2D,
        //         GL_TEXTURE_MAX_ANISOTROPY_EXT,
        //         halogen::util::SettingsManager::GetOption("render.maxAnisotropy")->as<double>()->get());
        // } else {
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        //     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // }
    }

    void Texture::activate() {
        glBindTexture(GL_TEXTURE_2D, handle);
    }

    void Texture::upload() {
        uint32_t mip = 0;
        for (TextureData td : data) {
            backend->UploadTextureData(handle, td, mip);
            mip++;
        }
    }

    void Texture::generateMips() {
        activate();
        //glGenerateMipmap(GL_TEXTURE_2D);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }

    int Texture::getWidth() {
        return width;
    }

    int Texture::getHeight() {
        return height;
    }

    void Texture::disableDataFreeingOnDelete() {
        freeDataOnTextureDelete = false;
    }

    TextureHandle_t Texture::GetHandle() {
        return handle;
    }

    Texture::~Texture() {
        //if (freeDataOnTextureDelete) {
        //    for (TextureData td : data) {
        //        std::free(td.data);
        //    }
        //}

        if (!isInitialised())
            return;

        //glDeleteTextures(1, &handle);
    }

    void uploadWaitingTextures() {
        std::lock_guard<std::mutex> guard(textureUploadMutex);
        for (auto t : waitingTextures) {
            t->upload();
            t->doneLoading = true;
            //if (t->hasMips && t->data.size() == 1)
            //    t->generateMips();
            //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

            for (TextureData td : t->data) {
                std::free(td.data);
            }

            t->freeDataOnTextureDelete = false;
        }

        waitingTextures.clear();
    }
}
}
