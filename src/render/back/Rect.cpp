#include <Rect.hpp>

namespace halogen {
namespace render {
    Rect::Rect() {
    }

    Rect::Rect(int pX, int pY, int eX, int eY) : position(pX, pY), extent(eX, eY) {}
}
}