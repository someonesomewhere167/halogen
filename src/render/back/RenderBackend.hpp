#pragma once

namespace halogen {
namespace render {
    enum class RenderBackendType {
        OpenGL,
        D3D11,
        Vulkan
    };

    enum class TextureUsage {
        Texture,
        RenderTarget
    };

    enum class ShaderType {
        Vertex,
        Fragment
    };

    struct TextureData {
        void* data;
        int dataLength;
        int width;
        int height;
    };

    typedef unsigned int TextureHandle_t;
    typedef unsigned int SamplerHandle_t;
    typedef unsigned int FramebufferHandle_t;
    typedef unsigned int ShaderHandle_t;
    typedef unsigned int ShaderComboHandle_t;
    typedef unsigned int UniformBufferHandle_t;
}
}