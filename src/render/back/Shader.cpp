#include "glad.h"
#include "hgio.h"
#include "render.h"
#include <glm/gtc/type_ptr.hpp>
#include <unordered_map>

namespace halogen {
namespace render {
    std::unordered_map<std::string, std::shared_ptr<Shader>> shaders;

    std::shared_ptr<Shader> loadShader(std::string name) {
        if (shaders.count(name) == 1)
            return shaders.at(name);

        std::shared_ptr<Shader> shader = std::make_shared<Shader>();
        shader->vertexHandle = backend->LoadShader(name, ShaderType::Vertex);
        shader->fragmentHandle = backend->LoadShader(name, ShaderType::Fragment);
        shader->comboHandle = backend->CreateShaderCombo(shader->vertexHandle, shader->fragmentHandle);
        
        shaders.insert({ name, shader });

        shader->name = name;

        return shader;
    }

    Shader::~Shader() {
        if (!isInitialised())
            return;
        backend->DestroyShaderCombo(comboHandle);
        backend->DestroyShader(vertexHandle);
        backend->DestroyShader(fragmentHandle);
    }

    void reloadShaders() {
        std::cout << shaders.size() << " shaders current loaded\n";
        for (std::pair<std::string, std::shared_ptr<Shader>> shaderPair : shaders) {
            std::shared_ptr<Shader> shader = shaderPair.second;
            backend->DestroyShaderCombo(shader->comboHandle);
            backend->DestroyShader(shader->fragmentHandle);
            backend->DestroyShader(shader->vertexHandle);

            shader->vertexHandle = backend->LoadShader(shader->name, ShaderType::Vertex);
            shader->fragmentHandle = backend->LoadShader(shader->name, ShaderType::Fragment);
            shader->comboHandle = backend->CreateShaderCombo(shader->vertexHandle, shader->fragmentHandle);
        }
    }
}
}
