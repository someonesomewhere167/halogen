#pragma once

namespace halogen
{
	namespace render
	{
		enum TextureFormat
		{
			R8,
			R16,
			R16F,
			RG8,
			RG16,
			RG16F,
			RGB8,
			RGB16,
			RGB16F,
			RGBA8,
			RGBA16,
			RGBA16F,
			R11G11B10F,
			RGBDXT1,
			RGBADXT1,
			RGBADXT3,
			RGBADXT5,
			D16,
			D24,
			D32,
			D32F,
			D24S8
		};
	}
}