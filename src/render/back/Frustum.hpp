#pragma once
#include "Plane.hpp"
#undef far
#undef near

namespace halogen {
namespace render {
    class Frustum {
    public:
        enum FrustumPlane {
            Top,
            Right,
            Bottom,
            Left,
            Near,
            Far
        };
        Frustum();
        Plane planes[6];
        bool ContainsSphere(glm::vec3 center, float radius);
        void SetCameraInfo(float fovRad, float ratio, float near, float far, glm::vec3 position, glm::vec3 forward, glm::vec3 up);
        void SetFromViewProj(glm::mat4 viewProj);

    private:
        float nearHeight, nearWidth, farHeight, farWidth;
        float fovRad, ratio, near, far;
    };
}
}