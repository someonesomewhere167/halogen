#pragma once
#include "Shader.hpp"
#include "texture.h"
#include <memory>
#include <string>

namespace halogen {
namespace render {
    class Cubemap {
    public:
        Cubemap();
        void Load(std::string name, float valueScale = 1.0f);
        void Filter(std::shared_ptr<Cubemap> other, std::shared_ptr<Shader> shader, int width, int height);

        // Requires that Filter() has already been called and that space is allocated for mips
        void FilterMip(std::shared_ptr<Cubemap> other, std::shared_ptr<Shader> shader, int width, int height, int mip);
        void Activate();
        int GetWidth() const;
        int GetHeight() const;

    private:
        unsigned int id;
        int width, height;
    };
}
}