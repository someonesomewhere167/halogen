#pragma once
#include <vector>
#include <glm/glm.hpp>

namespace halogen
{
	namespace render
	{
		struct Vertex
		{
			Vertex() : pos(), normal(), uv(), tangent() {}
			Vertex(glm::vec3 pos) : pos(pos), normal(), uv(), tangent() {}
			glm::vec3 pos;
			glm::vec3 normal;
			glm::vec2 uv;
			glm::vec3 tangent;
		};

		struct Mesh
		{
			Mesh();
			unsigned int vbo;
			unsigned int ibo;
			unsigned int vao;    
			bool uploaded;
			bool dynamic;
			float boundingSphereRadius;
			void CalculateBoundRadius();
			std::vector<Vertex> vertices;
			std::vector<unsigned int> indices;
			~Mesh();
		};
	}
}
