#include "Material.hpp"
#include "cpptoml.h"
#include "hgio.h"

namespace halogen {
namespace render {
    std::map<cpptoml::base_type, ShaderParameterType> toml2ParamType;
    std::unordered_map<std::string, std::shared_ptr<Material>> materialCache;

    void initialiseMappings() {
        toml2ParamType.insert({ cpptoml::base_type::FLOAT, ShaderParameterType::Float });
        toml2ParamType.insert({ cpptoml::base_type::INT, ShaderParameterType::Integer });
    }

    std::shared_ptr<Material> loadMatInternal(std::string name) {
        if (toml2ParamType.size() == 0)
            initialiseMappings();

        // cpptoml only supports loading from C++ streams, so we try to convert
        // from Halogen's data loading to a C++ stream
        std::string inputBase = util::loadFileToString("materials/" + name + ".toml");
        std::istringstream sstream(inputBase);
        std::istream& stream(sstream);

        cpptoml::parser parser(stream);
        auto table = parser.parse();

        auto paramsTable = table->get_table_qualified("material.parameters");
        auto samplersTable = table->get_table_qualified("material.samplers");

        auto material = std::make_shared<Material>();
        material->name = name;

        if (paramsTable) {
            // this is all so incredibly janky it's unbelievable
            // toml definitely isn't the best format for a material :D
            for (const auto& param : *paramsTable) {
                ShaderParameter parameter;

                if (param.second->is_value()) {
                    parameter.type = toml2ParamType[param.second->type()];

                    switch (parameter.type) {
                    case ShaderParameterType::Integer:
                        parameter.integerValue = param.second->as<int64_t>()->get();
                        break;
                    case ShaderParameterType::Float:
                        parameter.floatValue = param.second->as<double>()->get();
                        break;
                    }
                } else if (param.second->is_array()) {
                    auto arr = param.second->as_array();
                    auto arrV = arr->get();

                    switch (arrV.size()) {
                    case 2:
                        parameter.type = ShaderParameterType::Vec2;
                        parameter.vec2Value = glm::vec2(arrV[0]->as<double>()->get(), arrV[1]->as<double>()->get());
                        break;
                    case 3:
                        parameter.type = ShaderParameterType::Vec3;
                        parameter.vec3Value = glm::vec3(arrV[0]->as<double>()->get(), arrV[1]->as<double>()->get(), arrV[2]->as<double>()->get());
                        break;
                    case 4:
                        parameter.type = ShaderParameterType::Vec4;
                        parameter.vec4Value = glm::vec4(arrV[0]->as<double>()->get(), arrV[1]->as<double>()->get(), arrV[2]->as<double>()->get(), arrV[3]->as<double>()->get());
                        break;
                    }
                }

                material->AddParameter(param.first, parameter);
            }
        }

        if (samplersTable) {
            for (const auto& samp : *samplersTable) {
                ShaderSampler sampler;
                sampler.texture = loadTexture(samp.second->as<std::string>()->get());

                material->AddSampler(samp.first, sampler);
            }
        }

        return material;
    }

    std::shared_ptr<Material> loadMaterial(std::string name) {
        if (materialCache.count(name) == 1)
            return materialCache[name];
        auto mat = loadMatInternal(name);
        materialCache.insert({ name, mat });
        return mat;
    }

    ShaderParameter::ShaderParameter()
        : nameHash(0)
        , type(ShaderParameterType::Integer) {
    }

    ShaderParameter::ShaderParameter(int value)
        : type(ShaderParameterType::Integer)
        , integerValue(value) {
    }

    ShaderParameter::ShaderParameter(float value)
        : type(ShaderParameterType::Float)
        , floatValue(value) {
    }

    ShaderParameter::ShaderParameter(glm::vec2 value)
        : type(ShaderParameterType::Vec2)
        , vec2Value(value) {
    }

    ShaderParameter::ShaderParameter(glm::vec3 value)
        : type(ShaderParameterType::Vec3)
        , vec3Value(value) {
    }

    ShaderParameter::ShaderParameter(glm::vec4 value)
        : type(ShaderParameterType::Vec4)
        , vec4Value(value) {
    }

    ShaderParameter::ShaderParameter(glm::mat4 value)
        : type(ShaderParameterType::Mat4)
        , mat4Value(value) {
    }

    ShaderSampler::ShaderSampler() {}

    ShaderSampler::ShaderSampler(std::shared_ptr<Texture> texture)
        : texture(texture) {
    }

    Material::Material()
        : currentParemeterIndex(0)
        , currentSamplerIndex(0) {
    }

    int Material::AddParameter(std::string name, ShaderParameter parameter) {
        parameter.nameHash = stringHasher(name);
        params[currentParemeterIndex] = parameter;
        parameterNames[currentParemeterIndex] = name;
        return ++currentParemeterIndex;
    }

    ShaderParameter& Material::GetParameter(std::string name) {
        for (int i = 0; i < MAX_MATERIAL_PARAMETERS; i++) {
            if (params[i].nameHash == stringHasher(name))
                return params[i];
        }

        throw std::runtime_error("Could not find material parameter with name " + name);
    }

    ShaderParameter& Material::GetParameter(int index) {
        return params[index];
    }

    std::string Material::GetParameterName(int index) {
        return parameterNames[index];
    }

    int Material::AddSampler(std::string name, ShaderSampler sampler) {
        sampler.nameHash = stringHasher(name);
        samplers[currentSamplerIndex] = sampler;
        samplerNames[currentSamplerIndex] = name;
        return ++currentSamplerIndex;
    }

    ShaderSampler& Material::GetSampler(std::string name) {
        for (int i = 0; i < MAX_MATERIAL_SAMPLERS; i++) {
            if (samplers[i].nameHash == stringHasher(name))
                return samplers[i];
        }

        throw std::runtime_error("Could not find sampler with name " + name);
    }

    ShaderSampler& Material::GetSampler(int index) {
        return samplers[index];
    }

    std::string Material::GetSamplerName(int index) {
        return samplerNames[index];
    }

    void reloadMaterials() {
        std::cout << materialCache.size() << " materials loaded\n";
        for (auto pair : materialCache) {
            auto mat = pair.second;
            auto mat2 = loadMatInternal(mat->name);
            std::copy(std::begin(mat2->parameterNames), std::end(mat2->parameterNames), std::begin(mat->parameterNames));
            std::copy(std::begin(mat2->params), std::end(mat2->params), std::begin(mat->params));

            std::copy(std::begin(mat2->samplerNames), std::end(mat2->samplerNames), std::begin(mat->samplerNames));
            std::copy(std::begin(mat2->samplers), std::end(mat2->samplers), std::begin(mat->samplers));

            mat2->name = mat->name;
        }
    }
}
}