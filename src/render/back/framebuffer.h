#pragma once
#include "render.h"
#include <Rect.hpp>

namespace halogen
{
	namespace render
	{
		class Framebuffer
		{
		public:
			Framebuffer();
			void AttachColorTexture(std::shared_ptr<Texture> texture, int attachmentIndex);
			void AttachDepthTexture(std::shared_ptr<Texture> texture);
			void AttachDepthStencilTexture(std::shared_ptr<Texture> texture);
			void CopyTo(Framebuffer& target, Rect srcRect, Rect dstRect);
			std::shared_ptr<Texture> GetColorTexture(int attachmentIndex);
			std::shared_ptr<Texture> GetDepthTexture();
			void SetAsTarget();
			FramebufferHandle_t GetHandle();
			~Framebuffer();
		private:
			FramebufferHandle_t handle;
			std::vector<std::shared_ptr<Texture>> colorTextures;
			std::shared_ptr<Texture> depthTexture;
		};
	}
}