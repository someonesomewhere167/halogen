#pragma once
#include <glm/glm.hpp>
#include <memory>
#include <string>
#include <RenderBackend.hpp>

namespace halogen {
namespace render {
    class Shader {
    public:
        ShaderComboHandle_t comboHandle;
        ShaderHandle_t fragmentHandle;
        ShaderHandle_t vertexHandle;
        std::string name;
        
        void setFloatArray(std::string uniformName, float* value, int length) const;
        ~Shader();
    };
    std::shared_ptr<Shader> loadShader(std::string name);
    void reloadShaders();
}
}