#pragma once
#include "textureformat.h"
#include <memory>
#include <string>
#include <vector>
#include <RenderBackend.hpp>

namespace halogen
{
namespace render
{
    class FrameBuffer;

    class Texture
    {
    public:
        Texture(int width, int height, TextureFormat format, TextureUsage usage, std::vector<TextureData> data = std::vector<TextureData>());
        void activate();
        void upload();
        void generateMips();
        int getWidth();
        int getHeight();
        // long function name :)
        // this is mainly useful for generated textures where the data is automatically freed by a vector/smart ptr
        void disableDataFreeingOnDelete();
        ~Texture();
        bool doneLoading;
        TextureHandle_t GetHandle();
    private:
        TextureHandle_t handle;
        std::vector<TextureData> data;
        int width, height;
        int dataLength;
        friend std::shared_ptr<Texture> loadTexture(std::string name, bool useMips);
        friend void uploadWaitingTextures();
        TextureFormat format;
        TextureFormat readFormat;
        int dataType;
        bool isRenderTarget;
        bool freeDataOnTextureDelete;
        bool hasMips;
    };

    std::shared_ptr<Texture> loadTexture(std::string name, bool useMips = true);
    void uploadWaitingTextures();
}
}
