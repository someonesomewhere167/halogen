#pragma once
#include <BackendInitialisationException.hpp>
#include <RenderBackend.hpp>
#include <textureformat.h>
#include <string>
#include <Rect.hpp>

namespace halogen {
namespace render {
    struct RenderBackendInitSettings {
        int windowWidth;
        int windowHeight;
        bool fullscreen;
        int swapInterval;
        std::string windowTitle;
    };

    class RenderBackendBase {
        public:
        virtual void Init(RenderBackendInitSettings initSettings) = 0;
        virtual void Shutdown() = 0;

        virtual TextureHandle_t CreateTexture(int width, int height, TextureFormat format, TextureUsage usage) = 0;
        virtual void UploadTextureData(TextureHandle_t tex, TextureData data, uint32_t mip) = 0;
        virtual void SetShaderComboTexture(int slot, TextureHandle_t tex, ShaderComboHandle_t shaderCombo) = 0;
        virtual void DestroyTexture(TextureHandle_t tex) = 0;

        virtual SamplerHandle_t CreateSampler() = 0;
        // TODO: Add sampler settings
        virtual void SendSamplerToShaderCombo(int slot, SamplerHandle_t sampler, ShaderComboHandle_t shaderCombo) = 0;
        virtual void DestroySampler() = 0;

        virtual FramebufferHandle_t CreateFramebuffer() = 0;
        virtual void AddColorAttachment(FramebufferHandle_t framebuffer, TextureHandle_t texture, int attachmentIndex) = 0;
        virtual void AddDepthAttachment(FramebufferHandle_t framebuffer, TextureHandle_t texture) = 0;
        virtual void AddDepthStencilAttachment(FramebufferHandle_t framebuffer, TextureHandle_t texture) = 0;
        virtual void CopyFramebufferRegion(FramebufferHandle_t src, FramebufferHandle_t dst, Rect srcRect, Rect dstRect) = 0;
        virtual void SetTargetFramebuffer(FramebufferHandle_t target) = 0;
        virtual void DestroyFramebuffer(FramebufferHandle_t framebuffer) = 0;

        virtual ShaderHandle_t LoadShader(std::string shaderName, ShaderType type) = 0;
        virtual void DestroyShader(ShaderHandle_t shader) = 0;

        virtual ShaderComboHandle_t CreateShaderCombo(ShaderHandle_t vertex, ShaderHandle_t fragment) = 0;
        virtual void ActivateShaderCombo(ShaderComboHandle_t combo) = 0;
        virtual void DestroyShaderCombo(ShaderComboHandle_t combo) = 0;

        virtual UniformBufferHandle_t CreateUniformBuffer(size_t size) = 0;
        virtual UniformBufferHandle_t CreateStaticUniformBuffer(size_t size, void* data) = 0;
        virtual void* MapUniformBuffer(UniformBufferHandle_t handle) = 0;
        virtual void UnmapUniformBuffer(UniformBufferHandle_t handle) = 0;
        virtual void SetShaderComboUniformBuffer(int index, UniformBufferHandle_t handle, ShaderComboHandle_t shaderCombo) = 0;
        virtual void DestroyUniformBuffer(UniformBufferHandle_t handle) = 0;

        virtual void SetViewport(Rect newViewport) = 0;
        virtual void SetScreenAsTarget() = 0;
        virtual void SetFullscreen(bool fullscreen) = 0;
        virtual void SetSize(int width, int height) = 0;
        virtual void SwapBuffers() = 0;
        virtual void SetWindowTitle(std::string title) = 0;
        virtual RenderBackendType GetType() = 0;
    };
}
}