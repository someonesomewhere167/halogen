#include "glad.h"
#include "framebuffer.h"
#include "render.h"
#include <GLFW/glfw3.h>

namespace halogen {
namespace render {
    Framebuffer::Framebuffer() {
        handle = backend->CreateFramebuffer();
        colorTextures.resize(32);
    }

    void Framebuffer::AttachColorTexture(std::shared_ptr<Texture> texture, int attachmentIndex) {
        backend->AddColorAttachment(handle, texture->GetHandle(), attachmentIndex);
        colorTextures[attachmentIndex] = texture;
    }

    void Framebuffer::AttachDepthTexture(std::shared_ptr<Texture> texture) {
        backend->AddDepthAttachment(handle, texture->GetHandle());
        depthTexture = texture;
    }

    void Framebuffer::AttachDepthStencilTexture(std::shared_ptr<Texture> texture) {
        backend->AddDepthStencilAttachment(handle, texture->GetHandle());
        depthTexture = texture;
    }

    void Framebuffer::CopyTo(Framebuffer& target, Rect srcRect, Rect dstRect) {
        backend->CopyFramebufferRegion(handle, target.handle, srcRect, dstRect);
    }

    void Framebuffer::SetAsTarget() {
        backend->SetTargetFramebuffer(handle);
    }

    std::shared_ptr<Texture> Framebuffer::GetColorTexture(int attachmentIndex) {
        return colorTextures[attachmentIndex];
    }

    std::shared_ptr<Texture> Framebuffer::GetDepthTexture() {
        return depthTexture;
    }

    FramebufferHandle_t Framebuffer::GetHandle() {
        return handle;
    }

    Framebuffer::~Framebuffer() {
        backend->DestroyFramebuffer(handle);
    }
}
}
