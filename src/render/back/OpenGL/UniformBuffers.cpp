#include <glad.h>
#include <GLFW/glfw3.h>
#include <OpenGLBackend.hpp>

namespace halogen {
namespace render {
    struct OGLInternalUniformBufferInfo {
        size_t size;
        uint32_t bufferHandle;
        UniformBufferHandle_t publicHandle;
        bool isStaticBuffer;
    };

    UniformBufferHandle_t OpenGLBackend::CreateUniformBuffer(size_t size) {
        OGLInternalUniformBufferInfo* internalInfo = new OGLInternalUniformBufferInfo;
        internalInfo->publicHandle = lastUniformBufferHandle;
        glGenBuffers(1, &internalInfo->bufferHandle);
        glBindBuffer(GL_UNIFORM_BUFFER, internalInfo->bufferHandle);
        glBufferData(GL_UNIFORM_BUFFER, size, nullptr, GL_DYNAMIC_DRAW);
        internalInfo->size = size;
        internalInfo->isStaticBuffer = false;

        uniformBuffers.insert({ internalInfo->publicHandle, internalInfo });

        lastUniformBufferHandle++;

        return internalInfo->publicHandle;
    }

    UniformBufferHandle_t OpenGLBackend::CreateStaticUniformBuffer(size_t size, void* data) {
        OGLInternalUniformBufferInfo* internalInfo = new OGLInternalUniformBufferInfo;
        internalInfo->publicHandle = lastUniformBufferHandle;
        glGenBuffers(1, &internalInfo->bufferHandle);
        glBindBuffer(GL_UNIFORM_BUFFER, internalInfo->bufferHandle);
        glBufferData(GL_UNIFORM_BUFFER, size, data, GL_STATIC_DRAW);
        internalInfo->size = size;
        internalInfo->isStaticBuffer = true;

        uniformBuffers.insert({ internalInfo->publicHandle, internalInfo });

        lastUniformBufferHandle++;

        return internalInfo->publicHandle;
    }

    void* OpenGLBackend::MapUniformBuffer(UniformBufferHandle_t handle) {
        assert(!uniformBuffers.at(handle)->isStaticBuffer);
        glBindBuffer(GL_UNIFORM_BUFFER, uniformBuffers.at(handle)->bufferHandle);
        return glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
    }

    void OpenGLBackend::UnmapUniformBuffer(UniformBufferHandle_t handle) {
        assert(!uniformBuffers.at(handle)->isStaticBuffer);
        glBindBuffer(GL_UNIFORM_BUFFER, uniformBuffers.at(handle)->bufferHandle);
        glUnmapBuffer(GL_UNIFORM_BUFFER);
    }

    void OpenGLBackend::SetShaderComboUniformBuffer(int index, UniformBufferHandle_t handle, ShaderComboHandle_t shader) {
        glBindBufferRange(GL_UNIFORM_BUFFER, index, uniformBuffers.at(handle)->bufferHandle, 0, uniformBuffers.at(handle)->size);
    }

    void OpenGLBackend::DestroyUniformBuffer(UniformBufferHandle_t handle) {
        delete uniformBuffers.at(handle);
        uniformBuffers.erase(handle);
    }
}
}