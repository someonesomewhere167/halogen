// OpenGL doesn't have a concept of "samplers" - they're mushed in together with the textures.
// We have to work in order to emulate samplers...
// In the future, this will contain a lot of nice settings like mipmaps and filtering.
// For now, I just want to get this working.
#include "OpenGLBackend.hpp"

namespace halogen {
namespace render {
    SamplerHandle_t OpenGLBackend::CreateSampler() {
        SamplerHandle_t handle = lastSamplerHandle;
        lastSamplerHandle++;
        return handle;
    }

    void OpenGLBackend::SendSamplerToShaderCombo(int slot, SamplerHandle_t sampler, ShaderComboHandle_t shaderCombo) {

    }

    void OpenGLBackend::DestroySampler() {

    }
}
}