#include <glad.h>
#include <OpenGLBackend.hpp>

namespace halogen
{
namespace render {
    FramebufferHandle_t OpenGLBackend::CreateFramebuffer() {
        FramebufferHandle_t handle;
        glGenFramebuffers(1, &handle);

        return handle;
    }

    void OpenGLBackend::AddColorAttachment(FramebufferHandle_t framebuffer, TextureHandle_t handle, int attachmentIndex) {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachmentIndex, GL_TEXTURE_2D, handle, 0);
    }

    void OpenGLBackend::AddDepthAttachment(FramebufferHandle_t framebuffer, TextureHandle_t handle) {
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, handle, 0); // TODO: Actually support stencils
    }

    void OpenGLBackend::AddDepthStencilAttachment(FramebufferHandle_t framebuffer, TextureHandle_t handle) {
        assert(textures.count(handle) > 0);
        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, handle, 0);
    }

    void OpenGLBackend::CopyFramebufferRegion(FramebufferHandle_t src, FramebufferHandle_t dst, Rect srcRect, Rect dstRect) {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, src);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, dst);
        glm::ivec2 scorner = srcRect.position + srcRect.extent;
        glm::ivec2 dcorner = dstRect.position + dstRect.extent;
        glBlitFramebuffer(
            srcRect.position.x, srcRect.position.y, 
            scorner.x, scorner.y, 
            dstRect.position.x, dstRect.position.y, 
            dcorner.x, dcorner.y, 
            GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, 
            GL_NEAREST
        );
    }

    void OpenGLBackend::SetTargetFramebuffer(FramebufferHandle_t target) {
        glBindFramebuffer(GL_FRAMEBUFFER, target);
    }

    void OpenGLBackend::DestroyFramebuffer(FramebufferHandle_t framebuffer) {
        glDeleteFramebuffers(1, &framebuffer);
    }
}
}