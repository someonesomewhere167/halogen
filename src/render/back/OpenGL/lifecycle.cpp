#include <glad.h>
#include <Log.hpp>
#include <OpenGLBackend.hpp>
#include <GLFW/glfw3.h>
#include <Macros.hpp>
#include <input.h>

namespace halogen {
namespace render {

    std::string lastDebugMessage;
    int repeatCount;
    void onGlDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
        UNUSED(source);
        UNUSED(type);
        UNUSED(length);
        UNUSED(userParam);

        if (lastDebugMessage == std::string(message)) {
            repeatCount++;
            if(repeatCount % 60 == 0)
                util::Log::LogString("Above debug message has repeated " + std::to_string(repeatCount) + " times", util::LogCategory::RenderBackend);
            return;
        }

        if(severity == GL_DEBUG_SEVERITY_NOTIFICATION)
            return;

        lastDebugMessage = std::string(message);
        util::Log::LogString("OpenGL debug message (id " + std::to_string(id) + ", severity " + std::to_string(severity) + ": " + std::string(message), util::LogCategory::RenderBackend);
    }

    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
        input::KeyboardInputEvent kie;
        kie.key = key;
        if(action == GLFW_PRESS)
            kie.type = input::KeyboardInputEventType::KeyPress;
        else if(action == GLFW_RELEASE)
            kie.type = input::KeyboardInputEventType::KeyRelease;
        else
            return;

        input::pushKeyboardEvent(kie);
    }

    void OpenGLBackend::Init(RenderBackendInitSettings initSettings) {
        settings = initSettings;

        if (!glfwInit()) {
            throw BackendIntilisationException(BackendFailureReason::ExternalLibrary, RenderBackendType::OpenGL, "Failed at glfwInit()");
        }

        util::Log::LogString("Initialised GLFW.", util::LogCategory::RenderBackend);

        GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_AUTO_ICONIFY, false);
#ifndef NDEBUG
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
        util::Log::LogString("Creating debug context...", util::LogCategory::RenderBackend);
#else
        glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GL_TRUE);
#endif

        if(!initSettings.fullscreen)
            window = glfwCreateWindow(initSettings.windowWidth, initSettings.windowHeight, initSettings.windowTitle.c_str(), nullptr, nullptr);
        else {
            const GLFWvidmode* mode = glfwGetVideoMode(primaryMonitor);
            glfwWindowHint(GLFW_RED_BITS, mode->redBits);
            glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
            glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
            glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
            window = glfwCreateWindow(
                initSettings.windowWidth,
                initSettings.windowHeight,
                initSettings.windowTitle.c_str(),
                primaryMonitor,
                nullptr);
        }

        if (window == nullptr) {
            glfwTerminate();
            throw BackendIntilisationException(BackendFailureReason::ExternalLibrary, RenderBackendType::OpenGL, "Failed to create window");
        }

        glfwMakeContextCurrent(window);
        glfwSwapInterval(initSettings.swapInterval);
        glfwSetWindowSizeLimits(window, 320, 240, GLFW_DONT_CARE, GLFW_DONT_CARE);
        glfwSetKeyCallback(window, keyCallback);

        int glLoadResult = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

        if(!glLoadResult) {
            glfwTerminate();
            throw BackendIntilisationException(BackendFailureReason::ExternalLibrary, RenderBackendType::OpenGL, "Failed to initialise GLAD");
        }

        glDebugMessageCallback(onGlDebugMessage, nullptr);

        util::Log::LogString(std::string("Loaded OpenGL ") + std::to_string(GLVersion.major) + "." + std::to_string(GLVersion.minor), util::LogCategory::RenderBackend);
    }

    void OpenGLBackend::SetWindowTitle(std::string title) {
        glfwSetWindowTitle(window, title.c_str());
    }

    void OpenGLBackend::SetViewport(Rect newViewport) {
        glViewport(newViewport.position.x, newViewport.position.y, newViewport.extent.x, newViewport.extent.y);
    }

    void OpenGLBackend::SetScreenAsTarget() {
        // easy!
        // i'm not looking forward to this for directx
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void OpenGLBackend::SetFullscreen(bool fullscreen) {
        if (fullscreen) {
            GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();
            const GLFWvidmode* mode = glfwGetVideoMode(primaryMonitor);

            glfwWindowHint(GLFW_RED_BITS, mode->redBits);
            glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
            glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
            glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

            glfwSetWindowMonitor(window, primaryMonitor, 0, 0, mode->width, mode->height, mode->refreshRate);
            settings.windowWidth = mode->width;
            settings.windowHeight = mode->height;
            glfwSwapInterval(settings.swapInterval);
        } else {
            settings.windowWidth = 1280;
            settings.windowHeight = 720;
            glfwWindowHint(GLFW_DECORATED, true);
            glfwSetWindowMonitor(window, nullptr, 0, 0, 1280, 720, 0);
            glfwSwapInterval(settings.swapInterval);
        }
    }

    void OpenGLBackend::SetSize(int width, int height) {

    }

    void OpenGLBackend::SwapBuffers() {
        glfwPollEvents();
        glfwSwapBuffers(window);
        if(glfwWindowShouldClose(window)) std::exit(0); // lol
    }

    RenderBackendType OpenGLBackend::GetType() {
        return RenderBackendType::OpenGL;
    }

    void OpenGLBackend::Shutdown() {
        glfwTerminate();
    }
}
}
