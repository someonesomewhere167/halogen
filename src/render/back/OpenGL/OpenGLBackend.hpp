#pragma once
#include <RenderBackendBase.hpp>
#include <GLFW/glfw3.h>
#include <unordered_map>

namespace halogen {
namespace render {
    struct OpenGLInternalTextureData;
    struct OpenGLInternalFramebufferData;
    struct OGLInternalUniformBufferInfo;

    class OpenGLBackend : public RenderBackendBase {
    public:
        void Init(RenderBackendInitSettings initSettings) override;
        void Shutdown() override;

        TextureHandle_t CreateTexture(int width, int height, TextureFormat format, TextureUsage usage) override;
        void UploadTextureData(TextureHandle_t tex, TextureData data, uint32_t mip) override;
        void SetShaderComboTexture(int slot, TextureHandle_t tex, ShaderComboHandle_t shaderCombo) override;
        void DestroyTexture(TextureHandle_t tex) override;

        SamplerHandle_t CreateSampler() override;
        void SendSamplerToShaderCombo(int slot, SamplerHandle_t sampler, ShaderComboHandle_t shaderCombo) override;
        void DestroySampler() override;

        FramebufferHandle_t CreateFramebuffer() override;
        void AddColorAttachment(FramebufferHandle_t framebuffer, TextureHandle_t texture, int attachmentIndex) override;
        void AddDepthAttachment(FramebufferHandle_t framebuffer, TextureHandle_t texture) override;
        void AddDepthStencilAttachment(FramebufferHandle_t framebuffer, TextureHandle_t texture) override;
        void CopyFramebufferRegion(FramebufferHandle_t src, FramebufferHandle_t dst, Rect srcRect, Rect dstRect) override;
        void SetTargetFramebuffer(FramebufferHandle_t target) override;
        void DestroyFramebuffer(FramebufferHandle_t framebuffer) override;

        ShaderHandle_t LoadShader(std::string shaderName, ShaderType type) override;
        void DestroyShader(ShaderHandle_t shader) override;

        ShaderComboHandle_t CreateShaderCombo(ShaderHandle_t vertex, ShaderHandle_t fragment) override;
        void ActivateShaderCombo(ShaderComboHandle_t combo) override;
        void DestroyShaderCombo(ShaderComboHandle_t combo) override;

        UniformBufferHandle_t CreateUniformBuffer(size_t size) override;
        UniformBufferHandle_t CreateStaticUniformBuffer(size_t size, void* data) override;
        void* MapUniformBuffer(UniformBufferHandle_t handle) override;
        void UnmapUniformBuffer(UniformBufferHandle_t handle) override;
        void SetShaderComboUniformBuffer(int index, UniformBufferHandle_t handle, ShaderComboHandle_t shaderCombo) override;
        void DestroyUniformBuffer(UniformBufferHandle_t handle) override;
        
        void SetViewport(Rect newViewport) override;
        void SetScreenAsTarget() override;
        void SetFullscreen(bool fullscreen) override;
        void SetSize(int width, int height) override;
        void SwapBuffers() override;
        void SetWindowTitle(std::string title) override;
        RenderBackendType GetType() override;
    private:
        GLFWwindow* window;
        std::unordered_map<TextureHandle_t, OpenGLInternalTextureData*> textures;
        std::unordered_map<UniformBufferHandle_t, OGLInternalUniformBufferInfo*> uniformBuffers;
        RenderBackendInitSettings settings;
        SamplerHandle_t lastSamplerHandle;
        UniformBufferHandle_t lastUniformBufferHandle;
    };
}
}