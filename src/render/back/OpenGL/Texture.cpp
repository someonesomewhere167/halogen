#include <glad.h>
#include <GLFW/glfw3.h>
#include <OpenGLBackend.hpp>
#include <assert.h>

namespace halogen {
namespace render {
    struct OpenGLInternalTextureData {
        int width;
        int height;
        TextureFormat format;
        TextureUsage usage;
    };

    std::unordered_map<TextureFormat, uint32_t> texInternalFormats = {
        { TextureFormat::R8, GL_R8 },
        { TextureFormat::R16, GL_R16 },
        { TextureFormat::R16F, GL_R16F },
        { TextureFormat::RG8, GL_RG8 },
        { TextureFormat::RG16, GL_RG16 },
        { TextureFormat::RG16F, GL_RG16F },
        { TextureFormat::RGB8, GL_RGB8 },
        { TextureFormat::RGB16, GL_RGB16 },
        { TextureFormat::RGB16F, GL_RGB16F },
        { TextureFormat::RGBA8, GL_RGBA8 },
        { TextureFormat::RGBA16, GL_RGBA16 },
        { TextureFormat::RGBA16F, GL_RGBA16F },
        { TextureFormat::R11G11B10F, GL_R11F_G11F_B10F },
        { TextureFormat::RGBDXT1, GL_COMPRESSED_RGB_S3TC_DXT1_EXT },
        { TextureFormat::RGBADXT1, GL_COMPRESSED_RGBA_S3TC_DXT1_EXT },
        { TextureFormat::RGBADXT3, GL_COMPRESSED_RGBA_S3TC_DXT3_EXT },
        { TextureFormat::RGBADXT5, GL_COMPRESSED_RGBA_S3TC_DXT5_EXT },
        { TextureFormat::D32, GL_DEPTH_COMPONENT32 },
        { TextureFormat::D24, GL_DEPTH_COMPONENT24 },
        { TextureFormat::D24S8, GL_DEPTH24_STENCIL8 }
    };

    std::unordered_map<TextureFormat, uint32_t> texFormats = {
        { TextureFormat::R8, GL_RED },
        { TextureFormat::R16, GL_RED },
        { TextureFormat::R16F, GL_RED },
        { TextureFormat::RG8, GL_RG },
        { TextureFormat::RG16, GL_RG },
        { TextureFormat::RG16F, GL_RG },
        { TextureFormat::RGB8, GL_RGB },
        { TextureFormat::RGB16, GL_RGB },
        { TextureFormat::RGB16F, GL_RGB },
        { TextureFormat::RGBA8, GL_RGBA },
        { TextureFormat::RGBA16, GL_RGBA },
        { TextureFormat::RGBA16F, GL_RGBA },
        { TextureFormat::R11G11B10F, GL_RGB },
        { TextureFormat::RGBDXT1, GL_RGB },
        { TextureFormat::RGBADXT1, GL_RGBA },
        { TextureFormat::RGBADXT3, GL_RGBA },
        { TextureFormat::RGBADXT5, GL_RGBA },
        { TextureFormat::D32, GL_DEPTH_COMPONENT },
        { TextureFormat::D24, GL_DEPTH_COMPONENT },
        { TextureFormat::D24S8, GL_DEPTH_STENCIL }
    };

    std::unordered_map<TextureFormat, uint32_t> texTypes = {
        { TextureFormat::R8, GL_UNSIGNED_BYTE },
        { TextureFormat::R16, GL_UNSIGNED_SHORT },
        { TextureFormat::R16F, GL_FLOAT },
        { TextureFormat::RG8, GL_UNSIGNED_BYTE },
        { TextureFormat::RG16, GL_UNSIGNED_SHORT },
        { TextureFormat::RG16F, GL_FLOAT },
        { TextureFormat::RGB8, GL_UNSIGNED_BYTE },
        { TextureFormat::RGB16, GL_UNSIGNED_SHORT },
        { TextureFormat::RGB16F, GL_FLOAT },
        { TextureFormat::RGBA8, GL_UNSIGNED_BYTE },
        { TextureFormat::RGBA16, GL_UNSIGNED_SHORT },
        { TextureFormat::RGBA16F, GL_FLOAT },
        { TextureFormat::R11G11B10F, GL_FLOAT },
        { TextureFormat::RGBDXT1, GL_BYTE },
        { TextureFormat::RGBADXT1, GL_BYTE },
        { TextureFormat::RGBADXT3, GL_BYTE },
        { TextureFormat::RGBADXT5, GL_BYTE },
        { TextureFormat::D32, GL_UNSIGNED_INT },
        { TextureFormat::D24, GL_UNSIGNED_INT_24_8 },
        { TextureFormat::D24S8, GL_UNSIGNED_INT_24_8 }
    };

    bool FormatCompressed(TextureFormat format) {
        return format >= TextureFormat::RGBDXT1 && format <= TextureFormat::RGBADXT5;
    }

    TextureHandle_t OpenGLBackend::CreateTexture(int width, int height, TextureFormat format, TextureUsage usage) {
        TextureHandle_t tex;
        glGenTextures(1, &tex);

        OpenGLInternalTextureData* internalData = new OpenGLInternalTextureData;
        internalData->width = width;
        internalData->height = height;
        internalData->format = format;
        internalData->usage = usage;

        if (usage == TextureUsage::RenderTarget) {
            assert(!FormatCompressed(format));
            glBindTexture(GL_TEXTURE_2D, tex);
            //glTexImage2D(GL_TEXTURE_2D, 0, texInternalFormats[format], width, height, 0, texFormats[format], texTypes[format], 0);
            if(format != TextureFormat::D24S8)
                glTexImage2D(GL_TEXTURE_2D, 0, texInternalFormats[format], width, height, 0, texFormats[format], texTypes[format], nullptr);
            else
                glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }

        textures.insert({ tex, internalData });
        return tex;
    }

    void OpenGLBackend::UploadTextureData(TextureHandle_t tex, TextureData data, uint32_t mip) {
        assert(textures.count(tex) == 1);
        OpenGLInternalTextureData* internalData = textures[tex];
        assert(internalData->usage == TextureUsage::Texture);
        glBindTexture(GL_TEXTURE_2D, tex);

        if(FormatCompressed(internalData->format)) {
            glCompressedTexImage2D(GL_TEXTURE_2D, mip, texInternalFormats[internalData->format], data.width, data.height, 0, data.dataLength, data.data);
        } else {
            glTexImage2D(GL_TEXTURE_2D, mip, texInternalFormats[internalData->format], data.width, data.height, 0, texFormats[internalData->format], texTypes[internalData->format], data.data);
        }
    }

    void OpenGLBackend::SetShaderComboTexture(int slot, TextureHandle_t tex, ShaderComboHandle_t shaderCombo) {
        glUseProgram(shaderCombo);
        glActiveTexture(GL_TEXTURE0 + slot);
        glBindTexture(GL_TEXTURE_2D, tex);
    } 

    void OpenGLBackend::DestroyTexture(TextureHandle_t tex) {
        glDeleteTextures(1, &tex);
        textures.erase(tex);
    }
}
}