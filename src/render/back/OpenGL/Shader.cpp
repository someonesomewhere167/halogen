#include <glad.h>
#include <GLFW/glfw3.h>
#include <OpenGLBackend.hpp>
#include <Log.hpp>
#include <hgio.h>

namespace halogen {
namespace render {
    std::string GetShaderSuffix(ShaderType type) {
        switch(type) {
            case ShaderType::Vertex:
            return "vs";
            break;
            case ShaderType::Fragment:
            return "fs";
            break;
        }
    }

    GLenum GetGLShaderType(ShaderType type) {
        switch(type) {
            case ShaderType::Vertex:
            return GL_VERTEX_SHADER;
            break;
            case ShaderType::Fragment:
            return GL_FRAGMENT_SHADER;
            break;
        }
    }

    ShaderHandle_t OpenGLBackend::LoadShader(std::string shaderName, ShaderType type) {
        util::Log::LogString("Compiling shader " + shaderName, util::LogCategory::RenderBackend);
        std::string fileName = "shaders/opengl/" + shaderName + "_" + GetShaderSuffix(type) + ".glsl";
        std::string shaderContents = util::loadFileToString(fileName);

        unsigned int shaderHandle = glCreateShader(GetGLShaderType(type));

        const char* shaderSource = shaderContents.c_str();
        glShaderSource(shaderHandle, 1, &shaderSource, NULL);
        glCompileShader(shaderHandle);
        glObjectLabel(GL_SHADER, shaderHandle, fileName.size(), fileName.c_str());

        GLint success = 0;
        glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &success);

        if (success == GL_FALSE) {
            GLint maxLength = 0;
            glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &maxLength);

            // The maxLength includes the NULL character
            std::vector<GLchar> errorLog(maxLength);
            glGetShaderInfoLog(shaderHandle, maxLength, &maxLength, &errorLog[0]);

            util::Log::LogString("Error compiling shader " + shaderName + ":", util::LogCategory::RenderBackend);
            util::Log::LogString(std::string(&errorLog[0]), util::LogCategory::RenderBackend);

            glDeleteShader(shaderHandle);

            return -1;
        }

        return shaderHandle;
    }

    void OpenGLBackend::DestroyShader(ShaderHandle_t shader) {
        glDeleteShader(shader);
    }

    ShaderComboHandle_t OpenGLBackend::CreateShaderCombo(ShaderHandle_t vertex, ShaderHandle_t fragment) {
        ShaderComboHandle_t combo = glCreateProgram();

        assert(glIsShader(vertex));
        assert(glIsShader(fragment));
        glAttachShader(combo, vertex);
        glAttachShader(combo, fragment);
        glLinkProgram(combo);

        return combo;
    }

    void OpenGLBackend::ActivateShaderCombo(ShaderComboHandle_t combo) {
        glUseProgram(combo);
    }

    void OpenGLBackend::DestroyShaderCombo(ShaderComboHandle_t combo) {
        glDeleteProgram(combo);
    }
}
}