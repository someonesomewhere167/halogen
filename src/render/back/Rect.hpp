#pragma once
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    struct Rect {
        Rect();
        Rect(int pX, int pY, int eX, int eY);
        glm::ivec2 position;
        glm::ivec2 extent;
    };
}
}