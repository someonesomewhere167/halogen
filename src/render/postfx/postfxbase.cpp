#include "glad.h"
#include "postfxbase.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace halogen {
namespace render {
    void PostFXBase::addVertex(glm::vec3 pos, glm::vec2 uv) {
        Vertex vert;
        vert.pos = pos;
        vert.uv = uv;
        vert.normal = glm::vec3(1337.0f, 1337.0f, 1337.0f);
        vert.tangent = glm::vec3(1337.0f, 1337.0f, 1337.0f);
        effectMesh->vertices.push_back(vert);
    }

    void PostFXBase::createEffectMesh() {
        effectMesh = std::make_shared<Mesh>();
        //addVertex(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(0.0f, 1.0f));
        //addVertex(glm::vec3(1.0f, 1.0f, 0.0f), glm::vec2(1.0f, 1.0f));
        //addVertex(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.0f));
        //addVertex(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec2(1.0f, 0.0f));
        addVertex(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec2(0.0f, 0.0f));
        addVertex(glm::vec3(2.0f, 0.0f, 0.0f), glm::vec2(2.0f, 0.0f));
        addVertex(glm::vec3(0.0f, 2.0f, 0.0f), glm::vec2(0.0f, 2.0f));

        effectMesh->indices.push_back(0);
        effectMesh->indices.push_back(1);
        effectMesh->indices.push_back(2);

        //effectMesh->indices.push_back(1);
        //effectMesh->indices.push_back(2);
        //effectMesh->indices.push_back(3);
        uploadMesh(effectMesh);

        mvpUbh = backend->CreateUniformBuffer(sizeof(MVPMatrices));
        MVPMatrices* matrices = (MVPMatrices*)backend->MapUniformBuffer(mvpUbh);
        matrices->projection = glm::ortho(0.0f, 1.0f, 0.0f, 1.0f);
        backend->UnmapUniformBuffer(mvpUbh);
    }

    void PostFXBase::drawEffect(std::shared_ptr<Shader> shader) {
        //glDisable(GL_CULL_FACE);
        //glDisable(GL_DEPTH_TEST);
        //glUseProgram(shader->programHandle);
        backend->ActivateShaderCombo(shader->comboHandle);

        glBindVertexArray(effectMesh->vao);
        backend->SetShaderComboUniformBuffer(0, mvpUbh, shader->comboHandle);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, effectMesh->ibo);
        glDrawElements(GL_TRIANGLES, effectMesh->indices.size(), GL_UNSIGNED_INT, 0);
        //glEnable(GL_DEPTH_TEST);
        //glEnable(GL_CULL_FACE);
    }

    PostFXBase::~PostFXBase() {}
}
}