#include "posttint.h"
#include "framebuffer.h"
#include <glm/glm.hpp>
#include <imgui.h>
#include <Log.hpp>

namespace halogen {
namespace render {
    struct PostTintUniforms {
        glm::vec3 tint;
        float contrast;
    };

    PostTint::PostTint()
        : contrast(1.0f) {
        effectShader = loadShader("post_tint");
        createEffectMesh();
        #if IMGUI_ENABLED
        ubh = backend->CreateUniformBuffer(sizeof(PostTintUniforms));

        PostTintUniforms* tintUniforms = (PostTintUniforms*)backend->MapUniformBuffer(ubh);
        tintUniforms->tint = glm::vec3(1.0f, 1.0f, 1.0f);
        tintUniforms->contrast = 1.0f;
        backend->UnmapUniformBuffer(ubh);
        #else
        PostTintUniforms tintUniforms;
        tintUniforms.tint = glm::vec3(1.0f, 1.0f, 1.0f);
        tintUniforms.contrast = 1.5f;
        ubh = backend->CreateStaticUniformBuffer(sizeof(PostTintUniforms), &tintUniforms);
        #endif
    }

    void PostTint::render(std::shared_ptr<Framebuffer> src, std::shared_ptr<Framebuffer> dest, Camera& cam, std::shared_ptr<Framebuffer> original) {
#if IMGUI_ENABLED
        ImGui::Begin("Tint");
        ImGui::DragFloat("Contrast", &contrast);
        ImGui::End();
        // If ImGui is enabled, we have to deal with these values constantly chaning.
        // Remap the buffer and fiddle with it.
        PostTintUniforms* tintUniforms = (PostTintUniforms*)backend->MapUniformBuffer(ubh);
        tintUniforms->tint = glm::vec3(1.0f, 1.0f, 1.0f);
        tintUniforms->contrast = contrastf;
        backend->UnmapUniformBuffer(ubh);
#endif
        backend->SetShaderComboUniformBuffer(1, ubh, effectShader->comboHandle);
        dest->SetAsTarget();
        backend->SetShaderComboTexture(0, src->GetColorTexture(0)->GetHandle(), effectShader->comboHandle);

        drawEffect(effectShader);
    }
}
}