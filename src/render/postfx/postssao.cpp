#include "glad.h"
#include "postssao.h"
#include "framebuffer.h"
#include <SettingsManager.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>
#include <imgui.h>
#include <random>

namespace halogen {
namespace render {
    extern glm::vec3 taaOffset;
    glm::mat4 getCameraViewMatrix(Camera* camera);
    glm::mat4 getCameraProjectionMatrix(Camera* camera);

    std::shared_ptr<Texture> generateSSAORandomTexture() {
        std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
        std::default_random_engine generator;

        std::vector<glm::vec3> ssaoNoise;
        for (unsigned int i = 0; i < 16; i++) {
            glm::vec3 noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f); // rotate around z-axis (in tangent space)
            ssaoNoise.push_back(noise);
        }

        auto texture = std::make_shared<Texture>(4, 4, (TextureFormat)GL_RGB16F, ssaoNoise.data(), (TextureFormat)GL_RGB, GL_FLOAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        texture->upload();
        texture->disableDataFreeingOnDelete();
        texture->activate();

        return texture;
    }

    float lerp(float a, float b, float f) {
        return a + f * (b - a);
    }

    PostSSAO::PostSSAO()
        : intensity(1.0f)
        , radius(1.0f)
        , blurIterations(3)
        , blurRadius(3)
        , enabled(true)
        , samples(16)
        , rangeCheckRadius(0.25f)
        , depthCutoff(0.9995f)
        , depthFadeRange(0.0003f) {
        enabled = util::SettingsManager::GetOption("render.useSSAO")->as<bool>()->get();
        samples = util::SettingsManager::GetOption("render.ssaoSamples")->as<int64_t>()->get();
        effectShader = loadShader("post_ssao");
        blurShader = loadShader("post_blur");
        combineShader = loadShader("post_ssao_combine");
        blitShader = loadShader("post_blit");
        createEffectMesh();
        //noiseTexture = loadTexture("ssao_noise.png", false);
        noiseTexture = generateSSAORandomTexture();

        preBlurSsaoBuffer = std::make_shared<FrameBuffer>(TextureFormat::R8, false, 1280, 720);
        alternateBlurBuffer = std::make_shared<FrameBuffer>(TextureFormat::R8, false, 1280, 720);

        std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
        std::default_random_engine generator;
        for (int i = 0; i < 64; ++i) {
            glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
            sample = glm::normalize(sample);
            sample *= randomFloats(generator);
            float scale = float(i) / 64.0;

            // scale samples s.t. they're more aligned to center of kernel
            scale = lerp(0.1f, 1.0f, scale * scale);
            sample *= scale;
            ssaoKernel.push_back(sample);
            effectShader->setVec3("samples[" + std::to_string(i) + "]", ssaoKernel[i]);
        }

        glUseProgram(effectShader->programHandle);
    }

    void PostSSAO::render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) {
        if (src->getWidth() != preBlurSsaoBuffer->getWidth()) {
            preBlurSsaoBuffer->resize(src->getWidth(), src->getHeight());
            alternateBlurBuffer->resize(src->getWidth(), src->getHeight());
        }

#if IMGUI_ENABLED
        ImGui::Begin("SSAO");
        ImGui::DragFloat("Intensity", &intensity);
        //ImGui::DragFloat("Base:", &base);
        //ImGui::DragFloat("Area:", &area);
        //ImGui::DragFloat("Falloff:", &falloff, 1.0f, 0.0f, 0.0f, "%.8f");
        ImGui::DragInt("Samples", &samples, 1.0f, 1, 64);
        ImGui::DragFloat("Radius", &radius, 0.2f, 0.1f);
        ImGui::DragFloat("Depth Cutoff", &depthCutoff);
        ImGui::DragFloat("Depth Fade Range", &depthFadeRange);
        ImGui::DragFloat("RC Radius", &rangeCheckRadius, 1.0f, 0.1f);
        ImGui::DragInt("Blur Iterations:", &blurIterations);
        ImGui::DragFloat("Blur Radius", &blurRadius);
        ImGui::Checkbox("Enabled", &enabled);
        ImGui::End();
#endif

        if (!enabled) {
            src->blitTo(dest);
            return;
        }
        glm::mat4 taaMat(1.0f);
        taaMat = glm::translate(taaMat, taaOffset);
        effectShader->setMat4("taaMat", taaMat);
        effectShader->setFloat("intensity", intensity);
        effectShader->setFloat("radius", radius);
        effectShader->setFloat("depthCutoff", depthCutoff);
        effectShader->setFloat("depthFadeRange", depthFadeRange);
        effectShader->setFloat("time", glfwGetTime());
        effectShader->setInt("normalTex", 0);
        effectShader->setInt("noiseTex", 1);
        effectShader->setInt("posTex", 2);
        effectShader->setInt("depthTex", 3);
        effectShader->setInt("sampleCount", samples);
        preBlurSsaoBuffer->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        original->activateAttachment(GL_COLOR_ATTACHMENT1);
        glActiveTexture(GL_TEXTURE1);
        noiseTexture->activate();
        glActiveTexture(GL_TEXTURE2);
        original->activateAttachment(GL_COLOR_ATTACHMENT2);
        glActiveTexture(GL_TEXTURE3);
        original->activateDepthTexture();
        effectShader->setMat4("view", getCameraViewMatrix(&cam));
        effectShader->setMat4("projection2", getCameraProjectionMatrix(&cam));
        for (unsigned int i = 0; i < samples; ++i)
            effectShader->setVec3("samples[" + std::to_string(i) + "]", ssaoKernel[i]);
        drawEffect(effectShader);

        blurShader->setInt("sceneTexture", 0);
        alternateBlurBuffer->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        preBlurSsaoBuffer->activateColorTexture();
        drawEffect(blurShader);

        combineShader->setInt("sceneTexture", 0);
        combineShader->setInt("ssaoTexture", 1);
        dest->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        glActiveTexture(GL_TEXTURE1);
        //preBlurSsaoBuffer->activateColorTexture();
        alternateBlurBuffer->activateColorTexture();
        drawEffect(combineShader);
    }
}
}
