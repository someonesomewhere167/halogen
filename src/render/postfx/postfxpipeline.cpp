#include "postfxpipeline.h"
#include <algorithm>

namespace halogen {
namespace render {
    PostFXPipeline::PostFXPipeline(std::shared_ptr<Framebuffer> input, std::shared_ptr<Framebuffer> output)
        : input(input)
        , output(output) {
    }

    void PostFXPipeline::addPostFX(std::shared_ptr<PostFXBase> fx) {
        postFx.push_back(fx);
    }

    void PostFXPipeline::removePostFX(std::shared_ptr<PostFXBase> fx) {
        postFx.erase(std::remove(postFx.begin(), postFx.end(), fx), postFx.end());
    }

    void PostFXPipeline::render(Camera& cam) {
        //if (tempBuffer->getWidth() != input->getWidth() || tempBuffer->getHeight() != input->getHeight()) {
        //    tempBuffer->resize(input->getWidth(), input->getHeight());
        //}

        //input->blitTo(tempBuffer);

        for (size_t i = 0; i < postFx.size(); i++) {
            std::shared_ptr<PostFXBase> fx = postFx[i];
            /* In order to chain effects togther, just perform them
				 * sequentially on the same buffer. Effect #1 acts
				 * a blit to the output framebuffer
				 */
            fx->render(input, output, cam, input);
        }

        //tempBuffer->blitTo(output);
    }
}
}