#pragma once
#include "Shader.hpp"
#include "framebuffer.h"
#include "mesh.h"
#include "postfxbase.h"
#include "texture.h"
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    class PostPerspective : public PostFXBase {
    public:
        PostPerspective();
        void render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) override;
        float Zoom;
        int X;
        int Y;

    private:
        std::shared_ptr<Shader> blitShader;
    };
}
}