#include "glad.h"
#include "PostTAA.hpp"
#include "framebuffer.h"
#include <GLFW/glfw3.h>
#include <SettingsManager.hpp>
#include <algorithm>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

namespace halogen {
namespace render {
    PostTAA::PostTAA()
        : enabled(false) {
        enabled = util::SettingsManager::GetOption("render.useTAA")->as<bool>()->get();
        effectShader = loadShader("post_taa_blend");
        createEffectMesh();

        for (int i = 0; i < numBuffers; i++)
            previousBuffers[i] = std::make_shared<FrameBuffer>(TextureFormat::RGB8, true, 2, 2, true);
    }

    void PostTAA::render(std::shared_ptr<Framebuffer> src, std::shared_ptr<Framebuffer> dest, Camera& cam, std::shared_ptr<Framebuffer> original) {
#if IMGUI_ENABLED
        ImGui::Begin("TAA");
        ImGui::Checkbox("Enabled", &enabled);
        ImGui::End();
#endif
        if (!enabled) {
            src->blitTo(dest);
            return;
        }

        if (src->getHeight() != previousBuffers[0]->getHeight() || src->getWidth() != previousBuffers[0]->getWidth()) {
            for (int i = 0; i < numBuffers; i++) {
                previousBuffers[i]->resize(src->getWidth(), src->getHeight());
                src->blitTo(previousBuffers[i]);
            }
        }

        for (int i = 0; i < numBuffers; i++) {
            effectShader->setInt("previousBuffers[" + std::to_string(i) + "]", i + 2);
            glActiveTexture(GL_TEXTURE2 + i);
            previousBuffers[i]->activateColorTexture();
        }

        effectShader->setInt("velocityBuffer", 1);
        glActiveTexture(GL_TEXTURE1);
        original->activateAttachment(GL_COLOR_ATTACHMENT3);

        dest->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        drawEffect(effectShader);

        previousBuffers[0]->activate();
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        src->blitTo(previousBuffers[0]);
    }
}
}
