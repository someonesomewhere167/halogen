#pragma once
#include "Shader.hpp"
#include "framebuffer.h"
#include "mesh.h"
#include "postfxbase.h"
#include "texture.h"
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    class PostBloom : public PostFXBase {
    public:
        PostBloom();
        void render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) override;
        bool enabled;
        float threshold;
        int iterations;
        float softThreshold;
        float intensity;

    private:
        void createDownsampleTargets(int width, int height);
        std::shared_ptr<FrameBuffer> thresholdTarget;
        std::vector<std::shared_ptr<FrameBuffer>> downsampleTargets;
        std::shared_ptr<Shader> blurShader;
        std::shared_ptr<Shader> combineShader;
    };
}
}