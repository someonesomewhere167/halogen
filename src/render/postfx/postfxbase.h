#pragma once
#include "framebuffer.h"
#include "render.h"
#include <glm/glm.hpp>

namespace halogen
{
	namespace render
	{
		class PostFXBase
		{
		public:
			virtual void render(std::shared_ptr<Framebuffer> previous, std::shared_ptr<Framebuffer> dest, Camera& cam, std::shared_ptr<Framebuffer> original) = 0;
			virtual ~PostFXBase();
		protected:
			void createEffectMesh();
			void drawEffect(std::shared_ptr<Shader> shader);
			std::shared_ptr<Mesh> effectMesh;
			std::shared_ptr<Shader> effectShader;
		private:
			void addVertex(glm::vec3 post, glm::vec2 uv);
			UniformBufferHandle_t mvpUbh;
		};
	}
}