#include "glad.h"
#include "PostMotionBlur.hpp"
#include "framebuffer.h"
#include <GLFW/glfw3.h>
#include <SettingsManager.hpp>
#include <algorithm>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

namespace halogen {
namespace render {
    PostMotionBlur::PostMotionBlur()
        : enabled(false) {
        effectShader = loadShader("post_motion_blur");
        blitShader = loadShader("post_blit");
        createEffectMesh();
        enabled = util::SettingsManager::GetOption("render.useMotionBlur")->as<bool>()->get();
    }

    void PostMotionBlur::render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) {
#if IMGUI_ENABLED
        ImGui::Begin("Motion Blur");
        ImGui::Checkbox("Enabled:", &enabled);
        ImGui::End();
#endif

        if (!enabled) {
            src->blitTo(dest);
            return;
        }

        effectShader->setInt("sceneTex", 0);
        effectShader->setInt("velocityTex", 1);

        dest->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        glActiveTexture(GL_TEXTURE1);
        original->activateAttachment(GL_COLOR_ATTACHMENT3);
        drawEffect(effectShader);
    }
}
}
