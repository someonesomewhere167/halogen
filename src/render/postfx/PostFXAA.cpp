#include "glad.h"
#include "PostFXAA.hpp"
#include "framebuffer.h"
#include <GLFW/glfw3.h>
#include <SettingsManager.hpp>
#include <algorithm>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

namespace halogen {
namespace render {
    PostFXAA::PostFXAA()
        : enabled(false) {
        effectShader = loadShader("post_fxaa");
        blitShader = loadShader("post_blit");
        createEffectMesh();
        enabled = util::SettingsManager::GetOption("render.useFXAA")->as<bool>()->get();
    }

    void PostFXAA::render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) {
#if IMGUI_ENABLED
        ImGui::Begin("FXAA");
        ImGui::Checkbox("Enabled:", &enabled);
        ImGui::End();
#endif

        if (!enabled) {
            src->blitTo(dest);
            return;
        }

        dest->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        drawEffect(effectShader);
    }
}
}
