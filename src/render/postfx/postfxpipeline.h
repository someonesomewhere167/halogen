#pragma once
#include "framebuffer.h"
#include "postfxbase.h"

namespace halogen
{
	namespace render
	{
		class PostFXPipeline
		{
		public:
			PostFXPipeline(std::shared_ptr<Framebuffer> input, std::shared_ptr<Framebuffer> output);
			void render(Camera& cam);
			void addPostFX(std::shared_ptr<PostFXBase> fx);
			void removePostFX(std::shared_ptr<PostFXBase> fx);
		private:
			std::shared_ptr<Framebuffer> input;
			std::shared_ptr<Framebuffer> output;
			std::shared_ptr<Framebuffer> tempBuffer;
			std::vector<std::shared_ptr<PostFXBase>> postFx;
		};
	}
}