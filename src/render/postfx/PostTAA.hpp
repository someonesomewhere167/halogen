#pragma once
#include "Shader.hpp"
#include "framebuffer.h"
#include "mesh.h"
#include "postfxbase.h"
#include "texture.h"
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    class PostTAA : public PostFXBase {
    public:
        PostTAA();
        void render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) override;
        bool enabled;

    private:
        const int numBuffers = 1;
        std::shared_ptr<FrameBuffer> previousBuffers[1];
    };
}
}