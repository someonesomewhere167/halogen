#include "glad.h"
#include "PostPerspective.hpp"
#include <cmath>
#include <imgui.h>

namespace halogen {
namespace render {
    PostPerspective::PostPerspective()
        : Zoom(200.0f)
        , X(0)
        , Y(0) {
        effectShader = loadShader("post_perspective");
        createEffectMesh();
    }

    void PostPerspective::render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) {
#if IMGUI_ENABLED
        ImGui::Begin("Perspective");
        ImGui::DragFloat("Zoom", &Zoom);
        ImGui::End();
#endif

        effectShader->setInt("sceneTexture", 0);
        effectShader->setFloat("Zoom", Zoom);
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        drawEffect(effectShader);
    }
}
}