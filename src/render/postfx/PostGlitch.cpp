#include "glad.h"
#include "PostGlitch.hpp"
#include "framebuffer.h"
#include <GLFW/glfw3.h>
#include <algorithm>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

namespace halogen {
namespace render {
    PostGlitch::PostGlitch()
        : VerticalJump(0.0f)
        , ScanLineJitter(0.0f)
        , ColorDrift(0.0f)
        , HorizontalShake(0.0f)
        , enabled(false) {
        effectShader = loadShader("post_glitch");
        blitShader = loadShader("post_blit");
        createEffectMesh();
    }

    void PostGlitch::render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) {
#if IMGUI_ENABLED
        ImGui::Begin("Glitch");
        ImGui::DragFloat("Vertical Jump:", &VerticalJump);
        ImGui::DragFloat("Scan Line Jitter:", &ScanLineJitter);
        ImGui::DragFloat("Color Drift:", &ColorDrift);
        ImGui::DragFloat("Horizontal Shake:", &HorizontalShake);
        ImGui::Checkbox("Enabled:", &enabled);
        ImGui::End();
#endif

        if (!enabled) {
            src->blitTo(dest);
            return;
        }

        glm::vec2 cd(ColorDrift * 0.04f, glfwGetTime() * 606.11f);
        glm::vec2 sl(0.002f + std::pow(ScanLineJitter, 3) * 0.05f, std::clamp(1.0f - ScanLineJitter * 1.2f, 0.0f, 1.0f));
        glm::vec2 vj(VerticalJump, glfwGetTime() * VerticalJump * 11.3f);

        effectShader->setFloat("Time", glfwGetTime());
        effectShader->setVec2("ColorDrift", cd);
        effectShader->setVec2("VerticalJump", vj);
        effectShader->setVec2("ScanLineJitter", sl);
        effectShader->setFloat("HorizontalShake", HorizontalShake * 0.2f);

        dest->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        drawEffect(effectShader);
    }
}
}
