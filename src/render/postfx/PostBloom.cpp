#include "glad.h"
#include "PostBloom.hpp"
#include <imgui.h>

namespace halogen {
namespace render {
    PostBloom::PostBloom()
        : enabled(true)
        , threshold(110.0f)
        , iterations(3)
        , softThreshold(1.0f)
        , intensity(1.0f) {
        thresholdTarget = std::make_shared<FrameBuffer>(TextureFormat::RGB16F, false, 1280, 720);
        createDownsampleTargets(1280, 720);
        effectShader = loadShader("post_bloom_thresh");
        blurShader = loadShader("box_filter");
        combineShader = loadShader("post_bloom_combine");
        createEffectMesh();
    }

    void PostBloom::render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) {
        if (src->getWidth() != thresholdTarget->getWidth() || src->getHeight() != thresholdTarget->getHeight()) {
            thresholdTarget->resize(src->getWidth(), src->getHeight());
            createDownsampleTargets(src->getWidth(), src->getHeight());
        }
#if IMGUI_ENABLED
        ImGui::Begin("Bloom");
        ImGui::DragFloat("Threshold", &threshold);
        ImGui::DragFloat("Soft Threshold", &softThreshold);
        ImGui::DragFloat("Intensity", &intensity);
        static int newIter = 3;
        ImGui::DragInt("Iterations", &newIter);
        if (ImGui::Button("Apply Iterations")) {
            iterations = newIter;
            createDownsampleTargets(src->getWidth(), src->getHeight());
        }
        ImGui::Checkbox("Enabled", &enabled);
        ImGui::End();
#endif

        if (!enabled)
            return;

        effectShader->setFloat("thresholdValue", threshold);
        effectShader->setFloat("softThreshold", softThreshold);
        thresholdTarget->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        drawEffect(effectShader);

        std::shared_ptr<FrameBuffer> currentDestination;
        std::shared_ptr<FrameBuffer> currentSource = thresholdTarget;

        blurShader->setFloat("delta", 1.0f);
        for (int i = 0; i < iterations; i++) {
            currentDestination = downsampleTargets[i];
            currentDestination->activateDraw();
            glActiveTexture(GL_TEXTURE0);
            currentSource->activateColorTexture();
            glViewport(0, 0, currentDestination->getWidth(), currentDestination->getHeight());
            drawEffect(blurShader);
            currentSource = currentDestination;
        }

        blurShader->setFloat("delta", 0.5f);
        for (int i = iterations - 1; i > -1; i--) {
            currentDestination = downsampleTargets[i];
            currentDestination->activateDraw();
            glActiveTexture(GL_TEXTURE0);
            currentSource->activateColorTexture();
            if (currentDestination != currentSource) {
                glViewport(0, 0, currentDestination->getWidth(), currentDestination->getHeight());
                drawEffect(blurShader);
            }
            currentSource = currentDestination;
        }

        glViewport(0, 0, src->getWidth(), src->getHeight());

        combineShader->setInt("sceneTexture", 0);
        combineShader->setInt("blurredTex", 1);
        combineShader->setFloat("intensity", intensity);
        dest->activateDraw();
        glActiveTexture(GL_TEXTURE0);
        src->activateColorTexture();
        glActiveTexture(GL_TEXTURE1);
        currentDestination->activateColorTexture();
        drawEffect(combineShader);
    }

    void PostBloom::createDownsampleTargets(int width, int height) {
        if (downsampleTargets.size() > 0)
            downsampleTargets.clear();

        for (int i = 0; i < iterations; i++) {
            width /= 2;
            height /= 2;
            std::shared_ptr<FrameBuffer> fb = std::make_shared<FrameBuffer>(TextureFormat::RGB16F, false, width, height);
            downsampleTargets.push_back(fb);

            if (height < 2 || width < 2) {
                break;
            }
        }
    }
}
}