#pragma once
#include "postfxbase.h"
#include "Shader.hpp"
#include "mesh.h"

namespace halogen
{
	namespace render
	{
		class PostTint : public PostFXBase
		{
		public:
			PostTint();
			void render(std::shared_ptr<Framebuffer> src, std::shared_ptr<Framebuffer> dest, Camera& cam, std::shared_ptr<Framebuffer> original) override;
		private:
			float contrast;
			UniformBufferHandle_t ubh;
		};
	}
}