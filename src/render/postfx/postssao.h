#pragma once
#include "Shader.hpp"
#include "framebuffer.h"
#include "mesh.h"
#include "postfxbase.h"
#include "texture.h"

namespace halogen
{
namespace render
{
    class PostSSAO : public PostFXBase
    {
    public:
        PostSSAO();
        void render(std::shared_ptr<IFrameBuffer> src, std::shared_ptr<IFrameBuffer> dest, Camera& cam, std::shared_ptr<FrameBuffer> original) override;
        bool enabled;

    private:
        std::shared_ptr<Texture> noiseTexture;
        std::shared_ptr<FrameBuffer> preBlurSsaoBuffer;
        std::shared_ptr<FrameBuffer> alternateBlurBuffer;
        std::shared_ptr<Shader> blurShader;
        std::shared_ptr<Shader> combineShader;
        std::shared_ptr<Shader> blitShader;
        float intensity;
        float radius;
        float rangeCheckRadius;
        int blurIterations;
        float blurRadius;
        float depthCutoff;
        float depthFadeRange;
        int samples;
        std::vector<glm::vec3> ssaoKernel;
    };
}
}