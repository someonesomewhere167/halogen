#include "glad.h" // THIS MUST BE FIRST!!

#include "debugbreak.h"
#include "entt/entt.hpp"
#include "meshloader.h"
#include "render.h"
#include "texture.h"
#include <Macros.hpp>
#include <SettingsManager.hpp>
#include <chrono>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <hgio.h>
#include <input.h>
#include <iostream>
#include <renderdoc_app.h>
#include <stb_image.h>
#include <stb_image_write.h>
#include <vector>

#include "nanovg.h"
#define NANOVG_GL3_IMPLEMENTATION
#include "nanovg_gl.h"

#include "Cubemap.hpp"
#include "DebugDraw.h"
#include "Light.hpp"
#include "Material.hpp"
#include "ParticleRender.hpp"
#include "SkyRenderer.hpp"
//#include "PostBloom.hpp"
//#include "PostFXAA.hpp"
//#include "PostGlitch.hpp"
//#include "PostMotionBlur.hpp"
//#include "PostPerspective.hpp"
//#include "PostTAA.hpp"
#include "RenderdocSupport.hpp"
#include "halogenimgui.h"
#include "nuklear_draw.hpp"
#include "postfxpipeline.h"
//#include "postssao.h"
#include "posttint.h"
#include "renderdebug.h"
#include <Log.hpp>
#include <OpenGLBackend.hpp>
#include <RenderBackend.hpp>
#include <RenderBackendBase.hpp>

#define USE_OPENGL_BACKEND 1

namespace halogen {
namespace render {
    typedef std::chrono::high_resolution_clock Clock;

    int currentWidth, currentHeight;
    float renderScale;
    DebugMode dbgMode;
    NVGcontext* vg;
    int debugFont;
    int drawnVertices = 0;
    int drawnTriangles = 0;
    std::shared_ptr<Texture> defaultAlbedo;
    std::shared_ptr<Texture> defaultNormal;
    std::unique_ptr<PostFXPipeline> pipeline;
    std::shared_ptr<Framebuffer> preEffectFb;
    std::shared_ptr<Framebuffer> postEffectFb;
    //std::shared_ptr<Backbuffer> backbuffer;
    std::shared_ptr<Texture> windowIcon;
    std::shared_ptr<Texture> brdfLut;
    std::shared_ptr<Cubemap> sky;
    std::shared_ptr<Cubemap> skyConvoluted;
    std::shared_ptr<Cubemap> skyPrefiltered;
    std::shared_ptr<PostTint> tint;
    Camera lastCam;
    glm::vec3 taaOffset;

    GLFWimage icon;
    bool _isInitialised;
    bool isFullscreen;
    bool showDebugMenu =
#if DEBUG
        true;
#else
        false;
#endif
    RenderInitSettings* _settings;

    struct DrawCall {
        std::shared_ptr<Mesh> mesh;
        std::shared_ptr<Shader> shader;
        std::shared_ptr<Material> material;
        glm::vec3 viewPos;
        glm::mat4 transformMatrix;
        components::Transform transform;
        components::Transform lastTransform;
        glm::mat4 view;
        glm::mat4 lastView;
        glm::mat4 projection;
    };

    glm::mat4 shadowView;
    glm::mat4 shadowProj;
    Frustum shadowFrustum;

    RenderBackendBase* backend;
#if USE_OPENGL_BACKEND
    OpenGLBackend actualBackend;
#endif

    using HRC = std::chrono::high_resolution_clock;

    bool isInitialised() {
        return _isInitialised;
    }

    void setDebugMode(int modeFlag) {
        dbgMode = static_cast<DebugMode>(modeFlag);
        if ((dbgMode & DebugMode::Wireframe) == DebugMode::Wireframe)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    void onSizeChange(GLFWwindow* window, int width, int height) {
        UNUSED(window);

        /* Ignore invalid size changes
			* These tend to come through on Windows whenever
			* the game window is minimised
			*/

        if (width == 0 || height == 0)
            return;

        glViewport(0, 0, width, height);
        currentWidth = width;
        currentHeight = height;
        //preEffectFb->resize(width * renderScale, height * renderScale);
#if IMGUI_ENABLED
        ImGuiIO& io = ImGui::GetIO();
        io.DisplaySize = ImVec2((float)width, (float)height);
#endif
    }

    void onGlfwError(int error, const char* description) {
#if DEBUG
        DEBUG_BREAK;
#endif
        std::cerr << "Error " << error << ": " << description << "\n";
    }

    void setWindowTitle(std::string title) {
        backend->SetWindowTitle(title);
    }

    std::shared_ptr<Texture> createBrdfLut() {
        std::shared_ptr<Shader> brdfShader = loadShader("brdf_lut");
        std::shared_ptr<Texture> texture = std::make_shared<Texture>(512, 512, TextureFormat::RG16F, TextureUsage::RenderTarget);
        texture->upload();
        texture->activate();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        unsigned int fbo, rbo;
        glGenFramebuffers(1, &fbo);
        glGenRenderbuffers(1, &rbo);

        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glBindRenderbuffer(GL_RENDERBUFFER, rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->GetHandle(), 0);
        glViewport(0, 0, 512, 512);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        backend->ActivateShaderCombo(brdfShader->comboHandle);

        unsigned int quadVAO;
        unsigned int quadVBO;

        float quadVertices[] = {
            -1.0f,
            1.0f,
            0.0f,
            0.0f,
            1.0f,
            -1.0f,
            -1.0f,
            0.0f,
            0.0f,
            0.0f,
            1.0f,
            1.0f,
            0.0f,
            1.0f,
            1.0f,
            1.0f,
            -1.0f,
            0.0f,
            1.0f,
            0.0f,
        };
        // setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
        glBindVertexArray(quadVAO);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glBindVertexArray(0);

        glDeleteBuffers(1, &quadVBO);
        glDeleteVertexArrays(1, &quadVAO);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        return texture;
    }

    struct CubemapPrefilterUniforms {
        float roughness;
        int cubemapResolution;
    };

    void loadPBRData() {
        auto t1 = Clock::now();

        sky = std::make_shared<Cubemap>();
        sky->Load("sunlesssky");

        auto t2 = Clock::now();

        std::cout << "Sky loading took  " << (t2 - t1).count() / 1000 / 1000 << " milliseconds"
                  << "\n";

        t1 = Clock::now();

        skyConvoluted = std::make_shared<Cubemap>();
        skyConvoluted->Filter(sky, loadShader("cubemap_convolute"), 32, 32);

        skyPrefiltered = std::make_shared<Cubemap>();

        std::shared_ptr<Shader> cubemapPrefilter = loadShader("cubemap_prefilter");
        UniformBufferHandle_t ubh = backend->CreateUniformBuffer(sizeof(CubemapPrefilterUniforms));
        CubemapPrefilterUniforms* pCpu = (CubemapPrefilterUniforms*)backend->MapUniformBuffer(ubh);
        pCpu->cubemapResolution = sky->GetWidth() * 2; // the x2 just makes it look better for some reason
        pCpu->roughness = 0.0f;
        backend->UnmapUniformBuffer(ubh);
        backend->SetShaderComboUniformBuffer(1, ubh, cubemapPrefilter->comboHandle);
        skyPrefiltered->Filter(sky, cubemapPrefilter, 256, 256);
        skyPrefiltered->Activate();
        glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

        for (int i = 1; i < 5; i++) {
            CubemapPrefilterUniforms* pCpu = (CubemapPrefilterUniforms*)backend->MapUniformBuffer(ubh);
            pCpu->cubemapResolution = sky->GetWidth() * 2; // the x2 just makes it look better for some reason
            pCpu->roughness = (float)i / 4.0f;
            backend->UnmapUniformBuffer(ubh);
            backend->SetShaderComboUniformBuffer(1, ubh, cubemapPrefilter->comboHandle);
            skyPrefiltered->FilterMip(sky, cubemapPrefilter, 256, 256, i);
        }

        t2 = Clock::now();
        std::cout << "Cubemap filtering took " << (t2 - t1).count() / 1000 / 1000 << " milliseconds"
                  << "\n";

        t1 = Clock::now();

        brdfLut = createBrdfLut();

        t2 = Clock::now();
        std::cout << "BRDF LUT creation took " << (t2 - t1).count() / 1000 / 1000 << " milliseconds"
                  << "\n";
    }

    void loadWindowIcon() {
        unsigned long len;
        const stbi_uc* buf = (const stbi_uc*)util::loadFileToBuffer("textures/cosiconshadow.png", len);
        stbi_set_flip_vertically_on_load(false);
        int width, height, numChannels;
        void* data = stbi_load_from_memory(buf, len, &width, &height, &numChannels, 0);
        icon.height = height;
        icon.width = width;
        icon.pixels = (unsigned char*)data;
        //glfwSetWindowIcon(window, 1, &icon);
        stbi_image_free(data);
        std::free((void*)buf);
    }

    void onLightConstruction(entt::DefaultRegistry& registry, uint32_t ent) {
        Light& light = registry.get<Light>(ent);
        if (!_settings->shadowsEnabled)
            return;
        light.shadowFB = std::make_shared<Framebuffer>();
        //light.shadowFB->activateDepthTexture();
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    }

    void createFramebuffers() {
        preEffectFb = std::make_shared<Framebuffer>();
        int rtWidth = (float)currentWidth * renderScale;
        int rtHeight = (float)currentHeight * renderScale;
        // Color
        preEffectFb->AttachColorTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::RGB16F, TextureUsage::RenderTarget), 0);
        // Normals
        preEffectFb->AttachColorTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::RGB16F, TextureUsage::RenderTarget), 1);
        // Position
        preEffectFb->AttachColorTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::RGB16F, TextureUsage::RenderTarget), 2);
        // Velocity
        preEffectFb->AttachColorTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::RGB16F, TextureUsage::RenderTarget), 3);
        // Depth
        preEffectFb->AttachDepthStencilTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::D24S8, TextureUsage::RenderTarget));

        postEffectFb = std::make_shared<Framebuffer>();
        postEffectFb->AttachColorTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::RGB8, TextureUsage::RenderTarget), 0);
        postEffectFb->AttachDepthStencilTexture(std::make_shared<Texture>(rtWidth, rtHeight, TextureFormat::D24S8, TextureUsage::RenderTarget));
    }

    void init(RenderInitSettings* settings, entt::DefaultRegistry& registry) {
        auto t1 = Clock::now();

        RenderBackendInitSettings initSettings;
        initSettings.windowWidth = settings->windowWidth;
        initSettings.windowHeight = settings->windowHeight;
        initSettings.windowTitle = settings->windowTitle;
        initSettings.swapInterval = settings->swapInterval;
        initSettings.fullscreen = settings->fullscreen;

        currentWidth = initSettings.windowWidth;
        currentHeight = initSettings.windowHeight;

        backend = &actualBackend;
        backend->Init(initSettings);

        _isInitialised = true;

        _settings = settings;

        loadPBRData();

        glViewport(0, 0, currentWidth, currentHeight);

        glEnable(GL_STENCIL_TEST);

        vg = nvgCreateGL3(0);
        debugFont = nvgCreateFont(vg, "hgdefault", "data/Verdana.ttf");
        defaultAlbedo = loadTexture("default.crn");
        defaultNormal = loadTexture("default_normal.png");
        renderScale = util::SettingsManager::GetOption("render.renderScale")->as<double>()->get();
        createFramebuffers();

        pipeline = std::make_unique<PostFXPipeline>(preEffectFb, postEffectFb);
        //pipeline->addPostFX(std::make_shared<PostSSAO>());
        //pipeline->addPostFX(std::make_shared<PostGlitch>());
        //pipeline->addPostFX(std::make_shared<PostBloom>());
        pipeline->addPostFX(std::make_shared<PostTint>());
        //pipeline->addPostFX(std::make_shared<PostFXAA>());
        //pipeline->addPostFX(std::make_shared<PostTAA>());
        //pipeline->addPostFX(std::make_shared<PostMotionBlur>());
        //pipeline->addPostFX(std::make_shared<PostPerspective>());

        InitialiseParticleSystem();

        util::DebugDraw::Init();

        loadWindowIcon();

#if IMGUI_ENABLED
        // ImGui init
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
        //io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
        ImGui::StyleColorsDark();

        ImGui_ImplGlfw_InitForOpenGL(window, true);
        ImGui_ImplOpenGL3_Init("#version 410 core");

        io.DisplaySize = ImVec2((float)currentWidth, (float)currentHeight);
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
#endif

        auto t2 = Clock::now();
        std::cout << "render::init took " << (t2 - t1).count() / 1000 / 1000 << " milliseconds."
                  << "\n";

        if (_settings->shadowsEnabled) {
            const float shadowDistance = 40.0f;
            shadowView = glm::lookAt(glm::vec3(10.0f, 20.0f, 10.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
            shadowProj = glm::ortho(-shadowDistance, shadowDistance, -shadowDistance, shadowDistance, 0.1f, 200.0f);
        }

        initNk();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        registry.construction<Light>().connect<&onLightConstruction>();
        InitializeSky();
        //tint = std::make_shared<PostTint>();
    }

    void shutdown() {
        util::DebugDraw::Shutdown();
#if IMGUI_ENABLED
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
#endif

        backend->Shutdown();

        ShutdownRenderdoc();
        _isInitialised = false;
        delete _settings;
    }

    // void performDrawCall(DrawCall& dc, Light* lights, int activeLights, bool firstCall) {
    //     if (!dc.mesh->uploaded)
    //         uploadMesh(dc.mesh);

    //     dc.shader->setMat4("model", dc.transformMatrix);
    //     dc.shader->setMat4("lastModel", dc.lastTransform.calculateMatrix());
    //     if (firstCall) {
    //         dc.shader->setMat4("view", dc.view);
    //         dc.shader->setMat4("projection", dc.projection);
    //         dc.shader->setVec3("viewPos", dc.viewPos);
    //         dc.shader->setMat4("lastView", dc.lastView);
    //         glm::mat4 taaMat(1.0f);
    //         taaMat = glm::translate(taaMat, taaOffset);
    //         dc.shader->setMat4("taaMat", taaMat);
    //         dc.shader->setMat4("lightspaceTransform", shadowProj * shadowView);
    //         dc.shader->setVec3("lightDir", glm::vec3(0.5f, 0.5f, 0.0f));
    //         dc.shader->setInt("useShadows", _settings->shadowsEnabled);
    //         for (int i = 0; i < activeLights; i++) {
    //             dc.shader->setInt("activeLights[" + std::to_string(i) + "].type", (int)lights[i].type);
    //             dc.shader->setVec3("activeLights[" + std::to_string(i) + "].color", lights[i].color);
    //             if (lights[i].type == LightType::Point)
    //                 dc.shader->setFloat("activeLights[" + std::to_string(i) + "].pointRange", lights[i].pointRange);
    //             dc.shader->setVec3("activeLights[" + std::to_string(i) + "].posDir", lights[i].posDir);
    //         }
    //         dc.shader->setInt("activeLightCount", activeLights);
    //     }

    //     for (int i = 0; i < MAX_MATERIAL_PARAMETERS; i++) {
    //         ShaderParameter& parameter = dc.material->GetParameter(i);

    //         if (parameter.nameHash == 0)
    //             continue;

    //         std::string name = dc.material->GetParameterName(i);

    //         switch (parameter.type) {
    //         case ShaderParameterType::Integer:
    //             dc.shader->setInt(name, parameter.integerValue);
    //             break;
    //         case ShaderParameterType::Float:
    //             dc.shader->setFloat(name, parameter.floatValue);
    //             break;
    //         case ShaderParameterType::Vec2:
    //             dc.shader->setVec2(name, parameter.vec2Value);
    //             break;
    //         case ShaderParameterType::Vec3:
    //             dc.shader->setVec3(name, parameter.vec3Value);
    //             break;
    //         case ShaderParameterType::Vec4:
    //             dc.shader->setVec4(name, parameter.vec4Value);
    //             break;
    //         case ShaderParameterType::Mat4:
    //             dc.shader->setMat4(name, parameter.mat4Value);
    //             break;
    //         }
    //     }

    //     dc.shader->setInt("brdfLUT", 0);
    //     dc.shader->setInt("sky", 1);
    //     dc.shader->setInt("skyConvoluted", 2);
    //     dc.shader->setInt("skyPrefiltered", 3);
    //     dc.shader->setInt("shadowMap", 4);

    //     // defaults
    //     if (firstCall) {
    //         dc.shader->setInt("normalMap", 15);
    //         glActiveTexture(GL_TEXTURE15);
    //         defaultNormal->activate();

    //         glActiveTexture(GL_TEXTURE0);
    //         brdfLut->activate();

    //         glActiveTexture(GL_TEXTURE1);
    //         sky->Activate();

    //         glActiveTexture(GL_TEXTURE2);
    //         skyConvoluted->Activate();

    //         glActiveTexture(GL_TEXTURE3);
    //         skyPrefiltered->Activate();

    //         if (_settings->shadowsEnabled) {
    //             glActiveTexture(GL_TEXTURE4);
    //             for (int i = 0; i < activeLights; i++) {
    //                 if (lights[i].enabled && lights[i].shadowFB) {
    //                     //lights[i].shadowFB->activateDepthTexture();
    //                     break;
    //                 }
    //             }
    //         }
    //     }

    //     for (int i = 0; i < MAX_MATERIAL_SAMPLERS; i++) {
    //         ShaderSampler& sampler = dc.material->GetSampler(i);

    //         if (sampler.nameHash == 0)
    //             continue;

    //         std::string name = dc.material->GetSamplerName(i);

    //         dc.shader->setInt(name, i + 5);
    //         glActiveTexture(GL_TEXTURE5 + i);
    //         if (sampler.texture && sampler.texture->doneLoading)
    //             sampler.texture->activate();
    //         else
    //             defaultAlbedo->activate();
    //     }

    //     drawnVertices += dc.mesh->vertices.size();
    //     drawnTriangles += dc.mesh->indices.size() / 3;

    //     glUseProgram(dc.shader->programHandle);
    //     glBindVertexArray(dc.mesh->vao);
    //     glDrawElements(GL_TRIANGLES, dc.mesh->indices.size(), GL_UNSIGNED_INT, 0);
    //     glBindVertexArray(0);
    //     glActiveTexture(GL_TEXTURE0);
    // }

    glm::mat4 getCameraViewMatrix(Camera* camera) {
        return glm::lookAt(
            camera->transform.position,
            camera->transform.transformDirection(glm::vec3(0.0f, 0.0f, -1.0f)) + camera->transform.position,
            camera->transform.transformDirection(glm::vec3(0.0f, 1.0f, 0.0f)));
    }

    glm::mat4 getCameraLastViewMatrix(Camera* camera) {
        return glm::lookAt(
            camera->lastTransform.position,
            camera->lastTransform.transformDirection(glm::vec3(0.0f, 0.0f, -1.0f)) + camera->lastTransform.position,
            camera->lastTransform.transformDirection(glm::vec3(0.0f, 1.0f, 0.0f)));
    }

    glm::mat4 getCameraProjectionMatrix(Camera* camera) {
        // The camera FOV is horizontal for a 16:9 aspect ratio
        // We need to convert it to vertical
        float vertFov = 2 * atan(tan(glm::radians(camera->fov) / 2) * (16.f / 9.f));
        camera->frustrum.SetCameraInfo(
            vertFov,
            (float)currentWidth / (float)currentHeight,
            camera->nearPlane,
            camera->farPlane,
            camera->transform.position,
            camera->transform.transformDirection(glm::vec3(0.0f, 0.0f, -1.0f)) + camera->transform.position,
            camera->transform.transformDirection(glm::vec3(0.0f, 1.0f, 0.0f)));
        return glm::infinitePerspective(vertFov, (float)currentWidth / (float)currentHeight, camera->nearPlane); //, camera->farPlane);
    }

    void renderDebugEntities(Camera* camera) {
        util::DebugDraw::RenderDrawn(getCameraViewMatrix(camera), getCameraProjectionMatrix(camera));
    }

    void setFullscreenMode(bool fullscreen) {
        backend->SetFullscreen(fullscreen);

        isFullscreen = fullscreen;
    }

    // void drawEffect(std::shared_ptr<Shader> shader, std::shared_ptr<Mesh> effectMesh) {
    //     glDisable(GL_CULL_FACE);
    //     glDisable(GL_DEPTH_TEST);
    //     glUseProgram(shader->programHandle);

    //     glm::mat4 projection = glm::ortho(0.0f, 1.0f, 0.0f, 1.0f);

    //     shader->setMat4("projection", projection);

    //     glBindBuffer(GL_ARRAY_BUFFER, effectMesh->vbo);
    //     glBindVertexArray(effectMesh->vao);

    //     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, effectMesh->ibo);
    //     glDrawElements(GL_TRIANGLES, effectMesh->indices.size(), GL_UNSIGNED_INT, 0);
    //     glEnable(GL_DEPTH_TEST);
    //     glEnable(GL_CULL_FACE);
    // }

    // void performShadowDrawCall(DrawCall dc) {
    //     if (!dc.mesh->uploaded)
    //         uploadMesh(dc.mesh);

    //     static std::shared_ptr<Shader> shadowmapShader = loadShader("shadowmap");

    //     shadowmapShader->setMat4("model", dc.transformMatrix);
    //     shadowmapShader->setMat4("projection", shadowProj);
    //     shadowmapShader->setMat4("view", shadowView);

    //     glUseProgram(shadowmapShader->programHandle);
    //     glBindVertexArray(dc.mesh->vao);
    //     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, dc.mesh->ibo);
    //     glDrawElements(GL_TRIANGLES, dc.mesh->indices.size(), GL_UNSIGNED_INT, 0);

    //     drawnVertices += dc.mesh->vertices.size();
    //     drawnTriangles += dc.mesh->indices.size() / 3;
    // }

    void activatePreEffectDraw() {
        preEffectFb->SetAsTarget();
        Rect viewportRect;
        viewportRect.position = glm::ivec2(0, 0);
        viewportRect.extent = glm::ivec2(currentWidth * renderScale, currentHeight * renderScale);
        backend->SetViewport(viewportRect);
    }

    void activateScreenDraw() {
        backend->SetScreenAsTarget();
        Rect viewportRect;
        viewportRect.position = glm::ivec2(0, 0);
        viewportRect.extent = glm::ivec2(currentWidth, currentHeight);
        backend->SetViewport(viewportRect);
    }

    void setShadowViewMat(Light& light) {
        glm::vec3 position = glm::vec3(10.0f, 20.0f, 10.0f) + glm::floor(lastCam.transform.position);
        glm::vec3 lookAt = position - light.posDir;

        shadowFrustum.SetFromViewProj(glm::transpose(shadowView) * shadowProj);
        shadowView = glm::lookAt(position, lookAt, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    int frameOffset = 0;
    extern RENDERDOC_API_1_1_2* rdoc_api;

    void screenshot() {
        std::string name = "ss_";
        char buffer[32];
        std::time_t t = std::time(nullptr);
        std::strftime(buffer, sizeof(buffer), "%F %T", std::localtime(&t));
        name += buffer;
        name += ".png";
        void* pixels = std::malloc(3l * currentWidth * currentHeight);
        int width = currentWidth;
        int height = currentHeight;
        glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixels);
        stbi_flip_vertically_on_write(1);
        stbi_write_png(name.c_str(), width, height, 3, pixels, 3 * width);
        std::free(pixels);
    }

    constexpr glm::vec3 skyWavelength(0.650f, 0.570f, 0.475f);
    constexpr glm::vec3 skyWavelength4(0.17850625f, 0.10556000999999997f, 0.05090664062499999f);
    constexpr float skyKr = 0.0025f;
    constexpr float skyKr4Pi = skyKr * 4.0f * M_PI;
    constexpr float skyKm = 0.001f;
    constexpr float skyKm4Pi = skyKm * 4.0f * M_PI;
    constexpr float skySunBrightness = 20.0f;
    constexpr float skyG = -0.990f;
    constexpr float skyRayleighScaleDepth = 0.25f;
    constexpr float skyMieScaleDepth = 0.1f;
    constexpr float skyInnerRadius = 10.0f;
    constexpr float skyOuterRadius = 10.25f;

    void endFrame(float deltaTime, entt::DefaultRegistry& registry) {
        Light activeLights[16];
        int idx = 0;
        int activeLightCount = 0;
        int actualDrawCalls = 0;

        glDisable(GL_BLEND);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        registry.view<Light>().each([&](auto ent, Light& light) {
            activeLightCount++;
            if (activeLightCount >= 16) {
                return;
            }

            if (light.type == LightType::Point) {
                components::Transform& transform = registry.get<components::Transform>(ent);
                light.posDir = transform.position;
            }

            if (light.type == LightType::Directional && light.shadowsEnabled && _settings->shadowsEnabled && light.shadowFB && false) {
                setShadowViewMat(light);
                //light.shadowFB->activate();
                //glViewport(0, 0, light.shadowFB->getWidth(), light.shadowFB->getHeight());
                //glClear(GL_DEPTH_BUFFER_BIT);
                registry.view<components::Renderable, components::Transform>().each([deltaTime, &actualDrawCalls](const auto, components::Renderable& renderable, components::Transform& transform) {
                    glm::mat4 transformMatrix = transform.calculateMatrix();

                    DrawCall dc{
                        renderable.mesh,
                        renderable.shader,
                        renderable.material,
                        glm::vec3(0.0f),
                        transformMatrix,
                        transform,
                        renderable.lastTransform,
                    };

                    float scaleMax = std::max(dc.transform.scale.x, dc.transform.scale.y);
                    scaleMax = std::max(scaleMax, dc.transform.scale.z);

                    if (shadowFrustum.ContainsSphere(dc.transform.position, dc.mesh->boundingSphereRadius * scaleMax)) {
                        //performShadowDrawCall(dc);
                        actualDrawCalls++;
                    }
                });
            }
            activeLights[idx] = light;
            idx++;
        });

        activatePreEffectDraw();

        if (_settings->useTAA) {
            frameOffset++;
            if (frameOffset >= 2)
                frameOffset = 0;

            static glm::vec3 offset(0.5f, 0.5f, 0.0f);
#if IMGUI_ENABLED
            ImGui::Begin("TAA Offset");
            ImGui::DragFloat2("Offset", &offset.x);
            ImGui::End();
#endif

            switch (frameOffset) {
            case 0:
                taaOffset = -offset / glm::vec3(currentWidth, currentHeight, 1.0f);
                break;
            case 1:
                taaOffset = offset / glm::vec3(currentWidth, currentHeight, 1.0f);
                break;
            }
        } else {
            taaOffset = glm::vec3(0.0f);
        }

        std::shared_ptr<Shader> lastShader;

        registry.view<render::Camera>().each([&](const auto, render::Camera& cam) {
            registry.view<components::Renderable, components::Transform>().each([deltaTime, &cam, activeLightCount, &activeLights, &lastShader, &actualDrawCalls](const auto, components::Renderable& renderable, components::Transform& transform) {
                glm::mat4 transformMatrix = transform.calculateMatrix();

                DrawCall dc{
                    renderable.mesh, renderable.shader, renderable.material, cam.transform.position, transformMatrix, transform, renderable.lastTransform,
                    getCameraViewMatrix(&cam), getCameraLastViewMatrix(&cam), getCameraProjectionMatrix(&cam)
                };

                float scaleMax = std::max(dc.transform.scale.x, dc.transform.scale.y);
                scaleMax = std::max(scaleMax, dc.transform.scale.z);
                if (lastCam.frustrum.ContainsSphere(dc.transform.position, dc.mesh->boundingSphereRadius * scaleMax)) {
                    //performDrawCall(dc, activeLights, activeLightCount, true);
                    lastShader = dc.shader;
                    actualDrawCalls++;
                }
            });

            render::renderDebugEntities(&cam);
            lastCam = cam;
        });

        DrawEmitters(registry, lastCam, getCameraProjectionMatrix(&lastCam), getCameraViewMatrix(&lastCam), deltaTime);

        if ((dbgMode & DebugMode::Wireframe) == DebugMode::Wireframe)
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        renderDebugEntities(&lastCam);

        //backbuffer->activateDraw();
        //glBindFramebuffer(GL_FRAMEBUFFER, 0);
        //glClearColor(1.0f, 0.0f, 1.0f, 0.0f);
        //glClear(GL_COLOR_BUFFER_BIT);
        //pipeline->render(lastCam);
        DrawSky(getCameraViewMatrix(&lastCam), getCameraProjectionMatrix(&lastCam));

        activateScreenDraw();
        pipeline->render(lastCam);
        backend->CopyFramebufferRegion(postEffectFb->GetHandle(), 0, Rect(0, 0, currentWidth * renderScale, currentHeight * renderScale), Rect(0, 0, currentWidth, currentHeight));

        //tint->render(preEffectFb, nullptr, lastCam, preEffectFb);

        bool screenshotWithUI = util::SettingsManager::GetOption("render.screenshotWithUI")->as<bool>()->get();

        if (input::getActionDown("screenshot"_hs) && !screenshotWithUI)
            screenshot();

        if (showDebugMenu) {
            RenderDebugInfo info;
            info.deltaTime = deltaTime;
            info.triangleCount = drawnTriangles;
            info.vertexCount = drawnVertices;
            info.currentWidth = currentWidth;
            info.currentHeight = currentHeight;
            info.drawCalls = actualDrawCalls;
            info.lastCamPos = lastCam.transform.position;
            debugFrame(info, vg);
        }

        drawnTriangles = 0;
        drawnVertices = 0;
#if IMGUI_ENABLED
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
#endif
        nvgEndFrame(vg);
        drawNk();
#if IMGUI_ENABLED
        GLFWwindow* backup_current_context = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backup_current_context);
#endif

        if ((dbgMode & DebugMode::Wireframe) == DebugMode::Wireframe)
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    void startFrame(float deltaTime, entt::DefaultRegistry& registry) {
        bool screenshotWithUI = util::SettingsManager::GetOption("render.screenshotWithUI")->as<bool>()->get();
        // Start of new frame
        // We have to re-enable GL_DEPTH_TEST, as the UI system disables it

        activatePreEffectDraw();
#if IMGUI_ENABLED
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();

        ImGui::NewFrame();
#endif
        nvgBeginFrame(vg, currentWidth, currentHeight, 1.0f);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);

        if (input::getActionDown("fullscreen"))
            setFullscreenMode(!isFullscreen);

        if (input::getActionDown("devMenu"))
            showDebugMenu = !showDebugMenu;

        if (input::getActionDown("screenshot"_hs) && screenshotWithUI)
            screenshot();

        unloadUnusedMeshes();

        glClearColor(0.f, 0.f, 0.f, 1.0f);
        glClearDepthf(1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        registry.view<Camera>().each([](uint32_t ent, Camera& cam) {
            cam.lastTransform = cam.transform;
        });

        registry.view<components::Transform, components::Renderable>().each([](uint32_t ent, auto& transform, auto& renderable) {
            renderable.lastTransform = transform;
        });
    }

    bool frame(float deltaTime, entt::DefaultRegistry& registry) {
        endFrame(deltaTime, registry);
        backend->SwapBuffers();
        startFrame(deltaTime, registry);
        return true;
    }

    NVGcontext* getVGContext() {
        return vg;
    }

    int getDefaultVGFont() {
        return debugFont;
    }

    glm::ivec2 getCurrentScreenSize() {
        return glm::ivec2(currentWidth, currentHeight);
    }

    glm::ivec2 getCurrentRenderSize() {
        return glm::ivec2(currentWidth * renderScale, currentHeight * renderScale);
    }
}
}
