#include "RenderdocSupport.hpp"
#if defined(_WIN32)
#include <windows.h>
#elif defined(__unix__)
#include <dlfcn.h>
#endif
#include <cassert>
#include <iostream>
#include <renderdoc_app.h>

namespace halogen {
namespace render {
    RENDERDOC_API_1_1_2* rdoc_api = NULL;
    bool InitRenderdoc() {
#if defined(_WIN32)
        if (HMODULE mod = GetModuleHandleA("renderdoc.dll")) {
            pRENDERDOC_GetAPI RENDERDOC_GetAPI = (pRENDERDOC_GetAPI)GetProcAddress(mod, "RENDERDOC_GetAPI");
            int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_1_2, (void**)&rdoc_api);
            assert(ret == 1);
        }
#elif defined(__unix__)
        if (void* mod = dlopen("librenderdoc.so", RTLD_NOW)) {
            std::cout << "Loading renderdoc..."
                      << "\n";
            pRENDERDOC_GetAPI RENDERDOC_GetAPI = (pRENDERDOC_GetAPI)dlsym(mod, "RENDERDOC_GetAPI");
            int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_1_2, (void**)&rdoc_api);
            assert(ret == 1);
        }
#else
#error Unsupported compiler!
#endif
        if (rdoc_api) {
            rdoc_api->SetCaptureFilePathTemplate("rdoccaptures/halogen");
            return true;
        } else {
            return false;
        }
    }

    void ShutdownRenderdoc() {
        if (rdoc_api) {
            rdoc_api->Shutdown();
        }
    }
}
}