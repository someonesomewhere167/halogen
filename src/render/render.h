#pragma once
#include "Frustum.hpp"
#include "Shader.hpp"
#include "mesh.h"
#include "nanovg.h"
#include "texture.h"
#include "transform.h"
#include <GLFW/glfw3.h>
#include <entt/entt.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <stdexcept>
#include <string>
#include <vector>
#include <RenderBackendBase.hpp>

namespace halogen
{
namespace render
{
    class Material;

    struct RenderInitSettings
    {
        int msaaSamples;
        int windowHeight;
        int windowWidth;
        int swapInterval;
        int shadowmapRes;
        bool fullscreen;
        bool shadowsEnabled;
        std::string windowTitle;
        bool useTAA;
    };

    enum DebugMode
    {
        Wireframe = 1,
        ShowFrametimeGraph = 2
    };

    void init(RenderInitSettings*, entt::DefaultRegistry&);
    void shutdown();
    bool frame(float deltaTime, entt::DefaultRegistry& registry);
    void uploadMesh(std::shared_ptr<Mesh> mesh);
    void setWindowTitle(std::string title);
    void setDebugMode(int modeFlag);
    GLFWwindow* getWindow();
}

namespace components
{
    struct Renderable
    {
        Renderable(std::shared_ptr<render::Mesh> mesh, std::shared_ptr<render::Shader> shader, std::shared_ptr<render::Material> material);
        Transform lastTransform;
        std::shared_ptr<render::Mesh> mesh;
        std::shared_ptr<render::Shader> shader;
        std::shared_ptr<halogen::render::Material> material;
        bool draw;
    };
}

namespace render
{
    struct Camera
    {
        Camera();
        float fov;
        float nearPlane;
        float farPlane;
        components::Transform transform;
        components::Transform lastTransform;
        Frustum frustrum;
    };

    struct MVPMatrices {
        glm::mat4 projection;
        glm::mat4 view;
        glm::mat4 model;
    };
    void renderDebugEntities(Camera* camera);
    bool isInitialised();
    NVGcontext* getVGContext();
    int getDefaultVGFont();
    glm::ivec2 getCurrentScreenSize();
    glm::ivec2 getCurrentRenderSize();
    extern RenderBackendBase* backend;
}
}
