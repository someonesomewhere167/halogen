#pragma once
#include "render.h"
#include <memory>

namespace halogen
{
namespace render
{
    std::shared_ptr<Mesh> loadMesh(std::string path);
    void unloadAllMeshes();
    void unloadUnusedMeshes();
}
}