#pragma once
#include <nuklear_ws.h>

namespace halogen {
namespace render {
    void initNk();
    void drawNk();
    nk_context* getNkContext();
    struct nk_image loadNkImage(std::string path);
}
}