#include "glad.h"
#include "renderdebug.h"
#include "render.h"
#include <glm/gtc/matrix_transform.hpp>
#include <iomanip>
#include <sstream>
#include <string>

namespace halogen {
namespace render {
    bool frametimeGraphInitialised = false;
    std::shared_ptr<Shader> frametimeShader;
    std::shared_ptr<Mesh> frametimeMesh;
#define FRAMETIME_COUNT 128
    float frametimes[FRAMETIME_COUNT];
    int currrentFtIndex = -1;

    void addVertex(glm::vec3 pos, glm::vec2 uv) {
        Vertex vert;
        vert.pos = pos;
        vert.uv = uv;
        vert.normal = glm::vec3(133.0f, 133.0f, 133.0f);
        vert.tangent = glm::vec3(133.0f, 133.0f, 133.0f);
        frametimeMesh->vertices.push_back(vert);
    }

    void initFrametimeGraph() {
        frametimeShader = render::loadShader("frametimeGraph");
        frametimeGraphInitialised = true;
        frametimeMesh = std::make_shared<Mesh>();
        addVertex(glm::vec3(10.0f, 100.0f, 0.0f), glm::vec2(0.0f, 1.0f));
        addVertex(glm::vec3(400.0f, 100.0f, 0.0f), glm::vec2(1.0f, 1.0f));
        addVertex(glm::vec3(10.0f, 10.0f, 0.0f), glm::vec2(0.0f, 0.0f));
        addVertex(glm::vec3(400.0f, 10.0f, 0.0f), glm::vec2(1.0f, 0.0f));
        frametimeMesh->indices.push_back(0);
        frametimeMesh->indices.push_back(1);
        frametimeMesh->indices.push_back(2);

        frametimeMesh->indices.push_back(1);
        frametimeMesh->indices.push_back(2);
        frametimeMesh->indices.push_back(3);

        uploadMesh(frametimeMesh);
    }

    void drawFrametimeGraph(RenderDebugInfo info) {
        if (!frametimeGraphInitialised)
            initFrametimeGraph();

        currrentFtIndex = ++currrentFtIndex % FRAMETIME_COUNT;
        frametimes[currrentFtIndex] = info.deltaTime;

        //glUseProgram(frametimeShader->programHandle);

        glm::mat4 projection = glm::ortho(0.0f, (float)info.currentWidth, 0.0f, (float)info.currentHeight);

        //frametimeShader->setMat4("projection", projection);
        //frametimeShader->setFloat("frametimesLength", (float)FRAMETIME_COUNT);
        //frametimeShader->setFloatArray("frametimes", frametimes, FRAMETIME_COUNT);

        glBindVertexArray(frametimeMesh->vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, frametimeMesh->ibo);
        glDrawElements(GL_TRIANGLES, frametimeMesh->indices.size(), GL_UNSIGNED_INT, 0);
    }

    void debugFrame(RenderDebugInfo info, NVGcontext* vg) {
        nvgFontFace(vg, "hgdefault");
        nvgFontSize(vg, 20.0f);
        nvgFillColor(vg, nvgRGBA(255, 255, 255, 255));
        nvgText(vg, 10.0f, 20.0f, (std::to_string(info.vertexCount) + " verts").c_str(), 0);
        nvgText(vg, 10.0f, 40.0f, (std::to_string(info.triangleCount) + " tris").c_str(), 0);
        nvgText(vg, 10.0f, 60.0f, (std::to_string(info.deltaTime * 1000) + " ms").c_str(), 0);
        nvgText(vg, 10.0f, 80.0f, (std::to_string(1 / info.deltaTime) + " fps").c_str(), 0);
        std::string sizeString = "Screen size: " + std::to_string(info.currentWidth) + "x" + std::to_string(info.currentHeight);
        nvgText(vg, 10.0f, 100.0f, sizeString.c_str(), 0);
        nvgText(vg, 10.0f, 120.0f, (std::to_string(info.drawCalls) + " draw calls").c_str(), 0);
        std::stringstream stream;
        stream << std::fixed << std::setprecision(2) << "Last camera position: " << info.lastCamPos.x << "," << info.lastCamPos.y << "," << info.lastCamPos.z;
        nvgText(vg, 10.0f, 140.0f, stream.str().c_str(), 0);

        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        //drawFrametimeGraph(info);
    }
}
}
