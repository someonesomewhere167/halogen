#pragma once
#include <glm/glm.hpp>

namespace halogen {
namespace render {
    struct Plane {
        Plane()
            : normal(0.0f)
            , distance(0.0f) {}
        glm::vec3 normal;
        float distance;

        void SetNormalAndPoint(glm::vec3 normal, glm::vec3 point);
        void Set3Points(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3);
        void SetCoefficients(float a, float b, float c, float d);
    };
}
}