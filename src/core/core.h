#pragma once
#include <entt/entt.hpp>
#include <memory>
#include <string>

namespace halogen
{
namespace core
{
    class System
    {
    public:
        virtual void update(float deltaTime, entt::DefaultRegistry& registry) = 0;
        virtual std::string getName() = 0;

    protected:
        ~System(){};
    };

    bool isDedicatedServer();
    void init(int argc, char** argv);
    void registerSystem(std::shared_ptr<System> system);
    void deregisterSystem(std::shared_ptr<System> system);
    void beginLoop(void (*gameTick)(float));
    void exit();
    void shutdown();
    extern entt::DefaultRegistry entityRegistry;
}
}