#include "core.h"
#include "audio.h"
#include "hgdfile.h"
#include "hgdgroups.h"
#include "hgio.h"
#include "input.h"
#include "meshloader.h"
#include "physics.h"
#include "render.h"
#include <GLFW/glfw3.h>
#include <Log.hpp>
#include <RenderdocSupport.hpp>
#include <SettingsManager.hpp>
#include <StdOutLogTarget.hpp>
#include <chrono>
#include <cpptoml.h>
#include <imgui.h>
#include <iostream>
#include <physfs.h>
#include <sys/stat.h>

namespace halogen {
namespace core {
    bool shouldExit = false;
    bool _isDedicatedServer = false;
    entt::DefaultRegistry entityRegistry;
    gamedata::HGDFile* mainDataFile;
    gamedata::GameInfoGroup* gameInfo;
    std::vector<std::shared_ptr<System>> systems;
    std::unique_ptr<physics::Physics> phys;
    halogen::render::RenderInitSettings* renderInitSettings;

    bool isDedicatedServer() { return _isDedicatedServer; }

    void init(int argc, char** argv) {
        bool dedicatedServer = false;
        for (int i = 1; i < argc; i++) {
            std::string str(argv[i]);

            if (str == "--ds") {
                dedicatedServer = true;
            }
        }

        util::Log::AddLogTarget(std::make_shared<util::StdOutLogTarget>());
        util::Log::LogString("Initialised log.", util::LogCategory::Core);

        render::InitRenderdoc();

        int initReturnVal = PHYSFS_init(argv[0]);
        if (initReturnVal == 0) {
            PHYSFS_ErrorCode code = PHYSFS_getLastErrorCode();
            util::Log::LogString(std::string("Failed to initialise PhysFS due to ") + PHYSFS_getErrorByCode(code), util::LogCategory::Core);
            return;
        }

        PHYSFS_setWriteDir(".");

        if (PHYSFS_mount("data.zip", "", 0) == 0) {
            PHYSFS_ErrorCode code = PHYSFS_getLastErrorCode();
            if (code != PHYSFS_ERR_OK)
                util::Log::LogString(std::string("Error while mounting data.zip: ") + PHYSFS_getErrorByCode(code), util::LogCategory::Core);
        } else {
            // PhysFS sometimes likes to throw up a not_found error here even when it was found.
            // Clear it.
            PHYSFS_getLastErrorCode();
        }

        if (PHYSFS_mount("data", "", 1) == 0) {
            PHYSFS_ErrorCode code = PHYSFS_getLastErrorCode();
            if (code != PHYSFS_ERR_OK)
                util::Log::LogString(std::string("Error while mounting data directory: ") + PHYSFS_getErrorByCode(code), util::LogCategory::Core);
        } else {
            // Same as above.
            PHYSFS_getLastErrorCode();
        }

        mainDataFile = new gamedata::HGDFile("main.hgd");
        gamedata::HGDGroup* infoHGDGroup = mainDataFile->getGroup(0);
        gameInfo = new gamedata::GameInfoGroup(*infoHGDGroup);

        util::Log::LogString("Loaded data for " + gameInfo->getGameName(), util::LogCategory::Core);
        util::Log::LogString("Version " + gameInfo->getVersionString(), util::LogCategory::Core);

        struct stat configPathInfo;

        if (stat(util::getConfigPath().c_str(), &configPathInfo) != 0) {
            util::Log::LogString("Config directory doesn't exist, creating...", util::LogCategory::Core);
            util::createDirectory(util::getConfigPath());
        }

        std::string configFilePath = util::getConfigPath();
#ifdef _WIN32
        configFilePath += "\\config.toml";
#else
        configFilePath += "/config.toml";
#endif

        std::ifstream configStream(configFilePath);

        renderInitSettings = new halogen::render::RenderInitSettings;

        if (configStream.is_open()) {
            cpptoml::parser configParser(configStream);
            auto root = configParser.parse();
            util::SettingsManager::LoadFromTable(root);
            auto renderSettings = root->get_table("render");

            renderInitSettings->msaaSamples = *renderSettings->get_as<int>("msaaSamples");
            renderInitSettings->windowWidth = *renderSettings->get_as<int>("windowWidth");
            renderInitSettings->windowHeight = *renderSettings->get_as<int>("windowHeight");
            renderInitSettings->swapInterval = *renderSettings->get_as<int>("swapInterval");
            renderInitSettings->fullscreen = *renderSettings->get_as<bool>("fullscreen");
            renderInitSettings->shadowmapRes = *renderSettings->get_as<int>("shadowmapRes");
            renderInitSettings->useTAA = *renderSettings->get_as<bool>("useTAA");
            renderInitSettings->shadowsEnabled = *renderSettings->get_as<bool>("shadowsEnabled");
        } else {
            renderInitSettings->msaaSamples = 2;
            renderInitSettings->windowWidth = 1280;
            renderInitSettings->windowHeight = 720;
            renderInitSettings->fullscreen = false;
            renderInitSettings->swapInterval = 1;
            renderInitSettings->shadowmapRes = 2048;
            renderInitSettings->useTAA = false;
            renderInitSettings->shadowsEnabled = true;
        }

        renderInitSettings->windowTitle = gameInfo->getWindowTitle();

        if (!configStream.is_open()) {
            std::string inputBase = util::loadFileToString("defaultoptions.toml");
            std::istringstream sstream(inputBase);
            std::istream& stream(sstream);

            cpptoml::parser parser(stream);
            auto root = parser.parse();

            util::Log::LogString("Couldn't open config, saving new", util::LogCategory::Core);
            std::ofstream configOutStream(configFilePath);

            if (!configOutStream.is_open())
                throw std::runtime_error("Couldn't write to preferences!");
            configOutStream << *root;
            configOutStream.close();
        }

        if (!dedicatedServer) {
            render::init(renderInitSettings, entityRegistry);
            audio::init(entityRegistry);
            input::init();
        } else {
            glfwInit();
        }

        phys = std::make_unique<physics::Physics>(entityRegistry);

        _isDedicatedServer = dedicatedServer;
    }

    void registerSystem(std::shared_ptr<System> system) {
        systems.push_back(system);
    }

    void deregisterSystem(std::shared_ptr<System> system) {
        systems.erase(std::remove(systems.begin(), systems.end(), system), systems.end());
    }

    void exit() {
        shouldExit = true;
    }

    bool wireFrameOn = false;
    void debugTick() {
        if (input::getActionDown("wireframe")) {
            wireFrameOn = !wireFrameOn;

            if (wireFrameOn)
                render::setDebugMode(render::DebugMode::Wireframe);
            else
                render::setDebugMode(0);
        }
    }

    void beginLoop(void (*gameTick)(float)) {
        if (!isDedicatedServer()) {
            render::setWindowTitle(gameInfo->getWindowTitle());
        }
        double lastTime = glfwGetTime();

        //GLFWwindow* window = render::getWindow();

        double renderMsLF = 0.0;

        while (!shouldExit) {
            double currentTime = glfwGetTime();
            float deltaTime = currentTime - lastTime;
            lastTime = currentTime;
            if (!isDedicatedServer())
                input::startFrame();

#if !NDEBUG
            if (!isDedicatedServer())
                debugTick();
#endif
            auto pt1 = std::chrono::high_resolution_clock::now();
            phys->simulate(deltaTime);
            auto pt2 = std::chrono::high_resolution_clock::now();

            auto gt1 = std::chrono::high_resolution_clock::now();
            gameTick(deltaTime);
            auto gt2 = std::chrono::high_resolution_clock::now();
#if DEBUG && IMGUI_ENABLED
            if (!isDedicatedServer())
                ImGui::Begin("System Perf");
#endif
            for (std::shared_ptr<System> sys : systems) {
                auto t1 = std::chrono::high_resolution_clock::now();
                sys->update(deltaTime, entityRegistry);
                auto t2 = std::chrono::high_resolution_clock::now();

                auto gap = t2 - t1;
#if DEBUG && IMGUI_ENABLED
                if (!isDedicatedServer()) {
                    ImVec4 color(1.0f, 1.0f, 1.0f, 1.0f);
                    if (gap.count() / 1000 / 1000 >= 5) {
                        color.y = 0.0f;
                        color.z = 0.0f;
                    }
                    ImGui::TextColored(color, "%s: %fms", sys->getName().c_str(), (double)gap.count() / 1000.0 / 1000.0);
                }
#endif
            }

            if (!isDedicatedServer()) {
#if DEBUG && IMGUI_ENABLED
                ImVec4 color(1.0f, 1.0f, 1.0f, 1.0f);
                auto pgap = pt2 - pt1;
                if (pgap.count() / 1000 / 1000 >= 5) {
                    color.y = 0.0f;
                    color.z = 0.0f;
                }
                ImGui::TextColored(color, "Physics: %fms", (double)pgap.count() / 1000.0 / 1000.0);

                ImVec4 gtcolor(1.0f, 1.0f, 1.0f, 1.0f);
                auto gtgap = gt2 - gt1;
                if (gtgap.count() / 1000 / 1000 >= 5) {
                    color.y = 0.0f;
                    color.z = 0.0f;
                }
                ImGui::TextColored(color, "gameTick: %fms", (double)gtgap.count() / 1000.0 / 1000.0);

                ImGui::Text("Last render frame time: %f", renderMsLF);
                ImGui::End();
#endif
                audio::update(entityRegistry);
                auto t1 = std::chrono::high_resolution_clock::now();
                if (!render::frame(deltaTime, entityRegistry)) {
                    shouldExit = true;
                }
                auto t2 = std::chrono::high_resolution_clock::now();
                auto gap = t2 - t1;
                renderMsLF = (double)gap.count() / 1000.0 / 1000.0;
                //input::endFrame();
            }
        }
    }

    void shutdown() {
        if (!isDedicatedServer()) {
            input::shutdown();
            audio::shutdown();
            render::shutdown();
        } else {
            delete renderInitSettings;
        }
    }
}
}