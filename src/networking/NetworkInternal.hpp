#pragma once
#include "Network.hpp"
#include "yojimbo.h"

namespace halogen {
namespace networking {
    static const uint8_t DEFAULT_PRIVATE_KEY[yojimbo::KeyBytes] = { 0x14, 0xf5, 0x06, 0x41, 0x6b, 0xe5, 0x10, 0xa6, 0xf4, 0xb1, 0x13, 0xe7, 0x4e, 0xd3, 0x97, 0xfc, 0x37, 0x5b, 0xa7, 0x99, 0xb8, 0xbd, 0x80, 0xaf, 0xc3, 0x84, 0xe3, 0xbf, 0x65, 0xb3, 0x97, 0xa7 };

    extern std::vector<ICustomMessageProvider*> messageProviders;
    class GameMessageFactory : public yojimbo::MessageFactory {
    public:
        GameMessageFactory(yojimbo::Allocator& allocator)
            : MessageFactory(allocator, GetTotalMessageTypeCount()) {
        }

        yojimbo::Message* CreateMessageInternal(int type) {
            yojimbo::Message* message;
            yojimbo::Allocator& allocator = GetAllocator();
            (void)allocator;
            switch (type) {
                YOJIMBO_DECLARE_MESSAGE_TYPE((int)MessageType::PlayerJoin, PlayerJoinMessage);
                YOJIMBO_DECLARE_MESSAGE_TYPE((int)MessageType::AdminCommand, AdminCommandMessage);
                YOJIMBO_DECLARE_MESSAGE_TYPE((int)MessageType::RigidbodySync, RigidbodyPosition);
                YOJIMBO_DECLARE_MESSAGE_TYPE((int)MessageType::PlayerIdentityAllocation, PlayerIdentityAllocation);
            default:
                for (ICustomMessageProvider* provider : messageProviders) {
                    yojimbo::Message* msg = provider->CreateMessage(type, allocator);
                    if (msg == nullptr)
                        continue;
                    SetMessageType(msg, type);
                    return msg;
                }
                return nullptr;
                break;
            }
        }
    };
}
}