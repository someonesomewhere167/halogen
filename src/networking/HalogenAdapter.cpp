#include "NetworkInternal.hpp"
#include "yojimbo.h"

namespace halogen {
namespace networking {
    HalogenServerAdapter::HalogenServerAdapter(HalogenServer* server)
        : server(server) {
    }

    yojimbo::MessageFactory* HalogenServerAdapter::CreateMessageFactory(yojimbo::Allocator& allocator) {
        return YOJIMBO_NEW(allocator, GameMessageFactory, allocator);
    }

    void HalogenServerAdapter::OnServerClientConnected(int clientIndex) {
        if (server != NULL) {
            server->ClientConnected(clientIndex);
        }
    }

    void HalogenServerAdapter::OnServerClientDisconnected(int clientIndex) {
        if (server != NULL) {
            server->ClientDisconnected(clientIndex);
        }
    }

    HalogenClientAdapter::HalogenClientAdapter(HalogenClient* client)
        : client(client) {
    }

    yojimbo::MessageFactory* HalogenClientAdapter::CreateMessageFactory(yojimbo::Allocator& allocator) {
        return YOJIMBO_NEW(allocator, GameMessageFactory, allocator);
    }
}
}