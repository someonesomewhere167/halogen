#pragma once
#include "yojimbo.h"
#include <entt/entt.hpp>
#include <functional>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <memory>
#include <string>
#include <unordered_map>

namespace halogen {
namespace networking {
#define SERIALIZE_FUNC() template <typename Stream> \
bool Serialize(Stream& stream);

#define SERIALIZE_IMPL(type) template <typename Stream> \
bool type::Serialize(Stream& stream)

    static const int MAX_PLAYERS = 64;
    enum class Channel : int {
        Reliable,
        Unreliable,
        Count
    };

    enum class MessageType : int {
        PlayerJoin,
        AdminCommand,
        RigidbodySync,
        PlayerIdentityAllocation,
        Count
    };

    enum class ClientState {
        NotConnected,
        Connecting,
        Connected,
        ConnectionFailed
    };

    enum class CallbackType {
        CreatePlayer
    };

    struct NetworkIdentity {
    public:
        uint32_t id;
        SERIALIZE_FUNC();
        bool operator==(NetworkIdentity other);
    };

    struct SyncedRigidbody {};

    struct PlayerData {
        std::string playerName;
        bool isDeveloper;
        NetworkIdentity identity;
        SERIALIZE_FUNC();
    };

    typedef std::function<void(PlayerData)> CreatePlayerCallback;

    class PlayerJoinMessage : public yojimbo::Message {
    public:
        PlayerData playerData;

        PlayerJoinMessage();

        SERIALIZE_FUNC();

        YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
    };

    class PlayerIdentityAllocation : public yojimbo::Message {
    public:
        NetworkIdentity identity;
        PlayerIdentityAllocation();
        SERIALIZE_FUNC();
        YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
    };

    class RigidbodyPosition : public yojimbo::Message {
    public:
        RigidbodyPosition();
        NetworkIdentity identity;
        glm::vec3 position;
        glm::quat rotation;
        glm::vec3 velocity;
        SERIALIZE_FUNC();
        YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
    };

    class AdminCommandMessage : public yojimbo::Message {
    public:
        std::string command;

        AdminCommandMessage();

        SERIALIZE_FUNC();

        YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
    };

    struct GameConnectionConfig : public yojimbo::ClientServerConfig {
        GameConnectionConfig()
            : ClientServerConfig() {
            numChannels = (int)Channel::Count;
            channel[(int)Channel::Reliable].type = yojimbo::CHANNEL_TYPE_RELIABLE_ORDERED;
            channel[(int)Channel::Unreliable].type = yojimbo::CHANNEL_TYPE_UNRELIABLE_UNORDERED;
        }
    };

    class ICustomMessageProvider {
    public:
        virtual int GetMessageTypeCount() = 0;
        virtual yojimbo::Message* CreateMessage(int type, yojimbo::Allocator& allocator) = 0;
        virtual bool HandleMessage(int type, yojimbo::Message* message) = 0;
    };

    class HalogenServer;

    class HalogenServerAdapter : public yojimbo::Adapter {
    public:
        explicit HalogenServerAdapter(HalogenServer* server = NULL);

        yojimbo::MessageFactory* CreateMessageFactory(yojimbo::Allocator& allocator) override;

        void OnServerClientConnected(int clientIndex) override;

        void OnServerClientDisconnected(int clientIndex) override;

    private:
        HalogenServer* server;
    };

    class HalogenServer {
    public:
        HalogenServer(const yojimbo::Address& address, std::vector<ICustomMessageProvider*> providers);
        void ClientConnected(int clientIndex);
        void ClientDisconnected(int clientIndex);
        void Update(float deltaTime);
        void SetCreatePlayerCallback(CreatePlayerCallback callback);
        void SendToAll(std::function<yojimbo::Message*(int)> messageCreator, Channel channel, int excludeClient = -1);
        uint32_t GetEntityFromIdentity(NetworkIdentity identity);
        yojimbo::Message* CreateMessage(int clientIndex, int type);

    private:
        GameConnectionConfig connectionConfig;
        HalogenServerAdapter adapter;
        yojimbo::Server server;
        CreatePlayerCallback createPlayerCallback;
        void ReceiveMessages();
        void HandleMessage(int clientIndex, yojimbo::Message* message);
        void ProcessAdminCommandMessage(int clientIndex, AdminCommandMessage* message);
        void ProcessPlayerJoinMessage(int clientIndex, PlayerJoinMessage* message);
        void OnNetworkIdentityCreate(entt::DefaultRegistry& registry, uint32_t entity);
        PlayerData playerData[MAX_PLAYERS];
        std::vector<ICustomMessageProvider*> msgProviders;
        std::unordered_map<uint32_t, uint32_t> identityEntities;
    };

    class HalogenClient;

    class HalogenClientAdapter : public yojimbo::Adapter {
    public:
        explicit HalogenClientAdapter(HalogenClient* client = nullptr);

        yojimbo::MessageFactory* CreateMessageFactory(yojimbo::Allocator& allocator) override;

    private:
        HalogenClient* client;
    };

    class HalogenClient {
    public:
        HalogenClient(std::vector<ICustomMessageProvider*> providers);
        void Update(float deltaTime);
        ClientState GetState();
        void Connect(const yojimbo::Address& address);
        void Disconnect();
        void SetCreatePlayerCallback(CreatePlayerCallback callback);
        void SetLocalCreatePlayerCallback(CreatePlayerCallback callback);
        void SetUsername(std::string username);
        void SendMessage(yojimbo::Message* message, Channel channel);
        yojimbo::Message* CreateMessage(int type);
        uint32_t GetEntityFromIdentity(NetworkIdentity identity);
        NetworkIdentity GetLocalIdentity();

    private:
        GameConnectionConfig connectionConfig;
        HalogenClientAdapter adapter;
        void HandleMessage(yojimbo::Message* message);
        void HandlePlayerJoin(PlayerJoinMessage* message);
        void HandleRigidbodyPosition(RigidbodyPosition* message);
        void HandlePlayerIdentityAllocation(PlayerIdentityAllocation* message);
        void OnNetworkIdentityCreate(entt::DefaultRegistry& registry, uint32_t entity);
        yojimbo::Client client;
        bool sentJoinPacket;
        CreatePlayerCallback createPlayerCallback;
        CreatePlayerCallback localCreatePlayerCallback;
        PlayerData localPlayerData;
        std::string username;
        std::unordered_map<uint32_t, uint32_t> identityEntities;
        std::vector<ICustomMessageProvider*> msgProviders;
    };

    void Init(bool isDedicatedServer, std::vector<ICustomMessageProvider*> providers);
    void Update(float deltaTime);
    void Shutdown();
    std::shared_ptr<HalogenClient> GetClient();
    std::shared_ptr<HalogenServer> GetServer();
    int GetTotalMessageTypeCount();
    NetworkIdentity AllocateNetIdentity();
}
}