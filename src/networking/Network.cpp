#include "Network.hpp"
#include "NetworkInternal.hpp"
#include <memory>
#include <stdexcept>

namespace halogen {
namespace networking {
    std::shared_ptr<HalogenClient> client;
    std::shared_ptr<HalogenServer> server;
    bool isDedicatedServer;
    uint32_t latestNetID = 0;
    int totalMessageTypeCount = (int)MessageType::Count;
    std::vector<ICustomMessageProvider*> messageProviders;

    void Init(bool isDedicatedServer, std::vector<ICustomMessageProvider*> providers) {
        messageProviders = providers;
        for (ICustomMessageProvider* provider : providers) {
            totalMessageTypeCount += provider->GetMessageTypeCount();
        }

        if (!InitializeYojimbo())
            throw std::runtime_error("Could not initialise Yojimbo!");

        yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);

        networking::isDedicatedServer = isDedicatedServer;

        if (isDedicatedServer) {
            server = std::make_shared<HalogenServer>(yojimbo::Address("127.0.0.1", 2032), providers);
        } else {
            client = std::make_shared<HalogenClient>(providers);
        }
    }

    void Update(float deltaTime) {
        if (isDedicatedServer)
            server->Update(deltaTime);
        else
            client->Update(deltaTime);
    }

    void Shutdown() {
        if (client) {
            client->Disconnect();
            client.reset();
        }
        if (server) {
            server.reset();
        }
        ShutdownYojimbo();
    }

    std::shared_ptr<HalogenClient> GetClient() {
        return client;
    }

    std::shared_ptr<HalogenServer> GetServer() {
        return server;
    }

    int GetTotalMessageTypeCount() {
        return totalMessageTypeCount;
    }

    SERIALIZE_IMPL(PlayerData) {
        int bufferSize = 255;
        char* tempBuffer = (char*)std::malloc(bufferSize);
        if (tempBuffer == 0)
            return false;
        memset(tempBuffer, 0, bufferSize);

        if (playerName.size() > 0)
            strcpy(tempBuffer, playerName.c_str());

        serialize_string(stream, tempBuffer, bufferSize);
        serialize_bool(stream, isDeveloper);
        serialize_object(stream, identity);
        // this function is called for serialization and deserialization, so set the name
        playerName = std::string(tempBuffer);
        std::free(tempBuffer);
        return true;
    }

    PlayerJoinMessage::PlayerJoinMessage()
        : playerData() {
    }

    SERIALIZE_IMPL(PlayerJoinMessage) {
        serialize_object(stream, playerData);
        return true;
    }

    AdminCommandMessage::AdminCommandMessage()
        : command("") {
    }

    SERIALIZE_IMPL(AdminCommandMessage) {
        serialize_string(stream, (char*)command.c_str(), command.size());
        return true;
    }

    template bool NetworkIdentity::Serialize<yojimbo::ReadStream>(yojimbo::ReadStream& stream);
    template bool NetworkIdentity::Serialize<yojimbo::WriteStream>(yojimbo::WriteStream& stream);
    template bool NetworkIdentity::Serialize<yojimbo::MeasureStream>(yojimbo::MeasureStream& stream);

    SERIALIZE_IMPL(NetworkIdentity) {
        serialize_uint32(stream, id);
        return true;
    }

    bool NetworkIdentity::operator==(NetworkIdentity other) {
        return other.id == id;
    }

    NetworkIdentity AllocateNetIdentity() {
        return { ++latestNetID };
    }

    RigidbodyPosition::RigidbodyPosition()
        : identity()
        , position()
        , rotation()
        , velocity() {
    }

    SERIALIZE_IMPL(RigidbodyPosition) {
        serialize_object(stream, identity);
        serialize_float(stream, position.x);
        serialize_float(stream, position.y);
        serialize_float(stream, position.z);

        serialize_float(stream, rotation.x);
        serialize_float(stream, rotation.y);
        serialize_float(stream, rotation.z);
        serialize_float(stream, rotation.w);

        serialize_float(stream, velocity.x);
        serialize_float(stream, velocity.y);
        serialize_float(stream, velocity.z);
        return true;
    }

    PlayerIdentityAllocation::PlayerIdentityAllocation()
        : identity() {
    }

    SERIALIZE_IMPL(PlayerIdentityAllocation) {
        serialize_object(stream, identity);
        return true;
    }
}
}