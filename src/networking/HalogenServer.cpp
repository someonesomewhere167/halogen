#include "NetworkInternal.hpp"
#include "yojimbo.h"
#include <core.h>
#include <iostream>
#include <physics.h>

namespace halogen {
namespace networking {
    HalogenServer::HalogenServer(const yojimbo::Address& address, std::vector<ICustomMessageProvider*> providers)
        : adapter(this)
        , connectionConfig()
        , server(yojimbo::GetDefaultAllocator(), DEFAULT_PRIVATE_KEY, address, connectionConfig, adapter, 0.0)
        , msgProviders(providers) {
        server.Start(MAX_PLAYERS);
        if (!server.IsRunning())
            throw std::runtime_error("Could not start server.");

        core::entityRegistry.construction<NetworkIdentity>().connect<HalogenServer, &HalogenServer::OnNetworkIdentityCreate>(this);
    }

    void HalogenServer::ClientConnected(int clientIndex) {
        std::cout << "Client connected with ID " << clientIndex << "\n";
    }

    void HalogenServer::ClientDisconnected(int clientIndex) {
        std::cout << "Client disconnected with ID " << clientIndex << "\n";
    }

    void HalogenServer::Update(float deltaTime) {
        if (!server.IsRunning())
            return;

        server.AdvanceTime(server.GetTime() + deltaTime);
        server.ReceivePackets();
        ReceiveMessages();
        core::entityRegistry.view<physics::Rigidbody, NetworkIdentity, SyncedRigidbody>().each([&](auto ent, physics::Rigidbody& rigidbody, auto identity, auto) {
            SendToAll([&](int clientIndex) -> yojimbo::Message* {
                RigidbodyPosition* msg = (RigidbodyPosition*)server.CreateMessage(clientIndex, (int)MessageType::RigidbodySync);
                msg->identity = identity;
                msg->position = rigidbody.getPosition();
                msg->rotation = rigidbody.getRotation();
                msg->velocity = physics::bt2glm(rigidbody.bulletHandle->getLinearVelocity());
                return msg;
            },
                Channel::Unreliable);
        });
        server.SendPackets();
    }

    void HalogenServer::SetCreatePlayerCallback(CreatePlayerCallback callback) {
        createPlayerCallback = callback;
    }

    void HalogenServer::SendToAll(std::function<yojimbo::Message*(int)> messageCreator, Channel channel, int excludeClient) {
        for (int i = 0; i < MAX_PLAYERS; i++) {
            if (server.IsClientConnected(i) && i != excludeClient) {
                yojimbo::Message* msg = messageCreator(i);
                server.SendMessage(i, (int)channel, msg);
            }
        }
    }

    uint32_t HalogenServer::GetEntityFromIdentity(NetworkIdentity identity) {
        return identityEntities[identity.id];
    }

    yojimbo::Message* HalogenServer::CreateMessage(int clientIndex, int type) {
        return server.CreateMessage(clientIndex, type);
    }

    void HalogenServer::ReceiveMessages() {
        for (int i = 0; i < MAX_PLAYERS; i++) {
            if (server.IsClientConnected(i)) {
                for (int j = 0; j < connectionConfig.numChannels; j++) {
                    yojimbo::Message* message = server.ReceiveMessage(i, j);

                    while (message != nullptr) {
                        HandleMessage(i, message);
                        server.ReleaseMessage(i, message);
                        message = server.ReceiveMessage(i, j);
                    }
                }
            }
        }
    }

    void HalogenServer::HandleMessage(int clientIndex, yojimbo::Message* message) {
        switch ((MessageType)message->GetType()) {
        case MessageType::PlayerJoin:
            ProcessPlayerJoinMessage(clientIndex, (PlayerJoinMessage*)message);
            break;
        case MessageType::AdminCommand:
            ProcessAdminCommandMessage(clientIndex, (AdminCommandMessage*)message);
            break;
        default:
            for (ICustomMessageProvider* provider : messageProviders) {
                if (provider->HandleMessage(message->GetType(), message))
                    return;
            }
            break;
        };
    }

    void HalogenServer::ProcessAdminCommandMessage(int clientIndex, AdminCommandMessage* message) {
        PlayerData data = playerData[clientIndex];
        if (!data.isDeveloper) {
            std::cout << "Warning: Player " << data.playerName << " tried to send an admin command, but they aren't a developer."
                      << "\n";
            return;
        }

        std::cout << data.playerName << ": " << message->command << "\n";
    }

    void HalogenServer::ProcessPlayerJoinMessage(int clientIndex, PlayerJoinMessage* message) {
        std::cout << "Received a join message from " << message->playerData.playerName << "\n";

        message->playerData.identity = AllocateNetIdentity();
        playerData[clientIndex] = message->playerData;

        PlayerIdentityAllocation* identityMessage = (PlayerIdentityAllocation*)CreateMessage(clientIndex, (int)MessageType::PlayerIdentityAllocation);
        identityMessage->identity = message->playerData.identity;
        server.SendMessage(clientIndex, (int)Channel::Reliable, identityMessage);

        createPlayerCallback(message->playerData);

        SendToAll([&](int clientIndex) -> yojimbo::Message* {
            PlayerJoinMessage* clientMessage = (PlayerJoinMessage*)CreateMessage(clientIndex, (int)MessageType::PlayerJoin);
            clientMessage->playerData = message->playerData;
            return clientMessage;
        },
            Channel::Reliable, clientIndex);

        for (int i = 0; i < MAX_PLAYERS; i++) {
            if (i == clientIndex)
                continue;
            if (!server.IsClientConnected(i))
                continue;
            PlayerJoinMessage* clientMessage = (PlayerJoinMessage*)CreateMessage(clientIndex, (int)MessageType::PlayerJoin);
            clientMessage->playerData = playerData[i];
            server.SendMessage(clientIndex, (int)Channel::Reliable, clientMessage);
        }
    }

    void HalogenServer::OnNetworkIdentityCreate(entt::DefaultRegistry& registry, uint32_t entity) {
        identityEntities[registry.get<NetworkIdentity>(entity).id] = entity;
    }
}
}