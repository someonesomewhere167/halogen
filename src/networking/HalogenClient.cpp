#include "NetworkInternal.hpp"
#include <core.h>
#include <imgui.h>
#include <iostream>
#include <physics.h>

namespace halogen {
namespace networking {
    extern uint32_t latestNetID;
    HalogenClient::HalogenClient(std::vector<ICustomMessageProvider*> providers)
        : connectionConfig()
        , adapter()
        , client(yojimbo::GetDefaultAllocator(), yojimbo::Address("0.0.0.0"), connectionConfig, adapter, 0.0)
        , sentJoinPacket(false)
        , username("Error")
        , localPlayerData()
        , msgProviders(providers) {
        core::entityRegistry.construction<NetworkIdentity>().connect<HalogenClient, &HalogenClient::OnNetworkIdentityCreate>(this);
        localPlayerData.isDeveloper = true; // TODO: Remove this!
    }

    void HalogenClient::Update(float deltaTime) {
        if (client.IsConnected() || client.IsConnecting())
            client.AdvanceTime(client.GetTime() + deltaTime);

        if (client.IsConnected()) {
            client.ReceivePackets();
            if (!sentJoinPacket) {
                PlayerJoinMessage* joinMessage = (PlayerJoinMessage*)client.CreateMessage((int)MessageType::PlayerJoin);
                joinMessage->playerData = localPlayerData;
                client.SendMessage((int)Channel::Reliable, joinMessage);
                sentJoinPacket = true;
            }

            for (int i = 0; i < (int)Channel::Count; i++) {
                yojimbo::Message* message = client.ReceiveMessage(i);

                while (message != nullptr) {
                    HandleMessage(message);
                    client.ReleaseMessage(message);
                    message = client.ReceiveMessage(i);
                }
            }
        }

        client.SendPackets();
    }

    ClientState HalogenClient::GetState() {
        if (client.ConnectionFailed())
            return ClientState::ConnectionFailed;
        else if (client.IsConnected())
            return ClientState::Connected;
        else if (client.IsConnecting())
            return ClientState::Connecting;
        else
            return ClientState::NotConnected;
    }

    void HalogenClient::Connect(const yojimbo::Address& address) {
        uint64_t clientId;
        yojimbo::random_bytes((uint8_t*)&clientId, sizeof(uint64_t));
        client.InsecureConnect(DEFAULT_PRIVATE_KEY, clientId, address);
    }

    void HalogenClient::Disconnect() {
        client.Disconnect();
    }

    void HalogenClient::SetCreatePlayerCallback(CreatePlayerCallback callback) {
        createPlayerCallback = callback;
    }

    void HalogenClient::SetLocalCreatePlayerCallback(CreatePlayerCallback callback) {
        localCreatePlayerCallback = callback;
    }

    void HalogenClient::SetUsername(std::string username) {
        username = username;
        localPlayerData.playerName = username;
    }

    void HalogenClient::SendMessage(yojimbo::Message* message, Channel channel) {
        client.SendMessage((int)channel, message);
    }

    yojimbo::Message* HalogenClient::CreateMessage(int type) {
        return client.CreateMessage(type);
    }

    uint32_t HalogenClient::GetEntityFromIdentity(NetworkIdentity identity) {
        return identityEntities[identity.id];
    }

    NetworkIdentity HalogenClient::GetLocalIdentity() {
        return localPlayerData.identity;
    }

    void HalogenClient::HandleMessage(yojimbo::Message* message) {
        switch ((MessageType)message->GetType()) {
        case MessageType::PlayerJoin:
            HandlePlayerJoin((PlayerJoinMessage*)message);
            break;
        case MessageType::RigidbodySync:
            HandleRigidbodyPosition((RigidbodyPosition*)message);
            break;
        case MessageType::PlayerIdentityAllocation:
            HandlePlayerIdentityAllocation((PlayerIdentityAllocation*)message);
            break;
        default:
            for (ICustomMessageProvider* provider : messageProviders) {
                if (provider->HandleMessage(message->GetType(), message))
                    return;
            }
            break;
        }
    }

    void HalogenClient::HandleRigidbodyPosition(RigidbodyPosition* message) {
        uint32_t entity = identityEntities[message->identity.id];
        physics::Rigidbody& rigidbody = core::entityRegistry.get<physics::Rigidbody>(entity);
        components::Transform& transform = core::entityRegistry.get<components::Transform>(entity);
        transform.position = message->position;
        transform.rotation = message->rotation;
        rigidbody.setPosition(message->position);
        rigidbody.setRotation(message->rotation);
        rigidbody.bulletHandle->setLinearVelocity(physics::glm2bt(message->velocity));
    }

    void HalogenClient::HandlePlayerJoin(PlayerJoinMessage* message) {
        std::cout << "Player " << message->playerData.playerName << " has joined with ID " << message->playerData.identity.id << "They are" << (message->playerData.isDeveloper ? " a " : " not a ") << "developer."
                  << "\n";
        createPlayerCallback(message->playerData);
        latestNetID = message->playerData.identity.id;
    }

    void HalogenClient::HandlePlayerIdentityAllocation(PlayerIdentityAllocation* message) {
        localPlayerData.identity = message->identity;
        localCreatePlayerCallback(localPlayerData);
    }

    void HalogenClient::OnNetworkIdentityCreate(entt::DefaultRegistry& registry, uint32_t entity) {
        identityEntities[registry.get<NetworkIdentity>(entity).id] = entity;
    }
}
}