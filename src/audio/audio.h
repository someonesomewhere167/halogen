#pragma once
#include "dr_mp3.h"
#include "transform.h"
#include "vorbis.h"
#include <AL/al.h>
#include <AL/alc.h>
#include <entt/entt.hpp>
#include <glm/glm.hpp>
#include <physfs.h>
#include <string>

namespace halogen
{
namespace audio
{
    struct Listener
    {
        Listener();
        float volume;
    };

    enum class AudioFileFormat
    {
		MP3,
		Ogg
	};

    class Clip
    {
    public:
        Clip(std::string path, bool streaming);
        ~Clip();
        const ALuint* getBuffer() const;
        bool isStreaming() const;
        void streamToBuffer(int index);
        bool isStreamingComplete() const;
        void recreate();

    private:
        stb_vorbis_info info;
        uint32_t sampleCount;
        int16_t* data;
        ALuint buffers[2];
        stb_vorbis* vorb;
        drmp3 mp3;
        bool streaming;
        bool streamingComplete;
        int streamSampleCount;
        AudioFileFormat fileFormat;
    };

    class Source
    {
    public:
        Source();
        void setClip(Clip* clip);
        Clip* getClip() const;
        void setGain(float gain);
        float getGain() const;
        void play();
        void playOverlapped();
        void setPosition(glm::vec3 pos);
        void setRotation(glm::quat rot);
        bool isPlaying();
        void set2d(bool is2d);
        void setLooping(bool loop);
        void updateStream();
        ALuint handle;

    private:
        float gain;
        Clip* clip;
        bool is2d;
        int currentBufferIdx;
        friend void update(entt::DefaultRegistry& registry);
    };

    void init(entt::DefaultRegistry& registry);
    void update(entt::DefaultRegistry& registry);
    void shutdown();
    void setListenerValues(Listener* listener, components::Transform* transform);
}
}