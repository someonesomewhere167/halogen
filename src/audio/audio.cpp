#include "audio.h"
#include "AL/alext.h"
#include "transform.h"
#include "vorbis.h"
#include <entt/entt.hpp>
#include <iostream> // for std::cerr
#include <mutex>
#include <stdexcept>
#include <thread>
#include <transform.h>
#include <unordered_map>
#include <vector>
#include <Log.hpp>

static LPALCGETSTRINGISOFT alcGetStringiSOFT;
static LPALCRESETDEVICESOFT alcResetDeviceSOFT;

namespace halogen {
namespace audio {
    ALCdevice* device;
    ALCcontext* context;
    bool supportsHRTF = false;
    std::vector<Source*> streamingSources;
    std::mutex streamingSourcesMutex;
    std::thread streamingThreadRef;
    bool runStreamingThread = true;

    void checkError() {
        ALCenum error;

        error = alGetError();

        if (error != AL_NO_ERROR) {
            const char* alErrorString = alGetString(error);
            if (alErrorString != 0) {
                throw std::runtime_error("OpenAL error " + std::string(alErrorString));
            } else {
                throw std::runtime_error("OpenAL error " + std::to_string(error));
            }
        }
    }

    bool initialiseHRTF() {
/* Define a macro to help load the function pointers. */
#define LOAD_PROC(d, x, t) (x) = (t)alcGetProcAddress((d), #x)
        LOAD_PROC(device, alcGetStringiSOFT, LPALCGETSTRINGISOFT);
        LOAD_PROC(device, alcResetDeviceSOFT, LPALCRESETDEVICESOFT);
#undef LOAD_PROC

        ALint hrtfCount;
        alcGetIntegerv(device, ALC_NUM_HRTF_SPECIFIERS_SOFT, 1, &hrtfCount);

        if (hrtfCount == 0) {
            util::Log::LogString("No HRTFs found.", util::LogCategory::Audio);
            return false;
        }

        ALCint attr[3];

        attr[0] = ALC_HRTF_SOFT;
        attr[1] = ALC_FALSE;
        attr[2] = 0;

        if (!alcResetDeviceSOFT(device, attr)) {
            util::Log::LogString(std::string("Failed to reset device: ") + alcGetString(device, alcGetError(device)), util::LogCategory::Audio);
            return false;
        }

        return true;
    }

    void createSource(entt::DefaultRegistry& registry, uint32_t entity) {
        Source& source = registry.get<Source>(entity);
        alGenSources(1, &source.handle);
    }

    void destroySource(entt::DefaultRegistry& registry, uint32_t entity) {
        Source& source = registry.get<Source>(entity);
        if (source.getClip()->isStreaming())
            streamingSources.erase(std::find(streamingSources.begin(), streamingSources.end(), &source));
        alDeleteSources(1, &source.handle);
    }

    void streamingThread() {
        while (runStreamingThread) {
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            std::lock_guard<std::mutex> lg(streamingSourcesMutex);
            for (Source* source : streamingSources) {
                source->updateStream();
            }
        }
    }

    void init(entt::DefaultRegistry& registry) {
        device = alcOpenDevice(nullptr);

        if (!device)
            throw std::runtime_error("Could not open OpenAL device.");

        context = alcCreateContext(device, nullptr);

        int frequency;
        alcGetIntegerv(device, ALC_FREQUENCY, 1, &frequency);

        util::Log::LogString(std::string("OpenAL device has a frequency of ") + std::to_string(frequency), util::LogCategory::Audio);

        if (frequency != 44100)
            util::Log::LogString("In an ideal world, this would be 44100hz but it isn't. Fix it!", util::LogCategory::Audio);

        if (!alcMakeContextCurrent(context))
            checkError();

        supportsHRTF = alcIsExtensionPresent(device, "ALC_SOFT_HRTF");

        if (!supportsHRTF)
            std::cerr << "Error: ALC_SOFT_HRTF is unsupported. HRTF will be disabled.\n";
        //else if (!initialiseHRTF())
        //    std::cerr << "Error initialising HRTF. HRTF will be disabled.\n";

        registry.construction<Source>().connect<&createSource>();
        registry.destruction<Source>().connect<&destroySource>();
        if (!alcIsExtensionPresent(device, "ALC_EXT_disconnect"))
            util::Log::LogString("Device does not support the disconnect extension. Audio may cut out when an audio device is disconnected.", util::LogCategory::Audio);
        streamingThreadRef = std::thread(streamingThread);
    }

    void update(entt::DefaultRegistry& registry) {
        int connected;
        alcGetIntegerv(device, ALC_CONNECTED, 1, &connected);
        if (!connected) {
            util::Log::LogString("Device disconnected", util::LogCategory::Audio);
            alcDestroyContext(context);
            alcCloseDevice(device);
            device = alcOpenDevice(nullptr);
            context = alcCreateContext(device, nullptr);
            alcMakeContextCurrent(context);
            registry.view<Source>().each([=](const auto ent, Source& source) {
                alGenSources(1, &source.handle);
                if (source.clip)
                    source.clip->recreate();
                checkError();
            });
        }
        registry.view<Listener, components::Transform>().each([=](const auto ent, Listener& listener, components::Transform& transform) {
            setListenerValues(&listener, &transform);
        });

        registry.view<Source, components::Transform>().each([=](const auto ent, Source& source, components::Transform& transform) {
            source.setPosition(transform.position);
        });

        registry.view<Source>().each([=](const auto ent, Source& source) {
            std::vector<Source*>::iterator pos = std::find(streamingSources.begin(), streamingSources.end(), &source);
            if (source.clip && source.clip->isStreaming() && (pos != streamingSources.end() || streamingSources.size() == 0)) {
                std::lock_guard<std::mutex> lg(streamingSourcesMutex);
                streamingSources.push_back(&source);
            }
        });
        checkError();
    }

    void setListenerValues(Listener* listener, components::Transform* transform) {
        glm::vec3 pos = transform->position;
        alListener3f(AL_POSITION, pos.x, pos.y, pos.z);
        alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);

        glm::vec3 forward = transform->transformDirection(glm::vec3(0.0f, 0.0f, -1.0f));
        glm::vec3 up = transform->transformDirection(glm::vec3(0.0f, 1.0f, 0.0f));
        ALfloat listenerOrientation[6];

        listenerOrientation[0] = forward.x;
        listenerOrientation[1] = forward.y;
        listenerOrientation[2] = forward.z;
        listenerOrientation[3] = up.x;
        listenerOrientation[4] = up.y;
        listenerOrientation[5] = up.z;

        alListenerfv(AL_ORIENTATION, listenerOrientation);
    }

    void shutdown() {
        // Unfortunately, we can't just terminate the thread
        // Set the parameter and then wait for it to finish
        runStreamingThread = false;
        streamingThreadRef.join();
        alcMakeContextCurrent(nullptr);
        alcDestroyContext(context);
        alcCloseDevice(device);
    }
}

}