#include "audio.h"
#include "dr_mp3.h"
#include "vorbis.h"
#include <SettingsManager.hpp>
#include <hgio.h>
#include <iostream>
#include <stdexcept>

namespace halogen {
namespace audio {
    void checkError();

    Clip::Clip(std::string path, bool streaming)
        : sampleCount(0)
        , streaming(streaming)
        , streamingComplete(false) {
        streamSampleCount = (int)util::SettingsManager::GetOption("audio.streamSampleCount")->as<int64_t>()->get();
        std::string fileExt = path.substr(path.length() - 3);
        if (fileExt == "mp3")
            fileFormat = AudioFileFormat::MP3;
        else if (fileExt == "ogg")
            fileFormat = AudioFileFormat::Ogg;
        else
            throw std::runtime_error("Unknown audio file format " + fileExt);

        if (fileFormat == AudioFileFormat::MP3 && !streaming)
            throw std::runtime_error("Cannot open a non-streaming MP3!");
        unsigned long len;
        if (!streaming) {
            void* audioBuf = util::loadFileToBuffer("sound/" + path, len);
            sampleCount = stb_vorbis_decode_memory((const unsigned char*)audioBuf, len, &info.channels, (int*)&info.sample_rate, &data);
            std::free(audioBuf);
        } else {
            int error = 0;
            switch (fileFormat) {
            case AudioFileFormat::MP3:
                break;
            case AudioFileFormat::Ogg:
                vorb = stb_vorbis_open_filename(("sound/" + path).c_str(), &error, nullptr);
                if (error != VORBIS__no_error) {
                    throw std::runtime_error("Failed to open " + path);
                }
                info = stb_vorbis_get_info(vorb);
                break;
            default:
                break;
            }
        }

        if (sampleCount == (uint32_t)-1)
            throw std::runtime_error("Failed to open audio file " + path);

        alGenBuffers(streaming ? 2 : 1, buffers);
        checkError();

        ALenum format;
        if (info.channels == 2)
            format = AL_FORMAT_STEREO16;
        else
            format = AL_FORMAT_MONO16;

        if (streaming) {
            data = (int16_t*)malloc(streamSampleCount * sizeof(int16_t));
            streamToBuffer(0);
            streamToBuffer(1);
        } else
            alBufferData(buffers[0], format, data, info.channels * sampleCount * sizeof(short), info.sample_rate);
        checkError();
        std::cout << "Loaded " << path << " with a sample count of " << sampleCount << ", " << info.channels << " channels and a sample rate of " << info.sample_rate << "hz (" << (streaming ? "" : "not ") << "streaming).\n";
    }

    const ALuint* Clip::getBuffer() const {
        return buffers;
    }

    bool Clip::isStreaming() const {
        return streaming;
    }

    bool Clip::isStreamingComplete() const {
        return streamingComplete;
    }

    void Clip::streamToBuffer(int index) {
        int retrieved = stb_vorbis_get_samples_short_interleaved(vorb, info.channels, data, streamSampleCount);
        alBufferData(buffers[index], info.channels == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16, data, info.channels * retrieved * sizeof(short), info.sample_rate);
        int error = stb_vorbis_get_error(vorb);
        if (retrieved == 0) {
            std::cout << "Finished playing clip\n";
            streamingComplete = true;
        }

        if (error != VORBIS__no_error)
            std::cerr << "Vorbis error: " << error << "\n";
    }

    void Clip::recreate() {
        alGenBuffers(streaming ? 2 : 1, buffers);
        if (!streaming)
            alBufferData(buffers[0], info.channels == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16, data, info.channels * sampleCount * sizeof(short), info.sample_rate);
        else {
            streamToBuffer(0);
            streamToBuffer(1);
        }
        checkError();
    }

    Clip::~Clip() {
        alDeleteBuffers(1, buffers);
        std::free(data);
    }
}
}