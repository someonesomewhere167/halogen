#include "audio.h"
#include <iostream>
#include <stdexcept>

namespace halogen {
namespace audio {
    void checkError();

    Source::Source()
        : gain(0.0f)
        , clip(nullptr)
        , is2d(false)
        , currentBufferIdx(1) {
    }

    void Source::setClip(Clip* clip) {
        if (isPlaying() && clip != this->clip)
            throw std::runtime_error("Cannot change the audio clip while the source is playing!");
        else if (isPlaying())
            return;
        int queuedBuffers = 0;
        alGetSourcei(handle, AL_BUFFERS_QUEUED, &queuedBuffers);
        if (this->clip != nullptr && alIsBuffer(this->clip->getBuffer()[0]) && queuedBuffers > 0)
            alSourceUnqueueBuffers(handle, 1, (ALuint*)this->clip->getBuffer());
        checkError();

        this->clip = clip;
        if (clip->isStreaming())
            alSourceQueueBuffers(handle, 2, clip->getBuffer());
        else
            alSourceQueueBuffers(handle, 1, clip->getBuffer());
        checkError();
    }

    Clip* Source::getClip() const {
        return clip;
    }

    void Source::setGain(float gain) {
        alSourcef(handle, AL_GAIN, gain);
        this->gain = gain;
    }

    float Source::getGain() const {
        return gain;
    }

    void Source::play() {
        if (!isPlaying()) {
            alSourcePlay(handle);
            checkError();
        }
    }

    void Source::playOverlapped() {
        alSourcePlay(handle);
        checkError();
    }

    void Source::setPosition(glm::vec3 pos) {
        assert(alIsSource(handle));
        alSource3f(handle, AL_POSITION, pos.x, pos.y, pos.z);
        checkError();
    }

    void Source::setRotation(glm::quat rot) {
        glm::vec3 forward = rot * glm::vec3(0.0f, 0.0f, 1.0f);
        glm::vec3 up = rot * glm::vec3(0.0f, 1.0f, 0.0f);
        ALfloat listenerOrientation[6];

        listenerOrientation[0] = forward.x;
        listenerOrientation[1] = forward.y;
        listenerOrientation[2] = forward.z;
        listenerOrientation[3] = up.x;
        listenerOrientation[4] = up.y;
        listenerOrientation[5] = up.z;

        alSourcefv(handle, AL_ORIENTATION, listenerOrientation);
        checkError();
    }

    bool Source::isPlaying() {
        assert(alIsSource(handle));
        ALint sourceState;
        alGetSourcei(handle, AL_SOURCE_STATE, &sourceState);
        checkError();
        return sourceState == AL_PLAYING;
    }

    void Source::set2d(bool is2d) {
        this->is2d = is2d;
        if (is2d) {
            alDistanceModel(AL_NONE);
        } else {
            alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
        }
    }

    void Source::setLooping(bool loop) {
        alSourcei(handle, AL_LOOPING, loop);
    }

    void Source::updateStream() {
        if (!isPlaying() && !clip->isStreamingComplete())
            play();
        else if (!isPlaying())
            return;
        int buffersProcessed;
        alGetSourcei(handle, AL_BUFFERS_PROCESSED, &buffersProcessed);
        checkError();

        while (buffersProcessed >= 1) {
            checkError();

            int queuedBuffers = 0;
            alGetSourcei(handle, AL_BUFFERS_QUEUED, &queuedBuffers);
            unsigned int currBuf = clip->getBuffer()[currentBufferIdx];

            if (queuedBuffers > 1) {
                assert(alIsBuffer(clip->getBuffer()[currentBufferIdx]));
                alSourceUnqueueBuffers(handle, 1, &currBuf);
                checkError();
            }

            if (currentBufferIdx == 1)
                currentBufferIdx = 0;
            else
                currentBufferIdx = 1;
            clip->streamToBuffer(currentBufferIdx);
            currBuf = clip->getBuffer()[currentBufferIdx];
            alSourceQueueBuffers(handle, 1, &currBuf);
            buffersProcessed--;
            checkError();
        }
    }
}
}