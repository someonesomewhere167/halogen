#include "audio.h"
#include "core.h"
#include "game_events.h"
#include "input.h"
#include "render.h"

extern "C" void startHalogen(int argc, char** argv) {
    halogen::core::init(argc, argv);

    init();

    halogen::core::beginLoop(gameTick);

    shutdown();
    halogen::core::shutdown();
}