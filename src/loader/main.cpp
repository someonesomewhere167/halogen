#include "audio.h"
#include "core.h"
#include "game_events.h"
#include "input.h"
#include "render.h"
#include <Editor.hpp>
#include <chrono>
#include <iostream>

#if NDEBUG
#define NDEBUGTRY try
#else
#define NDEBUGTRY
#endif

std::unique_ptr<halogen::Editor> editor;
bool runEditor = false;
typedef std::chrono::high_resolution_clock Clock;

void tickWrapper(float deltaTime) {
    gameTick(deltaTime);
    if (runEditor)
        editor->Update();
}

int main(int argc, char** argv) {
    NDEBUGTRY {
        auto t1 = Clock::now();
        halogen::core::init(argc, argv);
        runEditor = argc > 1 && strcmp(argv[1], "--editor") == 0;

        if (runEditor)
            editor = std::make_unique<halogen::Editor>();

        init();

        auto t2 = Clock::now();

        std::cout << "Engine + game init took " << (t2 - t1).count() / 1000 / 1000 << " milliseconds."
                  << "\n";
    }
#if NDEBUG
    catch (std::exception e) {
        std::cerr << "Error initialising engine: " << e.what() << "\n";
        return -1;
    }
#endif

    NDEBUGTRY {
        halogen::core::beginLoop(tickWrapper);
    }
#if NDEBUG
    catch (std::exception e) {
        std::cerr << "Error running game: " << e.what() << "\n";
        shutdown();
        halogen::core::shutdown();
        return -1;
    }
#endif

    shutdown();
    halogen::core::shutdown();
    return 0;
}