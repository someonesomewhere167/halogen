#pragma once
#include "entt/entt.hpp"

namespace halogen {
class Hierarchy {
public:
    Hierarchy(entt::DefaultRegistry& registry);
    void Draw();
    void Toggle();
    void RegisterComponent(entt::DefaultRegistry::component_type type);

private:
    entt::DefaultRegistry& registry;
    bool show;
    std::vector<entt::DefaultRegistry::component_type> componentTypes;
};
}