#pragma once
#include <functional>
#include <map>
#include <string>

namespace halogen {
class MenuBar {
public:
    void AddMenuItem(std::string menu, std::string item, std::function<void(void)> onClick);
    void RemoveMenuItem(std::string menu, std::string item);
    void Draw();

private:
    struct MenuItem {
        std::string item;
        std::function<void()> onClick;
    };
    std::map<std::string, std::vector<MenuItem>> menuItems;
};
}
