#include "ModelList.hpp"
#include <core.h>
#include <imgui.h>
#include <iostream>
#include <meshloader.h>
#include <physfs.h>
#include <physics.h>
#include <render.h>
#include <transform.h>

namespace halogen {
std::vector<std::string> splitString(std::string s, std::string delimiter) {
    std::vector<std::string> result;
    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        result.push_back(token);
        s.erase(0, pos + delimiter.length());
    }

    result.push_back(s);

    return result;
}

void ModelList::Draw() {
    ImGui::Begin("Model List");

    char** files = PHYSFS_enumerateFiles("mesh");
    char** i;

    PHYSFS_ErrorCode err = PHYSFS_getLastErrorCode();

    if (err != PHYSFS_ERR_OK) {
        std::cerr << "Failed to enumerate mesh folder!"
                  << "\n";
    }

    for (i = files; *i != NULL; i++) {
        if (ImGui::Button(*i)) {
            std::string meshName(*i);

            /*auto entity = core::entityRegistry.create();
				core::entityRegistry.assign<halogen::components::Transform>(entity);
				std::shared_ptr<physics::BoxColliderParameters> p = std::make_shared<physics::BoxColliderParameters>();
				p->halfExtents = glm::vec3(2.0f, 2.0f, 2.0f);
				std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Box, p);

				core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 1.0f, entity).setPosition(glm::vec3(5.0f, 20.0f, 5.0f));

				std::shared_ptr<render::Mesh> mesh = render::loadMesh(splitString(meshName,".")[0]);
				render::Material* mat = new render::Material();

				mat->color = glm::vec3(1.0f, 1.0f, 1.0f);
				mat->specularPower = 32;
				mat->specularStrength = 0.1f;

				halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);*/
        }
    }

    ImGui::End();
}
}