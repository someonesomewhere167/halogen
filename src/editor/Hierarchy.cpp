#include "Hierarchy.hpp"
#include "imgui_stdlib.h"
#include <NameComponent.hpp>
#include <entt/entt.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>
#include <iostream>
#include <transform.h>

namespace halogen {

Hierarchy::Hierarchy(entt::DefaultRegistry& registry)
    : registry(registry)
    , show(true) {}

void Hierarchy::Draw() {
    if (!show)
        return;

    //auto view = registry.view<components::Transform>();
    bool open;
    ImGui::Begin("Hierarchy", &open);
    /*for (auto entity : view)
		{
			if (!registry.has<components::NameComponent>(entity))
			{
				registry.assign<components::NameComponent>(entity).name = "Entity " + std::to_string(entity);
			}
			components::NameComponent& nameComponent = registry.get<components::NameComponent>(entity);
			if (ImGui::TreeNode(nameComponent.name.c_str()))
			{
				components::Transform& transform = registry.get<components::Transform>(entity);
				ImGui::DragFloat3("Position:", glm::value_ptr(transform.position));
				ImGui::DragFloat4("Rotation:", glm::value_ptr(transform.rotation));
				ImGui::DragFloat3("Scale:", glm::value_ptr(transform.scale));
				
				ImGui::InputText("Name:", &nameComponent.name);

				ImGui::TreePop();
			}
		}*/

    auto view = registry.view(std::cbegin(componentTypes), std::cend(componentTypes));

    ImGui::End();

    if (!open)
        show = false;
}

void Hierarchy::Toggle() {
    show = !show;
}

void Hierarchy::RegisterComponent(entt::DefaultRegistry::component_type type) {
    componentTypes.push_back(type);
}
}