#include "Editor.hpp"
#include <core.h>
#include <input.h>

namespace halogen {
Editor::Editor()
    : menuBar()
    , hierarchy(core::entityRegistry)
    , modelList() {
    menuBar.AddMenuItem("File", "Exit", std::bind(&Editor::Exit, this));
    menuBar.AddMenuItem("View", "Show Hierarchy", [this]() { this->hierarchy.Toggle(); });
}

void Editor::Update() {
    menuBar.Draw();
    hierarchy.Draw();
    if (input::getActionDown("misc")) {
        hierarchy.Toggle();
    }
    modelList.Draw();
}

void Editor::Exit() {
    core::exit();
}
}