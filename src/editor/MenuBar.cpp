#include "MenuBar.hpp"
#include <imgui.h>

namespace halogen {
void MenuBar::AddMenuItem(std::string menu, std::string item, std::function<void(void)> onClick) {
    MenuItem menuItem;
    menuItem.item = item;
    menuItem.onClick = onClick;

    // We don't have to initialise the vector (in theory)
    menuItems[menu].push_back(menuItem);
}

void MenuBar::RemoveMenuItem(std::string menu, std::string item) {
    std::vector<MenuItem>& items = menuItems[menu];
    for (int i = 0; i < items.size(); i++) {
        if (items[i].item == item) {
            items.erase(items.begin() + i);
            break;
        }
    }
}

void MenuBar::Draw() {
    if (menuItems.size() > 0 && ImGui::BeginMainMenuBar()) {
        for (const auto& itemPair : menuItems) {
            if (ImGui::BeginMenu(itemPair.first.c_str())) {
                for (const auto& item : itemPair.second) {
                    if (ImGui::MenuItem(item.item.c_str()))
                        item.onClick();
                }
                ImGui::EndMenu();
            }
        }
        ImGui::EndMainMenuBar();
    }
}
}