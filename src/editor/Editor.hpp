#pragma once
#include "Hierarchy.hpp"
#include "MenuBar.hpp"
#include "ModelList.hpp"

namespace halogen {
class Editor {
public:
    Editor();
    void Update();
    void Exit();

private:
    MenuBar menuBar;
    Hierarchy hierarchy;
    ModelList modelList;
};
}