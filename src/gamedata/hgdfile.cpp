// On very small chance that someone in the future is trying to port this
// to a platform that isn't little endian, I am very, very sorry.
// I don't know how it happened, but for some reason the byte order
// seems to be reversed in the file, meaning that trying to read/write
// HGD data without using structs is broken.
// Abandon all hope ye who enter here.
#include "hgdfile.h"
#include <cstring>
#include <physfs.h>
#include <stdexcept>

namespace halogen {
namespace gamedata {
    struct HGDHeader {
        char magic[3];
        uint16_t formatVersion;
        uint16_t groupCount;
    };

    struct GroupHeader {
        uint16_t groupId;
        uint16_t memberCount;
        uint16_t memberLength;
    };

    const uint16_t CURRENT_FORMAT_VERSION = 1;
    void checkedRead(PHYSFS_File* handle, void* buffer, PHYSFS_uint64 len) {
        if (PHYSFS_readBytes(handle, buffer, len) != len)
            throw std::runtime_error("Unexpected EOF while reading HGD.");
    }

    bool checkMagic(char* magic) {
        return magic[0] == 'H' && magic[1] == 'G' && magic[2] == 'D';
    }

    HGDFile::HGDFile(std::string path) {
        PHYSFS_File* file = PHYSFS_openRead(path.c_str());

        PHYSFS_ErrorCode errCode = PHYSFS_getLastErrorCode();
        if (errCode != PHYSFS_ERR_OK) {
            throw std::runtime_error("Couldn't open " + path + ". Error was " + PHYSFS_getErrorByCode(errCode));
        }

        HGDHeader header;
        checkedRead(file, &header.magic, sizeof(char) * 3);
        checkedRead(file, &header.formatVersion, sizeof(uint16_t));
        checkedRead(file, &header.groupCount, sizeof(uint16_t));

        if (!checkMagic(header.magic))
            throw std::runtime_error("Invalid magic value for " + path);

        if (header.formatVersion != CURRENT_FORMAT_VERSION)
            throw std::runtime_error("Invalid format version " + std::to_string(header.formatVersion));

        groupCount = header.groupCount;

        for (uint16_t i = 0; i < groupCount; i++) {
            GroupHeader groupHeader;
            checkedRead(file, &groupHeader.groupId, sizeof(uint16_t));
            checkedRead(file, &groupHeader.memberCount, sizeof(uint16_t));
            checkedRead(file, &groupHeader.memberLength, sizeof(uint16_t));

            uint32_t totalMemberLength = groupHeader.memberCount * groupHeader.memberLength;
            void* memberData = std::malloc(totalMemberLength);
            PHYSFS_readBytes(file, memberData, totalMemberLength);

            HGDGroup group(groupHeader.groupId, groupHeader.memberCount, groupHeader.memberLength, (std::byte*)memberData);
            groups.insert({ groupHeader.groupId, group });
        }
    }

    HGDGroup* HGDFile::getGroup(uint16_t id) {
        return &groups.at(id);
    }
}
}