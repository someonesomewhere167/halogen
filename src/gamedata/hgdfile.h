#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include "hgdgroups.h"

namespace halogen
{
    namespace gamedata
    {
        class HGDFile
        {
            public:
            HGDFile(std::string path);
            HGDGroup* getGroup(uint16_t id);
            private:
            uint16_t groupCount;
            std::unordered_map<uint16_t, HGDGroup> groups;
        };
    }
}