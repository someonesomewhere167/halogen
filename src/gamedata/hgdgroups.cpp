#include "hgdgroups.h"
#include "hgdfile.h"
#include <cstring>
#include <iostream>
#include <streambuf>

namespace halogen {
namespace gamedata {
    HGDGroup::HGDGroup(uint16_t id, uint16_t memberCount, uint16_t memberLength, std::byte* memberData)
        : id(id)
        , memberCount(memberCount)
        , memberLength(memberLength)
        , memberData(memberData) {
    }

    uint16_t HGDGroup::getId() {
        return id;
    }

    uint16_t HGDGroup::getMemberCount() {
        return memberCount;
    }

    uint16_t HGDGroup::getMemberLength() {
        return memberLength;
    }

    std::byte* HGDGroup::getMemberData() {
        return memberData;
    }

    struct membuf
        : std::streambuf {
        membuf(std::byte* base, std::size_t size) {
            this->setp((char*)base, (char*)base + size);
            this->setg((char*)base, (char*)base, (char*)base + size);
        }
        std::size_t written() const { return this->pptr() - this->pbase(); }
        std::size_t read() const { return this->gptr() - this->eback(); }
    };

#define readVar(stream, var) stream.read((char*)&var, sizeof(var))

    GameInfoGroup::GameInfoGroup(HGDGroup internalGroup)
        : internalGroup(internalGroup)
        , majorVersion(999)
        , minorVersion(999)
        , patchVersion(255)
        , buildNumber(999) {
        membuf buffer(internalGroup.getMemberData(), internalGroup.getMemberLength());
        std::istream is(&buffer);

        uint16_t gameNameLength;
        is.read((char*)&gameNameLength, 2);
        gameName.resize(gameNameLength);
        is.read(gameName.data(), gameNameLength);

        uint16_t windowTitleLength;
        is.read((char*)&windowTitleLength, 2);
        windowTitle.resize(windowTitleLength);
        is.read(windowTitle.data(), windowTitleLength);

        readVar(is, majorVersion);
        readVar(is, minorVersion);
        readVar(is, patchVersion);
        readVar(is, buildNumber);
    }

    std::string GameInfoGroup::getGameName() {
        return gameName;
    }

    std::string GameInfoGroup::getWindowTitle() {
        return windowTitle;
    }

    std::string GameInfoGroup::getVersionString() {
        return std::to_string(majorVersion) + "." + std::to_string(minorVersion) + "." + std::to_string(patchVersion) + " (" + std::to_string(buildNumber) + ")";
    }

    ObjectGroup::ObjectGroup(HGDGroup internalGroup)
        : internalGroup(internalGroup) {
        int totalMemberLength = internalGroup.getMemberLength() * internalGroup.getMemberCount();
        membuf buffer(internalGroup.getMemberData(), totalMemberLength);
        std::istream is(&buffer);

        members.reserve(internalGroup.getMemberCount());
        for (int i = 0; i < internalGroup.getMemberCount(); i++) {
            ObjectMember member;
            is.read((char*)&member, sizeof(ObjectMember));
            members.push_back(member);
        }
    }
}
}