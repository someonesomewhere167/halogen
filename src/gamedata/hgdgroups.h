#pragma once
#include <cstdint>
#include <string>
#include <cstddef>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <vector>

namespace halogen
{
    namespace gamedata
    {
        class HGDGroup
        {
            public:
            HGDGroup(uint16_t id, uint16_t memberCount, uint16_t memberLength, std::byte* memberData);
            uint16_t getId();
            uint16_t getMemberCount();
            uint16_t getMemberLength();
            std::byte* getMemberData();
            private:
            uint16_t id;
            uint16_t memberCount;
            uint16_t memberLength;
            std::byte* memberData;
        };

        class GameInfoGroup
        {
            public:
            GameInfoGroup(HGDGroup internalGroup);
            std::string getGameName();
            std::string getWindowTitle();
            std::string getVersionString();
            private:
            std::string gameName;
            std::string windowTitle;
            uint16_t majorVersion;
            uint16_t minorVersion;
            uint8_t patchVersion;
            uint32_t buildNumber;
            HGDGroup internalGroup;
        };

        struct ObjectMaterial
        {
            float specularStrength;
            int specularPower;
            glm::vec3 color;
        };

        struct ObjectMember
        {
            char objectName[32];
            char meshName[32];
            char shaderName[32];
            float specularStrength;
            int specularPower;
            glm::vec3 color;
            glm::vec3 pos;
            glm::vec3 scale;
            glm::quat rot;
        };

        class ObjectGroup
        {
            public:
            ObjectGroup(HGDGroup internalGroup);
            std::vector<ObjectMember> members;
            private:
            HGDGroup internalGroup;
        };
        
    }
}