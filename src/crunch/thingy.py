f = open("files.txt", "r")

line = f.readline()

text = "[\n"

while line:
    text += "\"" + line.strip() + "\",\n"
    line = f.readline()

text += "]"

f.close()

new_f = open("meson.build", "w")
new_f.write("crunch_sources = " + text)
new_f.close()
