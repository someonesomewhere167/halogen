#include "physics.h"
#include <BulletCollision/CollisionDispatch/btCollisionDispatcherMt.h>
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#include <BulletCollision/CollisionShapes/btShapeHull.h>
#include <BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolverMt.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorldMt.h>
#include <algorithm>
#include <iostream>
#define __TBB_NO_IMPLICIT_LINKAGE 1
#include "PhysDebugDrawer.h"
#include <glm/gtx/matrix_decompose.hpp>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/tbb.h>

namespace halogen {
namespace physics {
    btDispatcher* dispatcher;
    btBroadphaseInterface* pairCache;
    btConstraintSolver* constraintSolver;
    btConstraintSolverPoolMt* solverPool;
    btCollisionConfiguration* collisionConfiguration;
    btDiscreteDynamicsWorld* world;

    glm::vec3 bt2glm(btVector3 btvec) {
        return glm::vec3(btvec.x(), btvec.y(), btvec.z());
    }

    glm::quat bt2glm(btQuaternion btQuat) {
        return glm::quat(btQuat.w(), btQuat.x(), btQuat.y(), btQuat.z());
    }

    btVector3 glm2bt(glm::vec3 glmvec) {
        return btVector3(glmvec.x, glmvec.y, glmvec.z);
    }

    Rigidbody::Rigidbody(std::shared_ptr<Collider> collider, float mass, uint32_t entityId)
        : collider(collider)
        , offset(0.0f, 0.0f, 0.0f)
        , mass(mass) {
    }

    glm::vec3 Rigidbody::getPosition() {
        btVector3 comPos = bulletHandle->getCenterOfMassPosition();
        if (collider->type == ColliderType::Compound)
            comPos -= glm2bt(getActualRotation() * std::static_pointer_cast<CompoundColliderParameters>(collider->parameters)->offset);
        return bt2glm(comPos);
    }

    glm::vec3 Rigidbody::getActualPosition() {
        return bt2glm(bulletHandle->getCenterOfMassPosition());
    }

    void Rigidbody::setPosition(glm::vec3 pos) {
        btTransform transform = bulletHandle->getCenterOfMassTransform();
        btVector3 newPos = glm2bt(pos);
        if (collider->type == ColliderType::Compound)
            newPos += glm2bt(getActualRotation() * std::static_pointer_cast<CompoundColliderParameters>(collider->parameters)->offset);
        transform.setOrigin(newPos);
        bulletHandle->setCenterOfMassTransform(transform);
    }

    glm::quat Rigidbody::getRotation() {
        btTransform transform;
        ((btDefaultMotionState*)bulletHandle->getMotionState())->getWorldTransform(transform);
        btQuaternion btRot = transform.getRotation();
        return bt2glm(btRot);
    }

    glm::quat Rigidbody::getActualRotation() {
        return bt2glm(bulletHandle->getWorldTransform().getRotation());
    }

    void Rigidbody::setRotation(glm::quat rot) {
        btQuaternion btRot(rot.x, rot.y, rot.z, rot.w);
        btTransform& transform = bulletHandle->getWorldTransform();
        transform.setRotation(btRot);
        bulletHandle->getMotionState()->setWorldTransform(transform);
    }

    void Rigidbody::setMass(float mass) {
        bulletHandle->setMassProps(mass, inertia);
        this->mass = mass;
    }

    float Rigidbody::getMass() {
        return this->mass;
    }

    void Rigidbody::updateCollider() {
        btVector3 inertia;

        world->removeRigidBody(bulletHandle);

        bulletHandle->setCollisionFlags(bulletHandle->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);

        glm::vec3 oldPos;
        if (collider->type == ColliderType::Compound)
            oldPos = getPosition();

        collider->update();

        if (collider->type == ColliderType::Compound)
            setPosition(oldPos);

        bulletHandle->setCollisionFlags(bulletHandle->getCollisionFlags() & !btCollisionObject::CF_KINEMATIC_OBJECT);

        bulletHandle->setCollisionShape(collider->shape);
        if (collider->type == ColliderType::Mesh && std::static_pointer_cast<MeshColliderParameters>(collider->parameters)->isStatic)
            inertia = btVector3(0.0f, 0.0f, 0.0f);
        else
            collider->shape->calculateLocalInertia(this->mass, inertia);
        bulletHandle->setMassProps(this->mass, inertia);
        bulletHandle->updateInertiaTensor();

        world->addRigidBody(bulletHandle);
    }

    void Rigidbody::offsetCOM(glm::vec3 offset) {
    }

    void Rigidbody::applyForce(glm::vec3 wsForceDir, glm::vec3 wsForcePos) {
        bulletHandle->applyForce(glm2bt(wsForceDir), glm2bt(wsForcePos - getActualPosition()));
    }

    glm::vec3 Rigidbody::getVelocity() {
        return bt2glm(bulletHandle->getLinearVelocity());
    }

    Collider::Collider(ColliderType type, std::shared_ptr<ColliderParameters> parameters)
        : type(type)
        , parameters(parameters) {
        shape = 0;
        update();
    }

    void Collider::update() {
        if (shape != nullptr)
            delete shape;
        switch (type) {
        case ColliderType::Box: {
            std::shared_ptr<BoxColliderParameters> cParams = std::static_pointer_cast<BoxColliderParameters>(parameters);
            glm::vec3 halfExtents = cParams->halfExtents;
            btVector3 bHalfExtents(halfExtents.x, halfExtents.y, halfExtents.z);
            btBoxShape* bShape = new btBoxShape(bHalfExtents);
            shape = bShape;
            break;
        }
        case ColliderType::Sphere: {
            std::shared_ptr<SphereColliderParameters> cParams = std::static_pointer_cast<SphereColliderParameters>(parameters);
            btSphereShape* bShape = new btSphereShape(cParams->radius);
            shape = bShape;
            break;
        }
        case ColliderType::Mesh: {
            std::shared_ptr<MeshColliderParameters> cParams = std::static_pointer_cast<MeshColliderParameters>(parameters);

            if (cParams->isStatic) {
                btIndexedMesh mesh;
                mesh.m_numVertices = (int)cParams->vertices.size();
                mesh.m_vertexStride = sizeof(float) * 3;
                mesh.m_vertexBase = (const unsigned char*)&cParams->vertices.data()->x;

                mesh.m_triangleIndexBase = (const unsigned char*)cParams->indices.data();
                mesh.m_triangleIndexStride = sizeof(int) * 3;
                mesh.m_numTriangles = cParams->indices.size() / 3;

                mesh.m_indexType = PHY_INTEGER;

                btTriangleIndexVertexArray* tiva = new btTriangleIndexVertexArray;

                tiva->addIndexedMesh(mesh);
                btBvhTriangleMeshShape* bShape = new btBvhTriangleMeshShape(tiva, false);
                shape = bShape;
            } else {
                btConvexHullShape convexHullShape(&cParams->vertices[0].x, cParams->vertices.size(), 3 * sizeof(float));
                //create a hull approximation
                convexHullShape.setMargin(0); // this is to compensate for a bug in bullet
                btShapeHull* hull = new btShapeHull(&convexHullShape);
                hull->buildHull(0); // note: parameter is ignored by buildHull
                btConvexHullShape* bShape = new btConvexHullShape(
                    (const btScalar*)hull->getVertexPointer(), hull->numVertices(), sizeof(btVector3));
                shape = bShape;
            }
            break;
        }
        case ColliderType::Compound: {
            std::shared_ptr<CompoundColliderParameters> cParams = std::static_pointer_cast<CompoundColliderParameters>(parameters);
            btCompoundShape* bShape = new btCompoundShape(true, cParams->subColliders.size());

            btVector3 avgPos(0.0f, 0.0f, 0.0f);

            for (SubCollider sc : cParams->subColliders) {
                btVector3 transformPos = sc.transform.getOrigin();
                avgPos += transformPos;
            }

            avgPos /= cParams->subColliders.size();

            for (SubCollider sc : cParams->subColliders) {
                btVector3 transformPos = sc.transform.getOrigin();
                sc.transform.setOrigin(sc.transform.getOrigin() - avgPos);
                bShape->addChildShape(sc.transform, sc.shape);
            }

            cParams->offset = bt2glm(avgPos);

            bool isNonMoving = bShape->isNonMoving();
            int nc = bShape->getNumChildShapes();

            shape = bShape;
            break;
        }
        case ColliderType::Capsule: {
            std::shared_ptr<CapsuleColliderParameters> cParams = std::static_pointer_cast<CapsuleColliderParameters>(parameters);
            btCapsuleShape* bShape = new btCapsuleShape(cParams->radius, cParams->height);
            shape = bShape;
            break;
        }
        case ColliderType::Heightfield: {

            break;
        }
        }
    }

    SubCollider::SubCollider(ColliderType type, std::shared_ptr<ColliderParameters> parameters)
        : Collider(type, parameters)
        , transform(btTransform::getIdentity()) {
        if (type == ColliderType::Compound)
            throw std::runtime_error("Cannot create a compound SubCollider!");
    }

    RigidbodyChildInfo::RigidbodyChildInfo()
        : set(false)
        , hasConstraint(false) {
    }

    class TaskSchedulerTBB : public btITaskScheduler {
        int m_numThreads;
        tbb::task_scheduler_init* m_tbbSchedulerInit;

    public:
        TaskSchedulerTBB()
            : btITaskScheduler("IntelTBB") {
            m_numThreads = 0;
            m_tbbSchedulerInit = nullptr;
        }

        ~TaskSchedulerTBB() {
            if (m_tbbSchedulerInit) {
                delete m_tbbSchedulerInit;
                m_tbbSchedulerInit = nullptr;
            }
        }

        virtual int getMaxNumThreads() const BT_OVERRIDE {
            return tbb::task_scheduler_init::default_num_threads();
        }

        virtual int getNumThreads() const BT_OVERRIDE {
            return m_numThreads;
        }

        virtual void setNumThreads(int numThreads) BT_OVERRIDE {
            m_numThreads = (std::max)(1, (std::min)(int(BT_MAX_THREAD_COUNT), numThreads));
            if (m_tbbSchedulerInit) {
                // destroys all previous threads
                delete m_tbbSchedulerInit;
                m_tbbSchedulerInit = NULL;
            }
            m_tbbSchedulerInit = new tbb::task_scheduler_init(m_numThreads);
            m_savedThreadCounter = 0;
            if (m_isActive) {
                btResetThreadIndexCounter();
            }
        }

        struct ForBodyAdapter {
            const btIParallelForBody* mBody;

            ForBodyAdapter(const btIParallelForBody* body)
                : mBody(body) {
            }
            void operator()(const tbb::blocked_range<int>& range) const {
                mBody->forLoop(range.begin(), range.end());
            }
        };

        virtual void parallelFor(int iBegin, int iEnd, int grainSize, const btIParallelForBody& body) BT_OVERRIDE {
            ForBodyAdapter tbbBody(&body);
            tbb::parallel_for(tbb::blocked_range<int>(iBegin, iEnd, grainSize),
                tbbBody,
                tbb::simple_partitioner());
        }

        struct SumBodyAdapter {
            const btIParallelSumBody* mBody;
            btScalar mSum;

            SumBodyAdapter(const btIParallelSumBody* body)
                : mBody(body)
                , mSum(btScalar(0)) {
            }
            SumBodyAdapter(const SumBodyAdapter& src, tbb::split)
                : mBody(src.mBody)
                , mSum(btScalar(0)) {
            }
            void join(const SumBodyAdapter& src) { mSum += src.mSum; }
            void operator()(const tbb::blocked_range<int>& range) {
                mSum += mBody->sumLoop(range.begin(), range.end());
            }
        };

        virtual btScalar parallelSum(int iBegin, int iEnd, int grainSize, const btIParallelSumBody& body) BT_OVERRIDE {
            SumBodyAdapter tbbBody(&body);
            tbb::parallel_deterministic_reduce(tbb::blocked_range<int>(iBegin, iEnd, grainSize), tbbBody);
            return tbbBody.mSum;
        }
    };

    void destroyRigidbody(entt::DefaultRegistry& registry, uint32_t entity) {
        assert(registry.has<Rigidbody>(entity));
        Rigidbody& rb = registry.get<Rigidbody>(entity);
        world->removeRigidBody(rb.bulletHandle);
        delete rb.bulletHandle;
        delete rb.collider->shape;
    }

    void createRigidbody(entt::DefaultRegistry& registry, uint32_t entity) {
        assert(registry.has<Rigidbody>(entity));
        Rigidbody& rb = registry.get<Rigidbody>(entity);

        btMotionState* motionState;
        motionState = new btDefaultMotionState();

        rb.bulletHandle = new btRigidBody(rb.mass, motionState, rb.collider->shape);

        world->addRigidBody(rb.bulletHandle);

        rb.updateCollider();

        rb.bulletHandle->setUserIndex(entity);
    }

    void createRigidbodyChild(entt::DefaultRegistry& registry, uint32_t entity) {
        RigidbodyChildInfo& ci = registry.get<RigidbodyChildInfo>(entity);
        Rigidbody& cR = registry.get<Rigidbody>(entity);
        Rigidbody& pR = registry.get<Rigidbody>(ci.parentEntity);
        cR.bulletHandle->setIgnoreCollisionCheck(pR.bulletHandle, true);
        pR.bulletHandle->setIgnoreCollisionCheck(cR.bulletHandle, true);

        auto& childT = registry.get<components::Transform>(entity);
        auto& parentT = registry.get<components::Transform>(ci.parentEntity);

        if (!ci.set) {
            ci.localPosition = childT.position - parentT.position;
            ci.localRotation = glm::inverse(parentT.rotation) * childT.rotation;
            ci.set = true;
        }

        std::cout << "Local position is " << ci.localPosition.x << "," << ci.localPosition.y << "," << ci.localPosition.z << "\n";
        /* btTransform frameInB;
        frameInB = btTransform::getIdentity();
        btTransform frameInA;
        frameInA = btTransform::getIdentity();
        frameInA.setOrigin(glm2bt(ci.localPosition));

        btGeneric6DofConstraint* constraint = new btGeneric6DofConstraint(*cR.bulletHandle, *pR.bulletHandle, frameInA, frameInB, false);
        constraint->setLinearLowerLimit(btVector3(0.0f, 0.0f, 0.0f));
        constraint->setLinearUpperLimit(btVector3(0.0f, 0.0f, 0.0f));
        constraint->setAngularLowerLimit(btVector3(0.0f, 0.0f, 0.0f));
        constraint->setAngularUpperLimit(btVector3(0.0f, 0.0f, 0.0f));
        world->addConstraint(constraint, true);*/

        ci.hasConstraint = true;
    }

    //#ifndef NDEBUG
    BulletDebugDrawer* dbgDraw;
    //#endif
    Physics::Physics(entt::DefaultRegistry& registry)
        : registry(registry) {
        btSetTaskScheduler(new TaskSchedulerTBB);
        //btGetTaskScheduler()->setNumThreads(1);
        btDefaultCollisionConstructionInfo cci;
        cci.m_defaultMaxPersistentManifoldPoolSize = 80000;
        cci.m_defaultMaxCollisionAlgorithmPoolSize = 80000;
        collisionConfiguration = new btDefaultCollisionConfiguration(cci);
        dispatcher = new btCollisionDispatcherMt(collisionConfiguration);
        pairCache = new btDbvtBroadphase;
        constraintSolver = new btSequentialImpulseConstraintSolverMt;
        solverPool = new btConstraintSolverPoolMt(6);

        world = new btDiscreteDynamicsWorldMt(dispatcher, pairCache, solverPool, nullptr, collisionConfiguration);
        world->setGravity(btVector3(0.0f, -9.81f, 0.0f));
        //#ifndef NDEBUG
        std::cout << "Setting up physics debug draw"
                  << "\n";
        dbgDraw = new BulletDebugDrawer;
        world->setDebugDrawer(dbgDraw);
        dbgDraw->setDebugMode(btIDebugDraw::DBG_DrawFrames);
        //dbgDraw->setDebugMode(btIDebugDraw::DBG_DrawWireframe);
        //#endif
        world->getSolverInfo().m_solverMode = btSolverMode::SOLVER_SIMD | btSolverMode::SOLVER_CACHE_FRIENDLY | btSolverMode::SOLVER_USE_WARMSTARTING;

        registry.destruction<Rigidbody>().connect<&destroyRigidbody>();
        registry.construction<Rigidbody>().connect<&createRigidbody>();
    }

    void Physics::syncRigidbody(Rigidbody& rigidbody, components::Transform& transform) {
        transform.position = rigidbody.getPosition();
        transform.rotation = rigidbody.getRotation();
    }

    btCollisionWorld::ClosestRayResultCallback raycast(glm::vec3 from, glm::vec3 to) {
        btVector3 bfrom(from.x, from.y, from.z);
        btVector3 bto(to.x, to.y, to.z);

        btCollisionWorld::ClosestRayResultCallback rayResult(bfrom, bto);
        world->rayTest(bfrom, bto, rayResult);

        return rayResult;
    }

    Physics::~Physics() {
        delete world;

        delete constraintSolver;
        delete pairCache;
        delete dispatcher;
        delete collisionConfiguration;
#ifndef NDEBUG
        delete dbgDraw;
#endif
    }

    bool firstFrame = true;

    void Physics::simulate(float deltaTime) {
        world->debugDrawWorld();

        if (!firstFrame) {
            registry.view<physics::Rigidbody, components::Transform>().each([=](const auto entity, physics::Rigidbody& rigidbody, components::Transform& transform) {
                if ((rigidbody.getPosition() != transform.position || rigidbody.getRotation() != transform.rotation)) {
                    if (rigidbody.collider->type != ColliderType::Compound) {
                        rigidbody.setPosition(transform.position);
                        rigidbody.setRotation(transform.rotation);
                    }
                }
            });
        }

        world->stepSimulation(deltaTime, 5);

        registry.view<physics::Rigidbody, components::Transform>().each([=](const auto entity, physics::Rigidbody& rigidbody, components::Transform& transform) {
            syncRigidbody(rigidbody, transform);
        });

        registry.view<RigidbodyChildInfo, components::Transform, Rigidbody>().each([=](const auto entity, RigidbodyChildInfo& ci, components::Transform& transform, Rigidbody& rb) {
            if (!ci.hasConstraint && registry.valid(ci.parentEntity) && registry.has<Rigidbody>(ci.parentEntity))
                createRigidbodyChild(registry, entity);
            auto& parentTransform = registry.get<components::Transform>(ci.parentEntity);
            glm::mat4 parentMatrix = parentTransform.calculateMatrix();

            transform.position = ci.localPosition;
            transform.rotation = ci.localRotation;

            glm::mat4 childMatrix = transform.calculateMatrix();

            glm::vec3 scale;
            glm::quat rotation;
            glm::vec3 translation;
            glm::vec3 skew;
            glm::vec4 perspective;

            glm::decompose(parentMatrix * childMatrix, scale, rotation, translation, skew, perspective);

            transform.rotation = rotation;
            transform.position = translation;
            rb.setPosition(transform.position);
            rb.setRotation(transform.rotation);
        });

        firstFrame = false;
    }

    CompoundColliderParameters::CompoundColliderParameters()
        : offset(0.0f)
        , subColliders() {
    }
}
}
