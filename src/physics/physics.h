#pragma once
#include "transform.h"
#include <btBulletDynamicsCommon.h>
#include <glm/glm.hpp>
#include <memory>
#include <vector>
#undef min
#undef max
#include <entt/entt.hpp>

namespace halogen
{
namespace physics
{
    glm::vec3 bt2glm(btVector3 btvec);
    btVector3 glm2bt(glm::vec3 glmvec);
    enum ColliderType
    {
        Box,
        Sphere,
        Mesh,
        Compound,
        Capsule,
        Heightfield
    };

    struct ColliderParameters
    {
    };

    struct Collider
    {
        Collider(ColliderType type, std::shared_ptr<ColliderParameters> parameters);
        ColliderType type;
        std::shared_ptr<ColliderParameters> parameters;
        btCollisionShape* shape;
        void update();
    };

    struct Rigidbody
    {
        Rigidbody(std::shared_ptr<Collider> collider, float mass, uint32_t entityId);
        std::shared_ptr<Collider> collider;
        glm::vec3 getPosition();
        glm::vec3 getActualPosition();
        glm::vec3 getVelocity();
        void setPosition(glm::vec3 pos);
        glm::quat getRotation();
        glm::quat getActualRotation();
        void setRotation(glm::quat rot);
        void setMass(float mass);
        float getMass();
        void updateCollider();
        void offsetCOM(glm::vec3 offset);
        void applyForce(glm::vec3 wsForceDir, glm::vec3 wsForcePos);
        btVector3 inertia;
        btRigidBody* bulletHandle;
        float mass;
        glm::vec3 offset;
    };

    struct RigidbodyChildInfo
    {
        RigidbodyChildInfo();
        uint32_t parentEntity;
        bool set;
        bool hasConstraint;
        glm::vec3 localPosition;
        glm::quat localRotation;
    };

    struct SphereColliderParameters : ColliderParameters
    {
        float radius;
    };

    struct BoxColliderParameters : ColliderParameters
    {
        glm::vec3 halfExtents;
    };

    struct MeshColliderParameters : ColliderParameters
    {
        bool isStatic;
        std::vector<glm::vec3> vertices;
        std::vector<int> indices;
    };

    struct CapsuleColliderParameters : ColliderParameters
    {
        float radius;
        float height;
    };

    struct SubCollider : Collider
    {
        SubCollider(ColliderType type, std::shared_ptr<ColliderParameters> parameters);
        btTransform transform;
    };

    struct CompoundColliderParameters : ColliderParameters
    {
        CompoundColliderParameters();
        std::vector<SubCollider> subColliders;
        glm::vec3 offset;
    };

    class Physics
    {
    public:
        Physics(entt::DefaultRegistry& registry);
        void syncRigidbody(Rigidbody& rigidbody, components::Transform& transform);
        void simulate(float deltaTime);
        ~Physics();

    private:
        entt::DefaultRegistry& registry;
    };

    btCollisionWorld::ClosestRayResultCallback raycast(glm::vec3 from, glm::vec3 to);

}
}