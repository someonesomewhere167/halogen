#include "PhysDebugDrawer.h"
#include "DebugDraw.h"
#include <glm/glm.hpp>

//#ifndef NDEBUG
namespace halogen {
namespace physics {
    BulletDebugDrawer::BulletDebugDrawer()
        : m_debugMode(0) {
    }

    void BulletDebugDrawer::drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
        util::DebugDraw::DrawLine(glm::vec3(from.x(), from.y(), from.z()), glm::vec3(to.x(), to.y(), to.z()));
        util::DebugDraw::DrawPoint(glm::vec3(from.x(), from.y(), from.z()));
        util::DebugDraw::DrawPoint(glm::vec3(to.x(), to.y(), to.z()));
    }

    void BulletDebugDrawer::setDebugMode(int debugMode) {
        m_debugMode = debugMode;
    }

    void BulletDebugDrawer::draw3dText(const btVector3& location, const char* textString) {
        //glRasterPos3f(location.x(),  location.y(),  location.z());
        //BMF_DrawString(BMF_GetFont(BMF_kHelvetica10),textString);
    }

    void BulletDebugDrawer::reportErrorWarning(const char* warningString) {
        printf(warningString);
    }

    void BulletDebugDrawer::drawContactPoint(const btVector3& pointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) {
        {
            //btVector3 to=pointOnB+normalOnB*distance;
            //const btVector3&from = pointOnB;
            //glColor4f(color.getX(), color.getY(), color.getZ(), 1.0f);

            //GLDebugDrawer::drawLine(from, to, color);

            //glRasterPos3f(from.x(),  from.y(),  from.z());
            //char buf[12];
            //sprintf(buf," %d",lifeTime);
            //BMF_DrawString(BMF_GetFont(BMF_kHelvetica10),buf);
        }
    }
}
}
//#endif