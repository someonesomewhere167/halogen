#include <Map.hpp>
#include <entt/core/hashed_string.hpp>
#include <iostream>

#define check(expr)                      \
    if (!expr) {                         \
        std::cout << #expr << " failed " \
                  << "\n";               \
        return 1;                        \
    }

int main(int argc, char** argv) {
    halogen::util::Map<entt::HashedString, int> map(33);
    check(!map.Has("hey_hs"));
    map.Insert("hey"_hs, 2);
    map.Insert("view"_hs, 1);
    map.Insert("projection"_hs, 3);
    check(!map.Has("brdfLUT"_hs)) return 0;
}