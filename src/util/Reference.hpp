#pragma once

namespace halogen {
namespace util {
    struct ReferenceInternal {
        int useCount;
    };

    template <class T>
    class Reference {
    public:
        Reference::Reference(T* ptr)
        : ptr(ptr)
        , internal(new ReferenceInternal) {
            internal->useCount = 1;
        }

        T* operator->() const {
            return ptr;
        }
        
        T& operator*() const {
            return *ptr;
        }
        
        Reference::Reference(const Reference& other) {
            ptr = other.ptr;
            internal = other.internal;
            internal->useCount++;
        }

        Reference::~Reference() {
            internal->useCount--;
            if (internal->useCount == 0) {
                delete ptr;
                delete internal;
            }
        }
    private:
        T* ptr;
        ReferenceInternal* internal;
    };
}
}