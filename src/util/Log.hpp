#pragma once
#include <string>
#include <memory>
#include <vector>

namespace halogen {
namespace util {
    enum class LogCategory {
        Misc,
        RenderBackend,
        RenderFrontend,
        Input,
        Audio,
        Game,
        Core
    };

    class LogTargetBase {
    public:
        virtual void WriteString(std::string string) = 0;
    };

    class Log {
    public:
        static void LogString(std::string string, LogCategory category);
        static void AddLogTarget(std::shared_ptr<LogTargetBase> target);
    private:
        static std::string LogCategoryToString(LogCategory category);
        static std::string GetDateTimeString();
        static void WriteString(std::string str);
        static std::vector<std::shared_ptr<LogTargetBase>> logTargets;
    };
}
}