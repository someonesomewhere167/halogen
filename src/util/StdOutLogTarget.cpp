#include "StdOutLogTarget.hpp"
#include <iostream>

namespace halogen {
namespace util {
    void StdOutLogTarget::WriteString(std::string string) {
        std::cout << string << "\n";
    }
}
}