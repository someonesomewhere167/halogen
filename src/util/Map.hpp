#pragma once
#include <functional>
#include <stdexcept>

namespace halogen {
namespace util {
    template <class K, class V>
    class Map {
    public:
        Map(int initialSize) {
            dataSize = initialSize * sizeof(V);
            data = (V*)std::malloc(dataSize);
            if (!data)
                throw std::runtime_error("Failed to allocate data for map");
            slotsFilled = (bool*)std::malloc(initialSize * sizeof(bool));
            if (!slotsFilled)
                throw std::runtime_error("Failed to allocate fillslots for map");
            numberElements = initialSize;
            std::memset(slotsFilled, 0, initialSize);
        }

        V& Get(K key) {
            size_t index = hasher(key) % numberElements;
            return data[index];
        }

        bool Has(K key) {
            return slotsFilled[hasher(key) % numberElements];
        }

        void Insert(K key, V value) {
            size_t index = hasher(key) % numberElements;
            if (slotsFilled[index])
                throw std::runtime_error("Hash collision");
            data[index] = value;
            slotsFilled[index] = true;
        }

        int NumElements() {
            return numberElements;
        }

        ~Map() {
            std::free(data);
        }

    private:
        V* data;
        bool* slotsFilled;
        size_t dataSize;
        size_t numberElements;
        std::hash<K> hasher;
    };
}
}