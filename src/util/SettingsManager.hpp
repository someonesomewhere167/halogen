#pragma once
#include <cpptoml.h>

namespace halogen {
namespace util {
    namespace SettingsManager {
        void LoadFromTable(std::shared_ptr<cpptoml::table> table);
        std::shared_ptr<cpptoml::base> GetOption(std::string path);
    }
}
}
