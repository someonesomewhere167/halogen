#pragma once
#include <glm/glm.hpp>
#include <vector>

namespace halogen
{
namespace util
{
    struct Point
    {
        glm::vec3 pos;
        glm::vec3 col;
    };

    struct LineVert
    {
        glm::vec3 pos;
        glm::vec3 col;
    };

    class DebugDraw
    {
    public:
        static void Init();
        static void DrawLine(glm::vec3 from, glm::vec3 to, glm::vec3 col = glm::vec3(1.0f));
        static void DrawPoint(glm::vec3 point, glm::vec3 col = glm::vec3(1.0f));
        static void RenderDrawn(glm::mat4 view, glm::mat4 projection);
        static void Shutdown();

    private:
        static std::vector<glm::vec3> linePos;
        static std::vector<glm::vec3> lineColors;
        static std::vector<Point> points;

        static unsigned int debugProgram;
        static unsigned int debugFs;
        static unsigned int debugVs;

        static unsigned int lineVao;
        static unsigned int pointVao;

        static unsigned int lineVbo;
        static unsigned int lineColVbo;
        static unsigned int pointVbo;

        static unsigned int lineVboSize;
        static unsigned int pointVboSize;
    };
}
}