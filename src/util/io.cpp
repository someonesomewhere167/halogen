#include "hgio.h"
#include <physfs.h>
#include <sstream>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define WINVER 0x0600
#define _WIN32_WINNT 0x0600
#include <shlobj.h>
#include <stringapiset.h>
#include <windows.h>
#endif

#ifdef __unix__
#include <errno.h>
#include <sys/stat.h>
#endif

#include <vector>

namespace halogen {
namespace util {
    std::string loadFileToString(std::string path) {
        PHYSFS_getLastErrorCode(); // clear any previous errors
        PHYSFS_File* file = PHYSFS_openRead(path.c_str());
        PHYSFS_ErrorCode err = PHYSFS_getLastErrorCode();

        if (err != PHYSFS_ERR_OK)
            throw std::runtime_error("Failed to open " + path);

        int64_t length = PHYSFS_fileLength(file);

        std::string fileContents;
        fileContents.resize(length);

        PHYSFS_readBytes(file, (void*)fileContents.data(), length);

        PHYSFS_close(file);

        return fileContents;
    }

    void* loadFileToBuffer(std::string path, unsigned long& bytesRead) {
        PHYSFS_File* file = PHYSFS_openRead(path.c_str());
        PHYSFS_ErrorCode err = PHYSFS_getLastErrorCode();

        if (err != PHYSFS_ERR_OK)
            return nullptr;

        int64_t length = PHYSFS_fileLength(file);

        void* buffer = std::malloc(length);

        PHYSFS_readBytes(file, buffer, length);

        PHYSFS_close(file);

        bytesRead = length;
        return buffer;
    }

    void saveBufferToFile(std::string path, unsigned long length, void* data) {
        PHYSFS_File* file = PHYSFS_openWrite(path.c_str());
        long result = PHYSFS_writeBytes(file, data, length);
        if (length != result)
            throw std::runtime_error("Failed to write file " + path);
        PHYSFS_close(file);
    }

    std::istream& loadFileToStream(std::string path) {
        PHYSFS_File* file = PHYSFS_openRead(path.c_str());
        PHYSFS_ErrorCode err = PHYSFS_getLastErrorCode();

        if (err != PHYSFS_ERR_OK)
            throw std::runtime_error("Failed to open " + path);

        int64_t length = PHYSFS_fileLength(file);

        std::string fileContents;
        fileContents.resize(length);

        PHYSFS_readBytes(file, (void*)fileContents.data(), length);

        PHYSFS_close(file);

        std::istringstream stream(fileContents);
        std::istream& istream(stream);

        return istream;
    }

#ifdef _WIN32
    static std::string win32_utf16_to_utf8(const wchar_t* wstr) {
        std::string res;
        // If the 6th parameter is 0 then WideCharToMultiByte returns the number of bytes needed to store the result.
        int actualSize = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, nullptr, 0, nullptr, nullptr);
        if (actualSize > 0) {
            //If the converted UTF-8 string could not be in the initial buffer. Allocate one that can hold it.
            std::vector<char> buffer(actualSize);
            actualSize = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, &buffer[0], buffer.size(), nullptr, nullptr);
            res = buffer.data();
        }
        if (actualSize == 0) {
            // WideCharToMultiByte return 0 for errors.
            const std::string errorMsg = "UTF16 to UTF8 failed with error code: " + GetLastError();
            throw std::runtime_error(errorMsg);
        }
        return res;
    }

    static wchar_t* utf8ToUtf16(std::string string) {
        int size = MultiByteToWideChar(CP_UTF8, 0, string.c_str(), -1, nullptr, 0);

        if (size > 0) {
            wchar_t* buffer = (wchar_t*)std::malloc(sizeof(wchar_t) * size);
            MultiByteToWideChar(CP_UTF8, 0, string.c_str(), -1, buffer, size);
            return buffer;
        } else if (size == 0) {
            const std::string errorMsg = "UTF8 to UTF16 failed with error code: " + GetLastError();

            throw std::runtime_error(errorMsg);
            return 0;
        }
    }
#endif

    std::string getConfigPath() {
#ifdef _WIN32
        LPWSTR wszPath = NULL;
        HRESULT hr;
        hr = SHGetKnownFolderPath(FOLDERID_RoamingAppData, KF_FLAG_CREATE, NULL, &wszPath);

        std::string result = win32_utf16_to_utf8(wszPath) + "\\halogen";

        CoTaskMemFree(wszPath);
        return result;
#elif defined(__unix__)
        std::string res;

        const char* tempRes = std::getenv("XDG_CONFIG_HOME");

        if (tempRes) {
            res = tempRes;
            return res + "/halogen";
        } else {
            return std::getenv("HOME") + std::string("/.config/halogen");
        }
#endif
    }

    bool createDirectory(std::string path) {
#ifdef _WIN32
        bool success = CreateDirectoryW(utf8ToUtf16(path), NULL);

        if (!success) {
            std::cerr << "Failed to create directory with error code " << GetLastError() << "\n";
        }

        return success;
#elif defined(__unix__)
        int error = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

        if (error == -1) {
            std::cerr << "Failed to create directory with error code " << errno << "\n";
        }

        return error == 0;
#endif
    }
}
}