#include <glad.h>
#include "DebugDraw.h"
#include "hgio.h"
#include <glm/gtc/type_ptr.hpp>

namespace halogen {
namespace util {
    unsigned int loadSubShader(std::string fileName, GLenum shaderType) {
        std::string shaderContents = util::loadFileToString(fileName);

        unsigned int shaderHandle = glCreateShader(shaderType);

        const char* shaderSource = shaderContents.c_str();
        glShaderSource(shaderHandle, 1, &shaderSource, NULL);
        glCompileShader(shaderHandle);
        glObjectLabel(GL_SHADER, shaderHandle, fileName.size(), fileName.c_str());

        GLint success = 0;
        glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &success);

        if (success == GL_FALSE) {
            GLint maxLength = 0;
            glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &maxLength);

            // The maxLength includes the NULL character
            std::vector<GLchar> errorLog(maxLength);
            glGetShaderInfoLog(shaderHandle, maxLength, &maxLength, &errorLog[0]);

            std::cerr << "Error compiling shader " << fileName << ":"
                      << "\n";
            std::cerr << &errorLog[0] << "\n";

            glDeleteShader(shaderHandle);

            return -1;
        }

        return shaderHandle;
    }

    std::vector<glm::vec3> DebugDraw::linePos;
    std::vector<glm::vec3> DebugDraw::lineColors;

    std::vector<Point> DebugDraw::points;

    unsigned int DebugDraw::debugProgram;
    unsigned int DebugDraw::debugFs;
    unsigned int DebugDraw::debugVs;

    unsigned int DebugDraw::lineVao;
    unsigned int DebugDraw::pointVao;

    unsigned int DebugDraw::lineVbo;
    unsigned int DebugDraw::lineColVbo;
    unsigned int DebugDraw::pointVbo;

    unsigned int DebugDraw::lineVboSize;
    unsigned int DebugDraw::pointVboSize;

    void DebugDraw::Init() {
        return;
        debugVs = loadSubShader("shaders/debug_vs.glsl", GL_VERTEX_SHADER);
        debugFs = loadSubShader("shaders/debug_fs.glsl", GL_FRAGMENT_SHADER);

        debugProgram = glCreateProgram();

        glAttachShader(debugProgram, debugFs);
        glAttachShader(debugProgram, debugVs);
        glLinkProgram(debugProgram);

        glObjectLabel(GL_PROGRAM, debugProgram, 16, "DebugDraw Shader");

        glGenBuffers(1, &lineVbo);
        glGenBuffers(1, &lineColVbo);
        glGenBuffers(1, &pointVbo);
        lineVboSize = 0;
        pointVboSize = 0;

        glGenVertexArrays(1, &lineVao);
        glGenVertexArrays(1, &pointVao);

        glBindVertexArray(lineVao);
        glBindBuffer(GL_ARRAY_BUFFER, lineVbo);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);
        glBindBuffer(GL_ARRAY_BUFFER, lineColVbo);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

        glBindVertexArray(pointVao);
        glBindBuffer(GL_ARRAY_BUFFER, pointVbo);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2, 0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2, (void*)offsetof(Point, col));
    }

    void DebugDraw::DrawLine(glm::vec3 from, glm::vec3 to, glm::vec3 col) {
#if IMGUI_ENABLED
        linePos.push_back(from);
        linePos.push_back(to);
        lineColors.push_back(col);
        lineColors.push_back(col);
#endif
    }

    void DebugDraw::DrawPoint(glm::vec3 point, glm::vec3 col) {
#if IMGUI_ENABLED
        points.push_back({ point, col });
#endif
    }

    void DebugDraw::RenderDrawn(glm::mat4 view, glm::mat4 projection) {
        //glDisable(GL_DEPTH_TEST);
#if IMGUI_ENABLED
        glUseProgram(debugProgram);

        glUniformMatrix4fv(glGetUniformLocation(debugProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(debugProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

        if (linePos.size() > 0) {
            glBindBuffer(GL_ARRAY_BUFFER, lineVbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * linePos.size(), linePos.data(), GL_STREAM_DRAW);

            glBindBuffer(GL_ARRAY_BUFFER, lineColVbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * lineColors.size(), lineColors.data(), GL_STREAM_DRAW);

            glBindVertexArray(lineVao);
            glDrawArrays(GL_LINES, 0, linePos.size());
        }

        if (points.size() > 0) {
            glBindBuffer(GL_ARRAY_BUFFER, pointVbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(Point) * points.size(), points.data(), GL_STREAM_DRAW);
            glBindVertexArray(pointVao);
            glPointSize(15.0f);
            glDrawArrays(GL_POINTS, 0, points.size());
        }
#endif
        linePos.clear();
        lineColors.clear();
        points.clear();
        //glEnable(GL_DEPTH_TEST);
    }

    void DebugDraw::Shutdown() {
        return;
        glDeleteProgram(debugProgram);
        glDeleteShader(debugFs);
        glDeleteShader(debugVs);
    }
}
}