#include "Reference.hpp"

namespace halogen {
namespace util {
    template <class T>
    Reference::Reference(T* ptr)
        : ptr(ptr)
        , internal(new ReferenceInternal) {
        internal->useCount = 1;
    }

    template <class T>
    T* Reference::operator->() const {
        return ptr;
    }

    template <class T>
    T& Reference::operator*() const {
        return *ptr;
    }

    template <class T>
    Reference::Reference(const Reference& other) {
        ptr = other.ptr;
        internal = other.internal;
        internal->useCount++;
    }

    template <class T>
    Reference::~Reference() {
        internal->useCount--;
        if (internal->useCount == 0) {
            delete ptr;
            delete internal;
        }
    }
}
}