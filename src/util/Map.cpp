#include "Map.hpp"
#include <functional>

namespace halogen {
namespace util {
    template <class K, class V>
    Map<K, V>::Map(int initialSize) {
        dataSize = initialSize * sizeof(V);
        data = (V*)std::malloc(dataSize);
        numberElements = initialSize;
    }

    template <class K, class V>
    V& Map<K, V>::Get(K key) {
        size_t index = hasher(key) % numberElements;
        return data[index];
    }

    template <class K, class V>
    void Map<K, V>::Insert(K key, V value) {
        size_t index = hasher(key) % numberElements;
        data[index] = value;
    }

    template <class K, class V>
    int Map<K, V>::NumElements() {
        return numberElements;
    }

    template <class K, class V>
    Map<K, V>::~Map() {
        std::free(data);
    }
}
}