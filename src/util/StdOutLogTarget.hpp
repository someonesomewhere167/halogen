#pragma once
#include <Log.hpp>

namespace halogen {
namespace util {
    class StdOutLogTarget : public LogTargetBase {
        void WriteString(std::string string) override;
    };
}
}