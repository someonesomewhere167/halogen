#pragma once
#include <cstdint>

namespace halogen {
namespace util {
    struct Voxel {
        uint8_t id;
    };

    class VoxelOctreeLeaf {
    };

    class VoxelOctreeNode {
    };

    class VoxelOctree {
    };
}
}