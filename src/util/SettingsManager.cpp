#include "SettingsManager.hpp"
#include <hgio.h>
#include <iostream>

namespace halogen {
namespace util {
    namespace SettingsManager {
        std::shared_ptr<cpptoml::table> root;
        std::shared_ptr<cpptoml::table> defaultRoot;
        void LoadFromTable(std::shared_ptr<cpptoml::table> table) {
            root = table;
            // validate the root
            for (const auto& thing : *table) {
                const auto& val = thing.second;
                if (!thing.second->is_table()) {
                    std::cerr << "Warning: Invalid settings file detected (setting without category). Ignoring.\n";
                }
            }

            std::string inputBase = util::loadFileToString("defaultoptions.toml");
            std::istringstream sstream(inputBase);
            std::istream& stream(sstream);

            cpptoml::parser parser(stream);
            defaultRoot = parser.parse();
        }

        std::shared_ptr<cpptoml::base> GetOption(std::string path) {
            if (root->contains_qualified(path))
                return root->get_qualified(path);
            else {
                std::cout << "WARNING: Resorting to default value for " << path << "\n";
                return defaultRoot->get_qualified(path);
            }
        }
    }
}
}