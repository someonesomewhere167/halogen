#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

namespace halogen
{
	namespace util
	{
		std::string loadFileToString(std::string path);
		void* loadFileToBuffer(std::string path, unsigned long &bytesRead);
        void saveBufferToFile(std::string path, unsigned long length, void* data);
		std::istream& loadFileToStream(std::string path);
		std::string getConfigPath();
		bool createDirectory(std::string path);
	}
}