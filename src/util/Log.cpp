#include <Log.hpp>
#include <chrono>

namespace halogen {
namespace util {
    std::vector<std::shared_ptr<LogTargetBase>> Log::logTargets;

    void Log::LogString(std::string string, LogCategory category) {
        WriteString(GetDateTimeString() + " [" + LogCategoryToString(category) + "] " + string);
    }

    void Log::AddLogTarget(std::shared_ptr<LogTargetBase> target) {
        logTargets.push_back(target);
    }

    std::string Log::LogCategoryToString(LogCategory category) {
        switch (category) {
        case LogCategory::Misc:
            return "Misc";
            break;
        case LogCategory::RenderBackend:
            return "Render Backend";
            break;
        case LogCategory::RenderFrontend:
            return "Render Frontend";
            break;
        case LogCategory::Input:
            return "Input";
            break;
        case LogCategory::Audio:
            return "Audio";
            break;
        case LogCategory::Game:
            return "Game";
            break;
        case LogCategory::Core:
            return "Core";
            break;
        }
    }

    std::string Log::GetDateTimeString() {
        auto dt = std::chrono::system_clock::now();
        std::time_t dt2 = std::chrono::system_clock::to_time_t(dt);
        char ts[64];
        strftime(ts, 64, "%H:%M:%S", localtime(&dt2));
        return std::string(ts);
    }

    void Log::WriteString(std::string str) {
        for (auto pBase : logTargets) {
            pBase->WriteString(str);
        }
    }
}
}