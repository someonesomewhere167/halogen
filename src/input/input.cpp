#include "input.h"
#include "cpptoml.h"
#include "hgio.h"
#include <GLFW/glfw3.h>
#include <functional>
#include <glm/glm.hpp>
#include <iostream>
#include <sstream>

namespace halogen {
namespace input {
    GLFWwindow* window;
    bool lastFrameState[GLFW_KEY_LAST - GLFW_KEY_SPACE];
    bool lastFrameMouseState[GLFW_MOUSE_BUTTON_LAST];
    bool keyState[GLFW_KEY_LAST - GLFW_KEY_SPACE];
    std::map<entt::HashedString, int> keyboardActions;
    std::map<entt::HashedString, int> mouseActions;
    double cursorLastX, cursorLastY;
    glm::vec2 cursorDelta;
    float currScroll;
    std::vector<KeyCallback> keyCallbacks;
    std::vector<CharCallback> charCallbacks;
    std::vector<KeyboardInputEvent> kieThisFrame;
    GLFWcharfun oldCharCallback;
    GLFWkeyfun oldKeyCallback;
    bool mouseLocked = false;

    void loadDefaultBindings() {
        std::string inputBase = util::loadFileToString("baseinput.toml");
        std::istringstream sstream(inputBase);
        std::istream& stream(sstream);

        cpptoml::parser parser(stream);
        std::shared_ptr<cpptoml::table> table = parser.parse();

        std::shared_ptr<cpptoml::table> kbdtable = table->get_table_qualified("controls.keyboard");

        for (const auto& t : *kbdtable) {
            auto value = kbdtable->get_as<int>(t.first);

            keyboardActions[entt::HashedString(t.first.c_str())] = *value;
        }

        std::shared_ptr<cpptoml::table> mouseTable = table->get_table_qualified("controls.mouse");

        for (const auto& t : *mouseTable) {
            auto value = mouseTable->get_as<int>(t.first);

            mouseActions[entt::HashedString(t.first.c_str())] = *value;
        }
    }

    void scrollCallback(GLFWwindow* window, double x, double y) {
        currScroll = y;
    }

    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
        for (KeyCallback callback : keyCallbacks) {
            callback(key, scancode, action, mods);
        }
        if (oldKeyCallback != nullptr)
            oldKeyCallback(window, key, scancode, action, mods);
    }

    void charCallback(GLFWwindow* window, unsigned int codepoint) {
        for (CharCallback callback : charCallbacks) {
            callback(codepoint);
        }
        if (oldCharCallback != nullptr)
            oldCharCallback(window, codepoint);
    }

    void init() {
        //window = glfwWindow;
        loadDefaultBindings();
        //glfwSetScrollCallback(window, scrollCallback);
        //oldKeyCallback = glfwSetKeyCallback(window, keyCallback);
        //oldCharCallback = glfwSetCharCallback(window, charCallback);
    }

    double cursorX, cursorY;

    void startFrame() {
        memcpy(lastFrameState, keyState, sizeof(keyState));

        //glfwPollEvents();
        //glfwGetCursorPos(window, &cursorX, &cursorY);
        cursorDelta = glm::vec2(cursorX - cursorLastX, cursorY - cursorLastY);
    }

    void endFrame() {
        //for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; i++) {
        //    lastFrameState[i - GLFW_KEY_SPACE] = glfwGetKey(window, i);
        //}

        //for (int i = 0; i < GLFW_MOUSE_BUTTON_LAST; i++) {
        //    lastFrameMouseState[i] = glfwGetMouseButton(window, i);
        //}

        memset(keyState, 0, sizeof(keyState));

        cursorLastX = cursorX;
        cursorLastY = cursorY;
        currScroll = 0.0f;
    }

    void lockMouse() {
        mouseLocked = true;
#if NDEBUG
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
#endif
    }

    void unlockMouse() {
        mouseLocked = false;
#if NDEBUG
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
#endif
    }

    bool getAction(std::string action) {
        return getAction(entt::HashedString(action.c_str()));
    }

    bool getAction(entt::HashedString action) {
#if !NDEBUG
        assert(keyboardActions.count(action) || mouseActions.count(action));
#endif
        if (keyboardActions.count(action) == 1)
            return keyState[keyboardActions[action] - GLFW_KEY_SPACE];
        // else if (mouseActions.count(action) == 1)
        //     return glfwGetMouseButton(window, mouseActions[action]);
        return false;
    }

    bool getActionDown(std::string action) {
        return getActionDown(entt::HashedString(action.c_str()));
    }

    bool getActionDown(entt::HashedString action) {
#if !NDEBUG
        assert(keyboardActions.count(action) || mouseActions.count(action));
#endif
        if (keyboardActions.count(action) == 1) {
            int key = keyboardActions[action];
            return keyState[key - GLFW_KEY_SPACE] && !lastFrameState[key - GLFW_KEY_SPACE];
        } 
        // else if (mouseActions.count(action) == 1) {
        //     int mouseButton = mouseActions[action];
        //     return glfwGetMouseButton(window, mouseButton) && !lastFrameMouseState[mouseButton];
        // }
        return false;
    }

    bool getActionUp(std::string action) {
        return getActionUp(entt::HashedString(action.c_str()));
    }

    bool getActionUp(entt::HashedString action) {
#if !NDEBUG
        //assert(keyboardActions.count(action) || mouseActions.count(action));
#endif
        if (keyboardActions.count(action) == 1) {
            int key = keyboardActions[action];
            return keyState[key - GLFW_KEY_SPACE] && lastFrameState[key - GLFW_KEY_SPACE];
        }
        // } else if (mouseActions.count(action) == 1) {
        //     int mouseButton = mouseActions[action];
        //     return !glfwGetMouseButton(window, mouseButton) && lastFrameMouseState[mouseButton];
        // }
        return false;
    }

    glm::vec2 getMouseDelta() {
        if (!mouseLocked)
            return glm::vec2(0.0f);
        return cursorDelta;
    }

    float getScrollWheel() {
        return currScroll;
    }

    void getCursorPos(double* x, double* y) {
        (*x) = cursorX;
        (*y) = cursorY;
    }

    void addKeyCallback(KeyCallback callback) {
        keyCallbacks.push_back(callback);
    }

    void addCharCallback(CharCallback callback) {
        charCallbacks.push_back(callback);
    }

    void shutdown() {
    }

    void pushKeyboardEvent(KeyboardInputEvent kie) {
        if(kie.type == KeyboardInputEventType::KeyPress) {
            keyState[kie.key - GLFW_KEY_SPACE] = true;
        } else {
            keyState[kie.key - GLFW_KEY_SPACE] = false;
        }
    }
}
}