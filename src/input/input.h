#pragma once
#include <entt/core/hashed_string.hpp>
#include <functional>
#include <glm/glm.hpp>
#include <string>

typedef struct GLFWwindow GLFWwindow;

namespace halogen
{
namespace input
{
    enum class KeyboardInputEventType {
        KeyPress,
        KeyRelease
    };
    struct KeyboardInputEvent {
        int key;
        KeyboardInputEventType type;
    };
    // key, scancode, action, mods
    typedef std::function<void(int, int, int, int)> KeyCallback;
    typedef std::function<void(unsigned int)> CharCallback;
    void init();
    void startFrame();
    void endFrame();
    void lockMouse();
    void unlockMouse();
    //bool getAction(std::string action);
    bool getAction(entt::HashedString action);
    //bool getActionDown(std::string action);
    bool getActionDown(entt::HashedString action);
    //bool getActionUp(std::string action);
    bool getActionUp(entt::HashedString action);
    glm::vec2 getMouseDelta();
    void getCursorPos(double* x, double* y);
    float getScrollWheel();
    void addKeyCallback(KeyCallback callback);
    void addCharCallback(CharCallback callback);
    void shutdown();
    void pushKeyboardEvent(KeyboardInputEvent kie);
}
}