#pragma once
#include <cstdint>

namespace cosgame {
namespace blocks {
    void CreateBlockAttachment(uint8_t blockId, uint32_t blockEntity);
    void CreateBlockSystems();
}
}