#pragma once
#include <core.h>

namespace cosgame {
namespace blocks {
    class BarrelThrusterSystem : public halogen::core::System {
    public:
        void update(float deltaTime, entt::DefaultRegistry& registry) override;
        std::string getName() override;
    };
}
}