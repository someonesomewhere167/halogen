#include "BarrelThrusterSystem.hpp"
#include "BarrelThruster.hpp"
#include "ModeledBlockComponent.hpp"
#include <DebugDraw.h>
#include <physics.h>

namespace cosgame {
namespace blocks {
    std::string BarrelThrusterSystem::getName() {
        return "BarrelThrusterSystem";
    }

    void BarrelThrusterSystem::update(float deltaTime, entt::DefaultRegistry& registry) {
        registry.view<BarrelThruster, ModeledBlockComponent, halogen::components::Transform, halogen::physics::RigidbodyChildInfo>().each([&](auto ent, BarrelThruster& thruster, ModeledBlockComponent& mbc, halogen::components::Transform& transform, halogen::physics::RigidbodyChildInfo& ci) {
            if (!thruster.currentlyFiring)
                return;

            thruster.timeRemaining -= deltaTime;

            if (thruster.timeRemaining <= 0.0f)
                thruster.currentlyFiring = false;

            halogen::physics::Rigidbody& shipRigidbody = registry.get<halogen::physics::Rigidbody>(mbc.shipEntity);
            halogen::components::Transform& shipTransform = registry.get<halogen::components::Transform>(mbc.shipEntity);
            shipRigidbody.applyForce(transform.transformDirection(glm::vec3(0.0f, 5000.0f, 0.0f)) * deltaTime, transform.position);
            halogen::util::DebugDraw::DrawPoint(transform.transformPoint(ci.localPosition - shipRigidbody.offset), glm::vec3(0.0f, 1.0f, 0.0f));
            halogen::util::DebugDraw::DrawPoint(shipRigidbody.getActualPosition(), glm::vec3(1.0f, 1.0f, 0.0f));
            halogen::util::DebugDraw::DrawLine(transform.position, transform.position + (transform.rotation * glm::vec3(0.0f, 1.0f, 0.0f)));
        });
    }
}
}