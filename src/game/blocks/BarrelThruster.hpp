#pragma once
#include <cstdint>

namespace cosgame {
namespace blocks {
    class BarrelThruster {
    public:
        static void AttachToEntity(uint32_t entity);
        BarrelThruster();
        bool used;
        bool currentlyFiring;
        float timeRemaining;
    };
}
}