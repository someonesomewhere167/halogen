#include "BarrelThruster.hpp"
#include "InteractableComponent.hpp"
#include <core.h>

namespace cosgame {
namespace blocks {
    BarrelThruster::BarrelThruster()
        : used(false)
        , currentlyFiring(false)
        , timeRemaining(5.0f) {
    }

    void BarrelThruster::AttachToEntity(uint32_t entity) {
        auto& thruster = halogen::core::entityRegistry.assign<BarrelThruster>(entity);
        auto& ic = halogen::core::entityRegistry.assign<InteractableComponent>(entity);
        ic.interactText = "Fire Barrel Thruster";
        ic.onInteract = [&](uint32_t, uint32_t ent) {
            if (thruster.used)
                return;
            thruster.currentlyFiring = true;
            thruster.used = true;
        };
    }
}
}