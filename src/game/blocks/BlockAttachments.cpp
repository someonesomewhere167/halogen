#include "BlockAttachments.hpp"
#include "BarrelThruster.hpp"
#include "BarrelThrusterSystem.hpp"
#include <core.h>
#include <cstdint>

namespace cosgame {
namespace blocks {
    void CreateBlockAttachment(uint8_t blockId, uint32_t blockEntity) {
        switch (blockId) {
        case 4:
            BarrelThruster::AttachToEntity(blockEntity);
            break;
        }
    }

    void CreateBlockSystems() {
        halogen::core::registerSystem(std::make_shared<BarrelThrusterSystem>());
    }
}
}