#pragma once

#if defined(_WIN32)
#define HGGAMEAPI __declspec(dllexport)
#else
#define HGGAMEAPI
#endif

HGGAMEAPI void init();
HGGAMEAPI void gameTick(float deltaTime);
HGGAMEAPI void shutdown();