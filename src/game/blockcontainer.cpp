#include "blockcontainer.h"
#include <rapidjson/rapidjson.h>

namespace cosgame {
BlockContainer::BlockContainer()
    : blocks() {
}

void BlockContainer::placeBlock(Block block) {
    assert(blocks.count(block.position) == 0);
    blocks.insert(std::make_pair(block.position, block));
}

void BlockContainer::placeBlock(LargeBlock block) {
    Block standardBlock = { block.id, block.position, block.xRotation, block.yRotation, block.zRotation };
    placeBlock(standardBlock);
    for (int x = 0; x < block.size.x; x++)
        for (int y = 0; y < block.size.y; y++)
            for (int z = 0; z < block.size.z; z++) {
                if (x == 0 && y == 0 && z == 0)
                    continue;

                FillerBlock fillerBlock = { block.id, block.position + Vec3s(x, y, z), block.position };
                fillerBlocks.insert({ fillerBlock.position, fillerBlock });
            }
    largeBlocks.insert({ block.position, block });
}

bool BlockContainer::isSolidBlockAt(Vec3s position) const {
    return blocks.count(position) == 1 && blocks.at(position).id == 1;
}

bool BlockContainer::isBlockAt(Vec3s position) const {
    return blocks.count(position) == 1 || fillerBlocks.count(position) == 1;
}

Block& BlockContainer::getBlockAt(Vec3s position) {
    if (fillerBlocks.count(position) == 1) {
        Block block;
        FillerBlock fillerBlock = fillerBlocks[position];
        block.id = fillerBlock.actualId;
        block.position = fillerBlock.centerPosition;
        return block;
    }

    return blocks.at(position);
}

void BlockContainer::destroyBlockAt(Vec3s position) {
    blocks.erase(position);

    if (blockEntities.count(position) > 0) {
        uint32_t entity = blockEntities.at(position);
        blockEntities.erase(position);
        entityBlocks.erase(entity);
    }

    if (largeBlocks.count(position) > 0) {
        LargeBlock block = largeBlocks.at(position);
        for (int x = 0; x < block.size.x; x++)
            for (int y = 0; y < block.size.y; y++)
                for (int z = 0; z < block.size.z; z++) {
                    if (x == 0 && y == 0 && z == 0)
                        continue;

                    fillerBlocks.erase(block.position + Vec3s(x, y, z));
                }
    }
}

BlockContainer::iterator BlockContainer::begin() {
    return blocks.begin();
}

BlockContainer::iterator BlockContainer::end() {
    return blocks.end();
}

int BlockContainer::getBlockCount() {
    return blocks.size();
}

void BlockContainer::setBlockEntity(Vec3s position, uint32_t entity) {
    blockEntities.insert({ position, entity });
    entityBlocks.insert({ entity, position });
}

uint32_t BlockContainer::getBlockEntity(Vec3s position) {
    return blockEntities[position];
}

Block BlockContainer::getEntityBlock(uint32_t entity) {
    return blocks[entityBlocks[entity]];
}
}