#pragma once
#include <cstdint>
#include <string>

namespace cosgame {
struct Item {
    int intId;
    std::string id;
    std::string name;
    std::string description;
    bool isTool;
};
}