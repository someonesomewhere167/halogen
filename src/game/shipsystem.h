#pragma once
#include <entt/entt.hpp>
#include <core.h>
#include "shipcomponent.h"
#include "block.h"
#include <glm/glm.hpp>

namespace cosgame
{
    namespace systems
    {
        class ShipSystem final : public halogen::core::System
        {
            public:
            void update(float deltaTime, entt::DefaultRegistry &resgistry) override;
			std::string getName() override;
            static void placeBlock(uint32_t shipEntity, cosgame::components::ShipComponent& ship, Block block, glm::vec3 rotation);
            static void destroyBlock(uint32_t shipEntity, cosgame::components::ShipComponent& ship, Vec3s pos);
            static void saveShip(uint32_t shipEntity);
            static void loadShip(std::string shipJson);
        };
    }
}