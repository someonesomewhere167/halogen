#pragma once
#include "InteractableComponent.hpp"
#include "playercomponent.h"
#include <core.h>
#include <iostream>
#include <transform.h>

using namespace halogen;

namespace cosgame {
void onTestInteraction(uint32_t playerEnt) {
    std::cout << "Interacted!"
              << "\n";
    physics::Rigidbody& rigidbody = core::entityRegistry.get<physics::Rigidbody>(playerEnt);
    rigidbody.bulletHandle->applyCentralImpulse(btVector3(0.0f, 100.0f, 0.0f));
}

void setUpTestInteractable() {
    auto entity = core::entityRegistry.create();
    core::entityRegistry.assign<halogen::components::Transform>(entity).position = glm::vec3(-5.0f, 0.0f, -5.0f);
    std::shared_ptr<physics::BoxColliderParameters> p = std::make_shared<physics::BoxColliderParameters>();
    p->halfExtents = glm::vec3(1.0f, 1.0f, 1.0f);
    std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Box, p);

    core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 50.0f, entity).setPosition(glm::vec3(5.0f, 20.0f, 5.0f));

    std::shared_ptr<render::Mesh> mesh = render::loadMesh("cube");
    render::Material* mat = new render::Material();

    mat->color = glm::vec3(1.0f, 1.0f, 1.0f);
    mat->specularPower = 32;
    mat->specularStrength = 0.1f;

    halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);

    InteractableComponent& interactable = core::entityRegistry.assign<InteractableComponent>(entity);
    interactable.onInteract = onTestInteraction;
}
}