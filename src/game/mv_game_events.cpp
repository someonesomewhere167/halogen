#include "publicinc/game_events.h"
#include <Material.hpp>
#include <core.h>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>
#include <input.h>
#include <iostream>
#include <meshloader.h>
#include <physfs.h>
#include <render.h>
#include <string>
#include <transform.h>

uint32_t entity;
using namespace halogen;
uint32_t camEntity;

void createCam() {
    camEntity = core::entityRegistry.create();
    components::Transform& tf = core::entityRegistry.assign<components::Transform>(camEntity);
    render::Camera& c = core::entityRegistry.assign<render::Camera>(camEntity);
    c.fov = 90.0f;
    c.nearPlane = 0.1f;
    c.farPlane = 100.0f;
    c.transform.position = glm::vec3(0.0f, 0.0f, 10.0f);
    tf.position = glm::vec3(0.0f, 0.0f, 10.0f);
}

HGGAMEAPI void init() {
    entity = core::entityRegistry.create();
    components::Transform& tf = core::entityRegistry.assign<components::Transform>(entity);
    tf.position = glm::vec3(0.0f);
    components::Renderable& rndr = core::entityRegistry.assign<components::Renderable>(entity, render::loadMesh("cube"), render::loadShader("standard_pbr"), render::loadMaterial("notex"));
    createCam();
}

std::vector<std::string> splitString(std::string s, std::string delimiter) {
    std::vector<std::string> result;
    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        result.push_back(token);
        s.erase(0, pos + delimiter.length());
    }

    result.push_back(s);

    return result;
}

HGGAMEAPI void gameTick(float deltaTime) {
    static glm::vec2 camRot(0.0f);
    glm::vec2 mouseDelta = input::getMouseDelta();
    camRot += mouseDelta;
    camRot.y = std::clamp(camRot.y, -89.9f, 89.9f);
    render::Camera& cam = core::entityRegistry.get<render::Camera>(camEntity);

    cam.transform.rotation = glm::angleAxis(glm::radians(camRot.y), glm::vec3(1.0f, 0.0f, 0.0f)) * glm::angleAxis(glm::radians(camRot.x), glm::vec3(0.0f, 1.0f, 0.0f));

    glm::vec3 moveAmount(0.0f, 0.0f, 0.0f);
    moveAmount.x -= halogen::input::getAction("left"_hs) ? 1.0f : 0.0f;
    moveAmount.x += halogen::input::getAction("right"_hs) ? 1.0f : 0.0f;
    moveAmount.y += halogen::input::getAction("up") ? 1.0f : 0.0f;
    moveAmount.y -= halogen::input::getAction("down") ? 1.0f : 0.0f;
    moveAmount.z -= halogen::input::getAction("forward"_hs) ? 1.0f : 0.0f;
    moveAmount.z += halogen::input::getAction("backward"_hs) ? 1.0f : 0.0f;

    cam.transform.position += cam.transform.transformDirection(moveAmount * deltaTime);

    ImGui::Begin("Models");
    char** files = PHYSFS_enumerateFiles("mesh");
    char** i;

    PHYSFS_ErrorCode err = PHYSFS_getLastErrorCode();

    if (err != PHYSFS_ERR_OK) {
        std::cerr << "Failed to enumerate mesh folder!"
                  << "\n";
    }

    for (i = files; *i != NULL; i++) {
        if (ImGui::Button(*i)) {
            std::string meshName(*i);

            std::shared_ptr<render::Mesh> mesh = render::loadMesh(splitString(meshName, ".")[0]);

            components::Renderable& r = core::entityRegistry.get<components::Renderable>(entity);
            r.mesh = mesh;
        }
    }
    ImGui::End();
}

HGGAMEAPI void shutdown() {
}