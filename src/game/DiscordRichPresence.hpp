#pragma once
#include <discord_rpc.h>
#include <string>

namespace cosgame {
class DiscordRichPresence {
public:
    DiscordRichPresence(const char* appId);
    ~DiscordRichPresence();
    void updatePresence();
    void init();

private:
    DiscordEventHandlers* handlers;
    static void onReady(const DiscordUser* user);
    static void onJoinRequest(const DiscordUser* request);
    static void onJoinGame(const char* joinSecret);
    bool initialised;
    std::string appId;
};

}