#include "PlayerInputMessage.hpp"

namespace cosgame {
PlayerInputMessage::PlayerInputMessage() {
}

SERIALIZE_IMPL(PlayerInputMessage) {
    serialize_object(stream, identity);
    serialize_object(stream, cmd);

    return true;
}
}
