#include "Chat.hpp"
#include "core.h"
#include "cosinternal.h"
#include <NameComponent.hpp>
#include <Network.hpp>
#include <imgui.h>

namespace cosgame {

template bool ChatMessage::Serialize<yojimbo::ReadStream>(yojimbo::ReadStream& stream);
template bool ChatMessage::Serialize<yojimbo::WriteStream>(yojimbo::WriteStream& stream);
template bool ChatMessage::Serialize<yojimbo::MeasureStream>(yojimbo::MeasureStream& stream);
SERIALIZE_IMPL(ChatMessage) {
    serialize_object(stream, authorIdentity);
    int bufferSize = 1024;
    char* tempBuffer = (char*)std::malloc(bufferSize);
    if (tempBuffer == 0)
        return false;
    memset(tempBuffer, 0, bufferSize);

    if (message.size() > 0 && message.size() < 1024)
        strcpy(tempBuffer, message.c_str());

    serialize_string(stream, tempBuffer, bufferSize);
    message = std::string(tempBuffer);
    std::free(tempBuffer);

    return true;
}

void Chat::Update() {
    ImGui::Begin("Chat");

    for (ReceivedMessage msg : messages) {
        ImGui::Text("%s: %s", msg.authorName.c_str(), msg.text.c_str());
    }

    static char currMsg[256];
    ImGui::InputText("msg", currMsg, 256);
    if (ImGui::Button("Send")) {
        ChatMessage* msg = (ChatMessage*)halogen::networking::GetClient()->CreateMessage((int)COSMessageType::ChatMessage);
        msg->authorIdentity = halogen::networking::GetClient()->GetLocalIdentity();
        msg->message = std::string(currMsg);
        halogen::networking::GetClient()->SendMessage(msg, halogen::networking::Channel::Reliable);
    }

    ImGui::End();
}

void Chat::AddMessage(ChatMessage* msg) {
    std::string playerName = halogen::core::entityRegistry.get<halogen::components::NameComponent>(halogen::networking::GetClient()->GetEntityFromIdentity(msg->authorIdentity)).name;
    messages.push_back({ msg->message, playerName });
}
}