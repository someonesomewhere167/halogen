#include "ItemManager.hpp"
#include <hgio.h>
#include <rapidjson/document.h>
#include <sqlite3.h>

namespace cosgame {
sqlite3* ItemManager::db;
sqlite3_stmt* ItemManager::itemByIntId;
sqlite3_stmt* ItemManager::itemByStrId;
int ItemManager::itemCount;

void sqliteCheck(int rc, sqlite3* db) {
    if (rc == SQLITE_OK)
        return;

    const char* errMsg = sqlite3_errmsg(db);
    sqlite3_close(db);
    throw std::runtime_error("SQLite error: " + std::string(errMsg));
}

void ItemManager::Init() {
    std::cout << "Initialising sqlite " << sqlite3_libversion() << "\n";
    int rc = sqlite3_open("data/items.db", &db);

    if (rc != SQLITE_OK) {
        std::cerr << "Failed to open database: " << sqlite3_errmsg(db) << "\n";
        sqlite3_close(db);
        throw std::runtime_error("Failed to open item database.");
        return;
    }

    rc = sqlite3_prepare_v2(db, "SELECT IntId, StrId, Name, Description FROM Items WHERE IntId = @intid", -1, &itemByIntId, 0);
    sqliteCheck(rc, db);
    rc = sqlite3_prepare_v2(db, "SELECT IntId, StrId, Name, Description FROM Items WHERE StrId = @strid", -1, &itemByStrId, 0);
    sqliteCheck(rc, db);

    sqlite3_stmt* countStmt;
    rc = sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM Items", -1, &countStmt, 0);
    sqliteCheck(rc, db);
    int step = sqlite3_step(countStmt);

    assert(step == SQLITE_ROW);
    itemCount = sqlite3_column_int(countStmt, 0);
    sqlite3_finalize(countStmt);

    std::cout << itemCount << " items in the database.\n";
}

int ItemManager::GetItemCount() {
    return itemCount;
}

Item ItemManager::GetItem(std::string id) {
    int idx = sqlite3_bind_parameter_index(itemByStrId, "@strid");
    int rc = sqlite3_bind_text(itemByStrId, idx, id.c_str(), id.size(), SQLITE_TRANSIENT);
    sqliteCheck(rc, db);
    int step = sqlite3_step(itemByStrId);
    if (step != SQLITE_ROW)
        throw std::runtime_error("Couldn't load item " + id);
    Item item {};
    item.intId = sqlite3_column_int(itemByStrId, 0);
    item.id = std::string((char*)sqlite3_column_text(itemByStrId, 1));
    item.name = std::string((char*)sqlite3_column_text(itemByStrId, 2));
    item.description = std::string((char*)sqlite3_column_text(itemByStrId, 3));
    sqlite3_reset(itemByStrId);
    return item;
}

Item ItemManager::GetItem(int id) {
    int idx = sqlite3_bind_parameter_index(itemByIntId, "@intid");
    int rc = sqlite3_bind_int(itemByIntId, idx, id);
    sqliteCheck(rc, db);
    int step = sqlite3_step(itemByIntId);
    if (step != SQLITE_ROW)
        return Item();
    Item item {};
    item.intId = sqlite3_column_int(itemByIntId, 0);
    item.id = std::string((char*)sqlite3_column_text(itemByIntId, 1));
    item.name = std::string((char*)sqlite3_column_text(itemByIntId, 2));
    item.description = std::string((char*)sqlite3_column_text(itemByIntId, 3));
    sqlite3_reset(itemByIntId);
    return item;
}
}