#pragma once
#include "block.h"
#include "vec3s.h"
#include <unordered_map>

namespace cosgame
{
class BlockContainer
{
public:
    BlockContainer();
    void placeBlock(Block block);
    void placeBlock(LargeBlock block);
    Block& getBlockAt(Vec3s position);
    void destroyBlockAt(Vec3s position);
    int getBlockCount();
    bool isSolidBlockAt(Vec3s position) const;
    bool isBlockAt(Vec3s position) const;
    typedef std::unordered_map<Vec3s, Block>::iterator iterator;
    typedef std::unordered_map<Vec3s, Block>::const_iterator const_iterator;
    iterator begin();
    iterator end();
    void setBlockEntity(Vec3s position, uint32_t entity);
    uint32_t getBlockEntity(Vec3s position);
    Block getEntityBlock(uint32_t entity);
private:
    std::unordered_map<Vec3s, Block> blocks;
    std::unordered_map<Vec3s, LargeBlock> largeBlocks;
    std::unordered_map<Vec3s, FillerBlock> fillerBlocks;
    std::unordered_map<Vec3s, uint32_t> blockEntities;
    std::unordered_map<uint32_t, Vec3s> entityBlocks;
};
}