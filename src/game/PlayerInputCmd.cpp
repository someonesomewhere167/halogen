#include "PlayerInputCmd.hpp"

namespace cosgame {
PlayerInputCmd::PlayerInputCmd()
    : movementAmount()
    , lookVec()
    , sprint(false)
    , jump(false) {
}

// forced template initialisations to fix compiler errors
template bool PlayerInputCmd::Serialize<yojimbo::ReadStream>(yojimbo::ReadStream& stream);
template bool PlayerInputCmd::Serialize<yojimbo::WriteStream>(yojimbo::WriteStream& stream);
template bool PlayerInputCmd::Serialize<yojimbo::MeasureStream>(yojimbo::MeasureStream& stream);

SERIALIZE_IMPL(PlayerInputCmd) {
    serialize_float(stream, movementAmount.x);
    serialize_float(stream, movementAmount.y);
    serialize_float(stream, movementAmount.z);
    serialize_float(stream, lookVec.x);
    serialize_float(stream, lookVec.y);
    serialize_bool(stream, sprint);
    serialize_bool(stream, jump);

    return true;
}
}