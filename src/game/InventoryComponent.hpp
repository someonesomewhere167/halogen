#pragma once
#include "Item.hpp"
#include <vector>

namespace cosgame {
namespace components {
    struct InventoryComponent {
        uint16_t maxItems;
        std::vector<Item> items;
    };
}
}