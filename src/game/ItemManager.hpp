#pragma once
#include "Item.hpp"
#include <sqlite3.h>
#include <string>
#include <unordered_map>

namespace cosgame {
class ItemManager {
public:
    static void Init();
    static int GetItemCount();
    static Item GetItem(std::string id);
    static Item GetItem(int id);

private:
    static sqlite3* db;
    static sqlite3_stmt* itemByIntId;
    static sqlite3_stmt* itemByStrId;
    static int itemCount;
};
}