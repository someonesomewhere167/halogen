#pragma once
#include <cstdint>
#include <functional>

namespace cosgame
{
    struct Vec3s
    {
        Vec3s();
        Vec3s(int16_t x, int16_t y, int16_t z);
        int16_t x, y, z;
        bool operator==(const Vec3s& other) const;
        Vec3s operator+(const Vec3s& other) const;
    };
}

namespace std 
{
    template<> struct hash<cosgame::Vec3s> 
    {
        size_t operator()(const cosgame::Vec3s& vec) const 
        {
            hash<int16_t> hasher;
            return hasher(vec.x) ^ (hasher(vec.y) << 1) ^ (hasher(vec.z) << 2);
        }
    };
}