#pragma once
#include <functional>

namespace cosgame {
struct InteractableComponent {
    std::function<void(uint32_t, uint32_t)> onInteract;
    std::string interactText;
};
}