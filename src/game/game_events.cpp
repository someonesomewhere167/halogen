#include "publicinc/game_events.h"
#include "Chat.hpp"
#include "DiscordRichPresence.hpp"
#include "InteractableComponent.hpp"
#include "InventoryComponent.hpp"
#include "InventoryUI.hpp"
#include "ItemManager.hpp"
#include "NetMessageTypes.hpp"
#include "PlayerInputMessage.hpp"
#include "blocks/BlockAttachments.hpp"
#include "cosinternal.h"
#include "playercomponent.h"
#include "playersystem.h"
#include "shipsystem.h"
#include "terrain/TerrainSystem.hpp"

#include <Light.hpp>
#include <Material.hpp>
#include <NameComponent.hpp>
#include <Network.hpp>
#include <ParticleRender.hpp>
#include <SettingsManager.hpp>
#include <audio.h>
#include <chrono>
#include <core.h>
#include <hgio.h>
#include <imgui.h>
#include <input.h>
#include <iostream>
#include <meshloader.h>
#include <noise/noise.h>
#include <nuklear_draw.hpp>
#include <physics.h>
#include <random>
#include <render.h>
#include <thread>
#include <transform.h>

using namespace cosgame;

cosgame::DiscordRichPresence richPresence("328505624050794496");
std::string username;
std::shared_ptr<TerrainSystem> terrainSystem;

void createLocalPlayer(halogen::networking::PlayerData data) {
    auto playerEntity = halogen::core::entityRegistry.create();

    auto& cam = halogen::core::entityRegistry.assign<halogen::render::Camera>(playerEntity);
    auto& playerComponent = halogen::core::entityRegistry.assign<cosgame::components::PlayerComponent>(playerEntity);
    playerComponent.isLocal = true;
    playerComponent.hasJetpack = false;
    halogen::core::entityRegistry.assign<halogen::components::Transform>(playerEntity).position = glm::vec3(8.0f, terrainSystem->GetHeightAt(glm::vec3(8.0f, 0.0f, 8.0f)) + 3.0f, 8.0f);
    halogen::core::entityRegistry.assign<halogen::components::NameComponent>(playerEntity, username);
    halogen::core::entityRegistry.assign<halogen::audio::Source>(playerEntity).set2d(true);
    components::InventoryComponent& inv = halogen::core::entityRegistry.assign<cosgame::components::InventoryComponent>(playerEntity);
    inv.maxItems = 25;

    halogen::core::entityRegistry.assign<halogen::audio::Listener>(playerEntity);
    halogen::core::entityRegistry.assign<halogen::networking::NetworkIdentity>(playerEntity, data.identity);
    /*halogen::render::Light& li = halogen::core::entityRegistry.assign<halogen::render::Light>(playerEntity);
    li.enabled = true;
    li.type = halogen::render::LightType::Point;
    li.color = glm::vec3(50.0f, 0.0f, 0.0f);*/

    std::shared_ptr<halogen::physics::CapsuleColliderParameters> params = std::make_shared<halogen::physics::CapsuleColliderParameters>();
    params->height = 1.0f;
    params->radius = 0.25f;

    std::shared_ptr<halogen::physics::Collider> collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Capsule, params);
    auto& playerRigidbody = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(playerEntity, collider, 10.0f, playerEntity);
    playerRigidbody.setPosition(glm::vec3(8.0f, terrainSystem->GetHeightAt(glm::vec3(8.0f, 0.0f, 8.0f)) + 3.0f, 8.0f));

    halogen::core::entityRegistry.assign<TerrainLoaderTag>(playerEntity);

    cam.fov = 90.0f;
    cam.nearPlane = 0.01f;
    cam.farPlane = 10000.0f;
}

void createWater() {
    auto entity = halogen::core::entityRegistry.create();
    halogen::core::entityRegistry.assign<halogen::components::NameComponent>(entity, "Water");
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    transform.scale = glm::vec3(20.0f, 1.0f, 20.0f);
    transform.position.y = -5.0f;

    auto mesh = halogen::render::loadMesh("plane");
    auto p = std::make_shared<halogen::physics::MeshColliderParameters>();

    p->isStatic = true;

    p->vertices.reserve(mesh->vertices.size());
    p->indices.reserve(mesh->indices.size());
    for (halogen::render::Vertex vert : mesh->vertices) {
        p->vertices.push_back(vert.pos * transform.scale);
    }

    for (int i : mesh->indices) {
        p->indices.push_back(i);
    }

    auto collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Mesh, p);

    auto& rb = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(entity, collider, 0.0f, entity);
    rb.setPosition(glm::vec3(0.0f, -1.2f, 0.0f));

    if (!halogen::core::isDedicatedServer()) {
        halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, halogen::render::loadShader("water"), halogen::render::loadMaterial("water"));
    }
}

void createJetpack() {
    auto entity = halogen::core::entityRegistry.create();
    halogen::core::entityRegistry.assign<halogen::components::NameComponent>(entity, "Jetpack");
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    halogen::core::entityRegistry.assign<halogen::networking::NetworkIdentity>(entity, halogen::networking::AllocateNetIdentity());

    auto mesh = halogen::render::loadMesh("jetpack_prototype");
    auto p = std::make_shared<halogen::physics::MeshColliderParameters>();

    p->isStatic = false;

    p->vertices.reserve(mesh->vertices.size());
    p->indices.reserve(mesh->indices.size());
    for (halogen::render::Vertex vert : mesh->vertices) {
        p->vertices.push_back(vert.pos * transform.scale);
    }

    for (int i : mesh->indices) {
        p->indices.push_back(i);
    }

    auto collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Mesh, p);

    auto& rb = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(entity, collider, 1.0f, entity);
    rb.setPosition(glm::vec3(5.0f, 70.0f, 10.0f));
    transform.position = glm::vec3(5.0f, 70.0f, 10.0f);

    if (!halogen::core::isDedicatedServer()) {
        auto mesh = halogen::render::loadMesh("jetpack_prototype");
        halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("ship_metal"));
    }

    auto& interactable = halogen::core::entityRegistry.assign<cosgame::InteractableComponent>(entity);
    interactable.onInteract = [](uint32_t interact, uint32_t playerEnt) {
        auto& playerComponent = halogen::core::entityRegistry.get<cosgame::components::PlayerComponent>(playerEnt);
        playerComponent.hasJetpack = true;
        halogen::core::entityRegistry.destroy(interact);
    };
    halogen::core::entityRegistry.assign<halogen::networking::SyncedRigidbody>(entity);
}

void initSystems() {
    std::shared_ptr<systems::PlayerSystem> playerSystem = std::make_shared<systems::PlayerSystem>();
    std::shared_ptr<systems::ShipSystem> shipSystem = std::make_shared<systems::ShipSystem>();
    terrainSystem = std::make_shared<TerrainSystem>();

    halogen::core::registerSystem(playerSystem);
    halogen::core::registerSystem(shipSystem);
    halogen::core::registerSystem(terrainSystem);
    blocks::CreateBlockSystems();
}

void createParticles(glm::vec3 pos) {
    std::random_device rd;
    std::mt19937 e2(rd());
    std::uniform_real_distribution<> dist(-10.0f, 10.0f);

    auto entity = halogen::core::entityRegistry.create();
    auto& emitter = halogen::core::entityRegistry.assign<halogen::render::ParticleEmitter>(entity, 15000);
    for (int i = 0; i < 15000; i++) {
        emitter.particlePositions[i] = pos;
        emitter.particleSpeeds[i] = glm::vec3(dist(e2), 100.0f, dist(e2));
        emitter.particleLifetimes[i] = 2.0f;
    }
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    transform.position = pos;
}

void createDirLight() {
    auto entity = halogen::core::entityRegistry.create();
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    transform.rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
    auto& light = halogen::core::entityRegistry.assign<halogen::render::Light>(entity);
    light.type = halogen::render::LightType::Directional;
    light.posDir = glm::vec3(0.5f, 0.5f, 0.0f);
    light.color = glm::vec3(3.14f);
    light.enabled = true;
    light.shadowsEnabled = true;
}

void createPointLight() {
    auto entity = halogen::core::entityRegistry.create();
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    transform.position = glm::vec3(0.0f, 80.6f, 0.0f);
    auto& light = halogen::core::entityRegistry.assign<halogen::render::Light>(entity);
    light.type = halogen::render::LightType::Point;
    light.color = glm::vec3(0.0f, 0.0f, 314.f);
    light.enabled = true;
}

void startGame() {
    initSystems();
    richPresence.updatePresence();
    createDirLight();
    char** ships = PHYSFS_enumerateFiles("savedShips");
    char** i;
    for (i = ships; *i != NULL; i++) {
        std::cout << "Detected saved ship " << *i << "\n";
        std::string shipJson = halogen::util::loadFileToString(std::string("savedShips/") + *i);
        systems::ShipSystem::loadShip(shipJson);
    }

    //createPointLight();
    //createParticles(glm::vec3(0.0f, terrainSystem->GetHeightAt(glm::vec3(0.0f)), 0.0f));
    //createParticles(glm::vec3(5.0f, 80.0f, 0.0f));
    //createParticles(glm::vec3(-5.0f, 80.0f, 0.0f));
}

enum UIState {
    MainMenu,
    ConnectionDialog,
    Connecting,
    Connected,
    Disconnected,
    CantConnect
};

UIState uiState;

void serverInit() {
    startGame();

    halogen::networking::GetServer()->SetCreatePlayerCallback([=](halogen::networking::PlayerData data) {
        auto playerEntity = halogen::core::entityRegistry.create();

        auto& playerComponent = halogen::core::entityRegistry.assign<cosgame::components::PlayerComponent>(playerEntity);
        playerComponent.isLocal = false;
        //playerComponent.hasJetpack = false;
        halogen::core::entityRegistry.assign<halogen::components::Transform>(playerEntity).position = glm::vec3(8.0f, 100.0f, 8.0f);
        halogen::core::entityRegistry.assign<halogen::components::NameComponent>(playerEntity, "Player " + data.playerName);
        halogen::core::entityRegistry.assign<cosgame::components::InventoryComponent>(playerEntity);
        halogen::core::entityRegistry.assign<halogen::networking::NetworkIdentity>(playerEntity, data.identity);
        halogen::core::entityRegistry.assign<halogen::networking::SyncedRigidbody>(playerEntity);

        std::shared_ptr<halogen::physics::CapsuleColliderParameters> params = std::make_shared<halogen::physics::CapsuleColliderParameters>();
        params->height = 1.0f;
        params->radius = 0.25f;

        std::shared_ptr<halogen::physics::Collider> collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Capsule, params);
        auto& playerRigidbody = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(playerEntity, collider, 10.0f, playerEntity);
        playerRigidbody.setPosition(glm::vec3(8.0f, 100.0f, 8.0f));
    });

    std::cout << "Server ready."
              << "\n";
}

void clientInit() {
    halogen::input::lockMouse();
    uiState = UIState::MainMenu;

    halogen::networking::GetClient()->SetCreatePlayerCallback([=](halogen::networking::PlayerData data) {
        auto playerEntity = halogen::core::entityRegistry.create();

        auto& playerComponent = halogen::core::entityRegistry.assign<cosgame::components::PlayerComponent>(playerEntity);
        playerComponent.isLocal = false;
        //playerComponent.hasJetpack = false;
        halogen::core::entityRegistry.assign<halogen::components::Transform>(playerEntity).position = glm::vec3(0.0f, 40.0f, 0.0f);
        halogen::core::entityRegistry.assign<halogen::components::NameComponent>(playerEntity, data.playerName);
        halogen::core::entityRegistry.assign<cosgame::components::InventoryComponent>(playerEntity);
        halogen::core::entityRegistry.assign<halogen::networking::NetworkIdentity>(playerEntity, data.identity);
        halogen::core::entityRegistry.assign<halogen::networking::SyncedRigidbody>(playerEntity);

        std::shared_ptr<halogen::physics::CapsuleColliderParameters> params = std::make_shared<halogen::physics::CapsuleColliderParameters>();
        params->height = 1.0f;
        params->radius = 0.25f;

        std::shared_ptr<halogen::physics::Collider> collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Capsule, params);
        auto& playerRigidbody = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(playerEntity, collider, 10.0f, playerEntity);
        playerRigidbody.setPosition(glm::vec3(0.0f, 40.0f, 0.0f));

        auto mesh = halogen::render::loadMesh("PHDrone");
        halogen::core::entityRegistry.assign<halogen::components::Renderable>(playerEntity, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("grass"));
    });

    halogen::networking::GetClient()->SetLocalCreatePlayerCallback(createLocalPlayer);
}

void clientNetworkUpdate() {
    static bool gameStarted = false;
    if (halogen::networking::GetClient()->GetState() == halogen::networking::ClientState::Connected && !gameStarted) {
        gameStarted = true;
        startGame();

        ItemManager::Init();
    }

    if (halogen::networking::GetClient()->GetState() == halogen::networking::ClientState::ConnectionFailed && uiState != UIState::CantConnect) {
        uiState = UIState::CantConnect;
    }

    if (halogen::networking::GetClient()->GetState() == halogen::networking::ClientState::NotConnected && gameStarted && uiState != UIState::Disconnected) {
        uiState = UIState::Disconnected;
    }
}

static bool menuOpen = false;
static bool firstFrame = true;

void clientUpdate(float deltaTime) {
    if (firstFrame) {
        firstFrame = false;
    }

    if (halogen::input::getActionDown("reloadShaders"_hs)) {
        halogen::render::reloadShaders();
        halogen::render::reloadMaterials();
    }

    if (halogen::input::getActionDown("openMenu"_hs)) {
        menuOpen = !menuOpen;
        if (menuOpen) {
            halogen::input::unlockMouse();
            //fullscreenPanel->CallVueFunction("openMenu");
        } else {
            halogen::input::lockMouse();
            //fullscreenPanel->CallVueFunction("closeMenu");
        }
    }

    halogen::core::entityRegistry.view<halogen::render::Light, halogen::components::Transform>().each([deltaTime](uint32_t ent, halogen::render::Light& light, halogen::components::Transform& transform) {
        transform.rotation *= glm::quat(glm::radians(glm::vec3(1.0f, 1.0f, 0.0f) * deltaTime));
        light.posDir = transform.transformDirection(glm::vec3(0.0f, 0.0f, 1.0f));
    });

    cosgame::ui::InventoryUI::Draw();
}

Chat chat;

class COSMessageProvider : public halogen::networking::ICustomMessageProvider {
public:
    int GetMessageTypeCount() override {
        return (int)COSMessageType::Count;
    }

    yojimbo::Message* CreateMessage(int type, yojimbo::Allocator& allocator) {
        switch ((COSMessageType)type) {
        case COSMessageType::PlayerInput:
            return YOJIMBO_NEW(allocator, PlayerInputMessage);
        case COSMessageType::ChatMessage:
            return YOJIMBO_NEW(allocator, ChatMessage);
        default:
            return nullptr;
        }
    }

    bool HandleMessage(int type, yojimbo::Message* message) {
        switch ((COSMessageType)type) {
        case COSMessageType::PlayerInput: {
            PlayerInputMessage* pim = (PlayerInputMessage*)message;
            assert(halogen::core::isDedicatedServer());
            uint32_t entity = halogen::networking::GetServer()->GetEntityFromIdentity(pim->identity);
            components::PlayerComponent& pc = halogen::core::entityRegistry.get<components::PlayerComponent>(entity);
            if (pc.lastInputCmd.jump)
                pim->cmd.jump = true;
            pc.lastInputCmd = pim->cmd;

            return true;
        } break;
        case COSMessageType::ChatMessage: {
            if (halogen::core::isDedicatedServer()) {
                std::cout << "Received chat message"
                          << "\n";
                halogen::networking::GetServer()->SendToAll([&](int cidx) -> yojimbo::Message* {
                    ChatMessage* chtMsg = (ChatMessage*)halogen::networking::GetServer()->CreateMessage(cidx, (int)COSMessageType::ChatMessage);
                    chtMsg->authorIdentity = ((ChatMessage*)message)->authorIdentity;
                    chtMsg->message = ((ChatMessage*)message)->message;
                    std::cout << "Created chat message for " << cidx << "\n";
                    return chtMsg;
                },
                    halogen::networking::Channel::Reliable, -5);
            } else {
                chat.AddMessage((ChatMessage*)message);
            }
            return true;
        } break;
        default: {
            return false;
        } break;
        }
    }
};

bool singleplayer = true;
HGGAMEAPI void init() {
    if (!singleplayer) {
        std::vector<halogen::networking::ICustomMessageProvider*> providers = { new COSMessageProvider };
        halogen::networking::Init(halogen::core::isDedicatedServer(), providers);
        if (halogen::core::isDedicatedServer())
            serverInit();
        else {
            richPresence.init();
            clientInit();
        }
    } else {
        ItemManager::Init();
        if (halogen::core::isDedicatedServer())
            throw std::runtime_error("A singleplayer dedicated server doesn't make any sense.");
        richPresence.init();
        startGame();
        createLocalPlayer(halogen::networking::PlayerData());
        halogen::input::lockMouse();
        //halogen::audio::Clip* clip = new halogen::audio::Clip("mus3.ogg", true);
        auto ent = halogen::core::entityRegistry.create();
        //auto& source = halogen::core::entityRegistry.assign<halogen::audio::Source>(ent);
        //auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(ent);
        //source.setClip(clip);
        //source.play();
        //source.set2d(true);
    }
}

HGGAMEAPI void gameTick(float deltaTime) {
    if (singleplayer) {
        clientUpdate(deltaTime);
        return;
    }

    halogen::networking::Update(deltaTime);
    if (!halogen::core::isDedicatedServer()) {
        if (halogen::input::getAction("devDamage"_hs)) {
            //fullscreenPanel->SetVueValue("health", 50);
            //fullscreenPanel->SetVueValue("isDeveloper", true);
            /*for (int i = 0; i < ItemManager::GetItemCount(); i++)
            {
                ultralight::JSObject object;
                object["name"] = itemData.name.c_str();
                object["id"] = itemData.id;
                object["description"] = itemData.description.c_str();
                fullscreenPanel->CallVueFunction("addDevInvItem", { (ultralight::JSValue)object });
            }*/
        }
        clientUpdate(deltaTime);
        clientNetworkUpdate();
        chat.Update();
    } else {
        static double prevTime = glfwGetTime();
        double currTime = glfwGetTime();
        double deltaTime = currTime - prevTime;
        prevTime = currTime;
        const int SERVER_TPS = 64;
        constexpr double TPS_TIME = 1.0 / (double)SERVER_TPS;
        // limit framerate
        double frameTimeMs = deltaTime * 1000.0;
        if (frameTimeMs < TPS_TIME)
            std::this_thread::sleep_for(std::chrono::milliseconds((int)(TPS_TIME - frameTimeMs)));
    }
}

HGGAMEAPI void shutdown() {
    halogen::core::entityRegistry.view<components::ShipComponent>().each([](uint32_t ent, auto& shipComponent) {
        cosgame::systems::ShipSystem::saveShip(ent);
    });
    if (!singleplayer)
        halogen::networking::Shutdown();
}