#include "playersystem.h"
#include "InteractableComponent.hpp"
#include "InventoryUI.hpp"
#include "ModeledBlockComponent.hpp"
#include "PlayerInputMessage.hpp"
#include "cosinternal.h"
#include "nanovg.h"
#include "shipcomponent.h"
#include "shipsystem.h"
#include <DebugDraw.h>
#include <InventoryComponent.hpp>
#include <ItemManager.hpp>
#include <Material.hpp>
#include <NameComponent.hpp>
#include <algorithm>
#include <audio.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>
#include <input.h>
#include <iostream>
#include <meshloader.h>
#include <nuklear_draw.hpp>
#include <nuklear_ws.h>
#include <physics.h>
#include <render.h>

namespace cosgame {
namespace systems {
    std::shared_ptr<halogen::audio::Clip> blockPlaceClip;

    enum class CrosshairType {
        Normal,
        Cross,
        Interaction,
        Dot
    };

    struct PlayerUIInfo {
        PlayerUIInfo()
            : inventoryOpen(false)
            , pauseMenuOpen(false)
            , type(CrosshairType::Normal) {}
        bool inventoryOpen;
        bool pauseMenuOpen;
        CrosshairType type;
    };

    struct LocalPlayerInfo {
        LocalPlayerInfo()
            : blockPlaceGuide(-1)
            , guideCreated(false) {}
        uint32_t blockPlaceGuide;
        bool guideCreated;
    };

    PlayerUIInfo localUIInfo;
    LocalPlayerInfo localPlayerInfo;
    void updateLocalPlayerUI(float deltaTime);

    void PlayerSystem::update(float deltaTime, entt::DefaultRegistry& registry) {
        halogen::core::entityRegistry.view<components::PlayerComponent>().each([=](const auto ent, components::PlayerComponent& player) {
            updatePlayer(player, ent, deltaTime);
        });
    }

    std::string PlayerSystem::getName() {
        return "PlayerSystem";
    }

    void PlayerSystem::updatePlayer(components::PlayerComponent& player, uint32_t entity, float deltaTime) {
        player.isGrounded = isPlayerGrounded(entity);

        if (player.isLocal) {
            if (!localPlayerInfo.guideCreated) {
                localPlayerInfo.blockPlaceGuide = halogen::core::entityRegistry.create();

                halogen::core::entityRegistry.assign<halogen::components::Transform>(localPlayerInfo.blockPlaceGuide).scale = glm::vec3(0.5f);
                halogen::core::entityRegistry.assign<halogen::components::Renderable>(
                    localPlayerInfo.blockPlaceGuide,
                    halogen::render::loadMesh("cube"),
                    halogen::render::loadShader("place_guide"),
                    halogen::render::loadMaterial("place_guide"));
                localPlayerInfo.guideCreated = true;
                blockPlaceClip = std::make_shared<halogen::audio::Clip>("blockplace.ogg", false);
            }

            updateLocalPlayer(player, entity, deltaTime);
        }

        if (halogen::core::isDedicatedServer()) {
            updatePlayerServer(player, entity, deltaTime);
        }
    }

    bool PlayerSystem::isPlayerGrounded(uint32_t playerEntity) {
        halogen::physics::Rigidbody& rb = halogen::core::entityRegistry.get<halogen::physics::Rigidbody>(playerEntity);
        halogen::components::Transform& transform = halogen::core::entityRegistry.get<halogen::components::Transform>(playerEntity);
        glm::vec3 from = rb.getPosition() - transform.transformDirection(glm::vec3(0.0f, 0.5f, 0.0f));
        glm::vec3 to = rb.getPosition() - transform.transformDirection(glm::vec3(0.0f, 1.0f, 0.0f));

        return halogen::physics::raycast(
            from,
            to)
            .hasHit();
    }

    void PlayerSystem::updatePlayerServer(components::PlayerComponent& player, uint32_t entity, float deltaTime) {
        halogen::components::Transform& transform = halogen::core::entityRegistry.get<halogen::components::Transform>(entity);

        halogen::physics::Rigidbody& rigidbody = halogen::core::entityRegistry.get<halogen::physics::Rigidbody>(entity);
        player.lookVec = player.lastInputCmd.lookVec;

        glm::quat bodyQuat = glm::angleAxis(glm::radians(player.lookVec.x), glm::vec3(0.0f, 1.0f, 0.0f));
        rigidbody.setRotation(bodyQuat);
        transform.rotation = bodyQuat;

        float moveSpeed = 5.0f;

        if (player.lastInputCmd.sprint) {
            moveSpeed = 10.0f;
        }

        if (player.lastInputCmd.jump && player.isGrounded) {
            rigidbody.bulletHandle->applyImpulse(btVector3(0.0f, 50.0f, 0.0f), btVector3(0.0f, -1.0f, 0.0f));
            // this is part of a janky workaround to make sure that
            // the player always jumps when commands received/frame > server tps
            player.lastInputCmd.jump = false;
        }

        glm::vec3 moveAmount = player.lastInputCmd.movementAmount;
        if (glm::length2(moveAmount) > 0.0f)
            moveAmount = glm::normalize(moveAmount);

        glm::vec3 targetVelocity = transform.transformDirection(moveAmount);
        targetVelocity *= moveSpeed;
        glm::vec3 velocity = rigidbody.getVelocity();
        glm::vec3 velocityChange = targetVelocity - velocity;
        glm::vec3 maxVelocityChange(0.5f, 0.0f, 0.5f);

        if (!player.isGrounded && !player.noclip)
            maxVelocityChange *= 0.25f;

        velocityChange = glm::clamp(velocityChange, -maxVelocityChange, maxVelocityChange);

        if (glm::length2(velocityChange) > 0.0f)
            rigidbody.bulletHandle->applyCentralImpulse(halogen::physics::glm2bt(velocityChange * rigidbody.getMass()));
    }

    bool crosshairSet = false;
    void setCrosshairType(CrosshairType type) {
        crosshairSet = true;
        localUIInfo.type = type;
    }

    void PlayerSystem::updateLocalPlayer(components::PlayerComponent& player, uint32_t entity, float deltaTime) {
#if IMGUI_ENABLED
        ImGui::Begin("Player");
#endif

        components::InventoryComponent& playerInv = halogen::core::entityRegistry.get<components::InventoryComponent>(entity);
        if (player.heldToolDrawn && getHeldTool(player, playerInv).id == "BlockTool")
            setCrosshairType(CrosshairType::Dot);

        halogen::render::Camera& cam = halogen::core::entityRegistry.get<halogen::render::Camera>(entity);

        glm::vec2 mouseDelta = halogen::input::getMouseDelta();
        player.lookVec += mouseDelta;
        player.lookVec.y = std::clamp(player.lookVec.y, -89.9f, 89.9f);

        glm::quat lookQuat = glm::angleAxis(glm::radians(player.lookVec.y), glm::vec3(1.0f, 0.0f, 0.0f)) * glm::angleAxis(glm::radians(player.lookVec.x), glm::vec3(0.0f, 1.0f, 0.0f));
        cam.transform.rotation = lookQuat;

        halogen::components::Transform& transform = halogen::core::entityRegistry.get<halogen::components::Transform>(entity);
        halogen::physics::Rigidbody& rigidbody = halogen::core::entityRegistry.get<halogen::physics::Rigidbody>(entity);

        glm::quat bodyQuat = glm::angleAxis(glm::radians(player.lookVec.x), glm::vec3(0.0f, 1.0f, 0.0f));
        rigidbody.setRotation(bodyQuat);
        transform.rotation = bodyQuat;

        if ((player.jetpackActive && player.hasJetpack) || player.noclip) {
            if (halogen::input::getAction("jump"_hs)) {
                rigidbody.bulletHandle->applyForce(btVector3(0.0f, 250.0f, 0.0f), btVector3(0.0f, 0.0f, 0.0f));
            }
        } else {
            if (halogen::input::getActionDown("jump"_hs) && player.isGrounded) {
                rigidbody.bulletHandle->applyImpulse(btVector3(0.0f, 50.0f, 0.0f), btVector3(0.0f, -1.0f, 0.0f));
            }
        }

        glm::vec3 moveAmount(0.0f, 0.0f, 0.0f);
        moveAmount.x -= halogen::input::getAction("left"_hs) ? 1.0f : 0.0f;
        moveAmount.x += halogen::input::getAction("right"_hs) ? 1.0f : 0.0f;

        //moveAmount.y += halogen::input::getAction("up") ? 1.0f : 0.0f;
        //moveAmount.y -= halogen::input::getAction("down") ? 1.0f : 0.0f;

        moveAmount.z -= halogen::input::getAction("forward"_hs) ? 1.0f : 0.0f;
        moveAmount.z += halogen::input::getAction("backward"_hs) ? 1.0f : 0.0f;
        if (!(player.jetpackActive && player.hasJetpack) && !player.noclip) {
            float moveSpeed = 5.0f;

            if (halogen::input::getAction("sprint"_hs))
                moveSpeed = 10.0f;

            if (glm::length2(moveAmount) > 0.0f)
                moveAmount = glm::normalize(moveAmount);

            glm::vec3 targetVelocity = transform.transformDirection(moveAmount);
            targetVelocity *= moveSpeed;

            glm::vec3 velocity = rigidbody.getVelocity();
            glm::vec3 velocityChange = targetVelocity - velocity;
            glm::vec3 maxVelocityChange(0.75f, 0.0f, 0.75f);

            if (!player.isGrounded && !player.noclip)
                maxVelocityChange *= 0.25f;

            velocityChange = glm::clamp(velocityChange, -maxVelocityChange, maxVelocityChange);

            if (glm::length2(velocityChange) > 0.0f)
                rigidbody.bulletHandle->applyCentralImpulse(halogen::physics::glm2bt(velocityChange * rigidbody.getMass()));
#if IMGUI_ENABLED
            ImGui::DragFloat3("Velocity:", &velocity.x);
            ImGui::DragFloat3("Velocity Change:", &velocityChange.x);
            ImGui::DragFloat3("Move Amount:", &moveAmount.x);
            ImGui::Checkbox("Grounded:", &player.isGrounded);
            ImGui::DragFloat("Move Speed:", &moveSpeed);
            ImGui::DragFloat3("Position:", &transform.position.x);
#endif
        } else {
            glm::vec3 velocity = rigidbody.getVelocity();
            rigidbody.bulletHandle->applyCentralForce(halogen::physics::glm2bt(transform.transformDirection(moveAmount * 100.0f)));
#if IMGUI_ENABLED
            ImGui::DragFloat3("Velocity:", &velocity.x);
            ImGui::DragFloat3("Move Amount:", &moveAmount.x);
            ImGui::Checkbox("Grounded:", &player.isGrounded);
            ImGui::DragFloat3("Position:", &transform.position.x);
#endif
        }

        cam.transform.position = rigidbody.getPosition() + transform.transformDirection(glm::vec3(0.0f, 0.6f, 0.0f));
        rigidbody.bulletHandle->activate(true);

        glm::vec3 to = cam.transform.position + cam.transform.transformDirection(glm::vec3(0.0f, 0.0f, -5.0f));
        glm::vec3 from = cam.transform.position;

        if (halogen::input::getActionDown("placeBlock"_hs)) {
            auto& as = halogen::core::entityRegistry.get<halogen::audio::Source>(entity);
            as.setClip(&*blockPlaceClip);
            as.playOverlapped();
            as.setLooping(false);
        }

        buildUpdate(player, halogen::input::getActionDown("placeBlock"_hs), halogen::input::getActionDown("breakBlock"_hs), from, to);
        interactUpdate(player, entity, from, to, halogen::input::getActionDown("interact"_hs));

        if (halogen::input::getActionDown("noclip"_hs) && player.hasJetpack) {
            player.jetpackActive = !player.jetpackActive;
            if (player.noclip)
                rigidbody.bulletHandle->setCollisionFlags(rigidbody.bulletHandle->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
            else
                rigidbody.bulletHandle->setCollisionFlags(rigidbody.bulletHandle->getCollisionFlags() & ~btCollisionObject::CF_NO_CONTACT_RESPONSE);
        }

        if (halogen::input::getActionDown("nextSlot"_hs)) {
            player.selectedIndex++;
            if (player.selectedIndex > 4) {
                player.selectedIndex = 0;
            }
        }

        if (halogen::input::getActionDown("openInventory"_hs)) {
            player.inventoryOpen = !player.inventoryOpen;
            if (player.inventoryOpen) {
                ui::InventoryUI::OpenInventory(playerInv);
            } else {
                ui::InventoryUI::CloseCurrentInventory();
            }
        }

        glm::vec3& blR = player.currentBlockRotation;

        blR.x += halogen::input::getActionDown("blockRotatePX") * 90.0f;
        blR.x -= halogen::input::getActionDown("blockRotateNX") * 90.0f;

        blR.y += halogen::input::getActionDown("blockRotatePY") * 90.0f;
        blR.y -= halogen::input::getActionDown("blockRotateNY") * 90.0f;

        blR.z += halogen::input::getActionDown("blockRotatePZ") * 90.0f;
        blR.z -= halogen::input::getActionDown("blockRotateNZ") * 90.0f;

#if IMGUI_ENABLED
        ImGui::End();
#endif

        updateLocalPlayerUI(deltaTime);
        if (!crosshairSet)
            localUIInfo.type = CrosshairType::Normal;
        crosshairSet = false;

        if (!halogen::networking::GetClient())
            return;

        PlayerInputMessage* pim = (cosgame::PlayerInputMessage*)halogen::networking::GetClient()->CreateMessage((int)COSMessageType::PlayerInput);
        pim->identity = halogen::core::entityRegistry.get<halogen::networking::NetworkIdentity>(entity);
        PlayerInputCmd cmd;
        cmd.jump = halogen::input::getActionDown("jump"_hs);
        cmd.sprint = halogen::input::getAction("sprint"_hs);
        cmd.lookVec = player.lookVec;
        cmd.movementAmount = moveAmount;
        pim->cmd = cmd;
        halogen::networking::GetClient()->SendMessage(pim, halogen::networking::Channel::Unreliable);
    }

    float specialRound(float x) {
        if (x > 0.f) {
            return std::floor(x);
        } else {
            return std::ceil(x);
        }
    }

    glm::vec3 specialRound(glm::vec3 x) {
        return glm::vec3(specialRound(x.x), specialRound(x.y), specialRound(x.z));
    }

    void PlayerSystem::buildUpdate(components::PlayerComponent& player, bool placeBlock, bool breakBlock, glm::vec3 from, glm::vec3 to) {
        halogen::components::Renderable& renderable = halogen::core::entityRegistry.get<halogen::components::Renderable>(localPlayerInfo.blockPlaceGuide);

        btCollisionWorld::ClosestRayResultCallback result = halogen::physics::raycast(from, to);
        if (!result.hasHit()) {

            halogen::components::Transform& guideTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(localPlayerInfo.blockPlaceGuide);
            guideTransform.rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
            guideTransform.position = to;

            if (placeBlock) {
                uint32_t newShipEnt = halogen::core::entityRegistry.create();
                halogen::components::Transform& shipTransform = halogen::core::entityRegistry.assign<halogen::components::Transform>(newShipEnt);

                shipTransform.position = to;
                halogen::core::entityRegistry.assign<halogen::components::NameComponent>(newShipEnt, "Placed Grid");
                auto& sc = halogen::core::entityRegistry.assign<cosgame::components::ShipComponent>(newShipEnt);

                std::shared_ptr<halogen::render::Mesh> mesh = std::make_shared<halogen::render::Mesh>();
                halogen::core::entityRegistry.assign<halogen::components::Renderable>(newShipEnt, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("ship_metal"));

                Block block;
                block.id = 1;
                block.position = Vec3s(0, 0, 0);

                systems::ShipSystem::placeBlock(newShipEnt, sc, block, player.currentBlockRotation);
            }
            return;
        }

        if (!placeBlock && !breakBlock) {
            renderable.draw = true;
            halogen::components::Transform& guideTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(localPlayerInfo.blockPlaceGuide);
            glm::vec3 hitPoint(result.m_hitPointWorld.x(), result.m_hitPointWorld.y(), result.m_hitPointWorld.z());
            glm::vec3 normal(result.m_hitNormalWorld.x(), result.m_hitNormalWorld.y(), result.m_hitNormalWorld.z());
            if (result.m_collisionObject == nullptr)
                return;
            uint32_t entity = result.m_collisionObject->getUserIndex();
            if (!halogen::core::entityRegistry.valid(entity))
                return;
            bool isModeledBlock = halogen::core::entityRegistry.has<ModeledBlockComponent>(entity);
            bool isShip = halogen::core::entityRegistry.has<cosgame::components::ShipComponent>(entity);
            if (isModeledBlock || isShip) {
                uint32_t scEntity = isModeledBlock ? halogen::core::entityRegistry.get<ModeledBlockComponent>(entity).shipEntity : entity;
                auto shipTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(scEntity);

                normal = glm::round(normal);
                glm::vec3 blockPos = (hitPoint + (normal * 0.5f));
                blockPos = shipTransform.inverseTransformPoint(blockPos);
                blockPos = glm::floor(blockPos);

                guideTransform.position = blockPos + glm::vec3(0.5f, 0.5f, 0.5f);
                guideTransform.rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
                glm::mat4 shipMatrix = shipTransform.calculateMatrix();
                glm::mat4 guideMatrix = shipMatrix * guideTransform.calculateMatrix();

                glm::vec3 scale;
                glm::quat rotation;
                glm::vec3 translation;
                glm::vec3 skew;
                glm::vec4 perspective;

                glm::decompose(guideMatrix, scale, rotation, translation, skew, perspective);
                guideTransform.rotation = rotation;
                guideTransform.position = translation;
            } else {
                guideTransform.position = hitPoint;
                guideTransform.rotation = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
            }
            return;
        }

        if (breakBlock) {
            glm::vec3 hitPoint = halogen::physics::bt2glm(result.m_hitPointWorld);
            glm::vec3 normal = halogen::physics::bt2glm(result.m_hitNormalWorld);

            uint32_t entity = result.m_collisionObject->getUserIndex();
            bool isModeledBlock = halogen::core::entityRegistry.has<ModeledBlockComponent>(entity);
            bool isShip = halogen::core::entityRegistry.has<cosgame::components::ShipComponent>(entity);
            if (isShip) {
                auto& ship = halogen::core::entityRegistry.get<components::ShipComponent>(entity);
                auto shipTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(entity);

                normal = glm::round(normal);
                glm::vec3 blockPos = (hitPoint - (normal * 0.5f));
                blockPos = shipTransform.inverseTransformPoint(blockPos);
                blockPos = glm::floor(blockPos);
                blockPos.z += 1;

                ship.blocks->destroyBlockAt(Vec3s(blockPos.x, blockPos.y, blockPos.z));
                ship.meshDirty = true;
            } else if (isModeledBlock) {
                auto& mbc = halogen::core::entityRegistry.get<ModeledBlockComponent>(entity);
                auto& ship = halogen::core::entityRegistry.get<cosgame::components::ShipComponent>(mbc.shipEntity);
                auto& shipTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(mbc.shipEntity);
                glm::vec3 blockPosF = shipTransform.inverseTransformPoint(halogen::core::entityRegistry.get<halogen::components::Transform>(entity).position) - glm::vec3(0.5f, 0.5f, -0.5f);
                Vec3s blockPos(std::round(blockPosF.x), std::round(blockPosF.y), std::round(blockPosF.z));
                std::cout << "Destroying block at " << blockPos.x << "," << blockPos.y << "," << blockPos.z << "\n";
                ShipSystem::destroyBlock(mbc.shipEntity, ship, blockPos);
                ship.meshDirty = true;
            }
        }

        if (placeBlock) {
            glm::vec3 hitPoint = halogen::physics::bt2glm(result.m_hitPointWorld);
            glm::vec3 normal = halogen::physics::bt2glm(result.m_hitNormalWorld);

            uint32_t entity = result.m_collisionObject->getUserIndex();
            bool isModeledBlock = halogen::core::entityRegistry.has<ModeledBlockComponent>(entity);
            bool isShip = halogen::core::entityRegistry.has<cosgame::components::ShipComponent>(entity);
            if (isModeledBlock || isShip) {
                // Placing a new block on an existing ship
                uint32_t scEntity = isModeledBlock ? halogen::core::entityRegistry.get<ModeledBlockComponent>(entity).shipEntity : entity;
                auto& ship = halogen::core::entityRegistry.get<components::ShipComponent>(scEntity);
                auto shipTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(scEntity);
                cosgame::Block block;
                block.id = player.selectedIndex + 1;

                normal = glm::round(normal);
                glm::vec3 blockPos = (hitPoint + (normal * 0.5f));
                blockPos = shipTransform.inverseTransformPoint(blockPos);
                blockPos = glm::floor(blockPos);
                blockPos.z += 1;

                block.position = Vec3s(blockPos.x, blockPos.y, blockPos.z);

                systems::ShipSystem::placeBlock(scEntity, ship, block, player.currentBlockRotation);
                ship.meshDirty = true;
            } else {
                // Static grid in ground
                uint32_t newShipEnt = halogen::core::entityRegistry.create();
                halogen::components::Transform& shipTransform = halogen::core::entityRegistry.assign<halogen::components::Transform>(newShipEnt);

                shipTransform.position = halogen::physics::bt2glm(result.m_hitPointWorld) - glm::vec3(0.5f, 0.5f, -0.5f);
                halogen::core::entityRegistry.assign<halogen::components::NameComponent>(newShipEnt, "Placed Grid");
                auto& sc = halogen::core::entityRegistry.assign<cosgame::components::ShipComponent>(newShipEnt);
                sc.isStatic = true;

                std::shared_ptr<halogen::render::Mesh> mesh = std::make_shared<halogen::render::Mesh>();
                halogen::core::entityRegistry.assign<halogen::components::Renderable>(newShipEnt, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("ship_metal"));

                Block block;
                block.id = 1;
                block.position = Vec3s(0, 0, 0);

                systems::ShipSystem::placeBlock(newShipEnt, sc, block, player.currentBlockRotation);
            }
        }
    }

    void PlayerSystem::interactUpdate(components::PlayerComponent& player, uint32_t playerEnt, glm::vec3 from, glm::vec3 to, bool interact) {
        btCollisionWorld::ClosestRayResultCallback result = halogen::physics::raycast(from, to);

        if (result.hasHit()) {
            uint32_t interactedEnt = result.m_collisionObject->getUserIndex();
#if IMGUI_ENABLED
            ImGui::Text("Currently looking at %i", interactedEnt);
#endif

            if (halogen::core::entityRegistry.has<InteractableComponent>(interactedEnt)) {
                localUIInfo.type = CrosshairType::Interaction;
                if (interact) {
                    InteractableComponent& interactable = halogen::core::entityRegistry.get<InteractableComponent>(interactedEnt);
                    interactable.onInteract(interactedEnt, playerEnt);
                }
            }
        }
    }

    Item PlayerSystem::getTool(int index, components::InventoryComponent& inventory) {
        int currentToolIdx = 0;
        for (Item i : inventory.items) {
            if (i.isTool) {
                if (index == currentToolIdx)
                    return i;
                currentToolIdx++;
            }
        }

        return Item();
    }

    Item PlayerSystem::getHeldTool(components::PlayerComponent& player, components::InventoryComponent& inventory) {
        assert(player.heldToolDrawn);
        return getTool(player.selectedIndex, inventory);
    }

    void updateLocalPlayerUI(float deltaTime) {
        static float totalTime = 0.0f;
        totalTime += deltaTime;
        glm::ivec2 screenSize = halogen::render::getCurrentScreenSize();
        nk_context* ctx = halogen::render::getNkContext();

        // Draw crosshair
        float crosshairSize = 15.0f;

        float barWidth = 5.0f;
        float centerSize = 30.0f;

        float xCenterSize = centerSize + (sin(totalTime * 5.0f) * 10.0f);

        NVGcontext* vCtx = halogen::render::getVGContext();
        glm::ivec2 screenCenter = halogen::render::getCurrentScreenSize() / glm::ivec2(2, 2);

        switch (localUIInfo.type) {
        case CrosshairType::Normal: {
            nvgBeginPath(vCtx);
            nvgFillColor(vCtx, nvgRGBA(0, 127, 255, 255));

            // Left side - right edge: center + centerSize. left edge: center + centerSize + crosshairSize;
            nvgRect(vCtx, screenCenter.x - centerSize - crosshairSize, screenCenter.y - (barWidth * 0.5f), crosshairSize, barWidth);
            // Right side
            nvgRect(vCtx, screenCenter.x + centerSize, screenCenter.y - (barWidth * 0.5f), crosshairSize, barWidth);

            // Top
            nvgRect(vCtx, screenCenter.x - (barWidth * 0.5f), screenCenter.y - centerSize - crosshairSize, barWidth, crosshairSize);
            // Bottom
            nvgRect(vCtx, screenCenter.x - (barWidth * 0.5f), screenCenter.y + centerSize, barWidth, crosshairSize);

            // Center Square
            // nvgRect(vCtx, screenCenter.x - 5.0f, screenCenter.y - 5.0f, 10.f, 10.f);

            nvgFill(vCtx);
            break;
        }
        case CrosshairType::Cross: {
            nvgBeginPath(vCtx);
            nvgTranslate(vCtx, screenCenter.x, screenCenter.y);
            nvgRotate(vCtx, nvgDegToRad(45.0f));

            nvgRect(vCtx, -xCenterSize, -(barWidth * 0.5f), xCenterSize * 2, barWidth);
            nvgRect(vCtx, -(barWidth * 0.5f), -xCenterSize, barWidth, xCenterSize * 2);

            nvgFillColor(vCtx, nvgRGBA(255, 0, 0, 255));
            nvgFill(vCtx);
            nvgResetTransform(vCtx);
            break;
        }
        case CrosshairType::Interaction: {
            nvgBeginPath(vCtx);
            nvgTranslate(vCtx, screenCenter.x, screenCenter.y);
            nvgRotate(vCtx, nvgDegToRad(45.0f));

            nvgRect(vCtx, -xCenterSize * 0.75f, -(barWidth * 0.5f), xCenterSize * 2 * 0.75f, barWidth);
            nvgRect(vCtx, -(barWidth * 0.5f), -xCenterSize * 0.75f, barWidth, xCenterSize * 2 * 0.75f);

            nvgFillColor(vCtx, nvgRGBA(0, 127, 255, 255));
            nvgFill(vCtx);
            nvgResetTransform(vCtx);

            nvgBeginPath(vCtx);
            nvgTranslate(vCtx, screenCenter.x, screenCenter.y);

            nvgRect(vCtx, -xCenterSize, -(barWidth * 0.5f), xCenterSize * 2, barWidth);
            nvgRect(vCtx, -(barWidth * 0.5f), -xCenterSize, barWidth, xCenterSize * 2);

            nvgFillColor(vCtx, nvgRGBA(0, 127, 255, 255));
            nvgFill(vCtx);
            nvgResetTransform(vCtx);
            break;
        }
        case CrosshairType::Dot: {
            nvgBeginPath(vCtx);
            nvgFillColor(vCtx, nvgRGBA(0, 127, 255, 255));
            nvgRect(vCtx, screenCenter.x - 5.0f, screenCenter.y - 5.0f, 10.f, 10.f);
            nvgFill(vCtx);
            break;
        }
        }

        nvgBeginPath(vCtx);
        nvgFillColor(vCtx, nvgRGB(100, 0, 0));
        nvgRoundedRect(vCtx, 5.0f, screenSize.y - 35.f, 510.f, 30.f, 5.0f);
        nvgFill(vCtx);

        nvgBeginPath(vCtx);
        nvgFillColor(vCtx, nvgRGB(255, 0, 0));
        nvgRect(vCtx, 10.0f, screenSize.y - 30.f, 500.f, 20.f);
        nvgFill(vCtx);
    }
}
}