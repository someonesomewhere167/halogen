#pragma once
#include "InventoryComponent.hpp"

namespace cosgame {
namespace ui {
    namespace InventoryUI {
        void OpenInventory(components::InventoryComponent& inventory);
        void CloseCurrentInventory();
        bool HasInventoryOpen();
        void Draw();
    }
}
}
