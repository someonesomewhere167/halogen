#include "DiscordRichPresence.hpp"
#include "InventoryComponent.hpp"
#include "TestInteractable.hpp"
#include "playercomponent.h"
#include "playersystem.h"
#include "publicinc/game_events.h"
#include "shipcomponent.h"
#include "shipsystem.h"
#include <DebugDraw.h>
#include <NameComponent.hpp>
#include <algorithm>
#include <audio.h>
#include <core.h>
#include <glm/gtx/quaternion.hpp>
#include <input.h>
#include <iostream>
#include <meshloader.h>
#include <physics.h>
#include <random>
#include <render.h>

using namespace halogen;
using namespace cosgame;
audio::Source* musicSource;
audio::Clip* gloryClip;
//audio::Clip* musicClips[3];
//float musicTimer = 0.0f;
std::random_device rd;
std::mt19937 rng(rd());
int ups[2];
uint32_t playerEntity;
std::unique_ptr<cosgame::DiscordRichPresence> richPresence;

void buildPhysicsEnt() {
    auto entity = core::entityRegistry.create();
    core::entityRegistry.assign<halogen::components::Transform>(entity);
    std::shared_ptr<physics::BoxColliderParameters> p = std::make_shared<physics::BoxColliderParameters>();
    p->halfExtents = glm::vec3(2.0f, 2.0f, 2.0f);
    std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Box, p);

    core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 1.0f, entity).setPosition(glm::vec3(5.0f, 20.0f, 5.0f));

    std::shared_ptr<render::Mesh> mesh = render::loadMesh("cube");
    render::Material* mat = new render::Material();

    mat->color = glm::vec3(1.0f, 1.0f, 1.0f);
    mat->specularPower = 32;
    mat->specularStrength = 0.1f;

    halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);
}

void buildPhysicsEntMesh() {
    auto entity = core::entityRegistry.create();
    core::entityRegistry.assign<halogen::components::Transform>(entity);
    std::shared_ptr<physics::MeshColliderParameters> p = std::make_shared<physics::MeshColliderParameters>();

    std::shared_ptr<render::Mesh> mesh = render::loadMesh("diamond");

    p->isStatic = false;

    p->vertices.reserve(mesh->vertices.size());
    p->indices.reserve(mesh->indices.size());
    for (render::Vertex vert : mesh->vertices) {
        p->vertices.push_back(vert.pos);
    }

    for (int i : mesh->indices) {
        p->indices.push_back(i);
    }

    std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Mesh, p);

    core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 0.0f, entity).setPosition(glm::vec3(0.0f, 20.0f, 0.0f));

    render::Material* mat = new render::Material();

    mat->color = glm::vec3(0.0f, 0.0f, 0.75f);
    mat->specularPower = 32;
    mat->specularStrength = 0.1f;

    halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);
}

void buildPhysicsEntGround() {
    auto entity = core::entityRegistry.create();
    core::entityRegistry.assign<halogen::components::NameComponent>(entity, "Ground");
    core::entityRegistry.assign<halogen::components::Transform>(entity);
    std::shared_ptr<physics::MeshColliderParameters> p = std::make_shared<physics::MeshColliderParameters>();

    std::shared_ptr<render::Mesh> mesh = render::loadMesh("terraintest");

    p->isStatic = true;

    p->vertices.reserve(mesh->vertices.size());
    p->indices.reserve(mesh->indices.size());
    for (render::Vertex vert : mesh->vertices) {
        p->vertices.push_back(vert.pos);
    }

    for (int i : mesh->indices) {
        p->indices.push_back(i);
    }

    std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Mesh, p);

    physics::Rigidbody& rb = core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 0.0f, entity);
    rb.setPosition(glm::vec3(0.0f, -10.0f, 0.0f));
    //rb.updateCollider();
    //rb.updateCollider();

    if (!core::isDedicatedServer()) {

        render::Material* mat = new render::Material();

        mat->color = glm::vec3(1.f, 1.f, 1.f);
        mat->specularPower = 32;
        mat->specularStrength = 0.0f;
        mat->albedo = render::loadTexture("grass.png");
        mat->tiling = glm::vec2(50.0f, 50.0f);

        halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);
    }
}

void buildPhysicsEntGroundFlat() {
    auto entity = core::entityRegistry.create();
    auto transform = core::entityRegistry.assign<halogen::components::Transform>(entity);

    std::shared_ptr<physics::BoxColliderParameters> p = std::make_shared<physics::BoxColliderParameters>();
    p->halfExtents = glm::vec3(25.0f, 1.0f, 25.0f);
    std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Box, p);

    core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 0.0f, entity).setPosition(glm::vec3(0.0f, -10.0f, 0.0f));

    transform.scale = glm::vec3(50.0f, 1.0f, 50.0f);

    std::shared_ptr<render::Mesh> mesh = render::loadMesh("cube");
    render::Material* mat = new render::Material();

    mat->color = glm::vec3(1.0f, 1.0f, 1.0f);
    mat->specularPower = 32;
    mat->specularStrength = 0.1f;

    halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);
}

void buildPhysicsEntSphere() {
    for (int x = 0; x < 5; x++)
        for (int y = 0; y < 1; y++)
            for (int z = 0; z < 5; z++) {
                auto entity = core::entityRegistry.create();
                auto transform = core::entityRegistry.assign<halogen::components::Transform>(entity);

                std::shared_ptr<physics::SphereColliderParameters> p = std::make_shared<physics::SphereColliderParameters>();
                p->radius = 1.0f;
                std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Sphere, p);

                core::entityRegistry.assign<physics::Rigidbody>(entity, collider, 10.0f, entity).setPosition(glm::vec3(x, y + 10.0f, z));

                if (!core::isDedicatedServer()) {
                    std::shared_ptr<render::Mesh> mesh = render::loadMesh("sphere");
                    render::Material* mat = new render::Material();

                    mat->color = glm::vec3(1.0f, 1.0f, 1.0f);
                    mat->specularPower = 32;
                    mat->specularStrength = 0.1f;

                    halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);
                }
            }
}

void buildSizeRefRenderEnt() {
    auto entity = core::entityRegistry.create();
    halogen::components::Transform& transform = core::entityRegistry.assign<halogen::components::Transform>(entity);
    transform.position = glm::vec3(2.0f, -8.0f, 2.0f);
    core::entityRegistry.assign<halogen::components::NameComponent>(entity, "Size Reference");

    if (!core::isDedicatedServer()) {
        std::shared_ptr<render::Mesh> mesh = render::loadMesh("person_size_ref");
        render::Material* mat = new render::Material();

        mat->color = glm::vec3(1.0f, 1.0f, 1.0f);
        mat->specularPower = 32;
        mat->specularStrength = 0.1f;

        halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, render::loadShader("standard"), mat);
    }
}

HGGAMEAPI void init() {
    /*musicClips[0] = new halogen::audio::Clip("mus1.ogg", true);
	musicClips[1] = new halogen::audio::Clip("mus1.ogg", true);
	musicClips[2] = new halogen::audio::Clip("mus3.ogg", true);    */
    //gloryClip = new halogen::audio::Clip("glory.ogg", true);
    if (!core::isDedicatedServer())
        musicSource = new audio::Source();
    //musicSource->setClip(gloryClip);
    //musicSource->setPosition(glm::vec3(0.0f, 0.0f, 0.0f));
    //musicSource->setGain(0.001f);
    //musicSource->play();

    std::shared_ptr<systems::ShipSystem> shipSystem = std::make_shared<systems::ShipSystem>();
    core::registerSystem(shipSystem);
    std::shared_ptr<systems::PlayerSystem> playerSystem = std::make_shared<systems::PlayerSystem>();
    core::registerSystem(playerSystem);

    for (int i = 0; i < 1; i++) {
        //break;
        auto ent = core::entityRegistry.create();
        auto sc = core::entityRegistry.assign<cosgame::components::ShipComponent>(ent);

        for (int x = 0; x < 1; x++)
            for (int y = 0; y < 1; y++)
                for (int z = 0; z < 1; z++) {
                    Block block;
                    block.id = 1;
                    block.position = Vec3s(x, y, z);
                    sc.blocks->placeBlock(block);
                }

        core::entityRegistry.assign<halogen::components::Transform>(ent).position = glm::vec3(0.0f, 0.0f, -15.0f);

        core::entityRegistry.assign<halogen::components::NameComponent>(ent, "Grid " + std::to_string(i));

        if (!core::isDedicatedServer()) {
            render::Material* shipMaterial = new render::Material();

            shipMaterial->color = glm::vec3(0.75f, 0.75f, 0.75f);
            shipMaterial->specularPower = 256;
            shipMaterial->specularStrength = 1.0f;
            shipMaterial->albedo = render::loadTexture("white.png");
            shipMaterial->normal = render::loadTexture("ship_normal.png");

            std::shared_ptr<render::Mesh> mesh = std::make_shared<render::Mesh>();
            halogen::core::entityRegistry.assign<halogen::components::Renderable>(ent, mesh, render::loadShader("standard"), shipMaterial);
        }
    }

    if (!core::isDedicatedServer()) {
        playerEntity = core::entityRegistry.create();
        render::Camera& cam = core::entityRegistry.assign<render::Camera>(playerEntity);
        core::entityRegistry.assign<cosgame::components::PlayerComponent>(playerEntity).isLocal = true;
        core::entityRegistry.assign<halogen::components::Transform>(playerEntity);
        core::entityRegistry.assign<halogen::components::NameComponent>(playerEntity, "Player");
        core::entityRegistry.assign<cosgame::components::InventoryComponent>(playerEntity);

        std::shared_ptr<physics::CapsuleColliderParameters> params = std::make_shared<physics::CapsuleColliderParameters>();
        params->height = 1.0f;
        params->radius = 0.25f;

        std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Capsule, params);
        physics::Rigidbody& playerRigidbody = core::entityRegistry.assign<physics::Rigidbody>(playerEntity, collider, 10.0f, playerEntity);
        playerRigidbody.bulletHandle->setAngularFactor(btVector3(0, 1, 0));
        playerRigidbody.bulletHandle->setFriction(1.0f);

        cam.fov = 90.0f;
        cam.nearPlane = 0.01f;
        cam.farPlane = 1000.0f;
        cam.transform.position = glm::vec3(-5.0f, -5.0f, 10.0f);
        input::lockMouse();
    }
    //buildPhysicsEnt();
    //buildPhysicsEntMesh();
    buildPhysicsEntGroundFlat();
    //buildPhysicsEntSphere();
    buildSizeRefRenderEnt();

    richPresence = std::make_unique<cosgame::DiscordRichPresence>();
    setUpTestInteractable();
}

HGGAMEAPI void gameTick(float deltaTime) {
    /*musicTimer += deltaTime;
    if(musicTimer > 100.0f)
    {
        musicTimer = 0.0f;
        if(!musicSource->isPlaying())
        {
            std::uniform_int_distribution<int> uni(0, 2);
            auto songIndex = uni(rng);

            musicSource->setClip(musicClips[songIndex]);
            musicSource->play();
        }
    }*/
    if (!core::isDedicatedServer()) {
        if (input::getActionDown("unlockMouse")) {
            input::unlockMouse();
        }

        if (input::getActionDown("lockMouse")) {
            input::lockMouse();
        }

        if (input::getActionDown("reloadShaders")) {
            render::reloadShaders();
        }
    }

    //halogen::util::DebugDraw::DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(10.0f, -10.0f, 10.0f));
    //halogen::util::DebugDraw::DrawPoint(glm::vec3(0.0f, 0.0f, 0.0f));
    //halogen::util::DebugDraw::DrawPoint(glm::vec3(10.0f, -10.0f, 10.0f));
}

HGGAMEAPI void shutdown() {
    delete musicSource;
}