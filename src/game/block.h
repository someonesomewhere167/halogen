#pragma once
#include "vec3s.h"
#include <cstdint>
#include <glm/glm.hpp>

namespace cosgame
{
const uint8_t FILLER_ID = 255;

enum class RotationState : char
{
	Zero = 0,
	Ninety = 1 << 0,
	HundredEighty = 1 << 1,
	TwoSeventy = 1 << 2
};

struct Block
{
    uint8_t id;
    Vec3s position;
    RotationState xRotation;
    RotationState yRotation;
    RotationState zRotation;
};

struct LargeBlock
{
    uint8_t id;
    Vec3s position;
    Vec3s size;
    RotationState xRotation;
    RotationState yRotation;
    RotationState zRotation;
};

struct FillerBlock
{
    uint8_t actualId;
    Vec3s position;
    Vec3s centerPosition;
};
}