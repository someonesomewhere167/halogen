#pragma once
#include <glm/glm.hpp>
#include "PlayerInputCmd.hpp"

namespace cosgame
{
namespace components
{
    struct PlayerComponent
    {
        PlayerComponent()
            : isLocal(false)
            , isGrounded(false)
            , hasJetpack(false)
            , noclip(false)
            , inventoryOpen(false)
            , selectedIndex(0)
            , lookVec(0.0f)
            , jetpackActive(false)
			, currentBlockRotation(0.0f)
            , heldToolDrawn(false) {};
        bool isLocal;
        bool isGrounded;
        bool hasJetpack;
        bool jetpackActive;
        bool noclip;
        bool inventoryOpen;
        bool heldToolDrawn;
        int selectedIndex;
        glm::vec2 lookVec;
        glm::vec3 currentBlockRotation;
        PlayerInputCmd lastInputCmd;
    };
}
}