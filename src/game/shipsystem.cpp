#include "shipsystem.h"
#include "ModeledBlockComponent.hpp"
#include "block.h"
#include "blocks/BlockAttachments.hpp"
#include "shipcomponent.h"
#include "transform.h"
#include <DebugDraw.h>
#include <Material.hpp>
#include <chrono>
#include <cstring>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <hgio.h>
#include <iostream>
#include <meshloader.h>
#include <physics.h>
#include <queue>
#include <random>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/reader.h>
#include <rapidjson/writer.h>
#include <render.h>
#include <thread>

using namespace halogen;

struct FinishedMeshMessage {
    FinishedMeshMessage(std::shared_ptr<render::Mesh> newMesh, uint32_t entity)
        : newMesh(newMesh)
        , entity(entity) {
    }
    std::shared_ptr<render::Mesh> newMesh;
    uint32_t entity;
};

std::queue<FinishedMeshMessage> finishedMeshMessages;

void addTriangle(std::shared_ptr<halogen::render::Mesh> mesh, int index0, int index1, int index2) {
    mesh->indices.push_back(index0);
    mesh->indices.push_back(index1);
    mesh->indices.push_back(index2);
}

void buildQuad(std::shared_ptr<halogen::render::Mesh> mesh, glm::vec3 offset, glm::vec3 widthDir, glm::vec3 lengthDir) {
    halogen::render::Vertex quadVerts[4];
    quadVerts[0].uv = glm::vec2(1.0f, 0.0f);
    quadVerts[1].uv = glm::vec2(0.0f, 0.0f);
    quadVerts[2].uv = glm::vec2(0.0f, 1.0f);
    quadVerts[3].uv = glm::vec2(1.0f, 1.0f);

    quadVerts[0].pos = offset;
    quadVerts[1].pos = offset + lengthDir;
    quadVerts[2].pos = offset + lengthDir + widthDir;
    quadVerts[3].pos = offset + widthDir;

    glm::vec3 norm = glm::cross(widthDir, lengthDir);
    for (int i = 0; i < 4; i++)
        quadVerts[i].normal = norm;

    glm::vec3 edge1 = quadVerts[1].pos - quadVerts[0].pos;
    glm::vec3 edge2 = quadVerts[2].pos - quadVerts[0].pos;
    glm::vec2 deltaUV1 = quadVerts[1].uv - quadVerts[0].uv;
    glm::vec2 deltaUV2 = quadVerts[2].uv - quadVerts[0].uv;

    float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

    glm::vec3 tangent;

    tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
    tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
    tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
    tangent = glm::normalize(tangent);

    for (int i = 0; i < 4; i++)
        quadVerts[i].tangent = tangent;

    int baseIndex = mesh->vertices.size();

    addTriangle(mesh, baseIndex, baseIndex + 2, baseIndex + 1);
    addTriangle(mesh, baseIndex, baseIndex + 3, baseIndex + 2);

    for (int i = 0; i < 4; i++)
        mesh->vertices.push_back(quadVerts[i]);
}

void remeshShip(cosgame::components::ShipComponent& shipComponent, halogen::components::Renderable& renderable, uint32_t entity) {
    while (shipComponent.currentlyMeshing) {
    }
    shipComponent.currentlyMeshing = true;
    shipComponent.meshDirty = false;

    std::shared_ptr<halogen::render::Mesh> newMesh = std::make_shared<halogen::render::Mesh>();

    newMesh->dynamic = true;

    glm::vec3 forward(0.0f, 0.0f, -1.0f);
    glm::vec3 right(1.0f, 0.0f, 0.0f);
    glm::vec3 up(0.0f, 1.0f, 0.0f);

    cosgame::BlockContainer* shipBlocks = shipComponent.blocks;

    for (auto it : *shipBlocks) {
        cosgame::Block block = it.second;
        cosgame::Vec3s pos = it.first;

        glm::vec3 nearCorner(pos.x, pos.y, pos.z);
        glm::vec3 farCorner(pos.x + 1.0f, pos.y + 1.0f, pos.z + -1.0f);

        if (block.id > 1)
            continue;

        if (!shipBlocks->isSolidBlockAt(pos + cosgame::Vec3s(0, 1, 0)))
            buildQuad(newMesh, farCorner, -right, -forward); // top

        if (!shipBlocks->isSolidBlockAt(pos + cosgame::Vec3s(0, -1, 0)))
            buildQuad(newMesh, nearCorner, forward, right); // bottom

        if (!shipBlocks->isSolidBlockAt(pos + cosgame::Vec3s(0, 0, 1)))
            buildQuad(newMesh, nearCorner, right, up); // front

        if (!shipBlocks->isSolidBlockAt(pos + cosgame::Vec3s(0, 0, -1)))
            buildQuad(newMesh, farCorner, -up, -right); // back

        if (!shipBlocks->isSolidBlockAt(pos + cosgame::Vec3s(1, 0, 0)))
            buildQuad(newMesh, farCorner, -forward, -up); // right

        if (!shipBlocks->isSolidBlockAt(pos + cosgame::Vec3s(-1, 0, 0)))
            buildQuad(newMesh, nearCorner, up, forward); // left
    }

    FinishedMeshMessage mm(newMesh, entity);
    finishedMeshMessages.push(mm);
    shipComponent.currentlyMeshing = false;
}

void regenerateShipCollisions(std::shared_ptr<physics::Collider> collider, cosgame::components::ShipComponent& shipComponent, components::Transform& transform, physics::Rigidbody& rb) {
    if (collider->type != halogen::physics::ColliderType::Compound)
        throw new std::runtime_error("Incorrect collider type!");

    std::shared_ptr<physics::CompoundColliderParameters> cParams = std::static_pointer_cast<physics::CompoundColliderParameters>(collider->parameters);

    cParams->subColliders.clear();
    cParams->subColliders.reserve(shipComponent.blocks->getBlockCount());

    glm::vec3 com(0.0f);

    for (auto block : *shipComponent.blocks) {
        cosgame::Vec3s blockPos = block.first;
        btVector3 btBlockPos((float)blockPos.x + 0.5f, (float)blockPos.y + 0.5f, (float)blockPos.z - 0.5f);

        com += glm::vec3(blockPos.x, blockPos.y, blockPos.z) + glm::vec3(0.5f, 0.5f, 0.5f);

        if (block.second.id > 1)
            continue;

        std::shared_ptr<physics::BoxColliderParameters> newCParams = std::make_shared<physics::BoxColliderParameters>();
        newCParams->halfExtents = glm::vec3(0.5f, 0.5f, 0.5f);
        physics::SubCollider blockCollider(physics::ColliderType::Box, newCParams);

        blockCollider.transform.setOrigin(btBlockPos);
        cParams->subColliders.push_back(blockCollider);
    }

    com /= shipComponent.blocks->getBlockCount();

    if (shipComponent.isStatic)
        rb.setMass(0.0f);
    else
        rb.setMass(shipComponent.blocks->getBlockCount() * 10.0f);
    rb.updateCollider();
}

void generateShipCollisions(uint32_t entity, cosgame::components::ShipComponent& shipComponent, entt::DefaultRegistry& registry) {
    std::shared_ptr<physics::CompoundColliderParameters> cParams = std::make_shared<physics::CompoundColliderParameters>();
    halogen::components::Transform& transform = registry.get<halogen::components::Transform>(entity);

    glm::vec3 com(0.0f);

    for (auto block : *shipComponent.blocks) {
        cosgame::Vec3s blockPos = block.first;
        btVector3 btBlockPos((float)blockPos.x + 0.5f, (float)blockPos.y + 0.5f, (float)blockPos.z - 0.5f);

        com += glm::vec3(blockPos.x + 0.5f, blockPos.y + 0.5f, blockPos.z + 0.5f);

        if (block.second.id > 1)
            continue;

        std::shared_ptr<physics::BoxColliderParameters> newCParams = std::make_shared<physics::BoxColliderParameters>();
        newCParams->halfExtents = glm::vec3(0.5f, 0.5f, 0.5f);
        physics::SubCollider blockCollider(physics::ColliderType::Box, newCParams);

        blockCollider.transform.setOrigin(btBlockPos);
        cParams->subColliders.push_back(blockCollider);
    }

    com /= shipComponent.blocks->getBlockCount();

    std::shared_ptr<physics::Collider> collider = std::make_shared<physics::Collider>(physics::ColliderType::Compound, cParams);

    physics::Rigidbody& rb = registry.assign<physics::Rigidbody>(entity, collider, shipComponent.isStatic ? 0.0f : shipComponent.blocks->getBlockCount() * 10.0f, entity);

    rb.setPosition(transform.position);
}

namespace cosgame {
namespace systems {
    std::string ShipSystem::getName() {
        return "ShipSystem";
    }

    void ShipSystem::update(float, entt::DefaultRegistry& registry) {
        registry.view<components::ShipComponent>().each([&](const auto entity,
                                                            components::ShipComponent& shipComponent) {
            halogen::components::Transform& transform = registry.get<halogen::components::Transform>(entity);
            util::DebugDraw::DrawPoint(transform.position, glm::vec3(0.0f, 0.0f, 1.0f));
            if (registry.has<physics::Rigidbody>(entity)) {
                physics::Rigidbody& rb = registry.get<physics::Rigidbody>(entity);
                util::DebugDraw::DrawPoint(rb.getActualPosition(), glm::vec3(1.0f, 0.0f, 0.0f));
            }

            if (shipComponent.meshDirty) {
                if (shipComponent.blocks->getBlockCount() == 0) {
                    registry.destroy(entity);
                    return;
                }

                if (!core::isDedicatedServer()) {
                    // workaround: this should be [&, =entity], buuut the vs compiler reports an internal error
                    std::thread([=, &shipComponent, &registry] { 
							uint32_t ent = entity;  
							remeshShip(shipComponent, (halogen::components::Renderable&)registry.get<halogen::components::Renderable>(ent), ent); }).detach();
                }

                if (!registry.has<physics::Rigidbody>(entity)) {
                    generateShipCollisions(entity, shipComponent, registry);
                } else {
                    physics::Rigidbody& rb = registry.get<physics::Rigidbody>(entity);
                    regenerateShipCollisions(rb.collider, shipComponent, transform, rb);
                }

                shipComponent.meshDirty = false;
            }
        });

        while (finishedMeshMessages.size() > 0) {
            FinishedMeshMessage mm = finishedMeshMessages.front();
            bool hasRenderable = halogen::core::entityRegistry.has<halogen::components::Renderable>(mm.entity);
            if (!hasRenderable && finishedMeshMessages.size() > 1)
                continue;
            else if (!hasRenderable)
                break;
            halogen::components::Renderable& renderable = halogen::core::entityRegistry.get<halogen::components::Renderable>(mm.entity);

            render::uploadMesh(mm.newMesh);
            renderable.mesh.swap(mm.newMesh);

            finishedMeshMessages.pop();
        }
    }

    bool approxEqual(float a, float b) {
        float diff = a - b;
        return abs(diff) < 0.01f;
    }

    RotationState angleToRotState(float angle) {
        if (angle < 0.0f) {
            angle = fmod(angle, 360.f);
            angle = 360.f - abs(angle);
        } else {
            angle = fmod(angle, 360.0f);
        }
        if (approxEqual(angle, 0.f))
            return RotationState::Zero;
        if (approxEqual(angle, 90.f))
            return RotationState::Ninety;
        if (approxEqual(angle, 180.f))
            return RotationState::HundredEighty;
        if (approxEqual(angle, 270.f))
            return RotationState::TwoSeventy;
    }

    Block setBlockRotation(glm::vec3 rot, Block block) {
        block.xRotation = angleToRotState(rot.x);
        block.yRotation = angleToRotState(rot.y);
        block.zRotation = angleToRotState(rot.z);
        return block;
    }

    void ShipSystem::placeBlock(uint32_t shipEntity, cosgame::components::ShipComponent& ship, Block block, glm::vec3 rotation) {
        if (ship.blocks->isBlockAt(block.position))
            return;
        block = setBlockRotation(rotation, block);
        rapidjson::Document blockDocument;
        blockDocument.Parse(halogen::util::loadFileToString("blocks/" + std::to_string(block.id) + ".json").c_str());

        // solid block
        if (block.id == 1) {
            ship.blocks->placeBlock(block);
            return;
        }

        rapidjson::Value& sizeVal = blockDocument["blockSize"];
        glm::vec3 size(sizeVal["x"].GetFloat(), sizeVal["y"].GetFloat(), sizeVal["z"].GetFloat());

        if (size.x > 1.05f || size.y > 1.05f || size.z > 1.05f) {
            LargeBlock largeBlock;
            largeBlock.id = block.id;
            largeBlock.position = block.position;
            largeBlock.size = Vec3s(size.x, size.y, size.z);
            ship.blocks->placeBlock(largeBlock);
        } else {
            ship.blocks->placeBlock(block);
        }

        std::string modelName(blockDocument["model"].GetString());
        std::string materialName(blockDocument["material"].GetString());
        std::string shaderName(blockDocument["shader"].GetString());

        const auto shipTransform = core::entityRegistry.get<halogen::components::Transform>(shipEntity);
        auto entity = core::entityRegistry.create();
        auto& transform = core::entityRegistry.assign<halogen::components::Transform>(entity);

        transform.position = shipTransform.position + (glm::vec3(block.position.x, block.position.y, block.position.z) + glm::vec3(0.5f, 0.5f, -0.5f));
        transform.rotation = glm::quat(glm::radians(rotation)) * shipTransform.rotation;

        if (!core::isDedicatedServer()) {
            auto& renderable = core::entityRegistry.assign<halogen::components::Renderable>(entity, render::loadMesh(modelName), render::loadShader(shaderName), render::loadMaterial(materialName));
        }

        auto colliderArray = blockDocument["colliders"].GetArray();

        assert(colliderArray.Size() != 0);

        auto cParams = std::make_shared<physics::CompoundColliderParameters>();

        for (int i = 0; i < colliderArray.Size(); i++) {
            rapidjson::Value& colliderVal = colliderArray[i];

            if (strcmp(colliderVal["type"].GetString(), "box") == 0) {
                auto params = std::make_shared<physics::BoxColliderParameters>();
                rapidjson::Value& sizeVal = colliderVal["size"];
                params->halfExtents = glm::vec3(sizeVal["x"].GetFloat(), sizeVal["y"].GetFloat(), sizeVal["z"].GetFloat()) * 0.5f;
                physics::SubCollider collider(physics::ColliderType::Box, std::static_pointer_cast<physics::ColliderParameters>(params));
                if (colliderVal.HasMember("offset")) {
                    rapidjson::Value& offsetVal = colliderVal["offset"];
                    collider.transform.setOrigin(btVector3(offsetVal["x"].GetFloat(), offsetVal["y"].GetFloat(), offsetVal["z"].GetFloat()));
                }
                cParams->subColliders.push_back(collider);
            }

            if (strcmp(colliderVal["type"].GetString(), "mesh") == 0) {
                auto params = std::make_shared<physics::MeshColliderParameters>();
                std::shared_ptr<render::Mesh> mesh = render::loadMesh(colliderVal["model"].GetString());

                params->vertices.reserve(mesh->vertices.size());
                for (halogen::render::Vertex vert : mesh->vertices) {
                    params->vertices.push_back(vert.pos);
                }
                physics::SubCollider collider(physics::ColliderType::Mesh, std::static_pointer_cast<physics::ColliderParameters>(params));
                cParams->subColliders.push_back(collider);
            }
        }

        auto compoundCollider = std::make_shared<physics::Collider>(physics::ColliderType::Compound, std::static_pointer_cast<physics::ColliderParameters>(cParams));
        auto& rigidbody = core::entityRegistry.assign<physics::Rigidbody>(entity, compoundCollider, ship.isStatic ? 0.0f : 1.0f, entity);
        auto& mbComponent = core::entityRegistry.assign<ModeledBlockComponent>(entity);
        mbComponent.shipEntity = shipEntity;
        blocks::CreateBlockAttachment(block.id, entity);

        if (!ship.isStatic) {
            auto& rci = core::entityRegistry.assign<physics::RigidbodyChildInfo>(entity);
            rci.parentEntity = shipEntity;
            rci.localPosition = (glm::vec3(block.position.x, block.position.y, block.position.z) + glm::vec3(0.5f, 0.5f, -0.5f));
            rci.localRotation = glm::quat(glm::radians(rotation));
            rci.set = true;
        } else {
            rigidbody.setPosition(shipTransform.transformPoint(glm::vec3(block.position.x, block.position.y, block.position.z) + glm::vec3(0.5f, 0.5f, -0.5f)));
        }
        //rci.localPosition = glm::vec3(block.position.x, block.position.y, block.position.z) + glm::vec3(0.5f, 0.5f, -0.5f);

        rigidbody.setRotation(transform.rotation);
        ship.blocks->setBlockEntity(block.position, entity);
    }

    void ShipSystem::destroyBlock(uint32_t shipEntity, cosgame::components::ShipComponent& ship, Vec3s pos) {
        assert(ship.blocks->isBlockAt(pos));
        Block destroyed = ship.blocks->getBlockAt(pos);

        if (destroyed.id != 1) {
            uint32_t blockEntity = ship.blocks->getBlockEntity(pos);
            assert(core::entityRegistry.valid(blockEntity));
            core::entityRegistry.destroy(blockEntity);
        }

        ship.blocks->destroyBlockAt(pos);
    }

    void ShipSystem::saveShip(uint32_t shipEntity) {
        auto& shipComponent = halogen::core::entityRegistry.get<components::ShipComponent>(shipEntity);
        auto& shipTransform = halogen::core::entityRegistry.get<halogen::components::Transform>(shipEntity);

        rapidjson::Document shipDocument(rapidjson::kObjectType);
        rapidjson::Value blockArray(rapidjson::kArrayType);
        rapidjson::Document::AllocatorType& allocator = shipDocument.GetAllocator();

        for (auto p : *shipComponent.blocks) {
            rapidjson::Value blockVal;
            blockVal.SetObject();
            blockVal.AddMember("id", p.second.id, allocator);

            rapidjson::Value posVal(rapidjson::kObjectType);
            posVal.AddMember("x", p.second.position.x, allocator);
            posVal.AddMember("y", p.second.position.y, allocator);
            posVal.AddMember("z", p.second.position.z, allocator);
            blockVal.AddMember("position", posVal, allocator);

            rapidjson::Value rotVal(rapidjson::kObjectType);
            rotVal.AddMember("x", (int)p.second.xRotation, allocator);
            rotVal.AddMember("y", (int)p.second.yRotation, allocator);
            rotVal.AddMember("z", (int)p.second.zRotation, allocator);
            blockVal.AddMember("rotation", rotVal, allocator);

            blockArray.PushBack(blockVal, allocator);
        }

        shipDocument.AddMember("blocks", blockArray, allocator);
        shipDocument.AddMember("x", shipTransform.position.x, allocator);
        shipDocument.AddMember("y", shipTransform.position.y, allocator);
        shipDocument.AddMember("z", shipTransform.position.z, allocator);
        shipDocument.AddMember("static", shipComponent.isStatic, allocator);

        FILE* fp = fopen((std::string("ship ") + std::to_string(shipEntity)).c_str(), "w");
        char* writeBuffer = (char*)std::malloc(65536);
        rapidjson::FileWriteStream ws(fp, writeBuffer, 65536);
        rapidjson::Writer<rapidjson::FileWriteStream> writer(ws);
        shipDocument.Accept(writer);
        fclose(fp);
        std::free(writeBuffer);
    }

    float rotStateToAngle(RotationState state) {
        if (state == RotationState::Zero)
            return 0.f;
        if (state == RotationState::Ninety)
            return 90.f;
        if (state == RotationState::HundredEighty)
            return 180.f;
        if (state == RotationState::TwoSeventy)
            return 270.f;
    }

    glm::vec3 getBlockRotVec(Block& block) {
        return glm::vec3(rotStateToAngle(block.xRotation), rotStateToAngle(block.yRotation), rotStateToAngle(block.zRotation));
    }

    void ShipSystem::loadShip(std::string shipJson) {
        rapidjson::Document shipDocument;
        shipDocument.Parse(shipJson.c_str());

        auto ent = halogen::core::entityRegistry.create();

        halogen::components::Transform& shipTransform = halogen::core::entityRegistry.assign<halogen::components::Transform>(ent);
        shipTransform.position.x = shipDocument["x"].GetFloat();
        shipTransform.position.y = shipDocument["y"].GetFloat();
        shipTransform.position.z = shipDocument["z"].GetFloat();

        auto& sc = halogen::core::entityRegistry.assign<cosgame::components::ShipComponent>(ent);
        sc.isStatic = shipDocument["static"].GetBool();

        rapidjson::Value& blockArray = shipDocument["blocks"];
        for (auto v = blockArray.Begin(); v < blockArray.End(); v++) {
            Block block;
            block.id = v->GetObject()["id"].GetInt();

            rapidjson::Value& posVal = v->GetObject()["position"];
            Vec3s blockPos;
            blockPos.x = posVal["x"].GetInt();
            blockPos.y = posVal["y"].GetInt();
            blockPos.z = posVal["z"].GetInt();
            block.position = blockPos;

            rapidjson::Value& rotVal = v->GetObject()["rotation"];
            block.xRotation = (RotationState)rotVal["x"].GetInt();
            block.yRotation = (RotationState)rotVal["y"].GetInt();
            block.zRotation = (RotationState)rotVal["z"].GetInt();
            glm::vec3 blockRotVec = getBlockRotVec(block);
            placeBlock(ent, sc, block, blockRotVec);
        }

        std::shared_ptr<halogen::render::Mesh> mesh = std::make_shared<halogen::render::Mesh>();
        halogen::core::entityRegistry.assign<halogen::components::Renderable>(ent, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("ship_metal"));
    }
}
}