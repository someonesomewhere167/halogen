#pragma once
#include "PlayerInputCmd.hpp"
#include <Network.hpp>
#include <yojimbo.h>

namespace cosgame {
class PlayerInputMessage : public yojimbo::Message {
public:
    PlayerInputMessage();
    PlayerInputCmd cmd;
    halogen::networking::NetworkIdentity identity;
    SERIALIZE_FUNC();
    YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};
}