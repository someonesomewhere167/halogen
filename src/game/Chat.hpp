#pragma once
#include <Network.hpp>
#include <cstdint>
#include <string>
#include <vector>

namespace cosgame {
class ChatMessage : public yojimbo::Message {
public:
    SERIALIZE_FUNC();
    YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
    std::string message;
    halogen::networking::NetworkIdentity authorIdentity;
};

class Chat {
public:
    void Update();
    void AddMessage(ChatMessage* msg);

private:
    struct ReceivedMessage {
        std::string text;
        std::string authorName;
    };
    std::vector<ReceivedMessage> messages;
};
}