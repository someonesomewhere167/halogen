#include "vec3s.h"

namespace cosgame {
Vec3s::Vec3s()
    : x(0)
    , y(0)
    , z(0) {}

Vec3s::Vec3s(int16_t x, int16_t y, int16_t z)
    : x(x)
    , y(y)
    , z(z) {}

bool Vec3s::operator==(const Vec3s& other) const {
    return x == other.x && y == other.y && z == other.z;
}

Vec3s Vec3s::operator+(const Vec3s& other) const {
    return Vec3s(x + other.x, y + other.y, z + other.z);
}
}