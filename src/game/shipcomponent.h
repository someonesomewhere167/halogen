#pragma once
#include <cstdint>
#include "blockcontainer.h"
#include "render.h"
#include <mutex>

namespace cosgame
{
    namespace components
    {
        struct ShipComponent
        {
            ShipComponent();
            uint16_t id;
            bool meshDirty;
            BlockContainer* blocks;
			bool currentlyMeshing;
			bool isStatic;
        };
    }
}