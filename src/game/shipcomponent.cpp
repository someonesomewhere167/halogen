#include "shipcomponent.h"

namespace cosgame {
namespace components {
    ShipComponent::ShipComponent()
        : meshDirty(true)
        , currentlyMeshing(false)
        , id(0)
        , isStatic(false) {
        blocks = new BlockContainer();
    }
}
}