#include "DiscordRichPresence.hpp"
#include <cstring>
#include <discord_rpc.h>
#include <functional>
#include <iostream>

namespace cosgame {
DiscordRichPresence::DiscordRichPresence(const char* appId)
    : initialised(false)
    , appId(appId) {
    handlers = new DiscordEventHandlers;
    handlers->ready = onReady;
    handlers->joinRequest = onJoinRequest;
}

void DiscordRichPresence::init() {
    initialised = true;
    Discord_Initialize(appId.c_str(), handlers, 1, nullptr);
    updatePresence();
}

void DiscordRichPresence::onReady(const DiscordUser* user) {
    std::cout << "Rich presence initialised to user " << user->username << "\n";
}

void DiscordRichPresence::onJoinRequest(const DiscordUser* user) {
    std::cout << user->username << " is requesting to join."
              << "\n";
}

void DiscordRichPresence::onJoinGame(const char* joinSecret) {
    std::cout << "Joining a game using " << joinSecret << "\n";
}

void DiscordRichPresence::updatePresence() {
    ::DiscordRichPresence discordPresence;
    memset(&discordPresence, 0, sizeof(discordPresence));
    discordPresence.state = "Testing the engine.";
    discordPresence.joinSecret = "asdfghjkl";
    discordPresence.partyMax = 16;
    discordPresence.partySize = 1;
    discordPresence.partyId = "hfuiehwfi";
    discordPresence.startTimestamp = 1;
    Discord_UpdatePresence(&discordPresence);
}

DiscordRichPresence::~DiscordRichPresence() {
    if (initialised)
        Discord_Shutdown();
}
}
