#pragma once
#include <Network.hpp>
#include <glm/glm.hpp>

namespace cosgame {
class PlayerInputCmd {
public:
    PlayerInputCmd();
    glm::vec3 movementAmount;
    glm::vec2 lookVec;
    bool sprint;
    bool jump;
    SERIALIZE_FUNC();
    YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};
}