#pragma once
#include <Network.hpp>
enum class COSMessageType 
{
    PlayerInput = (int)halogen::networking::MessageType::Count,
    ChatMessage,
    Count
};