#pragma once
#include "InventoryComponent.hpp"
#include "Item.hpp"
#include "playercomponent.h"

#include <core.h>
#include <entt/entt.hpp>
#include <glm/glm.hpp>

namespace cosgame
{
namespace systems
{
    class PlayerSystem final : public halogen::core::System
    {
    public:
        void update(float deltaTime, entt::DefaultRegistry& resgistry) override;
        std::string getName() override;

    private:
        void updatePlayer(cosgame::components::PlayerComponent& player, uint32_t entity, float deltaTime);
        void updatePlayerServer(components::PlayerComponent& player, uint32_t entity, float deltaTime);
        void updateLocalPlayer(components::PlayerComponent& player, uint32_t entity, float deltaTime);
        void buildUpdate(components::PlayerComponent& player, bool placeBlock, bool destroyBlock, glm::vec3 from, glm::vec3 to);
        bool isPlayerGrounded(uint32_t playerEntity);
        void interactUpdate(components::PlayerComponent& player, uint32_t playerEnt, glm::vec3 from, glm::vec3 to, bool interact);
        Item getTool(int index, components::InventoryComponent& inventory);
        Item getHeldTool(components::PlayerComponent& player, components::InventoryComponent& inventory);
    };
}
}