#pragma once
#include <noise/noise.h>
#include <rapidjson/document.h>

namespace cosgame {
noise::module::Module* ParseGraph(rapidjson::Document& graph);
}