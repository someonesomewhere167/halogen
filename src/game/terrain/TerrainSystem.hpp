#pragma once
#include "TerrainChunk.hpp"
#include <core.h>
#include <deque>
#include <entt/entt.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>
#include <noise/noise.h>
#include <unordered_map>

namespace cosgame {
struct TerrainLoaderTag {};
class TerrainSystem : public halogen::core::System {
public:
    TerrainSystem();
    uint32_t CreateEntityFor(TerrainChunk& chunk, int lod = 1);
    void update(float deltaTime, entt::DefaultRegistry& registry) override;
    std::string getName() override;
    void CreateChunk(int cx, int cy, int lod = 1);
    void DestroyChunk(int cx, int cy);
    float GetHeightAt(glm::vec3 worldPos);

private:
    float GetHeightAt(int x, int y);
    struct CachedChunkData {
        float data[TCHUNK_SIZE * TCHUNK_SIZE];
    };
    std::unordered_map<glm::ivec2, CachedChunkData> cachedChunks;
    std::unordered_map<glm::ivec2, TerrainChunk> loadedChunks;
    noise::module::Module* noiseModule;
    std::vector<noise::module::Module*> modules;
    struct ChunkCreationInfo {
        glm::ivec2 pos;
        int lod;
    };
    std::deque<ChunkCreationInfo> toCreate;
    int viewRadius;
};
}