#include "GraphParser.hpp"

namespace cosgame {
noise::module::Module* ParseModule(rapidjson::Value& node) {
    std::string nodeType = std::string(node["type"].GetString());
    if (nodeType == "RidgedMultifractal") {
        rapidjson::Value& options = node["options"];
        noise::module::RidgedMulti* multi = new noise::module::RidgedMulti();
        multi->SetLacunarity(options["lacunarity"].GetFloat());
        multi->SetOctaveCount(options["octaves"].GetInt());
        multi->SetFrequency(options["frequency"].GetFloat());
        return multi;
    } else if (nodeType == "Perlin") {
        rapidjson::Value& options = node["options"];
        noise::module::Perlin* perlin = new noise::module::Perlin();
        perlin->SetLacunarity(options["lacunarity"].GetFloat());
        perlin->SetOctaveCount(options["octaves"].GetInt());
        perlin->SetPersistence(options["persistence"].GetFloat());
        perlin->SetFrequency(options["frequency"].GetFloat());
        return perlin;
    } else if (nodeType == "Billow") {
        rapidjson::Value& options = node["options"];
        noise::module::Billow* billow = new noise::module::Billow();
        billow->SetLacunarity(options["lacunarity"].GetFloat());
        billow->SetOctaveCount(options["octaves"].GetInt());
        billow->SetPersistence(options["persistence"].GetFloat());
        billow->SetFrequency(options["frequency"].GetFloat());
        return billow;
    } else if (nodeType == "Scale") {
        noise::module::Module* connectedTo = ParseModule(node["connectedTo"].GetArray()[0]);
        rapidjson::Value& options = node["options"];
        noise::module::ScalePoint* scalePoint = new noise::module::ScalePoint;
        scalePoint->SetXScale(options["xScale"].GetFloat());
        scalePoint->SetYScale(options["yScale"].GetFloat());
        scalePoint->SetZScale(options["zScale"].GetFloat());
        scalePoint->SetSourceModule(0, *connectedTo);
        return scalePoint;
    } else if (nodeType == "ScaleBias") {
        noise::module::Module* connectedTo = ParseModule(node["connectedTo"].GetArray()[0]);
        rapidjson::Value& options = node["options"];
        noise::module::ScaleBias* scaleBias = new noise::module::ScaleBias;
        scaleBias->SetBias(options["bias"].GetFloat());
        scaleBias->SetScale(options["scale"].GetFloat());
        scaleBias->SetSourceModule(0, *connectedTo);
        return scaleBias;
    } else if (nodeType == "Blend") {
        rapidjson::GenericArray<false, rapidjson::Value::ValueType> connectedToJ = node["connectedTo"].GetArray();
        noise::module::Module* input1 = ParseModule(connectedToJ[0]);
        noise::module::Module* input2 = ParseModule(connectedToJ[1]);
        noise::module::Module* controller = ParseModule(connectedToJ[2]);

        noise::module::Blend* blend = new noise::module::Blend;
        blend->SetSourceModule(0, *input1);
        blend->SetSourceModule(1, *input2);
        blend->SetControlModule(*controller);

        return blend;
    } else if (nodeType == "Add") {
        rapidjson::GenericArray<false, rapidjson::Value::ValueType> connectedToJ = node["connectedTo"].GetArray();
        noise::module::Module* input1 = ParseModule(connectedToJ[0]);
        noise::module::Module* input2 = ParseModule(connectedToJ[1]);

        noise::module::Add* add = new noise::module::Add;
        add->SetSourceModule(0, *input1);
        add->SetSourceModule(1, *input2);

        return add;
    } else if (nodeType == "Abs") {
        noise::module::Module* connectedTo = ParseModule(node["connectedTo"].GetArray()[0]);
        noise::module::Abs* abs = new noise::module::Abs;
        abs->SetSourceModule(0, *connectedTo);
        return abs;
    } else if (nodeType == "Clamp") {
        noise::module::Module* connectedTo = ParseModule(node["connectedTo"].GetArray()[0]);
        rapidjson::Value& options = node["options"];
        noise::module::Clamp* clamp = new noise::module::Clamp;
        clamp->SetBounds(options["min"].GetDouble(), options["max"].GetDouble());
        clamp->SetSourceModule(0, *connectedTo);
        return clamp;
    } else if (nodeType == "Turbulence") {
        noise::module::Module* connectedTo = ParseModule(node["connectedTo"].GetArray()[0]);
        rapidjson::Value& options = node["options"];
        noise::module::Turbulence* turbulence = new noise::module::Turbulence;
        turbulence->SetPower(options["power"].GetDouble());
        turbulence->SetFrequency(options["frequency"].GetDouble());
        turbulence->SetRoughness(options["roughness"].GetDouble());
        turbulence->SetSourceModule(0, *connectedTo);
        return turbulence;
    } else if (nodeType == "Const") {
        noise::module::Const* c = new noise::module::Const;
        rapidjson::Value& options = node["options"];
        c->SetConstValue(options["value"].GetDouble());
        return c;
    }
}

noise::module::Module* ParseGraph(rapidjson::Document& graph) {
    rapidjson::Value& firstNode = graph["outputNode"]["connectedTo"];
    return ParseModule(firstNode);
}
}