#pragma once
#include <cstdint>
#include <vector>

namespace cosgame {
// Chunks will only be 8x8 in debug builds
// Larger chunks result in better performance in release builds
// but make moving around in debug builds painful
#ifdef DEBUG
#define TCHUNK_SIZE 8
#else
#define TCHUNK_SIZE 100
#endif
struct TerrainChunk {
    TerrainChunk();
    int x, y;
    float data[TCHUNK_SIZE * TCHUNK_SIZE];
    uint32_t entity;
    std::vector<uint32_t> spawnedEntities;
    float GetDataAt(int x, int y);
    void SetDataAt(int x, int y, float value);
};
}