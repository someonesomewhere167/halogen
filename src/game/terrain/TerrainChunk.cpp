#include "TerrainChunk.hpp"

namespace cosgame {
TerrainChunk::TerrainChunk()
    : spawnedEntities() {
}
float TerrainChunk::GetDataAt(int x, int y) {
    return data[y * TCHUNK_SIZE + x];
}

void TerrainChunk::SetDataAt(int x, int y, float value) {
    data[y * TCHUNK_SIZE + x] = value;
}
}