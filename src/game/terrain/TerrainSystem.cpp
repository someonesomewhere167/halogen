#include "TerrainSystem.hpp"
#include "GraphParser.hpp"
#include "playercomponent.h"
#include <InteractableComponent.hpp>
#include <InventoryComponent.hpp>
#include <ItemManager.hpp>
#include <Material.hpp>
#include <PerlinNoise.hpp>
#include <SettingsManager.hpp>
#include <chrono>
#include <cstring>
#include <glm/gtx/norm.hpp>
#include <hgio.h>
#include <imgui.h>
#include <iostream>
#include <mesh.h>
#include <meshloader.h>
#include <noise/noise.h>
#include <physics.h>
#include <random>
#include <render.h>
#include <stb_image.h>
#include <transform.h>
#include <unordered_set>

using HRC = std::chrono::high_resolution_clock;

namespace cosgame {
double toMs(std::chrono::nanoseconds ns) {
    return (double)ns.count() / 1000.0 / 1000.0;
}

TerrainSystem::TerrainSystem()
    : viewRadius(4) {
    viewRadius = halogen::util::SettingsManager::GetOption("terrain.viewRadius")->as<int64_t>()->get();
    rapidjson::Document doc;
    doc.Parse(halogen::util::loadFileToString("terrain.json").c_str());

    noise::module::Module* mod = ParseGraph(doc);

    noise::module::ScalePoint* scalePoint = new noise::module::ScalePoint;
    scalePoint->SetScale(0.001);
    scalePoint->SetSourceModule(0, *mod);

    noise::module::ScaleBias* scaleBias = new noise::module::ScaleBias;
    scaleBias->SetScale(100.0);
    scaleBias->SetSourceModule(0, *scalePoint);
    noiseModule = scaleBias;

    if (halogen::core::isDedicatedServer()) {
        // TODO: Actually load/unload terrain based on players
        for (int x = -4; x < 4; x++)
            for (int y = -4; y < 4; y++) {
                CreateChunk(x, y);
            }
    }
}

struct Face {
    int ia, ib, ic;
};

uint32_t createJetpack(glm::vec3 pos) {
    auto entity = halogen::core::entityRegistry.create();
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);

    auto mesh = halogen::render::loadMesh("jetpack");
    auto p = std::make_shared<halogen::physics::MeshColliderParameters>();

    p->isStatic = false;

    p->vertices.reserve(mesh->vertices.size());
    p->indices.reserve(mesh->indices.size());
    for (halogen::render::Vertex vert : mesh->vertices) {
        p->vertices.push_back(vert.pos * transform.scale);
    }

    for (int i : mesh->indices) {
        p->indices.push_back(i);
    }

    auto collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Mesh, p);

    auto& rb = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(entity, collider, 0.0f, entity);
    rb.setPosition(pos);
    transform.position = pos;

    if (!halogen::core::isDedicatedServer()) {
        halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("ship_metal"));
    }

    auto& interactable = halogen::core::entityRegistry.assign<cosgame::InteractableComponent>(entity);
    interactable.onInteract = [](uint32_t interact, uint32_t playerEnt) {
        auto& playerComponent = halogen::core::entityRegistry.get<cosgame::components::PlayerComponent>(playerEnt);
        playerComponent.hasJetpack = true;
        halogen::core::entityRegistry.destroy(interact);
    };

    return entity;
}

uint32_t createRock(glm::vec3 pos, float scale, glm::quat rot) {
    auto entity = halogen::core::entityRegistry.create();
    auto& t = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    t.position = pos;
    t.scale = glm::vec3(scale);
    t.rotation = rot;
    auto mesh = halogen::render::loadMesh("rock");
    auto rndrble = halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, mesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("rock"));
    std::shared_ptr<halogen::physics::MeshColliderParameters> p = std::make_shared<halogen::physics::MeshColliderParameters>();
    p->isStatic = true;

    p->vertices.reserve(mesh->vertices.size());
    p->indices.reserve(mesh->indices.size());
    for (halogen::render::Vertex vert : mesh->vertices) {
        p->vertices.push_back(vert.pos * t.scale);
    }

    for (int i : mesh->indices) {
        p->indices.push_back(i);
    }

    std::shared_ptr<halogen::physics::Collider> collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Mesh, p);
    auto& playerRigidbody = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(entity, collider, 0.0f, entity);

    auto& interactable = halogen::core::entityRegistry.assign<InteractableComponent>(entity);
    interactable.onInteract = [](uint32_t interacted, uint32_t player) {
        halogen::core::entityRegistry.destroy(interacted);
        auto& inv = halogen::core::entityRegistry.get<components::InventoryComponent>(player);
        inv.items.push_back(cosgame::ItemManager::GetItem("rock"));
    };

    return entity;
}

uint32_t TerrainSystem::CreateEntityFor(TerrainChunk& chunk, int lod) {
    std::shared_ptr<halogen::render::Mesh> chunkMesh = std::make_shared<halogen::render::Mesh>();
    chunkMesh->dynamic = false;
    chunkMesh->vertices.reserve(((TCHUNK_SIZE - 1) / lod) * ((TCHUNK_SIZE - 1) / lod) * 4);
    float maxHeight = TCHUNK_SIZE * TCHUNK_SIZE + TCHUNK_SIZE * TCHUNK_SIZE;

    for (int y = 0; y < (TCHUNK_SIZE - 1); y += lod)
        for (int x = 0; x < (TCHUNK_SIZE - 1); x += lod) {
            glm::vec3 tl, tr, bl, br;
            tl = glm::vec3(0.0f + x, chunk.GetDataAt(x, y), 0.0f + y);
            tr = glm::vec3(lod + x, chunk.GetDataAt(std::min(x + lod, (TCHUNK_SIZE - 1)), y), 0.0f + y);
            bl = glm::vec3(0.0f + x, chunk.GetDataAt(x, std::min(y + lod, (TCHUNK_SIZE - 1))), std::min(lod + y, (TCHUNK_SIZE - 1)));
            br = glm::vec3(lod + x, chunk.GetDataAt(std::min(x + lod, (TCHUNK_SIZE - 1)), std::min(y + lod, (TCHUNK_SIZE - 1))), std::min(lod + y, (TCHUNK_SIZE - 1)));

            maxHeight = std::max(maxHeight, std::max(glm::length2(tl), std::max(glm::length2(tr), std::max(glm::length2(bl), glm::length2(br)))));

            halogen::render::Vertex tlv, trv, blv, brv;

            tlv.pos = tl;
            trv.pos = tr;
            blv.pos = bl;
            brv.pos = br;

            tlv.uv = glm::vec2(0.0f);
            trv.uv = glm::vec2(lod, 0.0f);
            blv.uv = glm::vec2(0.0f, lod);
            brv.uv = glm::vec2(lod, lod);

            tlv.normal = glm::vec3(0.0f);
            trv.normal = glm::vec3(0.0f);
            blv.normal = glm::vec3(0.0f);
            brv.normal = glm::vec3(0.0f);

            tlv.tangent = glm::vec3(0.0f);
            trv.tangent = glm::vec3(0.0f);
            blv.tangent = glm::vec3(0.0f);
            brv.tangent = glm::vec3(0.0f);

            chunkMesh->vertices.push_back(tlv);
            chunkMesh->vertices.push_back(trv);
            chunkMesh->vertices.push_back(blv);
            chunkMesh->vertices.push_back(brv);
        }
    std::vector<Face> faces;

    int i = 0;

    faces.reserve(((TCHUNK_SIZE - 1) / lod) * ((TCHUNK_SIZE - 1) / lod) * 2);
    chunkMesh->indices.reserve(((TCHUNK_SIZE - 1) / lod) * ((TCHUNK_SIZE - 1) / lod) * 6);

    for (int y = 0; y < (TCHUNK_SIZE - 1); y += lod)
        for (int x = 0; x < (TCHUNK_SIZE - 1); x += lod) {
            chunkMesh->indices.push_back(i + 0);
            chunkMesh->indices.push_back(i + 2);
            chunkMesh->indices.push_back(i + 1);
            faces.push_back({ i + 0, i + 2, i + 1 });

            chunkMesh->indices.push_back(i + 1);
            chunkMesh->indices.push_back(i + 2);
            chunkMesh->indices.push_back(i + 3);
            faces.push_back({ i + 1, i + 2, i + 3 });
            i += 4;
        }

    // calculate normals & tangents
    for (Face face : faces) {
        halogen::render::Vertex va = chunkMesh->vertices[face.ia];
        halogen::render::Vertex vb = chunkMesh->vertices[face.ib];
        halogen::render::Vertex vc = chunkMesh->vertices[face.ic];

        glm::vec3 a = va.pos;
        glm::vec3 b = vb.pos;
        glm::vec3 c = vc.pos;

        glm::vec3 e1 = a - b;
        glm::vec3 e2 = c - b;
        glm::vec3 no = glm::cross(e1, e2);

        chunkMesh->vertices[face.ia].normal += no;
        chunkMesh->vertices[face.ib].normal += no;
        chunkMesh->vertices[face.ic].normal += no;

        glm::vec3 edge1 = b - a;
        glm::vec3 edge2 = c - a;
        glm::vec2 deltaUV1 = vb.uv - va.uv;
        glm::vec2 deltaUV2 = vc.uv - va.uv;

        float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        glm::vec3 tangent = (edge1 * deltaUV2.y - edge2 * deltaUV1.y) * f;
        tangent = glm::normalize(tangent);

        chunkMesh->vertices[face.ia].tangent = tangent;
        chunkMesh->vertices[face.ib].tangent = tangent;
        chunkMesh->vertices[face.ic].tangent = tangent;
    }

    for (int j = 0; j < chunkMesh->vertices.size(); j++) {
        chunkMesh->vertices[j].normal = -glm::normalize(chunkMesh->vertices[j].normal);
        chunkMesh->vertices[j].tangent = glm::normalize(chunkMesh->vertices[j].tangent);
    }

    chunkMesh->boundingSphereRadius = sqrtf(maxHeight);

    auto entity = halogen::core::entityRegistry.create();
    auto& transform = halogen::core::entityRegistry.assign<halogen::components::Transform>(entity);
    if (!halogen::core::isDedicatedServer())
        auto& renderable = halogen::core::entityRegistry.assign<halogen::components::Renderable>(entity, chunkMesh, halogen::render::loadShader("standard_pbr"), halogen::render::loadMaterial("grass"));

    if (lod <= 2) {
        auto colliderParameters = std::make_shared<halogen::physics::MeshColliderParameters>();
        colliderParameters->isStatic = true;

        colliderParameters->vertices.reserve(chunkMesh->vertices.size());
        colliderParameters->indices.reserve(chunkMesh->indices.size());
        for (halogen::render::Vertex vert : chunkMesh->vertices) {
            colliderParameters->vertices.push_back(vert.pos);
        }

        for (int i : chunkMesh->indices) {
            colliderParameters->indices.push_back(i);
        }

        auto collider = std::make_shared<halogen::physics::Collider>(halogen::physics::ColliderType::Mesh, colliderParameters);

        auto& rb = halogen::core::entityRegistry.assign<halogen::physics::Rigidbody>(entity, collider, 0.0f, entity);
        rb.setPosition(glm::vec3(chunk.x * (TCHUNK_SIZE - 1), 1.0f, chunk.y * (TCHUNK_SIZE - 1)));
    }
    transform.position = glm::vec3(chunk.x * (TCHUNK_SIZE - 1), 1.0f, chunk.y * (TCHUNK_SIZE - 1));

    if (lod <= 1 && chunk.spawnedEntities.size() == 0) {
        int chunkSeed = 17;
        chunkSeed = chunkSeed * 31 + chunk.x;
        chunkSeed = chunkSeed * 31 + chunk.y;
        std::mt19937 rng(chunkSeed);
        std::uniform_real_distribution<float> spawnDistribution(0.0f, 1.0f);
        if (spawnDistribution(rng) > 0.5f) {
            chunk.spawnedEntities.reserve(3);
            for (int i = 0; i < 3; i++) {
                std::uniform_real_distribution<float> xDist(chunk.x * (TCHUNK_SIZE - 1), chunk.x * (TCHUNK_SIZE - 1) + TCHUNK_SIZE - 1);
                std::uniform_real_distribution<float> yDist(chunk.y * (TCHUNK_SIZE - 1), chunk.y * (TCHUNK_SIZE - 1) + TCHUNK_SIZE - 1);
                float x = xDist(rng);
                float y = yDist(rng);
                glm::vec3 jetpackPos(x, GetHeightAt(glm::vec3(x, 0.0f, y)) + 1.3f, y);
                chunk.spawnedEntities.push_back(createJetpack(jetpackPos));
            }
        }

        if (spawnDistribution(rng) > 0.2f) {
            std::uniform_int_distribution<int> numberDist(5, 20);
            int rockCount = numberDist(rng);
            chunk.spawnedEntities.reserve(chunk.spawnedEntities.size() + rockCount);
            for (int i = 0; i < rockCount; i++) {
                std::uniform_real_distribution<float> xDist(chunk.x * (TCHUNK_SIZE - 1), chunk.x * (TCHUNK_SIZE - 1) + TCHUNK_SIZE - 1);
                std::uniform_real_distribution<float> yDist(chunk.y * (TCHUNK_SIZE - 1), chunk.y * (TCHUNK_SIZE - 1) + TCHUNK_SIZE - 1);
                std::uniform_real_distribution<float> sDist(0.5f, 2.0f);
                std::uniform_real_distribution<float> rotDistY(0.f, 360.f);
                std::uniform_real_distribution<float> rotDistX(0.f, 20.f);
                float x = xDist(rng);
                float y = yDist(rng);
                float scale = sDist(rng);
                float rotY = rotDistY(rng);
                float rotX = rotDistX(rng);

                glm::vec3 jetpackPos(x, GetHeightAt(glm::vec3(x, 0.0f, y)) + 1.3f, y);
                glm::quat rotq(glm::vec3(rotX, rotY, 0.0f));
                chunk.spawnedEntities.push_back(createRock(jetpackPos, scale, rotq));
            }
        }
    }
    return entity;
}

std::string TerrainSystem::getName() {
    return "TerrainSystem";
}

void TerrainSystem::update(float deltaTime, entt::DefaultRegistry& registry) {
    static glm::ivec2 lastChunkPos(INT32_MAX, INT32_MAX);
    glm::ivec2 iChunkPos;
    registry.view<TerrainLoaderTag, halogen::components::Transform>().each([&](auto entity, auto, halogen::components::Transform& transform) {
        glm::vec2 chunkPos(transform.position.x / (float)(TCHUNK_SIZE - 1), transform.position.z / (float)(TCHUNK_SIZE - 1));

        if (transform.position.x < 0)
            iChunkPos.x = chunkPos.x - 1;
        else
            iChunkPos.x = chunkPos.x;
        if (transform.position.z < 0)
            iChunkPos.y = chunkPos.y - 1;
        else
            iChunkPos.y = chunkPos.y;

        if (lastChunkPos != iChunkPos) {
            std::unordered_set<glm::ivec2> generatedChunks;

            if (lastChunkPos != glm::ivec2(INT32_MAX, INT32_MAX)) {
                toCreate.clear();
                for (int x = -viewRadius; x < viewRadius; x++)
                    for (int y = -viewRadius; y < viewRadius; y++) {
                        generatedChunks.insert(glm::ivec2(lastChunkPos.x + x, lastChunkPos.y + y));
                    }

                for (int x = -viewRadius; x < viewRadius; x++)
                    for (int y = -viewRadius; y < viewRadius; y++) {
                        generatedChunks.erase(glm::ivec2(iChunkPos.x + x, iChunkPos.y + y));
                    }

                for (glm::ivec2 c : generatedChunks) {
                    if (loadedChunks.count(c) > 0) {
                        DestroyChunk(c.x, c.y);
                        cachedChunks.erase(c);
                    }
                }
            }

            for (int x = -viewRadius; x < viewRadius; x++)
                for (int y = -viewRadius; y < viewRadius; y++) {
                    int dist = sqrt((x * x) + (y * y));
                    glm::ivec2 newChunkPos(iChunkPos.x + x, iChunkPos.y + y);
                    toCreate.push_front({ newChunkPos, std::min(20, std::max(dist * 2, 1)) });
                }

            lastChunkPos = iChunkPos;
            std::sort(toCreate.begin(), toCreate.end(), [=](ChunkCreationInfo a, ChunkCreationInfo b) {
                glm::ivec2 aRelP = (a.pos - iChunkPos);
                glm::ivec2 bRelP = (b.pos - iChunkPos);
                float aDist = (aRelP.x * aRelP.x) + (aRelP.y * aRelP.y);
                float bDist = (bRelP.x * bRelP.x) + (bRelP.y * bRelP.y);
                return aDist < bDist;
            });
        }
    });

    auto startTime = std::chrono::high_resolution_clock::now();
    int createdChunks = 0;
    long ccNs = 0; // Time spent in CreateChunk() in ns
    while (toCreate.size() > 0) {
        auto currTime = std::chrono::high_resolution_clock::now();
        auto elapsed = (currTime - startTime);

        if (elapsed.count() > 45 * 100 * 1000)
            break;

        ChunkCreationInfo cci = toCreate.front();
        toCreate.pop_front();
        if (loadedChunks.count(cci.pos) > 0) {
            DestroyChunk(cci.pos.x, cci.pos.y);
        }

        auto t1 = std::chrono::high_resolution_clock::now();
        CreateChunk(cci.pos.x, cci.pos.y, cci.lod);
        auto t2 = std::chrono::high_resolution_clock::now();
        ccNs += (t2 - t1).count();
        createdChunks++;
    }

#if IMGUI_ENABLED
    ImGui::Begin("Terrain");
    ImGui::Text("Time spent in CreateChunk(): %fms", (double)ccNs / 1000.0 / 1000.0);
    ImGui::Text("Chunks created this frame: %i", createdChunks);
    ImGui::Text("Loaded chunks (memory usage): %i (%fkb)", loadedChunks.size(), (loadedChunks.size() * sizeof(TerrainChunk)) / 1000.0);
    ImGui::Text("Cached chunks (memory usage): %i (%fkb)", cachedChunks.size(), (cachedChunks.size() * sizeof(CachedChunkData)) / 1000.0);
    ImGui::Text("Waiting chunks: %i", toCreate.size());
    ImGui::DragInt("View Radius", &viewRadius);
    if (ImGui::Button("Recreate Chunks")) {
        std::vector<glm::ivec2> toErase;

        for (std::pair<glm::ivec2, TerrainChunk> cp : loadedChunks) {
            halogen::core::entityRegistry.destroy(loadedChunks[cp.first].entity);
            toErase.push_back(cp.first);
        }

        for (glm::ivec2 pos : toErase) {
            loadedChunks.erase(pos);
            cachedChunks.erase(pos);
        }

        lastChunkPos = glm::ivec2(INT32_MAX, INT32_MAX);
    }
    ImGui::End();
#endif
}

void TerrainSystem::CreateChunk(int cx, int cy, int lod) {
    TerrainChunk chunk;
    chunk.x = cx;
    chunk.y = cy;

    if (cachedChunks.count(glm::ivec2(cx, cy))) {
        memcpy(chunk.data, cachedChunks.at(glm::ivec2(cx, cy)).data, sizeof(float) * TCHUNK_SIZE * TCHUNK_SIZE);
    } else {
        for (long x = 0; x < TCHUNK_SIZE; x += 2)
            for (long y = 0; y < TCHUNK_SIZE; y += 2) {
                float height = noiseModule->GetValue((cx * (TCHUNK_SIZE - 1)) + x, 0, (cy * (TCHUNK_SIZE - 1)) + y);
                chunk.SetDataAt(x, y, height);
            }

        for (char x = 0; x < TCHUNK_SIZE; x += 2)
            for (char y = 0; y < TCHUNK_SIZE; y += 2) {
                float validHeight = chunk.GetDataAt(x, y);

                float height = validHeight;
                if (y + 2 < (TCHUNK_SIZE - 1) && x + 2 < (TCHUNK_SIZE - 1)) {
                    height += chunk.GetDataAt(x + 2, y + 2);
                    height *= 0.5f;
                } else {
                    height += noiseModule->GetValue((cx * (TCHUNK_SIZE - 1)) + x + 2, 0, (cy * (TCHUNK_SIZE - 1)) + y + 2);
                    height *= 0.5f;
                }
                chunk.SetDataAt(x + 1, y + 1, height);

                height = validHeight;
                if (y + 2 < (TCHUNK_SIZE - 1)) {
                    height += chunk.GetDataAt(x, y + 2);
                    height *= 0.5f;
                } else {
                    height += noiseModule->GetValue((cx * (TCHUNK_SIZE - 1)) + x, 0, (cy * (TCHUNK_SIZE - 1)) + y + 2);
                    height *= 0.5f;
                }
                chunk.SetDataAt(x, y + 1, height);

                height = validHeight;
                if (x + 2 < (TCHUNK_SIZE - 1)) {
                    height += chunk.GetDataAt(x + 2, y);
                    height *= 0.5f;
                } else {
                    height += noiseModule->GetValue((cx * (TCHUNK_SIZE - 1)) + x + 2, 0, (cy * (TCHUNK_SIZE - 1)) + y);
                    height *= 0.5f;
                }
                chunk.SetDataAt(x + 1, y, height);
            }
        CachedChunkData ccd;
        memcpy(ccd.data, chunk.data, sizeof(float) * TCHUNK_SIZE * TCHUNK_SIZE);
        cachedChunks.insert({ glm::ivec2(cx, cy), ccd });
    }
    chunk.entity = CreateEntityFor(chunk, lod);
    loadedChunks.insert({ glm::ivec2(cx, cy), chunk });
}

void TerrainSystem::DestroyChunk(int cx, int cy) {
    TerrainChunk c = loadedChunks[glm::ivec2(cx, cy)];
    for (uint32_t ent : c.spawnedEntities) {
        if (halogen::core::entityRegistry.valid(ent))
            halogen::core::entityRegistry.destroy(ent);
    }
    halogen::core::entityRegistry.destroy(loadedChunks[glm::ivec2(cx, cy)].entity);
    loadedChunks.erase(glm::ivec2(cx, cy));
}

float TerrainSystem::GetHeightAt(int x, int y) {
    int chunkX, chunkY;
    chunkX = x / TCHUNK_SIZE;
    chunkY = y / TCHUNK_SIZE;

    return loadedChunks[glm::ivec2(chunkX, chunkY)].GetDataAt(x, y);
}

float TerrainSystem::GetHeightAt(glm::vec3 worldPos) {
    return noiseModule->GetValue(worldPos.x, 0.0, worldPos.z);
}
}