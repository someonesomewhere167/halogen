#include "InventoryUI.hpp"
#include <hgio.h>
#include <input.h>
#include <nuklear_draw.hpp>
#include <nuklear_ws.h>
#include <render.h>
#include <unordered_map>

namespace cosgame {
namespace ui {
    namespace InventoryUI {
        typedef struct nk_image nkimg;
        components::InventoryComponent currentInventory;
        bool open = false;
        bool itemInfoWindowOpen = false;
        std::unordered_map<std::string, nkimg> itemImages;
        Item currentItemInfoItem;

        nkimg LoadInventoryImage(std::string name) {
            if (itemImages.count(name) > 0)
                return itemImages[name];
            nkimg img = halogen::render::loadNkImage("ui/icons/" + name + ".png");
            itemImages.insert({ name, img });
            return img;
        }

        void LoadInventoryImages() {
            for (Item i : currentInventory.items) {
                if (itemImages.count(i.id) == 0)
                    LoadInventoryImage(i.id);
            }
        }

        void OpenInventory(components::InventoryComponent& inventory) {
            halogen::input::unlockMouse();
            open = true;
            currentInventory = inventory;
            LoadInventoryImages();
        }

        void CloseCurrentInventory() {
            open = false;
            currentInventory = components::InventoryComponent();
            halogen::input::lockMouse();
        }

        bool HasInventoryOpen() {
            return open;
        }

        void DrawInventoryWindow(nk_context* ctx) {
            nk_menubar_begin(ctx);
            nk_layout_row_static(ctx, 25, 75, 2);
            if (nk_button_label(ctx, "Close"))
                CloseCurrentInventory();
            nk_menubar_end(ctx);
            int nCols = sqrt(std::max((int)currentInventory.items.size(), (int)currentInventory.maxItems));
            nk_layout_row_static(ctx, 75, 75, nCols);

            int i;
            for (i = 0; i < currentInventory.items.size(); i++) {
                if (nk_button_image_label(ctx, itemImages[currentInventory.items[i].id], currentInventory.items[i].name.c_str(), NK_TEXT_ALIGN_CENTERED)) {
                    itemInfoWindowOpen = true;
                    currentItemInfoItem = currentInventory.items[i];
                }
            }

            for (i = i; i < currentInventory.maxItems; i++) {
                nk_button_label(ctx, "");
            }
        }

        void Draw() {
            if (!open)
                return;
            nk_context* ctx = halogen::render::getNkContext();
            glm::ivec2 sSize = halogen::render::getCurrentScreenSize();
            if (nk_begin(ctx, "Inventory", nk_rect(10.0f, 10.0f, sSize.x - 20.0f, sSize.y - 20.0f), NK_WINDOW_TITLE | NK_WINDOW_BORDER)) {
                DrawInventoryWindow(ctx);
            }
            nk_end(ctx);

            if (itemInfoWindowOpen) {
                if (nk_begin(ctx, "Item Info", nk_rect(10.0f, 10.0f, 500.0f, 200.0f), NK_WINDOW_TITLE | NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE)) {
                    nk_layout_row_dynamic(ctx, 25.0f, 1);
                    nk_label(ctx, std::string("Name: " + currentItemInfoItem.name).c_str(), NK_TEXT_ALIGN_LEFT);
                    nk_label(ctx, currentItemInfoItem.description.c_str(), NK_TEXT_ALIGN_LEFT);
                    if (nk_button_label(ctx, "Close"))
                        itemInfoWindowOpen = false;
                }
                nk_end(ctx);
            }
        }
    }
}
}
