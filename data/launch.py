import ctypes, sys

hggame = ctypes.cdll.LoadLibrary("./libhg_game.so")

argc = len(sys.argv)
array_type = ctypes.c_char_p * argc
argv = array_type()

for i, param in enumerate(sys.argv):
    argv[i] = param.encode()

hggame.startHalogen.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_char_p)]

hggame.startHalogen(argc, argv)