function getTemplate(name)
{
    var contents;
    if(!window.loadText)
    {
        $.ajax({
            url: `templates/${name}.html`,
            async: false,
            dataType: "text"
        }).done(function(data, textStatus) {
            contents = data;
        }).fail(function() {
            console.log("Error loading " + name);
        });
    }
    else
    {
        contents = loadText(`templates/${name}.html`);
    }
    return contents;
}

function loadHUD()
{
    Vue.component("progress-bar", {
        props: ['percent'],
        template: getTemplate("progress-bar")
    })
    Vue.component("health-bar", {
        props: ['health'],
        template: getTemplate("health-bar")
    });

    Vue.component("inventory", {
        template: getTemplate("inventory"),
        mounted: function() {
            var numRows = 6;
            var numColumns = 8;
            
            var rowElement = document.createElement("tr");
            
            for(var i = 0; i < numColumns; i++)
            {
                var columnElement = document.createElement("td");
                var contentElement = document.createElement("div");
				contentElement.classList.add("inventory-slot");
				contentElement.innerHTML = "";
                columnElement.appendChild(contentElement);
                rowElement.appendChild(columnElement);
            }
			
            for(var i = 0; i < numRows; i++)
            {
                this.$refs["inv-parent"].appendChild(rowElement.cloneNode(true));
            }
        },
		methods: {
			setSlot: function(slotIdx, item) {
				console.log("Setting slot " + slotIdx);
				const numRows = 6;
				const numColumns = 8;
				
				var rowNum = Math.floor(slotIdx / numColumns);
				var columnNum = slotIdx % numColumns;
				var el = this.$refs["inv-parent"].children[rowNum].children[columnNum].children[0];
				
				if(el.children.length > 0) {
					// for of loops don't work, so i have to resort to this :(
					for(var i = 0; i < el.children.length; i++) {
						var childNode = el.children[i];
						childNode.remove();
					}
				}
				
				var imgEl = document.createElement("img");
				
				imgEl.src = "icons/" + item.id + ".png";
				
				imgEl.addEventListener("mouseover", (e) => {
					this.$refs["item-hover"].innerHTML = item.name;
					this.$refs["item-hover"].style.display = "block";
					this.$refs["item-hover"].style.top = e.clientY + "px";
					this.$refs["item-hover"].style.left = e.clientX + "px";
				});
				
				imgEl.addEventListener("mousemove", (e) => {
					this.$refs["item-hover"].style.top = e.clientY + "px";
					this.$refs["item-hover"].style.left = e.clientX + "px";
				});
				
				imgEl.addEventListener("mouseout", (e) => {
					this.$refs["item-hover"].style.display = "none";
				});
				
				el.appendChild(imgEl);
			}
		}
    });

    Vue.component("hotbar", {
        template: getTemplate("hotbar"),
        methods: {
            setActiveSlot: function(slotIndex) {
                for(var i = 1; i < 6; i++) {
					this.$refs["slot" + i].classList.remove("activated");
				}
                this.$refs["slot" + slotIndex].classList.add("activated");
            }
        }
    });

    Vue.component("infinite-inventory", {
        template: getTemplate("infinite-inventory"),
        data: function() {
            return {
                items: []
            };
        },
        methods: {
            addItem: function(item)
            {
                this.items.push(item);
                this.$forceUpdate();
            }
        }
    });

    Vue.component('devmode-warning', {
        template: getTemplate("devmode-warning")
    });

    Vue.component('dev-console', {
        template: getTemplate("dev-console"),
        methods: {
            submitCommand: function() {
                var commandText = this.$refs.command.value;
                console.log("Command " + commandText + " submitted");
            }
        }
    });
	
	Vue.component('crosshair', {
		template: getTemplate("crosshair")
    });
    
    Vue.component('options-menu', {
        template: getTemplate("options-menu"),
        data: function () {
            return { uiScale: 1.0, vSync: false, shadows: false };
        },
        methods: {
            changeUIScale: function (e) {
                console.log(e);
                console.log(e.target.value);
            }
        }
    });
	
	Vue.component('game-menu', {
        template: getTemplate("game-menu"),
        data: function() {
            return { "optionsMenuOpen": false };
        },
		methods: {
			easterEggs: function() {
				if(Math.floor(Math.random() * 100) == 50)
					$(this.$refs.savingWorldEE).show().fadeOut(500);
				else
					$(this.$refs.savingWorldEE).hide();
			}
		}
	});

    window.hud = new Vue({
        el: "#ui",
        data: {
            "health": 100,
            "isDeveloper": false,
            "showInventory": false,
            "menuOpen": false,
            "deltaTime": 0.0
        },
        methods: {
            addDevInvItem: function(itemObject) {
                this.$refs.infInv.addItem(itemObject);
            },
            setHotbarActiveSlot: function(slotIndex) {
				console.log("Slot " + slotIndex + " active.");
                this.$refs.hotbar.setActiveSlot(slotIndex);
            },
            openInventory: function() {
                this.showInventory = true;
            },
            closeInventory: function() {
                this.showInventory = false;
            },
			setInventoryContents: function(invContents) {
				console.log("Inventory has " + invContents.length + " items");
				if(invContents.length > 48) {
					console.log("Tried to set inventory contents that were too big!");
				}
				
				for(var i = 0; i < invContents.length; i++) {
					console.log("Slot " + i + " has item with ID " + invContents[i].id);
					this.$refs.inv.setSlot(i, invContents[i]);
				}
			},
			openMenu: function() {
				this.menuOpen = true;
				this.$refs.gameMenu.easterEggs();
			},
			closeMenu: function() {
				this.menuOpen = false;
			},
			setBuildInfoText: function(text) {
				this.$refs.buildInfo.innerHTML = text;
			},
			setUIScale: function(scale) {
				document.documentElement.style.setProperty('--scale-factor', scale);
			}
        }
    });
	
	console.log("Initialised HUD.");
}

if(vueName == undefined)
{
    var vueName = "vue.js";
}
$.getScript("common/" + vueName, loadHUD);
