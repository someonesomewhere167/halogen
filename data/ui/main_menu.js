function hideAll()
{
	$("#multiplayer-connection").hide();
	$("#connecting-message").hide();
	$("#menu").hide();
	$("#disconnected").hide();
	$("#cantConnect").hide();
}

function init()
{
	hideAll();
	$("#menu").show();
}

function switchToMC()
{
	hideAll();
	$("#multiplayer-connection").show();
}

function onConnectPress()
{
	console.log("Connect pressed, calling connect()");
	connect($("#ip").val(), $("#port").val(), $("#username").val());
}

function onStartConnecting()
{
	hideAll();
	$("#connecting-message").show();
}

function disconnected()
{
	hideAll();
	$("#disconnected").show();
}

function cantConnect()
{
	hideAll();
	$("#cantConnect").show();
}