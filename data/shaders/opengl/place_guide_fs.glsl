#version 330 core
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec3 FragNorm;
layout (location = 2) out vec3 FragPosV;

in VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	mat3 TBN;
  vec3 norm;
  vec3 vsPos;
} fs_in;

uniform vec3 objectColor;

float dither2x2(vec2 position, float brightness) {
    int x = int(mod(position.x, 2.0));
    int y = int(mod(position.y, 2.0));
    int index = x + y * 2;
    float limit = 0.0;
    
    if (x < 8) {
    	if (index == 0) limit = 0.25;
    	if (index == 1) limit = 0.75;
    	if (index == 2) limit = 1.00;
    	if (index == 3) limit = 0.50;
    }
    
    return brightness < limit ? 0.0 : 1.0;
}

void main()
{	
	if(dither2x2(gl_FragCoord.xy, 0.25) < 0.25)
		discard;
		
	FragColor = vec4(objectColor * 50, 1.0);
    FragNorm = fs_in.norm;
    FragPosV = fs_in.vsPos;
} 