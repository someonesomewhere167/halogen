#version 330 core
out vec4 FragColor;

in vec2 uv;

uniform float frametimes[128];
uniform float frametimesLength;

void main()
{
	float frametime = frametimes[int(floor(uv.x * frametimesLength))] * 1000;
	
	vec3 color;
	
	if(frametime > 16.7)
	{
		color = vec3(1.0, 0.0, 0.0);
	}
	else
	{
		color = vec3(1.0, 1.0, 1.0);
	}
	
	if((uv.y * 50) > frametime)
	{
		discard;
	}
    FragColor = vec4(color * (frametime / 50.0), 1.0);
} 