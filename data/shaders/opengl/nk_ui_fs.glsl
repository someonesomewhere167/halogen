#version 430 core
layout(binding = 0) uniform sampler2D tex;

in vec2 fragUv;
in vec4 fragCol;

out vec4 outColor;

void main() {
	outColor = fragCol * texture(tex, fragUv);
}
