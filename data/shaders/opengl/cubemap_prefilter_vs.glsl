#version 440 core
layout (location = 0) in vec3 aPos;

out vec3 WorldPos;

layout(std140, binding = 0) uniform MVPMatrices {
    mat4 projection;
    mat4 view;
    mat4 model;
};

void main()
{
    WorldPos = aPos;  
    gl_Position =  projection * view * vec4(WorldPos, 1.0);
}