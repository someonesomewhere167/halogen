#version 330 core
out vec4 FragColor;

uniform sampler2D sceneTexture;
uniform sampler2D ssaoTexture;
in vec2 uv;

void main() 
{
	vec4 color;
	float ssaoAmount = texture(ssaoTexture, uv).r;
	FragColor = texture(sceneTexture, uv) * vec4(ssaoAmount, ssaoAmount, ssaoAmount, 1.0);
	//FragColor = texture(ssaoTexture, uv);
}