#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aUV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 transform;

out vec2 uv;

void main()
{
	vec3 vPos = aPos;
	vPos.xy *= transform.zw;
	vPos.x += transform.x;
	vPos.y += transform.y;
    gl_Position = projection * vec4(vPos, 1.0);
	//normal = mat3(transpose(inverse(model))) * aNorm;
	uv = aUV;
	uv.y = 1.0 - uv.y;
}