#version 330 core
out vec4 FragColor;

uniform sampler2D sceneTexture;
in vec2 uv;


uniform vec2 ScanLineJitter; // (displacement, threshold)
uniform vec2 VerticalJump;   // (amount, time)
uniform float HorizontalShake;
uniform vec2 ColorDrift;     // (amount, time)
uniform float Time;

float nrand(float x, float y)
{
    return fract(sin(dot(vec2(x, y), vec2(12.9898, 78.233))) * 43758.5453);
}

void main()
{
    float u = uv.x;
    float v = uv.y;

    // Scan line jitter
    float jitter = nrand(v, Time) * 2 - 1;
    jitter *= step(ScanLineJitter.y, abs(jitter)) * ScanLineJitter.x;

    // Vertical jump
    float jump = mix(v, fract(v + VerticalJump.y), VerticalJump.x);

    // Horizontal shake
    float shake = (nrand(Time, 2) - 0.5) * HorizontalShake;

    // Color drift
    float drift = sin(jump + ColorDrift.y) * ColorDrift.x;

    vec4 src1 = texture(sceneTexture, fract(vec2(u + jitter + shake, jump)));
    vec4 src2 = texture(sceneTexture, fract(vec2(u + jitter + shake + drift, jump)));

    FragColor = vec4(src1.r, src2.g, src1.b, 1);
}