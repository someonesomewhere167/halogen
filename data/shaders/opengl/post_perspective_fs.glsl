#version 330 core
out vec4 FragColor;

in vec2 uv;

uniform sampler2D sceneTexture;
uniform float Zoom;

vec2 adjustCoords(vec2 coords, float oldHeight, float newHeight)
{
    coords -= vec2(0.5, 0.5);
    coords *= newHeight / oldHeight;
    coords += vec2(0.5, 0.5);
    return coords;
}

void main()
{
	vec2 fragCoord = textureSize(sceneTexture, 0) * uv;
	vec2 iResolution = textureSize(sceneTexture, 0);
    float ImageWidth = iResolution.x;
    float ImageHeight = iResolution.y;
    float i = fragCoord.x;
    float CurrentSinStep = ((i - ImageWidth / 2.0) / (ImageWidth / 3.1415) + 3.1415 / 2.0);
    float CurrentHeight = max(1.0f, ImageHeight + sin(CurrentSinStep) * Zoom - Zoom);
    vec3 col = texture(sceneTexture, adjustCoords(uv, ImageHeight, CurrentHeight)).xyz;
    
    FragColor = vec4(col, 1.0);
}