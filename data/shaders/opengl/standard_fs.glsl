#version 330 core
out vec4 FragColor;

in VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	mat3 TBN;
} fs_in;

uniform float specularStrength;
uniform int specularPower;
uniform vec3 objectColor;
uniform sampler2D albedo;
uniform sampler2D normalMap;
//uniform sampler2D heightMap;
uniform vec2 tiling;
uniform float height_scale;
uniform vec3 viewPos;

//vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
//{ 
//    float height =  texture(heightMap, texCoords).r;    
//    vec2 p = viewDir.xy / viewDir.z * (height * height_scale);
//    return texCoords - p;    
//}

void main()
{
	vec3 texturedColor = pow(objectColor * texture2D(albedo, fs_in.TexCoords * tiling).xyz, vec3(2.2));
    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
	//FragColor = vec4(uv * tiling, 0.0, 1.0);
	
	vec3 normal = normalize(texture(normalMap, fs_in.TexCoords * tiling).xyz * 2.0 - 1.0);
	normal = normalize(fs_in.TBN * normal); 
    
    vec3 lightDir = vec3(0.0, 1.0, 0.0);
    vec3 reflectDir = reflect(-lightDir, normal); 

    float diff = max(dot(normal, lightDir), 0.0);

    vec3 diffuse = diff * vec3(3.14, 3.14, 3.14);
    vec3 ambient = vec3(1) * texturedColor;

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), specularPower);
    vec3 specular = specularStrength * spec * vec3(3.14, 3.14, 3.14);

    vec3 result = (diffuse + specular + ambient) * texturedColor;
	
	//FragColor = vec4(abs(TBN[2] - normal), 1.0);

    FragColor = vec4(result, 1.0);
	//FragColor = vec4(1.0);
} 