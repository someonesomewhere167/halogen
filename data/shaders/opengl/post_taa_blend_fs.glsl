#version 330 core
out vec4 FragColor;

in vec2 uv;

uniform sampler2D sceneTexture;
uniform sampler2D velocityBuffer;
uniform sampler2D previousBuffers[3];

void main()
{
	vec2 motionVector = texture(velocityBuffer, uv).xy;
	float speed = length(motionVector);
	if(abs(speed) > 0.00001)
	{
		FragColor = texture(sceneTexture, uv);
		return;
	}
	else
	{
		// Try and find the location of the pixel last frame
		vec2 texelSize = vec2(1.0) / textureSize(velocityBuffer, 0);
		FragColor = (texture(sceneTexture, uv) + texture(previousBuffers[0], uv)) * 0.5;
	}
}