#version 330 core
out vec4 FragColor;

in VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	vec3 ViewPos;
	vec3 TangentFragPos;
	vec3 TangentViewPos;
    vec4 FragPosLightSpace;
	mat3 TBN;
} fs_in;

uniform sampler2D normal;


struct Light {
    int type;
    vec3 posDir; // position for point+spot, direction for directional
    vec3 color;
    float pointRange;
};

uniform Light activeLights[16];
uniform int activeLightCount;

const float PI = 3.14159265359;
const int LT_DIR = 0;
const int LT_POINT = 1;
const int LT_SPOT = 2;

void main()
{	
	vec3 V = normalize(fs_in.ViewPos - fs_in.FragPos);
	vec2 texCoords = fs_in.TexCoords;
	
	vec3 Lo = vec3(0.0);
	vec4 texr = texture(normal, fs_in.TexCoords);
	vec3 unpackedNorm = (texr.rgb * 2.0) - 1.0;
	
	FragColor = vec4(unpackedNorm, 1.0);
	
	for(int i = 0; i < activeLightCount; i++)
	{
		// assume it's a directional light
		
		Light l = activeLights[i];
		if(l.type == LT_DIR)
		{
			Lo += l.color * max(dot(unpackedNorm, l.posDir), 0.0);
		}
		else if(l.type == LT_POINT)
		{
			vec3 dir = l.posDir - fs_in.FragPos;
			float dist = distance(l.posDir, fs_in.FragPos);
			float attenuation = 1.0 / (dist * dist);
			Lo += l.color * max(dot(unpackedNorm, dir), 0.0) * attenuation;
		}
	}

    FragColor = vec4(Lo, texr.a);
}
