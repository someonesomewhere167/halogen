#version 430 core
layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec4 col;

layout(std140, binding = 0) uniform ProjectionBlock {
	mat4 projection;
};

out vec2 fragUv;
out vec4 fragCol;

void main() 
{
	fragUv = texCoord;
	fragCol = col / vec4(255);
	gl_Position = projection * vec4(pos.xy, 0.0, 1.0);
}