#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec2 aUV;
layout (location = 3) in vec3 aTangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 taaMat;
uniform vec3 viewPos;
uniform mat4 lightspaceTransform;

out VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	vec3 ViewPos;
	vec3 TangentFragPos;
	vec3 TangentViewPos;
    vec4 FragPosLightSpace;
	mat3 TBN;
} vs_out;

void main()
{
    gl_Position = taaMat * projection * view * model * vec4(aPos, 1.0);
	
	vec3 T = normalize(mat3(model) * aTangent);
	vec3 N = normalize(mat3(model) * aNorm);
	
	//T = normalize(T - dot(T, N) * N);
	vec3 B = cross(N, T);
	
	mat3 TBN = mat3(T, B, N);
	
	vs_out.FragPos = (model * vec4(aPos, 1.0)).xyz;
	vs_out.TexCoords = aUV;
	vs_out.ViewPos = viewPos;
	vs_out.TangentViewPos = TBN * viewPos;
	vs_out.TangentFragPos = vec3(model * vec4(aPos, 1.0));
	vs_out.TBN = TBN;
	vs_out.FragPosLightSpace = lightspaceTransform * vec4(vs_out.FragPos, 1.0);
}