#version 440 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aUV;

layout (binding = 0, std140) uniform MVPMatrices {
    mat4 projection;
    mat4 view;
    mat4 model;
};

out vec2 uv;

void main()
{
    gl_Position = projection * vec4(aPos, 1.0);
	//normal = mat3(transpose(inverse(model))) * aNorm;
	uv = aUV;
} 