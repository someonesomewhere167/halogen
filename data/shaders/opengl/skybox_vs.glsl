#version 450 core
layout (location = 0) in vec3 aPos;

layout(std140, binding = 0) uniform MVPMatrices {
    mat4 projection;
    mat4 view;
    mat4 model;
};

layout(std140, binding = 1) uniform MVPMatricesLast {
    mat4 lastProjection;
    mat4 lastView;
    mat4 lastModel;
};

out vec3 texCoords;
out vec4 csPos;
out vec4 csPosL;

void main(){
	vec4 pos = projection  * vec4(mat3(view) * aPos, 1.0);
	/*
	The z component of the output position should be equal to its w component which will 
	result in a z component that is always equal to 1.0, because when the perspective division
	is applied its z component translates to w / w = 1.0. This way, the depth buffer is tricked
	into believing that the skybox has the maximum depth value of 1.0 and it fails the depth 
	test wherever there's a different object in front of it.
	*/
	gl_Position = pos.xyww;
	texCoords = aPos;
	csPos = (projection * view * vec4(aPos, 1.0));
	csPosL = (projection * lastView * vec4(aPos, 1.0));
}