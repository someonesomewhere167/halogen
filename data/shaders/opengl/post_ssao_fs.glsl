#version 330 core
out float FragColor;
uniform sampler2D normalTex;
uniform sampler2D noiseTex;
uniform sampler2D posTex;
uniform sampler2D depthTex;
in vec2 texCoord;

uniform vec3 samples[64];
uniform mat4 projection2;
uniform mat4 view;
uniform mat4 taaMat;
uniform float radius = 1.0;
uniform float rangeCheckRadius = 1.0;
uniform float bias = 0.025;
uniform float intensity = 1.0;
uniform int sampleCount;
uniform float depthCutoff;
uniform float depthFadeRange;
uniform float time;

void main()
{
	vec2 noiseScale = textureSize(posTex, 0) / 4.0;
	vec3 worldPos = texture(posTex, texCoord).xyz;
	vec3 normal = normalize(texture(normalTex, texCoord).xyz);
	vec3 randomVec = texture(noiseTex, (texCoord + vec2(time)) * noiseScale).xyz;
	vec3 tangent   = normalize(randomVec - normal * dot(randomVec, normal));
	vec3 bitangent = cross(normal, tangent);
	mat3 TBN       = mat3(tangent, bitangent, normal);
	float depth = texture(depthTex, texCoord).x;
	if (depth > depthCutoff + depthFadeRange)
	{
		FragColor = 1.0;
		return;
	}
	float occlusion = 0.0;
	for(int i = 0; i < sampleCount; ++i)
	{
	    // get sample position
	    vec3 sample = TBN * samples[i]; // From tangent to view-space
	    sample = worldPos + sample * radius;

		vec4 offset = vec4(sample, 1.0);
		offset = taaMat * projection2 * offset;
		offset.xyz /= offset.w;
		offset.xyz = offset.xyz * 0.5 + 0.5;

		float sampleDepth = texture(posTex, offset.xy).z;

		float rangeCheck = smoothstep(0.0, 1.0, rangeCheckRadius / abs(worldPos.z - sampleDepth));
		occlusion += (sampleDepth >= sample.z + bias ? 1.0 : 0.0) * rangeCheck * intensity;
	}  

	occlusion = max(1.0 - (occlusion / sampleCount), 0.0);

	FragColor = occlusion;
	//FragColor = mix(occlusion, 1.0, clamp(0, 1, depth - depthCutoff / depthFadeRange));
}