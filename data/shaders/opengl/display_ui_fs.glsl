#version 330 core
out vec4 FragColor;

in vec2 uv;

uniform sampler2D uiTexture;

void main()
{
	vec4 uiColour = textureLod(uiTexture, uv, 0.0);
	FragColor = uiColour;
}