#version 330 core
out vec4 FragColor;

uniform sampler2D sceneTexture;
uniform float delta;
in vec2 uv;

vec3 sample(vec2 uv)
{
    return texture(sceneTexture, uv).rgb;
}

vec3 sampleBox (vec2 uv) {
    vec2 texelSize = vec2(1.0) / textureSize(sceneTexture, 0);
	vec4 o = texelSize.xyxy * vec2(-delta, delta).xxyy;
	vec3 s =
		sample(uv + o.xy) + sample(uv + o.zy) +
		sample(uv + o.xw) + sample(uv + o.zw);
	return s * 0.25;
}

void main() 
{
	FragColor = vec4(sampleBox(uv), 1.0);
}