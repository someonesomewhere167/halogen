#version 440 core
layout (location = 0) out vec4 oColor;
layout (location = 1) out vec3 oNormals;
layout (location = 2) out vec3 oPos;
layout (location = 3) out vec2 oVel;

in VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	vec3 TangentFragPos;
	vec3 TangentViewPos;
    vec4 FragPosLightSpace;
	mat3 TBN;
	vec3 viewSpacePos;
	vec4 csPos;
	vec4 csPosL;
} fs_in;

//MATBEGIN
layout(binding = 1, std140) uniform Material {
    layout(offset = 0) float metallic;
    layout(offset = 4) float roughness;
    layout(offset = 8) vec3 objectColor;
    layout(offset = 20) float heightScale;
    layout(offset = 24) vec3 textureUse;
};
//MATEND

layout(binding = 2, std140) uniform ShaderParams {
    layout(offset = 0) vec3 viewPos;
    layout(offset = 12) int useShadows;
};

struct Light {
    int type;
    vec3 posDir; // position for point+spot, direction for directional
    vec3 color;
    float pointRange;
};
layout(binding = 3, std140) uniform LightBuffer {
    layout(offset = 0) int activeLightCount;
    layout(offset = 4) Light activeLights[16];
};

layout(binding = 0) uniform sampler2D albedo;
layout(binding = 1) uniform sampler2D normalMap;
layout(binding = 2) uniform sampler2D heightMap;
layout(binding = 3) uniform sampler2D metalMap;
layout(binding = 4) uniform sampler2D roughMap;
layout(binding = 5) uniform sampler2D brdfLUT;
layout(binding = 6) uniform sampler2DShadow shadowMap;
layout(binding = 7) uniform sampler2D occlusionMap;
layout(binding = 8) uniform samplerCube sky;
layout(binding = 9) uniform samplerCube skyConvoluted;
layout(binding = 10) uniform samplerCube skyPrefiltered;

const float PI = 3.14159265359;
const int LT_DIR = 0;
const int LT_POINT = 1;
const int LT_SPOT = 2;

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{ 
    // number of depth layers
    const float minLayers = 8;
    const float maxLayers = 32;
    float numLayers = 8.0;
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy / viewDir.z * heightScale; 
    vec2 deltaTexCoords = P / numLayers;
  
    // get initial values
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = 1.0 - texture(heightMap, currentTexCoords).r;
      
    while(currentLayerDepth < currentDepthMapValue)
    {
        // shift texture coordinates along direction of P
        currentTexCoords -= deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = 1.0 - texture(heightMap, currentTexCoords).r;  
        // get depth of next layer
        currentLayerDepth += layerDepth;  
    }
    
    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = (1.0 - texture(heightMap, prevTexCoords).r) - currentLayerDepth + layerDepth;
 
    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}
// ----------------------------------------------------------------------------
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
// ----------------------------------------------------------------------------
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
// ----------------------------------------------------------------------------
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
	float f = pow(1.0 - cosTheta, 5.0);
    return f + F0 * (1.0 - f);
}
// ----------------------------------------------------------------------------
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}   
// ----------------------------------------------------------------------------
float getRand(vec4 seed4)
{
    float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
    return fract(sin(dot_product) * 43758.5453);
}
float shadowmapResult(vec4 fragPosLightspace, vec3 lightDir, vec3 normal)
{
    if(useShadows == 0)
        return 1.0;
    //float bias = max(0.007 * (1.0 - clamp(dot(normal, lightDir), 0, 1)), 0.001); 
    float bias = 0.002;
    fragPosLightspace.z -= bias;
    vec3 projCoords = fragPosLightspace.xyz / fragPosLightspace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float currentDepth = projCoords.z;
    float shadow = 0.0;
    if(projCoords.z > 1.0)
        return 1.0;

    vec2 poissonDisk[4] = vec2[](
      vec2( -0.94201624, -0.39906216 ),
      vec2( 0.94558609, -0.76890725 ),
      vec2( -0.094184101, -0.92938870 ),
      vec2( 0.34495938, 0.29387760 )
    );
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);

    for(int i = 0; i < 4; i++)
    {
        shadow += texture(shadowMap, projCoords + vec3(poissonDisk[i] * texelSize, 0.0));
    }
    shadow /= 4.0;
    return shadow;
}

vec3 calcDirLight(vec3 lightDir, float roughness, float metalness, vec3 N, vec3 V, vec3 albedo, vec3 F0)
{
    vec3 radiance = vec3(110);
	vec3 L = lightDir;
	vec3 H = normalize(V + L);

    // Cook-Torrance BRDF
    float NDF = DistributionGGX(N, H, roughness);   
    float G   = GeometrySmith(N, V, L, roughness);    
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);        
    
    vec3 nominator    = NDF * G * F;
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
    vec3 specular = nominator / denominator;
    
     // kS is equal to Fresnel
    vec3 kS = F;
    // for energy conservation, the diffuse and specular light can't
    // be above 1.0 (unless the surface emits light); to preserve this
    // relationship the diffuse component (kD) should equal 1.0 - kS.
    vec3 kD = vec3(1.0) - kS;
    // multiply kD by the inverse metalness such that only non-metals 
    // have diffuse lighting, or a linear blend if partly metal (pure metals
    // have no diffuse light).
    kD *= 1.0 - metalness;	                
        
    // scale light by NdotL
    float NdotL = max(dot(N, L), 0.0);        
    // add to outgoing radiance Lo
    return (kD * albedo / PI + specular) * radiance * NdotL; // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
}

vec3 calcPointLight(vec3 lightPos, float roughness, float metalness, vec3 N, vec3 V, vec3 albedo, vec3 F0, vec3 worldPos, vec3 radiance)
{
	vec3 L = lightPos - worldPos;
    float distance = length(L);
    L = normalize(L);
	vec3 H = normalize(V + L);

    float attenuation = 1.0 / (distance * distance);
    radiance *= attenuation;

    // Cook-Torrance BRDF
    float NDF = DistributionGGX(N, H, roughness);
    float G   = GeometrySmith(N, V, L, roughness);
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);        
    
    vec3 nominator    = NDF * G * F;
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
    vec3 specular = nominator / denominator;
    
     // kS is equal to Fresnel
    vec3 kS = F;
    // for energy conservation, the diffuse and specular light can't
    // be above 1.0 (unless the surface emits light); to preserve this
    // relationship the diffuse component (kD) should equal 1.0 - kS.
    vec3 kD = vec3(1.0) - kS;
    // multiply kD by the inverse metalness such that only non-metals 
    // have diffuse lighting, or a linear blend if partly metal (pure metals
    // have no diffuse light).
    kD *= 1.0 - metalness;	                
        
    // scale light by NdotL
    float NdotL = max(dot(N, L), 0.0);        
    // add to outgoing radiance Lo
    return (kD * albedo / PI + specular) * radiance * NdotL; // note that we already multiplied the BRDF by the Fresnel (kS) so we won't multiply by kS again
}

vec3 decodeNormal (vec2 texVal)
{
    vec3 n;
    n.xy = texVal*2-1;
    n.z = sqrt(1-dot(n.xy, n.xy));
    return n;
}


void main()
{	
	//FragColor = texture(heightMap, fs_in.TexCoords);
	//return;
	vec3 V = normalize(viewPos - fs_in.FragPos);
	vec2 texCoords;
	if(heightScale > 0.0)
	{
		texCoords = mod(ParallaxMapping(fs_in.TexCoords, V * fs_in.TBN), vec2(1.0));
		if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0)
			discard;
	}
	else
	{
		texCoords = fs_in.TexCoords;
	}
    // input lighting data
    vec3 N = normalize(decodeNormal(texture(normalMap, texCoords).xy));
	N = normalize(fs_in.TBN * N); 
    vec3 R = reflect(-V, N); 
	
	vec3 albedo = pow(objectColor * texture(albedo, texCoords).xyz, vec3(2.2));
	
	float metalFinal = textureUse.x > 0 ? texture(metalMap, texCoords).r : metallic;
	float roughFinal = textureUse.y > 0 ? texture(roughMap, texCoords).r : roughness;

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metalFinal);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < activeLightCount; ++i) 
    {
        // calculate per-light radiance
        Light light = activeLights[i];
        if(light.type == LT_DIR)
        {
            Lo += calcDirLight(light.posDir, roughFinal, metalFinal, N, V, albedo, F0) * shadowmapResult(fs_in.FragPosLightSpace, light.posDir, N);
        }

        if(light.type == LT_POINT)
        {
            Lo += calcPointLight(light.posDir, roughFinal, metalFinal, N, V, albedo, F0, fs_in.FragPos, light.color);
        }
    }   
    //Lo += calcDirLight(lightDir, roughFinal, metalFinal, N, V, albedo, F0);
    
    // ambient lighting (we now use IBL as the ambient term)
    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughFinal);
    
    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - metalFinal;	  
    
    vec3 irradiance = textureLod(skyPrefiltered, N, 4).rgb * 20;
    vec3 diffuse      = irradiance * albedo;
    
    // sample both the pre-filter map and the BRDF lut and combine them together as per the Split-Sum approximation to get the IBL specular part.
    const float MAX_REFLECTION_LOD = 4.0;
    vec3 prefilteredColor = textureLod(skyPrefiltered, R,  roughFinal * MAX_REFLECTION_LOD).rgb * 20;
    vec2 brdf  = texture(brdfLUT, vec2(max(dot(N, V), 0.0), roughFinal)).rg;
	//vec2 brdf = IntegrateBRDF(max(dot(N, V), 0.0), roughFinal);
    vec3 specular = prefilteredColor * (F * brdf.x + brdf.y);

    vec3 ambient = (kD * diffuse + specular);
	
	if(textureUse.z > 0.5)
	{
		ambient *= texture(occlusionMap, texCoords).r;
	}
    
    vec3 color = ambient + Lo; 

    oColor = vec4(color , 1.0); // colour
    oNormals = N; // normals
    oPos = fs_in.viewSpacePos; // position
	vec2 pos1 = fs_in.csPos.xy / fs_in.csPos.w;
	vec2 pos2 = fs_in.csPosL.xy / fs_in.csPosL.w;
	oVel = pos1 - pos2;
}
