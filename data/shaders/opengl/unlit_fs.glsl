#version 330 core
out vec4 FragColor;

in VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	vec3 ViewPos;
	vec3 TangentFragPos;
	vec3 TangentViewPos;
    vec4 FragPosLightSpace;
	mat3 TBN;
} fs_in;


uniform sampler2D albedo;

void main()
{	
	FragColor = vec4(pow(texture(albedo, fs_in.TexCoords).xyz, vec3(2.2)), 1.0);
}
