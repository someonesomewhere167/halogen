#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec2 aUV;
layout (location = 3) in vec3 aTangent;

layout(std140, binding = 0) uniform MVPMatrices {
    mat4 projection;
    mat4 view;
    mat4 model;
};

layout(std140, binding = 1) uniform MVPMatricesLast {
    mat4 lastProjection;
    mat4 lastView;
    mat4 lastModel;
};

layout(std140, binding = 2) uniform AdditionalVSInfo {
	mat4 taaMat;
	mat4 lightspaceTransform;
	vec3 viewPos;
};

out VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	vec3 TangentFragPos;
	vec3 TangentViewPos;
    vec4 FragPosLightSpace;
	mat3 TBN;
	vec3 viewSpacePos;
	vec4 csPos;
	vec4 csPosL;
} vs_out;

void main()
{
    gl_Position = taaMat * projection * view * model * vec4(aPos, 1.0);
	
	vs_out.csPos = (projection * view * model * vec4(aPos, 1.0));
	vs_out.csPosL = (projection * lastView * lastModel * vec4(aPos, 1.0));
	
	
	vec3 T = normalize(mat3(model) * aTangent);
	vec3 N = normalize(mat3(model) * aNorm);
	
	//T = normalize(T - dot(T, N) * N);
	vec3 B = cross(N, T);
	
	mat3 TBN = mat3(T, B, N);
	
	vs_out.FragPos = (taaMat * model * vec4(aPos, 1.0)).xyz;
	vs_out.TexCoords = aUV;
	vs_out.TangentViewPos = TBN * viewPos;
	vs_out.TangentFragPos = vec3(taaMat * model * vec4(aPos, 1.0));
	vs_out.TBN = TBN;
	vs_out.FragPosLightSpace = lightspaceTransform * vec4(vs_out.FragPos, 1.0);
	vs_out.viewSpacePos = (taaMat * view * model * vec4(aPos, 1.0)).xyz;
}