#version 330 core
out vec4 FragColor;

uniform sampler2D sceneTexture;
uniform float thresholdValue;
uniform float softThreshold;
in vec2 uv;

void main() 
{
	vec4 color;
	color = texture(sceneTexture, uv);
	float brightness = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));
	float knee = thresholdValue * softThreshold;
	float soft = brightness - thresholdValue + knee;
	soft = clamp(soft, 0, 2 * knee);
	soft = soft * soft / (4 * knee + 0.00001);
	float contribution = max(soft, brightness - thresholdValue);
	contribution /= max(brightness, 0.00001);
	FragColor = vec4(color.xyz * contribution, 1.0);
}