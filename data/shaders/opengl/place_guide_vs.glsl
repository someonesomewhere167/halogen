#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec2 aUV;
layout (location = 3) in vec3 aTangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 taaMat;

out VS_OUT {
	vec3 FragPos;
	vec2 TexCoords;
	mat3 TBN;
	vec3 norm;
	vec3 vsPos;
} vs_out;

void main()
{
    gl_Position = taaMat * projection * view * model * vec4(aPos, 1.0);
	
	vec3 T = normalize(vec3(model * vec4(aTangent,   0.0)));
	vec3 N = normalize(vec3(model * vec4(aNorm,    0.0)));
	
	T = normalize(T - dot(T, N) * N);
	vec3 B = cross(N, T);
	
	vs_out.FragPos = (model * vec4(aPos, 1.0)).xyz;
	vs_out.TexCoords = aUV;
	vs_out.TBN = mat3(T, B, N);
	vs_out.norm = N;
	vs_out.vsPos = (view * model * vec4(aPos, 1.0)).xyz;
}