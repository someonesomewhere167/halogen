#version 330 core
out vec4 FragColor;

uniform sampler2D sceneTexture;
uniform sampler2D blurredTex;
uniform float intensity;
in vec2 uv;

void main() 
{
	FragColor = vec4((intensity * texture(blurredTex, uv).rgb) + texture(sceneTexture, uv).rgb, 1.0);
}