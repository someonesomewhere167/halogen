#version 440 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 squareVertices;
layout(location = 1) in vec3 xyz; // Position of the center of the particule and size of the square

layout(binding = 0, std140) uniform MVPMatrices {
	mat4 projection;
	mat4 view;
	mat4 model;
};

uniform vec3 camRight;
uniform vec3 camUp;

out vec2 uv;

void main() {
	vec3 particleCenter_wordspace = xyz.xyz;
    float particleSize = 0.1;
	
	vec3 vertexPosition_worldspace = 
		particleCenter_wordspace
		+ camRight * squareVertices.x * particleSize
		+ camUp * squareVertices.y * particleSize;

	// Output position of the vertex
	gl_Position = projection * view * vec4(vertexPosition_worldspace, 1.0f);
    uv = squareVertices.xy + vec2(0.5, 0.5);
}