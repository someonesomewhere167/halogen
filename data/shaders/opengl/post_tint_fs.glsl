#version 440 core
out vec4 FragColor;

in vec2 uv;

layout(binding = 0) uniform sampler2D sceneTexture;
layout(binding = 1, std140) uniform PostTintVariables {
  vec4 tintContrast;
};

float A = 0.15;
float B = 0.50;
float C = 0.10;
float D = 0.20;
float E = 0.02;
float F = 0.30;
float W = 11.2;

vec3 Uncharted2Tonemap(vec3 x)
{
  return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

vec4 sharpen(sampler2D tex, vec2 coords, vec2 renderSize) {
  float dx = 1.0 / renderSize.x;
  float dy = 1.0 / renderSize.y;
  vec4 sum = vec4(0.0);
  sum += -1. * texture(tex, coords + vec2( -1.0 * dx , 0.0 * dy));
  sum += -1. * texture(tex, coords + vec2( 0.0 * dx , -1.0 * dy));
  sum += 5. * texture(tex, coords + vec2( 0.0 * dx , 0.0 * dy));
  sum += -1. * texture(tex, coords + vec2( 0.0 * dx , 1.0 * dy));
  sum += -1. * texture(tex, coords + vec2( 1.0 * dx , 0.0 * dy));
  return sum;
}

void main()
{
	//vec3 texColor = sharpen(sceneTexture, uv, textureSize(sceneTexture, 0)).xyz;
	vec3 texColor = texture(sceneTexture, uv).xyz;
	float ExposureBias = 0.1f;
	vec3 curr = Uncharted2Tonemap(ExposureBias*texColor);
	
	vec3 whiteScale = 1.0f/Uncharted2Tonemap(vec3(W));
	
  vec3 color = curr*whiteScale;
  // janky contrast controls ftw
  color.rgb = ((color.rgb - 0.5) * tintContrast.w) + (0.5 * tintContrast.w);
	
	vec3 retColor = pow(color, vec3(1/2.2));
	FragColor = vec4(retColor * tintContrast.xyz, 1.0);
}