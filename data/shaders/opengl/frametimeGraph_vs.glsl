#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNorm;
layout (location = 2) in vec2 aUV;
layout (location = 3) in vec3 aTangent;

uniform mat4 projection;

/*
out vec3 normal;
out vec3 FragPos;
*/
out vec2 uv;

void main()
{
    gl_Position = projection /* * view * model */ * vec4(aPos, 1.0);
    /* FragPos = (model * vec4(aPos, 1.0)).xyz;
	normal = mat3(transpose(inverse(model))) * aNorm; */
	uv = aUV;
}