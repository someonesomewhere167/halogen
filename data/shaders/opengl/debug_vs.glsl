// VERT
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aCol;

uniform mat4 view;
uniform mat4 projection;

out vec3 col;

void main()
{
	vec4 realPos =	(projection * view * vec4(aPos, 1.0));
    gl_Position = vec4(realPos.xy, -1.0, realPos.w);
    col = aCol;
}