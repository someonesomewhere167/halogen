#version 450 core

in vec3 texCoords;
in vec4 csPos;
in vec4 csPosL;
layout (location = 0) out vec4 oColor;
layout (location = 1) out vec3 oNormals;
layout (location = 2) out vec3 oPos;
layout (location = 3) out vec2 oVel;

layout (binding = 0) uniform samplerCube sky;

void main(){
	oColor = texture(sky, texCoords) * 20;
	oNormals = vec3(0);
	oPos = vec3(1000);
	vec3 ndcPos = (csPos / csPos.w).xyz;
	vec3 prevNdcPos = (csPosL / csPosL.w).xyz;
	oVel = vec2(0.0001);
}