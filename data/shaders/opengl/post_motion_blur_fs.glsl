#version 330 core
out vec4 FragColor;
uniform sampler2D velocityTex;
in vec2 texCoord;
uniform sampler2D sceneTex;

vec3 sampleAt(vec2 coord)
{
	return texture(sceneTex, clamp(coord, vec2(0.0), vec2(1.0))).xyz;
}

void main()
{
	vec2 motionVector = texture(velocityTex, texCoord).xy * 0.5;
	vec3 color = texture(sceneTex, texCoord).xyz;
	vec2 texelSize = vec2(1.0) / textureSize(velocityTex, 0);
	float speed = length(motionVector / texelSize);
	int nSamples = clamp(int(speed), 1, 16);
	
	for (int i = 1; i < nSamples; ++i) {
      vec2 offset = motionVector * (float(i) / float(nSamples - 1) - 0.5);
      color += sampleAt(texCoord + offset);
    }
   
    color /= float(nSamples);
	FragColor = vec4(color, 1.0);
	//FragColor = vec4(vec3(float(nSamples) / 8.0), 1.0);
}