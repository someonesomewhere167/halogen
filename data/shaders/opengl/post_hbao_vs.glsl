#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aUV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 TexCoord;
out vec2 Position;

void main()
{
    gl_Position = projection * vec4(aPos, 1.0);
	TexCoord = aUV;
	Position = aUV * vec2(2.0, -2.0) + vec2(-1.0, 1.0);
}