import random, math, png

def circ_rand(radius):
    a = random.uniform(0, 6.283185307179586476925286766559)
    return (math.cos(a) * radius, math.sin(a) * radius)

arr = []

for i in range(4):
    arr.append([0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0])

for y in range(4):
    for x in range(4):
        xy = circ_rand(1.0)
        z = random.uniform(0.0, 1.0)
        w = random.uniform(0.0, 1.0)
        offset = x * 4
        arr[y][offset + 0] = int(xy[0] * 127) + 127
        arr[y][offset + 1] = int(xy[1] * 127) + 127
        arr[y][offset + 2] = int(z * 127) + 127
        arr[y][offset + 3] = int(w * 127) + 127

print(arr)
f = open('hbaonoise.png', 'wb')
w = png.Writer(4, 4, alpha=True)
w.write(f, arr)
f.close()
