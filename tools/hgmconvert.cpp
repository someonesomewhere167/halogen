#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include <fstream>

void processNode(
    std::vector<aiVector3D> &vertices, 
    std::vector<int> &indices, 
    std::vector<aiVector3D> &normals,
    std::vector<aiVector2D> &uvs,
    std::vector<aiVector3D> &tangents,
    aiNode* node,
    const aiScene* scene)
{
    for(int i = 0; i < node->mNumMeshes; i++)
    {
        aiQuaternion rotation;
        aiVector3D translation;
        node->mTransformation.DecomposeNoScaling(rotation, translation);

        aiMatrix3x3 rotationMatrix = rotation.GetMatrix();

        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        for(int j = 0; j < mesh->mNumVertices; j++)
        {
            aiVector3D transformedVertex = node->mTransformation * mesh->mVertices[j];
            vertices.push_back(transformedVertex);
            
            aiVector3D transformedNormal = rotationMatrix * mesh->mNormals[j];
            normals.push_back(transformedNormal);
        }

        for(int j = 0; j < mesh->mNumFaces; j++)
        {
            // We specified the triangulate flag, so in theory
            // we won't have to deal with any faces with more than
            // three indices
            indices.push_back(mesh->mFaces[j].mIndices[0]);
            indices.push_back(mesh->mFaces[j].mIndices[1]);
            indices.push_back(mesh->mFaces[j].mIndices[2]);
        }

        for(int j = 0; j < mesh->mNumVertices; j++)
        {
            aiVector3D uv3d = mesh->mTextureCoords[0][j];
            aiVector2D uv(uv3d.x, uv3d.y);
            uvs.push_back(uv);
        }

        for(int j = 0; j < mesh->mNumVertices; j++)
        {
            tangents.push_back(mesh->mTangents[j]);
        }
    }

    if(node->mNumChildren > 0)
    {
        for(int i = 0; i < node->mNumChildren; i++)
        {
            processNode(vertices, indices, normals, uvs, tangents, node->mChildren[i], scene);
        }
    }
}

int main(int argc, char const *argv[])
{
    if(argc == 1)
    {
        std::cerr << "Usage: hgmconvert <source file>" << std::endl;
        return 1;
    }

    std::string filePath(argv[1]);

    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(filePath,
        aiProcess_Triangulate |
        aiProcess_OptimizeMeshes |
        aiProcess_OptimizeGraph |
        aiProcess_ValidateDataStructure |
        aiProcess_ImproveCacheLocality |
        aiProcess_GenUVCoords |
        aiProcess_FlipUVs |
        aiProcess_CalcTangentSpace |
        aiProcess_JoinIdenticalVertices |
        aiProcess_GenSmoothNormals
    );

    if(!scene)
    {
        std::cerr << "hgmconvert: failed to import file" << std::endl;
        std::cerr << importer.GetErrorString() << std::endl;
        return 1;
    }

    std::string writePath;
    if(argc == 2)
    {
        std::size_t dotPosition = filePath.rfind('.');

        if(dotPosition != std::string::npos)
        {
            writePath = filePath.substr(0, filePath.size() - dotPosition);
        }
        else
        {
            writePath = filePath + ".hgm";
        }
    }
    else
    {
        writePath = std::string(argv[2]);
    }

    for(unsigned int i = 0; i < scene->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[i];
        if(!mesh->HasNormals())
        {
            std::cerr << "hgmconvert: mesh " << mesh->mName.C_Str() << " doesn't have normals" << std::endl;
            return 1; 
        }

        if(!mesh->HasTextureCoords(0))
        {
            std::cerr << "hgmconvert: mesh " << mesh->mName.C_Str() << " has invalid UVs or none at all" << std::endl;
            return 1;
        }

        if(!mesh->HasTangentsAndBitangents())
        {
            std::cerr << "hgmconvert: mesh " << mesh->mName.C_Str() << " doesn't have tangents/bitangents" << std::endl;
        }
    }

    std::vector<aiVector3D> vertices;
    std::vector<int> indices;
    std::vector<aiVector3D> normals;
    std::vector<aiVector2D> uvs;
    std::vector<aiVector3D> tangents;

    processNode(vertices, indices, normals, uvs, tangents, scene->mRootNode, scene);

    // Now the vectors are filled
    // Write the data out to disk
    std::ofstream outFile(writePath, std::ios::binary);

    outFile << "HGM2";
    int vertexCount = vertices.size();
    outFile.write((const char*)&vertexCount, sizeof(int));

    for(int i = 0; i < vertices.size(); i++)
    {
        outFile.write((const char*)&vertices[i], 3 * sizeof(ai_real));
        outFile.write((const char*)&normals[i], 3 * sizeof(ai_real));
        outFile.write((const char*)&uvs[i], 2 * sizeof(ai_real));
        outFile.write((const char*)&tangents[i], 3 * sizeof(ai_real));
    }

    int indexCount = indices.size();
    outFile.write((const char*)&indexCount, sizeof(int));
    for(int index : indices)
    {
        outFile.write((const char*)&index, sizeof(int));
    }

    outFile.close();

    std::cout << "Saved to " << writePath << std::endl;
    return 0;
}
