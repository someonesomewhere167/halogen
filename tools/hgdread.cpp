#include <cstdio>
#include <cstdint>
#include <memory>
#include <string>
#include <iostream>

struct HGDHeader
{
    char magic[3] = {'H','G','D'};
    uint16_t formatVersion = 1;
    uint16_t groupCount;
};

struct GroupHeader
{
    uint16_t groupId;
    uint16_t memberCount;
    uint16_t memberLength;
};

struct GameInfoMember
{
    uint16_t gameNameLength;
    std::string gameName;
    uint16_t windowTitleLength;
    std::string windowTitle;
    uint16_t majorVersion;
    uint16_t minorVersion;
    uint8_t patchVersion;
    uint32_t buildNumber;
};

int main(int argc, char** argv)
{
    FILE* f;
    f = fopen(argv[1], "r");
    
    HGDHeader header;
    fread(&header, sizeof(HGDHeader), 1, f);

    GroupHeader groupHeader;

    GameInfoMember gameInfoMember;

    fread(&groupHeader, sizeof(groupHeader), 1, f);

    // Unfortunately, due to the strings, we can't just blit the GameInfoMember structure to disk
    // We have to manually write each field out :|

    fread(&gameInfoMember.gameNameLength, sizeof(uint16_t), 1, f);
    gameInfoMember.gameName.resize(gameInfoMember.gameNameLength);
    fread((void*)gameInfoMember.gameName.data(), sizeof(char), gameInfoMember.gameNameLength, f);

    fread(&gameInfoMember.windowTitleLength, sizeof(uint16_t), 1, f);
    gameInfoMember.windowTitle.resize(gameInfoMember.windowTitleLength);
    fread((void*)gameInfoMember.windowTitle.data(), sizeof(char), gameInfoMember.windowTitleLength, f);

    fread(&gameInfoMember.majorVersion, 9, 1, f);
    fflush(f);
    fclose(f);

    std::cout << "Header: " << header.magic << std::endl;
    std::cout << "Format version: " << header.formatVersion << std::endl;
    std::cout << "Group count: " << header.groupCount << std::endl;

    std::cout << "Game name: " << gameInfoMember.gameName << std::endl;
}