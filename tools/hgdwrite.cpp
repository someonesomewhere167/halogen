#include <cstdio>
#include <cstdint>
#include <cstring>
#include <memory>
#include <string>

struct HGDHeader
{
    char magic[3] = {'H','G','D'};
    uint16_t formatVersion = 1;
    uint16_t groupCount;
};

struct GroupHeader
{
    uint16_t groupId;
    uint16_t memberCount;
    uint16_t memberLength;
};

struct GameInfoMember
{
    uint16_t gameNameLength;
    std::string gameName;
    uint16_t windowTitleLength;
    std::string windowTitle;
    uint16_t majorVersion;
    uint16_t minorVersion;
    uint8_t patchVersion;
    uint32_t buildNumber;
};

// These two structs have a memory layout identical to that of GLM.
// Touch them and you'll break everything. (unless something is already broken)

struct Vec3
{
    Vec3() :
    x(0.f),
    y(0.f),
    z(0.f)
    {}

    Vec3(float x, float y, float z) :
    x(x),
    y(y),
    z(z)
    {}

    float x, y, z;
};

struct Quat
{
    Quat();
    Quat(float w, float x, float y, float z) : 
    x(x),
    y(y),
    z(z),
    w(w) {}
    float x, y, z, w;
};

struct ObjectMaterial
{
    ObjectMaterial(float specularStrength, int specularPower, Vec3 color) :
    specularStrength(specularStrength),
    specularPower(specularPower),
    color(color) {}
    float specularStrength;
    int specularPower;
    Vec3 color;
};

struct ObjectMember
{
    ObjectMember(ObjectMaterial material, Vec3 pos, Quat rot) :
    material(material),
    pos(pos),
    rot(rot) 
    {
        memset(objectName, 0, 32);
        memset(meshName, 0, 32);
        memset(shaderName, 0, 32);
    }

    char objectName[32];
    char meshName[32];
    char shaderName[32];
    ObjectMaterial material;
    Vec3 pos;
	Vec3 scale;
    Quat rot;
};

void writeMember(FILE* f, const char* objectName, const char* meshName, const char* shaderName, Vec3 pos, Vec3 scale, ObjectMaterial material)
{
    ObjectMember member(material, pos, Quat(1.0f, 0.0f, 0.0f, 0.0f));
    std::strcpy(member.objectName, objectName);
    std::strcpy(member.meshName, meshName);
    std::strcpy(member.shaderName, shaderName);
    member.pos = pos;
    member.scale = scale;
    member.rot.w = 1.0f;
    member.rot.x = 0.0f;
    member.rot.y = 0.0f;
    member.rot.z = 0.0f;
    member.material = material;

    fwrite(member.objectName, sizeof(char), 32, f);
    fwrite(member.meshName, sizeof(char), 32, f);
    fwrite(member.shaderName, sizeof(char), 32, f);

    fwrite(&member.material.specularStrength, sizeof(float), 1, f);
    fwrite(&member.material.specularPower, sizeof(int), 1, f);
    fwrite(&member.material.color.x, sizeof(float), 3, f);
	
	if ( std::fpclassify(member.pos.z) == FP_SUBNORMAL )
	{
		printf("Uh oh. Denormalised float detected!!!\n");
	}

    fwrite(&member.pos.x, sizeof(float), 10, f);
}

int main(int argc, char** argv)
{
    FILE* f;
    f = fopen(argv[1], "wb");
    
    HGDHeader header;
    header.groupCount = 2;
    fwrite(header.magic, sizeof(char), 3, f);
    fwrite(&header.formatVersion, sizeof(uint16_t), 1, f);
    fwrite(&header.groupCount, sizeof(uint16_t), 1, f);

    GroupHeader groupHeader;
    groupHeader.groupId = 0;
    groupHeader.memberCount = 1;

    GameInfoMember gameInfoMember;
    gameInfoMember.gameName = "Clash of Steel";
    gameInfoMember.gameNameLength = gameInfoMember.gameName.size();
    gameInfoMember.windowTitle = "Clash of Steel";
    gameInfoMember.windowTitleLength = gameInfoMember.windowTitle.size();
    gameInfoMember.majorVersion = 1;
    gameInfoMember.minorVersion = 2;
    gameInfoMember.patchVersion = 4;
    gameInfoMember.buildNumber = 4;

    groupHeader.memberLength = (sizeof(char) * gameInfoMember.gameNameLength) + (sizeof(char) * gameInfoMember.windowTitleLength) + (sizeof(uint16_t) * 4) + sizeof(uint8_t) + sizeof(uint32_t);

    fwrite(&groupHeader.groupId, sizeof(uint16_t), 1, f);
    fwrite(&groupHeader.memberCount, sizeof(uint16_t), 1, f);
    fwrite(&groupHeader.memberLength, sizeof(uint16_t), 1, f);


    fwrite(&gameInfoMember.gameNameLength, sizeof(uint16_t), 1, f);
    fwrite(gameInfoMember.gameName.data(), sizeof(char), gameInfoMember.gameNameLength, f);

    fwrite(&gameInfoMember.windowTitleLength, sizeof(uint16_t), 1, f);
    fwrite(gameInfoMember.windowTitle.data(), sizeof(char), gameInfoMember.windowTitleLength, f);

    fwrite(&gameInfoMember.majorVersion, sizeof(uint16_t), 1, f);
    fwrite(&gameInfoMember.minorVersion, sizeof(uint16_t), 1, f);
    fwrite(&gameInfoMember.patchVersion, sizeof(uint8_t), 1, f);
    fwrite(&gameInfoMember.buildNumber, sizeof(uint32_t), 1, f);
	
	int monkeyCount = 0;

    //writeObjectGroup(f);
    GroupHeader header2;
    header2.groupId = 1;
    header2.memberCount = monkeyCount;
    header2.memberLength = (sizeof(char) * (32 * 3)) + (sizeof(float) * (14)) + sizeof(int);

    fwrite(&header2.groupId, sizeof(uint16_t), 1, f);
    fwrite(&header2.memberCount, sizeof(uint16_t), 1, f);
    fwrite(&header2.memberLength, sizeof(uint16_t), 1, f);
    
    //writeMember(f, "obj1\0", "cube\0", "standard\0", Vec3(0.0f, 1.0f, 0.0f), Vec3(20.0f, 0.5f, 20.0f), ObjectMaterial(0.0f, 32, Vec3(0.0f, 0.5f, 1.0f)));
	
	for(int i = 0; i < monkeyCount; i++)
	{
		// Write out a monkey object
		writeMember(f, "obj2\0", "menger\0", "standard\0", Vec3(0.0f, 3.0f, (float)i * 3), Vec3(1.0f, 1.0f, 1.0f), ObjectMaterial(1.0f, 32, Vec3(0.0f, 0.0f, 1.0f)));
	}
    
    fflush(f);
    fclose(f);
}