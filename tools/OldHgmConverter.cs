﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assimp;
using System.IO;

namespace HalogenMeshConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1 && args.Length != 2)
            {
                Console.WriteLine("Usage: HalogenMeshConverter <source file> <(optional)output file>");
                return;
            }

            AssimpContext context = new AssimpContext();

            Scene scene;
            try
            {
                scene = context.ImportFile(args[0],
                    PostProcessSteps.Triangulate |
                    PostProcessSteps.OptimizeMeshes |
                    PostProcessSteps.OptimizeGraph |
                    PostProcessSteps.ValidateDataStructure |
                    PostProcessSteps.ImproveCacheLocality |
                    PostProcessSteps.GenerateUVCoords |
                    PostProcessSteps.FlipUVs |
                    PostProcessSteps.CalculateTangentSpace |
                    PostProcessSteps.JoinIdenticalVertices |
                    PostProcessSteps.GenerateSmoothNormals 
                );
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error: Source file not found.");
                return;
            }

            string writePath;
            if(args.Length == 1)
            {
                writePath = Path.GetFileNameWithoutExtension(args[0]) + ".hgm";
            }
            else
            {
                writePath = args[1];
            }

            // Check meshes
            foreach(Mesh mesh in scene.Meshes)
            {
                if (!mesh.HasNormals)
                {
                    Console.WriteLine("Error: Mesh has no normals.");
                    return;
                }

                if (mesh.UVComponentCount.Length == 0 || mesh.UVComponentCount[0] != 2)
                {
                    Console.WriteLine("Warning: Mesh has invalid UVs or no UVs at all.");
                }

                if(!mesh.HasTangentBasis)
                {
                    Console.WriteLine("Error: Mesh does not have tangents.");
                    return;
                }
            }

            List<Vector3D> vertices = new List<Vector3D>();
            List<int> indices = new List<int>();
            List<Vector3D> normals = new List<Vector3D>();
            List<Vector3D> uvs = new List<Vector3D>();
            List<Vector3D> tangents = new List<Vector3D>();

            ProcessNode(vertices, normals, indices, uvs, tangents, scene.RootNode, scene);

            BinaryWriter writer;

            try
            {
                writer = new BinaryWriter(File.Open(writePath, FileMode.Create, FileAccess.Write, FileShare.None));
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Could not open output file for writing.");
                return;
            }

            writer.Write(new char[] { 'H', 'G', 'M', '2' });

            writer.Write(vertices.Count);
            for(int i = 0; i < vertices.Count; i++)
            {
                writer.Write(vertices[i].X);
                writer.Write(vertices[i].Y);
                writer.Write(vertices[i].Z);

                writer.Write(normals[i].X);
                writer.Write(normals[i].Y);
                writer.Write(normals[i].Z);

                writer.Write(uvs[i].X);
                writer.Write(uvs[i].Y);

                writer.Write(tangents[i].X);
                writer.Write(tangents[i].Y);
                writer.Write(tangents[i].Z);
            }

            writer.Write(indices.Count);
            foreach(int index in indices)
            {
                writer.Write(index);
            }


            writer.Close();
        }

        private static void ProcessNode(List<Vector3D> vertices, List<Vector3D> normals, List<int> indices, List<Vector3D> uvs, List<Vector3D> tangents, Node node, Scene scene)
        {
            foreach (int meshIndex in node.MeshIndices)
            {

                node.Transform.Decompose(out Vector3D scale, out Quaternion rotation, out Vector3D translation);

                Matrix3x3 rotMat = rotation.GetMatrix();
                Mesh mesh = scene.Meshes[meshIndex];
                foreach (Vector3D vertex in mesh.Vertices)
                {
                    Vector3D transformed = node.Transform * vertex;
                    vertices.Add(transformed);
                }

                foreach (Face face in mesh.Faces)
                {
                    indices.Add(face.Indices[0]);
                    indices.Add(face.Indices[1]);
                    indices.Add(face.Indices[2]);
                }

                foreach (Vector3D normal in mesh.Normals)
                {
                    normals.Add(rotMat * normal);
                }

                foreach (Vector3D texCoord in mesh.TextureCoordinateChannels[0])
                {
                    uvs.Add(texCoord);
                }

                foreach (Vector3D tangent in mesh.Tangents)
                {
                    tangents.Add(tangent);
                }
            }

            if(node.HasChildren)
            {
                foreach(Node node2 in node.Children)
                {
                    ProcessNode(vertices, normals, indices, uvs, tangents, node2, scene);
                }
            }
        }
    }
}
