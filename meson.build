project('halogen', 'cpp', 'c', default_options : ['cpp_std=c++17','c_std=c11'])

extern_include_dir = include_directories('extern_include')

add_global_arguments('-DGLM_ENABLE_EXPERIMENTAL', language : 'cpp')
add_global_arguments('-DCPPTOML_NO_RTTI', language : 'cpp')
add_global_arguments('-DGLM_LANG_STL11_FORCED', language : 'cpp')
add_global_arguments('-DGLM_FORCE_SIMD_AVX2', language : 'cpp')
add_global_arguments('-DGLM_FORCE_DEFAULT_ALIGNED_GENTYPES', language : 'cpp')

if get_option('buildtype').startswith('debug')
	add_global_arguments('-DDEBUG=1', language : 'cpp')
	add_global_arguments('-DIMGUI_ENABLED=0', language: 'cpp')
endif

if get_option('buildtype').startswith('release')
	add_global_arguments('-DNDEBUG=1', language : 'cpp')
	#add_global_arguments('-DIMGUI_ENABLED=0', language: 'cpp')
endif

if meson.get_compiler('cpp').get_id() != 'msvc'
	add_global_arguments('-Wall', language : 'cpp')
	add_global_arguments('-ffast-math', language : 'cpp')
	add_global_arguments('-march=haswell', language: 'cpp')
else
	add_global_arguments('-D_ENABLE_EXTENDED_ALIGNED_STORAGE=1', language: 'cpp')
	add_global_arguments('/MP', language: 'cpp')
	add_global_arguments('/fp:fast', language: 'cpp')
	add_global_arguments('/arch:AVX', language: 'cpp')
endif

if host_machine.system() == 'linux'
    add_global_arguments('-DPOSIX', language: 'cpp')
    add_global_arguments('-DLINUX', language: 'cpp')
    add_global_arguments('-DGNUC', language: 'cpp')
endif

glfw = subproject('glfw3')
glfw_dep = glfw.get_variable('glfw_dep')

if(get_option('networking_support'))
endif

if host_machine.system() == 'windows'
	cpp = meson.get_compiler('cpp')
	if build_machine.system() == 'linux'
		openal_win = cpp.find_library('OpenAL32', dirs: [meson.source_root() + '/lib'])
	elif build_machine.system() == 'windows'
		openal_win = cpp.find_library('OpenAL32', dirs: [meson.source_root() + '\\lib'])
	endif
endif

# External Libraries
subdir('src/glad')
subdir('src/physfs')
subdir('src/nanovg')
subdir('src/sqlite')
subdir('src/crunch')

# Engine Internals
subdir('src/util')
subdir('src/components')
subdir('src/gamedata')

# Main Modules
subdir('src/audio')
subdir('src/input')
#subdir('src/ui')
subdir('src/render')
subdir('src/physics')
subdir('src/core')
if(get_option('networking_support'))
	subdir('src/networking')
endif

# Game
subdir('src/game')

# Editor
subdir('src/editor')

thread_dep = dependency('threads')

_halogen_deps = [hg_core_dep, hg_render_dep, hg_audio_dep, hg_input_dep, hg_components_dep, hg_physics_dep, game_dep, editor_dep, physfs_dep, thread_dep, nanovg_dep]

if get_option('halogen_as_library')
    shared_library('hg_game', 'src/loader/libversion.cpp', dependencies: _halogen_deps, include_directories: extern_include_dir)
else
    executable('halogen', 'src/loader/main.cpp', dependencies: _halogen_deps, include_directories: extern_include_dir)
endif
