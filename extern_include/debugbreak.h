#if defined(_MSC_VER)
#define DEBUG_BREAK __debugbreak()
#elif defined(__GNUC__)
#define DEBUG_BREAK assert(false)
#endif