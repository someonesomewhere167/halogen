Halogen: Material System
===

The material system in Halogen is very simple. The file format itself is simply a TOML configuration
file consisting of two tables: `material.parameters` and `material.samplers`. They are
placed under `data/materials`.

material.parameters
---

This table consists of value parameters for the material. Currently supported value types are:

- int
- float
- vec2
- vec3
- vec4

An integer or float can be defined rather simply:

```toml
myInt=1
myFloat=1.0
```

The vectors are simply arrays of float values:

```toml
myVec2=[1.0, 2.0]
myVec3=[1.0, 2.0, 3.0]
myVec4=[1.0, 2.0, 3.0, 4.0]
```

material.samplers
---

This table consists of textures for the material. Defining a texture is simple:

```toml
myTexture="path/to/texture.png"
```

Example of a material file
---

Here is an example material file for the physically-based standard shader. It sets basic
parameters such as `metallic` and `roughness`, in addition to slightly more complicated vector
parameters and textures.

```toml
[material.parameters]
objectColor=[0.76862745098, 0.78039215686, 0.78039215686]
metallic=1.0
roughness=0.1
tiling=[1.0, 1.0]
heightScale=0.0

[material.samplers]
albedo="white.png"
normalMap="ship_normal_combined.png"
```