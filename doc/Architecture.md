Halogen: Architecture
===

Halogen is going to be highly modular, with a dependency tree looking something like this:

```
Loader -> Core -> |--> render
                  |--> audio
                  etc...
```

The game will then reference each of these modules, and should be able to call into them directly without them having to depend on each other.
The core's job should be to handle initialisation and communication between these modules. That being said, the communication between these modules should be minimal in order to avoid everything becoming an inter-dependent mess.