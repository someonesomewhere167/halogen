Halogen - UBO/cbuffer notes
===

A few notes on OpenGL UBOs vs D3D11 cbuffers:

- OpenGL has indices that can be used to specify the index of the uniform buffer with the location(binding=x) statement
- D3D11 uses the register(b0) statement
- Some fiddling with the struct packing may be required - this would be bad as it may have to be different between OGL/D3D.
- The process for changing UBO content in D3D11 is as follows:

Init: The buffer must first be created with ID3D11DeviceContext::CreateBuffer(). It has to have a buffer description passed into it.
1. Map the buffer. (ID3D11DeviceContext::Map())
2. Fill the buffer with data.
3. Unmap the buffer (ID3D11DeviceContext::Unmap())
4. Call PSSetConstantBuffers or VSSetConstantBuffers

- In OpenGL, we have two options on how to proceed. We can map the buffer, which may be good for working with both OGL and D3D, or we can use the standarad glBufferData() way.
- The mapped way is similar to D3D, but uses glMapBufferRange() rather than D3D functions.

Init: The buffer must first be created with glGenBuffers(), glBindBuffer() and glBufferData().
1. Map the buffer. (glMapBufferRange())
2. Fill the buffer with data.
3. Unmap the buffer (glUnmapBuffer())
4. Call glBindBufferBase()

- In the end, I think I'm going to go with the mapped buffer method as it is the lowest common denominator between D3D and OpenGL.

Halogen API
===

Everything will be on the backend object, of course.

UniformBufferHandle_t CreateUniformBuffer(int index, size_t size);
void* MapUniformBuffer(UniformBufferHandle_t handle);
void UnmapUniformBuffer(UniformBufferHandle_t handle);
void DestroyUniformBuffer(UniformBufferHandle_t handle);
