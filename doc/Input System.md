Halogen: Input System
===

Upon the first load, Halogen will load the `baseinput.toml` file in the data directory. This
will then be saved to the config path so the user can modify the input settings.

Currently, the input file consists of two tables: `controls.keyboard` and `controls.mouse`.
These do exactly what their names suggest they do - they contain the mappings of action
names to buttons for the aformentioned input devices.

For keyboard controls, GLFW key identifiers are used. An up-to-date list can be found at
https://www.glfw.org/docs/latest/group__keys.html . Similarly, mouse controls use GLFW mouse
button identifiers. These can be found here: https://www.glfw.org/docs/latest/group__buttons.html .