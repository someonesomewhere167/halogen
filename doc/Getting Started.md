Halogen: Getting Started
===

Congratulations on your purchase of a genuine Halogen™-brand engine! This guide will aid you in setting up your first game using the engine.

## A Note on Structure

Firstly, a quick note on the engine's structure: the engine is made of four fundamental parts. These are the `core`, the `loader`, the `game`, and the other engine modules. When the final executable is run, the loader is executed. The loader will initialise the core and begin running the game.  **You will probably never need to touch the loader.** The main functions you need to be concerned about are defined in `game_events.cpp`:

- `init` - Called when the game is first initialised. Here, you should set up the game world (and initialise networking if you are making a multiplayer game).
- `gameTick` - Called with a single argument, `deltaTime`, which is the time since the last frame in seconds. Here you should update the world. You probably will not need to use this very often - most of your logic should be in `systems`, which operate over entities and are a much better choice for any sort of game logic.
- `shutdown` - Called when the game is shutting down. Note that this is *before* the engine internals are shutdown, so you still have access to all APIs.

## Generating the Solution - Windows Only

This guide assumes that you have the following installed:

- Visual Studio 2017 **with the C++ development pack**
- The Meson Build System

## Your First Game

Now that that's out of the way, let's get into actually setting up a game!

Firstly, you will want to head over to the `game` folder within `src`. This is where all the code for your game will be contained.  Next, open up `game_events.cpp`. This is where we will write the first few bits of code.

Add `#include <imgui.h>` to the top of the file. This will allow you access to the Immediate Mode GUI system integrated into Halogen. It is a very useful tool for debugging, but right now we'll use it to show a simple message on the screen.

Go down to the `gameTick` function and add the following:

```cpp
ImGui::Begin("Info");
ImGui::Text("deltaTime: %f", deltaTime);
ImGui::End();
```

